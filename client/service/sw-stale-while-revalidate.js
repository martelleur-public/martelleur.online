/**
 * Constants used for cache names, versions are
 * generated from a build script.
 */
const STATIC_CACHE_NAME = 'static-v/*VERSION_PLACEHOLDER*/'
const DYNAMIC_CACHE_NAME = 'dynamic-v/*VERSION_PLACEHOLDER*/'

/**
 * Assets to be cached are generated from a built script.
 */
const assetsToCache = [/*ASSET_PLACEHOLDER*/]

/**
 * Use the install event to cache static files.
 */
self.addEventListener('install', (event) => {
  console.log('service worker install event')
  event.waitUntil(
    caches.open(STATIC_CACHE).then(cache => {
      cache.addAll(assetsToCache)
    })
  )
})

/**
 * Use the activate event to clear old caches.
 */
self.addEventListener('activate', (event) => {
  console.log('service worker active event')
  event.waitUntil(
    caches.keys().then(keyList => {
      return Promise.all(keyList.map(key => {
        if (key !== STATIC_CACHE_NAME && key !== DYNAMIC_CACHE_NAME) {
          return caches.delete(key)
        }
      }))
    })
  )
  self.clients.claim()
})

/**
 * Use fetch event to implement stale while revalidate strategy.
 */
self.addEventListener('fetch', (event) => {
  console.log('service worker fetch event')
  event.respondWith(
    caches.match(event.request).then(async response => {
      // Cached response is found, return it
      if (response != null) {
        return response
      }

      // Otherwise fetch from the network
      return await fetch(event.request).then(async fetchResponse => {
        return await caches.open(DYNAMIC_CACHE_NAME).then(cache => {
          cache.put(event.request.url, fetchResponse.clone())
          return fetchResponse
        })
      })
    }).catch(() => {
      // Fallback code if necessary.
      return new Response('An error occurred', {
        status: 500,
        statusText: 'An error occurred while fetching the resource'
      })
    })
  )
})

/**
 * Use to activate new service worker.
 */
self.addEventListener('message', (event) => {
  if (event.data && event.data.action === 'skipWaiting') {
    self.skipWaiting()
  }
})

/**
 * Use push event to listen for notifications from server.
 */
self.addEventListener('push', (event) => {
  const data = event.data
  if (data == null) return
  const notification = {
    title: data.json().notification.title,
    body: data.json().notification.body,
    url: data.json().notification.url
  }
  const options = {
    body: notification.body,
    icon: 'icon-192.png', // Icon to be displayed in the notification
    badge: 'icon-192.png', // Small icon shown on the notification's badge (e.g., for Android)
    image: 'icon-192.png', // Larger image to be displayed in the notification content
    data: { url: notification.url }, // Store the URL here
    actions: [
      {
        action: 'open_url',
        title: 'Open website',
        icon: 'icon-192.png',
      }
    ]
  }

  event.waitUntil(
    self.registration.showNotification(notification.title, options)
  )
})

/**
 * Used to open the url in the notification.
 */
self.addEventListener('notificationclick', (event) => {
  event.notification.close()
  if (event.action === 'open_url') {
    console.log('event open url:', event.notification)
    clients.openWindow(event.notification.data.url)
  }
})
