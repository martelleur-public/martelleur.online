import os

def main():
    assets_str = get_all_dist_assets_as_string()
    replace_placeholder_with_string_assets(assets_str)

def get_all_dist_assets_as_string():
    dist_path = 'dist'
    all_files = [os.path.join(dp, f) for dp, dn, filenames in os.walk(dist_path) for f in filenames]
    assets = [f"'{f.replace('dist/', '')}'," for f in all_files]
    return ' '.join(assets)

def replace_placeholder_with_string_assets(assets_str):
    template_path = 'service/sw-cache-first.js'
    output_path = 'dist/sw-cache-first.js'

    with open(template_path, 'r') as template:
        content = template.read().replace('/*ASSET_PLACEHOLDER*/', assets_str)

    with open(output_path, 'w') as out:
        out.write(content)

    print(f'Service worker generated at {output_path}')

if __name__ == '__main__':
    main()
