/**
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

import type { Config } from 'jest'

const config: Config = {
  preset: 'ts-jest',
  clearMocks: true,
  restoreMocks: true,
  testEnvironment: 'jsdom',
  setupFilesAfterEnv: ['<rootDir>/test/jest.setup.ts'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|webp)$': '<rootDir>/test/mock/file-mock.js',
    "^@martelleur.online/webcraft$": "<rootDir>/../lib/webcraft/dist/cjs/src/index.js",
    "^@app$": "<rootDir>/src/app/index",
    "^@components$": "<rootDir>/src/components/index",
    "^@config$": "<rootDir>/src/config/index",
    "^@constants$": "<rootDir>/src/constants/index",
    "^@event$": "<rootDir>/src/event/index",
    "^@features$": "<rootDir>/src/features/index",
    "^@icons$": "<rootDir>/src/icons/index",
    "^@infrastructure$": "<rootDir>/src/infrastructure/index",
    "^@layouts$": "<rootDir>/src/layouts/index",
    "^@lib$": "<rootDir>/src/lib/index",
    "^@pages$": "<rootDir>/src/pages/index",
    "^@router$": "<rootDir>/src/router/index",
    "^@repository$": "<rootDir>/src/repository/index",
    "^@service$": "<rootDir>/src/service/index",
    "^@state$": "<rootDir>/src/state/index",
    "^@store$": "<rootDir>/src/store/index",
    "^@style$": "<rootDir>/src/style/index",
    "^@sw$": "<rootDir>/src/sw/index",
    "^@utility$": "<rootDir>/src/utility/index",
    "^@app/(.*)$": "<rootDir>/src/app/$1",
    "^@components/(.*)$": "<rootDir>/src/components/$1",
    "^@config/(.*)$": "<rootDir>/src/config/$1",
    "^@constants/(.*)$": "<rootDir>/src/constants/$1",
    "^@event/(.*)$": "<rootDir>/src/event/$1",
    "^@features/(.*)$": "<rootDir>/src/features/$1",
    "^@icons/(.*)$": "<rootDir>/src/icons/$1",
    "^@infrastructure/(.*)$": "<rootDir>/src/infrastructure/$1",
    "^@layouts/(.*)$": "<rootDir>/src/layouts/$1",
    "^@lib/(.*)$": "<rootDir>/src/lib/$1",
    "^@pages/(.*)$": "<rootDir>/src/pages/$1",
    "^@repository/(.*)$": "<rootDir>/src/repository/$1",
    "^@router/(.*)$": "<rootDir>/src/router/$1",
    "^@service/(.*)$": "<rootDir>/src/service/$1",
    "^@state/(.*)$": "<rootDir>/src/state/$1",
    "^@store/(.*)$": "<rootDir>/src/store/$1",
    "^@style/(.*)$": "<rootDir>/src/style/$1",
    "^@sw/(.*)$": "<rootDir>/src/sw/$1",
    "^@utility/(.*)$": "<rootDir>/src/utility/$1",
  }
}

export default config
