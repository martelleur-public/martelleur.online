import type { AppState } from '@store'

export function isStateColorThemeNameUpdated (
  prevState: AppState,
  nextState: AppState
): boolean {
  return prevState.ui.colorThemeName !== nextState.ui.colorThemeName
}

export function isStateFontThemeNameUpdated (
  prevState: AppState,
  nextState: AppState
): boolean {
  return prevState.ui.fontThemeName !== nextState.ui.fontThemeName
}

export function isStateNotificationDecisionUpdated (
  prevState: AppState,
  nextState: AppState
): boolean {
  return prevState.ui.notifactionDecision !== nextState.ui.notifactionDecision
}

export function isSidebarOpenChanged (
  prevState: AppState,
  nextState: AppState
): boolean {
  return prevState.ui.sidebarIsOpen !== nextState.ui.sidebarIsOpen
}
