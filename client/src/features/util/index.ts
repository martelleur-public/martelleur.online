export { ShouldRender } from './should-render'

export {
  isStateColorThemeNameUpdated,
  isStateFontThemeNameUpdated,
  isStateNotificationDecisionUpdated,
  isSidebarOpenChanged
} from './state-comparison-functions'
