import type { AppState } from '@store'
import { customLogger } from '@utility'

type RenderDecorator<T extends { state: AppState }> = (target: T, propertyKey: string, descriptor: PropertyDescriptor) => PropertyDescriptor
type ComparisonFunction = (prev: AppState, next: AppState) => boolean

/**
 * Used as decorator for subscription method
 * to store.
 */
export function ShouldRender<T extends { state: AppState }> (
  ...comparisonFunctions: ComparisonFunction[]
): RenderDecorator<T> {
  const decorator = function (target: T, propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    const originalMethod = descriptor.value
    customLogger('target: ', target)
    customLogger('propertyKey: ', propertyKey)
    customLogger('descriptor: ', descriptor)
    customLogger('comparisonFunctions: ', comparisonFunctions)

    descriptor.value = function (this: T, nextState: AppState) {
      const shouldRender = comparisonFunctions.some(comparison => comparison(this.state, nextState))
      this.state = nextState
      if (shouldRender) {
        originalMethod.call(this, nextState)
      }
    }
    return descriptor
  }
  return decorator
}
