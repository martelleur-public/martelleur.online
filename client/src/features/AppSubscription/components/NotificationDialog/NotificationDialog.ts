import {
  assertElementExist,
  DefineCustomElement,
  ResponsiveDialogCloseEvent,
  type ResponsiveDialog
} from '@martelleur.online/webcraft'

import { notificationDialogTemplate } from './NotificationDialog.template'
import { StoreProvider } from '@store'
import { customLogger } from '@utility'
import { createActionNotificationDecision } from '@state'
import { NotificationDecision } from '@constants'
import { type AppealingButton } from '@components'

export const nameNotificationDialog = 'notification-dialog'

@DefineCustomElement(nameNotificationDialog)
export class NotificationDialog extends StoreProvider {
  private readonly _root
  private readonly _dialog: ResponsiveDialog
  private readonly _acceptButton: AppealingButton
  private readonly _denyButton: AppealingButton

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    this._root.appendChild(notificationDialogTemplate.content.cloneNode(true))
    this._dialog = assertElementExist(this._root.querySelector<ResponsiveDialog>('responsive-dialog'))
    this._acceptButton = assertElementExist(this._root.querySelector<AppealingButton>('#accept-notifications'))
    this._denyButton = assertElementExist(this._root.querySelector<AppealingButton>('#deny-notifications'))
    this.bindEventHandlers()
  }

  private bindEventHandlers (): void {
    this._handleDenyNotification = this._handleDenyNotification.bind(this)
    this._handleCloseNotification = this._handleCloseNotification.bind(this)
    this._handleAcceptNotification = this._handleAcceptNotification.bind(this)
  }

  connectedCallback (): void {
    this._denyButton.addEventListener('click', this._handleDenyNotification)
    this._acceptButton.addEventListener('click', this._handleAcceptNotification)
    document.addEventListener(ResponsiveDialogCloseEvent.eventName, this._handleDenyNotification)
  }

  private _handleDenyNotification (): void {
    this.store.dispatch(createActionNotificationDecision(NotificationDecision.DENIED))
    this.remove()
  }

  private _handleCloseNotification (): void {
    this.store.dispatch(createActionNotificationDecision(NotificationDecision.DENIED))
    this.remove()
  }

  private _handleAcceptNotification (): void {
    this.store.dispatch(createActionNotificationDecision(NotificationDecision.GRANTED))
    this.remove()
  }

  open (): void {
    this._dialog.open()
  }

  close (): void {
    this._dialog.close()
    this.remove()
  }

  disconnectedCallback (): void {
    customLogger(`${this.constructor.name} dialog disconnected`)
    document.removeEventListener(ResponsiveDialogCloseEvent.eventName, this._handleAcceptNotification)
  }
}
