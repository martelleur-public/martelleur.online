import '@martelleur.online/webcraft'

import '@components/AppealingButton/AppealingButton'

export const notificationDialogTemplate = document.createElement('template')

notificationDialogTemplate.innerHTML = `
<responsive-dialog>
    <h3 slot="title">Stay Connected!</h3>
    <div slot="body" class="dialog-main">
        <p>Would you like to receive notifications from martelleur.online?</p>
    </div>
    <div slot="footer" class="dialog-footer">
        <appealing-button
            id="accept-notifications"
            data-text="Yes, Keep Me Updated!"
            data-test="accept-notification">
        </appealing-button>
        <appealing-button
            id="deny-notifications"
            data-text="No, Thanks"
            data-test="deny-notification">
        </appealing-button>
    </div>
</responsive-dialog>

<style>
.dialog-main {
    display: flex;
    justify-content: center;
    align-items: center;
}

.dialog-footer {
    display: flex;
    justify-content: center;
    gap: 10px;
    flex-wrap: wrap;
    align-items: center;
}
</style>
`
