import type { NotificationSubscriptionState } from './NotificationSubscription.state'
import '@components/AppealingButton/AppealingButton'
import { NotificationDecision } from '@constants'

export function createTemplate (state: NotificationSubscriptionState): HTMLTemplateElement {
  const template = document.createElement('template')
  template.innerHTML = `
  <appealing-button
    id="subscribe-button"
    data-text="Stay Updated!">
  </appealing-button>
  <style>
    appealing-button {
      display: ${getDisplayValue(state)};
    }
  </style>
  `
  return template
}

function getDisplayValue (state: NotificationSubscriptionState): 'initial' | 'none' {
  let display: 'initial' | 'none' = 'initial'
  if (state.notificationDecision === NotificationDecision.GRANTED ||
    state.notificationDecision === NotificationDecision.DENIED) {
    display = 'none'
  }
  return display
}
