import {
  DefineCustomElement,
  updateShadowRootContent
} from '@martelleur.online/webcraft'

import { createTemplate } from './NotificationSubscription.template'
import { customLogger } from '@utility'
import type { NotificationSubscriptionState } from './NotificationSubscription.state'
import { NotificationDialog } from '../NotificationDialog/NotificationDialog'
import { NotificationDecision } from '@constants'
import type { AppealingButton } from '@components'
import {
  ReactiveStoreSubscriber,
  type AppState,
  type IReactiveState
} from '@store'

export const nameNotificationSubscription = 'notification-subscription'

@DefineCustomElement(nameNotificationSubscription)
export class NotificationSubscription extends ReactiveStoreSubscriber {
  private readonly reactiveState: IReactiveState<NotificationSubscriptionState>
  private readonly _root: ShadowRoot

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    const initState: NotificationSubscriptionState = {
      notificationDecision: this.store.getState().ui.notifactionDecision
    }
    this.reactiveState = this.configureReactiveState(initState)
    this._askUserToSubscribe = this._askUserToSubscribe.bind(this)
  }

  subscribeToStore (state: AppState): void {
    try {
      this.reactiveState.state = {
        notificationDecision: state.ui.notifactionDecision
      }
    } catch (error) {
      customLogger('Error when updating element state')
    }
  }

  connectedCallback (): void {
    this.render()
  }

  render (): void {
    this._toggleEventHandlers(false)
    updateShadowRootContent(this._root, createTemplate(this.reactiveState.state))
    this._toggleEventHandlers(true)
  }

  private _toggleEventHandlers (on: boolean): void {
    const isDefaultDescision = this.reactiveState.state.notificationDecision === NotificationDecision.DEFAULT
    if (!(isDefaultDescision)) {
      return
    }
    const subscribeButton = this._root.querySelector<AppealingButton>('#subscribe-button')
    if (on) {
      subscribeButton?.addEventListener('click', this._askUserToSubscribe)
    } else {
      subscribeButton?.removeEventListener('click', this._askUserToSubscribe)
    }
  }

  private _askUserToSubscribe (): void {
    const notificationDialog = new NotificationDialog()
    document.body.appendChild(notificationDialog)
    notificationDialog.open()
  }
}
