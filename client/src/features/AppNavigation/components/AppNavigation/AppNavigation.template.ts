import * as WebCraft from '@martelleur.online/webcraft'

import '@features/AppSetting'
import '@icons/AppIcon/AppIcon'
import { customLogger } from '@utility'
import { getBaseURL } from '@config'

export const template = document.createElement('template')
customLogger(`Use Components from library:
- ${WebCraft.AppLink.elementName}
- ${WebCraft.RemoteLink.elementName}
- ${WebCraft.ResponsiveHeader.elementName}
`)

const baseURL = getBaseURL()

template.innerHTML = `
<responsive-header>
    <app-icon data-size="medium" slot="icon"></app-icon>
    <h2 slot="title">martelleur.online</h2>
    <div slot="links">
        <app-link
            title="Web development"
            href="${baseURL}/index"
            data-text="Web">
        </app-link>
        <app-link
            title="Git repositories"
            href="${baseURL}/git"
            data-text="Git">
        </app-link>
        <app-link
            title="Games built with webcomponents"
            href="${baseURL}/game"
            data-text="Games">
        </app-link>
        <remote-link
            title="Library @martelleur/WebCraft"
            href="https://gitlab.com/martelleur-public/martelleur.online/-/blob/main/lib/webcraft/doc/todo/project-webcraft-library.md#project-webcraft-library"
            data-text="WebCraft">
        </remote-link>
    </div>
    <notification-subscription slot="item"></notification-subscription>
    <app-settings slot="menu"></app-settings>
</responsive-header>
<style>
*, *::before, *::after {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

:host {
    width: 100%;
    max-height: 100%;
}

h2[slot=title] {
    cursor: pointer;
    font-family: var(--FONT_SECONDARY);
}

app-icon, h2[slot=title] {
    cursor: pointer;
}
</style>
`
