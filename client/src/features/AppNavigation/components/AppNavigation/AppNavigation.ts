import { DefineCustomElement, assertElementExist } from '@martelleur.online/webcraft'

import { template } from './AppNavigation.template'
import { InfoDialog } from '@features'

@DefineCustomElement('app-navigation')
export class AppNavigation extends HTMLElement {
  private readonly _root: ShadowRoot
  private readonly _titleIcon: HTMLElement
  private readonly _title: HTMLElement

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    this._root.appendChild(template.content.cloneNode(true))
    this._titleIcon = assertElementExist(this._root.querySelector<HTMLElement>('app-icon'))
    this._title = assertElementExist(this._root.querySelector<HTMLElement>('h2[slot=title]'))
    this._bindEventHandlers()
  }

  private _bindEventHandlers (): void {
    this._handleSelectTitle = this._handleSelectTitle.bind(this)
  }

  connectedCallback (): void {
    [this._title, this._titleIcon].forEach(element => {
      element.addEventListener('click', this._handleSelectTitle)
    })
  }

  private _handleSelectTitle (): void {
    const infoDialog = new InfoDialog()
    document.body.appendChild(infoDialog)
    infoDialog.open()
  }
}
