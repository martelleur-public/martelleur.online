import {
  DefineCustomElement,
  assertElementExist
} from '@martelleur.online/webcraft'

import { appThreeDimensionTemplate } from './AppThreeDimension.template'
import type { AppThreeDimensionState } from './AppThreeDimension.state'
import { customLogger } from '@utility'
import type { RotatingCube } from '@components'
import {
  ReactiveStoreSubscriber,
  type AppState,
  type IReactiveState
} from '@store'

/**
 * Intiate 3D componets from this components because of rendering timing issues.
 *
 * @TODO Should be further investigated to see if this is actually true when
 * nesting components that use three.js library. Could very well be
 * inheritance size problem.
 */

export const nameAppThreeDimension = 'app-three-dimension'

@DefineCustomElement(nameAppThreeDimension)
export class AppThreeDimension extends ReactiveStoreSubscriber {
  private readonly _shadowRoot: ShadowRoot
  private readonly reactiveState: IReactiveState<AppThreeDimensionState>
  private readonly _rotatingCube: RotatingCube

  constructor () {
    super()
    this._shadowRoot = this.attachShadow({ mode: 'open' })
    const initState: AppThreeDimensionState = {
      colorThemeName: this.store.getState().ui.colorThemeName
    }
    this.reactiveState = this.configureReactiveState(initState)
    this._shadowRoot.appendChild(appThreeDimensionTemplate.content.cloneNode(true))
    this._rotatingCube = assertElementExist(this._shadowRoot.querySelector<RotatingCube>('rotating-cube'))
    this._bindEventHandlersToThis()
  }

  private _bindEventHandlersToThis (): void {
    this._handleWindowResize = this._handleWindowResize.bind(this)
  }

  subscribeToStore (state: AppState): void {
    customLogger(`${this.constructor.name} listener called`)
    try {
      this.reactiveState.state = {
        colorThemeName: state.ui.colorThemeName
      }
    } catch (error) {
      customLogger('Error when updating element state')
    }
  }

  render (): void {
    customLogger(`${this.constructor.name} render called`)
    this._rotatingCube.updateColors(this.reactiveState.state.colorThemeName)
  }

  connectedCallback (): void {
    setTimeout(() => {
      customLogger('Init Rotating cube and start animation')
      window.addEventListener('resize', this._handleWindowResize)
      this._rotatingCube.init(
        this.clientWidth, this.clientHeight, this.reactiveState.state.colorThemeName
      )
    }, 0)
  }

  private _handleWindowResize = (): void => {
    this._rotatingCube.updateSize(this.clientWidth, this.clientHeight)
  }
}
