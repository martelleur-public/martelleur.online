import { type ColorThemeNameType } from '@style/theme-colors'

export interface AppThreeDimensionState {
  colorThemeName: ColorThemeNameType
}
