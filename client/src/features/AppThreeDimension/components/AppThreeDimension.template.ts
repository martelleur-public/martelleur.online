import '@components/RotatingCube/RotatingCube'
import { appDefaultContainerStyle } from '@style/app-container-style'

export const appThreeDimensionTemplate = document.createElement('template')

appThreeDimensionTemplate.innerHTML = `
<rotating-cube></rotating-cube>
${appDefaultContainerStyle}
<style>
:host,
rotating-cube {
    width: 30vw;
    height: 30vw;
}
</style>
`
