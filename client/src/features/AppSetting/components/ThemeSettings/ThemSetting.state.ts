import { type ColorThemeNameType } from '@style/theme-colors'

export interface ThemeSettingState {
  colorThemeName: ColorThemeNameType
}
