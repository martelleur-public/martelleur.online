import '@components/ColorPalette/ColorPalette'
import { ColorThemeName } from '@style/theme-colors'
import { appDefaultContainerStyle } from '@style/app-container-style'
import type { ThemeSettingState } from './ThemSetting.state'

export function createTemplate (state: ThemeSettingState): HTMLTemplateElement {
  const template = document.createElement('template')
  template.innerHTML = `
  <div class="theme-radio" id="${ColorThemeName.DARK}">
    Dark theme:
    <color-palette data-color-theme="${ColorThemeName.DARK}"></color-palette>
  </div>
  <div class="theme-radio" id="${ColorThemeName.WARM}">
    Warm theme:
    <color-palette data-color-theme="${ColorThemeName.WARM}"></color-palette>
  </div>
  <div class="theme-radio" id="${ColorThemeName.COLD}">
    Cold theme:
    <color-palette data-color-theme="${ColorThemeName.COLD}"></color-palette>
  </div>
  <div class="theme-radio" id="${ColorThemeName.RETRO}">
    Retro theme:
    <color-palette data-color-theme="${ColorThemeName.RETRO}"></color-palette>
  </div>
  <div class="theme-radio" id="${ColorThemeName.LIGHT}">
    Light theme:
    <color-palette data-color-theme="${ColorThemeName.LIGHT}"></color-palette>
  </div>
  ${appDefaultContainerStyle}
  <style>
  :host {
    display: flex;
    flex-direction: column;
    width: 100%;
  }
  
  .theme-radio {
    display: flex;
    align-items: center;
    min-height: 54px;
    justify-content: space-between;
    border: 2px solid var(--BORDER_DARK);
    padding: 10px;
    margin: 2px 0px;
    border-radius: 5px;
    background-color: var(--BACKGROUND_MEDIUM);
    cursor: pointer;
  }
  
  #${state.colorThemeName},
  .theme-radio:hover {
    background-color: var(--BACKGROUND_LIGHT_X);
    border-color: var(--BORDER_LIGHT_X);
    color: var(--TEXT_SECONDARY);
  }
  </style>
  `
  return template
}
