import { DefineCustomElement, updateShadowRootContent } from '@martelleur.online/webcraft'

import { createTemplate } from './ThemeSettings.template'
import { customLogger } from '@utility'
import type { ThemeSettingState } from './ThemSetting.state'
import { createActionUpdateColorTheme } from '@state'
import type { ColorThemeNameType } from '@style/theme-colors'
import {
  ReactiveStoreSubscriber,
  type AppState,
  type IReactiveState
} from '@store'

export const nameThemeSettings = 'theme-settings'

@DefineCustomElement(nameThemeSettings)
export class ThemeSettings extends ReactiveStoreSubscriber {
  private readonly _root: ShadowRoot
  private readonly reactiveState: IReactiveState<ThemeSettingState>

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    const initState: ThemeSettingState = {
      colorThemeName: this.store.getState().ui.colorThemeName
    }
    this.reactiveState = this.configureReactiveState(initState)
    this._handleApplyTheme = this._handleApplyTheme.bind(this)
  }

  subscribeToStore (state: AppState): void {
    try {
      this.reactiveState.state = {
        colorThemeName: state.ui.colorThemeName
      }
    } catch (error) {
      customLogger('Error when updating element state')
    }
  }

  connectedCallback (): void {
    this.render()
  }

  render (): void {
    this._toggleEventHandlers(false)
    updateShadowRootContent(this._root, createTemplate(this.reactiveState.state))
    this._toggleEventHandlers(true)
  }

  private _toggleEventHandlers (on: boolean): void {
    const themes = this._root.querySelectorAll('.theme-radio')
    themes.forEach(node => {
      if (!(node instanceof HTMLDivElement)) return
      if (on) node.addEventListener('click', this._handleApplyTheme)
      else node.removeEventListener('click', this._handleApplyTheme)
    })
  }

  private _handleApplyTheme (event: MouseEvent): void {
    if (!(event.target instanceof HTMLDivElement)) {
      return
    }
    const colorThemeName = event.target.id as ColorThemeNameType
    this.store.dispatch(createActionUpdateColorTheme(colorThemeName))
  }
}
