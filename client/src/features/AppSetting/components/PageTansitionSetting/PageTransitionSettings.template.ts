import { PageTransition } from '@martelleur.online/webcraft'

import { appDefaultContainerStyle } from '@style/app-container-style'
import type { PageTransitionSettingState } from './PageTransitionSettings.state'

export function createTemplate (state: PageTransitionSettingState): HTMLTemplateElement {
  const template = document.createElement('template')

  template.innerHTML = `
<div class="transition-radio" id="${PageTransition.DARK}">
  <p>Dark</p>
  <div class="transition-show-case">
    <div class="dark-transition"></div>
  </div>
</div>
<div class="transition-radio" id="${PageTransition.SLIDE}">
  <p>Slide</p>
  <div class="transition-show-case">
    <div class="slide-transition"></div>
  </div>
</div>
<div class="transition-radio" id="${PageTransition.SHRINK}">
  <p>Shrink</p>
  <div class="transition-show-case">
    <div class="shrink-transition"></div>
  </div>
</div>
<div class="transition-radio" id="${PageTransition.ROTATION_SHRINK}">
  <p>Shrink and rotate</p>
  <div class="transition-show-case">
  <div class="shrink-rotation-transition"></div>
  </div>
</div>
<div class="transition-radio" id="${PageTransition.NONE}">
  <p>No effect</p>
  <div class="transition-show-case">
    <div class="no-transition"></div>
  </div>
</div>
${appDefaultContainerStyle}
<style>
:host {
  display: flex;
  flex-direction: column;
  width: 100%;
}

.transition-radio {
  display: flex;
  align-items: center;
  height: 54px;
  justify-content: space-between;
  border: 2px solid var(--BORDER_DARK);
  padding: 10px;
  margin: 2px 0px;
  border-radius: 5px;
  background-color: var(--BACKGROUND_MEDIUM);
  cursor: pointer;
}

#${state.pageTrasnition},
.transition-radio:hover {
  background-color: var(--BACKGROUND_LIGHT_X);
  border-color: var(--BORDER_LIGHT_X);
  color: var(--TEXT_SECONDARY);
}

.transition-show-case {
  width: calc(100% / 4);
  height: 100%;
  overflow: hidden;
}

.dark-transition,
.slide-transition,
.shrink-transition,
.shrink-rotation-transition,
.no-transition {
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: var(--BACKGROUND_DARK);
  color: var(--TEXT_PRIMARY);
}

.slide-transition{
  position: relative;
  top: 0;
  left: 0;
}

.dark-transition::after {
  content: "PAGE 1";
  animation: darkning-effect 4s infinite alternate;
}

.slide-transition::after {
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  content: "PAGE 1";
  animation: slide-effect 4s infinite;
}

.slide-transition.page-2::after {
  content: "PAGE 2";
}

.shrink-transition::after {
  content: "PAGE 1";
  animation: shrink-effect 4s infinite alternate;
}

.shrink-rotation-transition::after {
  content: "PAGE 1";
  animation: shrink-rotation-effect 4s infinite alternate;
}

.no-transition::after {
  content: "PAGE 1";
  animation: no-effect 4s infinite alternate;
}


@keyframes darkning-effect {
  0% {
    content: "PAGE 1";
    color: var(--TEXT_PRIMARY);
  }
  43% ,57% {
    color: var(--BACKGROUND_DARK);
  }
  100% {
    content: "PAGE 2";
    color: var(--TEXT_PRIMARY);
  }
}

@keyframes slide-effect {
  0% {
    transform: translateX(100%);
  }
  40%, 45% {
    transform: translateX(0%);
  }
  100% {
    transform: translateX(-100%);
  }
}

@keyframes shrink-effect {
  0%, 10% {
    content: "PAGE 1";
    transform: scale(1);
  }
  50% {
    transform: scale(0);
  }
  90%, 100% {
    content: "PAGE 2";
    transform: scale(1);
  }
}

@keyframes shrink-rotation-effect {
  0%, 10% {
    content: "PAGE 1";
    transform: scale(1) rotate(360deg);
  }
  50% {
    transform: scale(0);
  }
  90%, 100% {
    content: "PAGE 2";
    transform: scale(1) rotate(360deg);
  }
}

@keyframes no-effect {
  0% {
    content: "PAGE 1";
  }
  100%  {
    content: "PAGE 2";
  }
}

</style>
`
  return template
}
