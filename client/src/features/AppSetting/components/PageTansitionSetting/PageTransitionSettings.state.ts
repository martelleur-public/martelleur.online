import type { PageTransitionType } from '@martelleur.online/webcraft'

export interface PageTransitionSettingState {
  pageTrasnition: PageTransitionType
}
