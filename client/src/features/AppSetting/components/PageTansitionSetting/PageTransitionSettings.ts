import {
  DefineCustomElement,
  PageTransition,
  type PageTransitionType,
  updateShadowRootContent
} from '@martelleur.online/webcraft'

import { createTemplate } from './PageTransitionSettings.template'
import type { PageTransitionSettingState } from './PageTransitionSettings.state'
import { customLogger } from '@utility'
import { createActionUpdatePageTransition } from '@state'
import {
  ReactiveStoreSubscriber,
  type AppState,
  type IReactiveState
} from '@store'

export const namePageTransitionSettings = 'page-transition-settings'

@DefineCustomElement(namePageTransitionSettings)
export class FontSettings extends ReactiveStoreSubscriber {
  private readonly _root: ShadowRoot
  private readonly reactiveState: IReactiveState<PageTransitionSettingState>
  private _transitionIntervall: NodeJS.Timeout | undefined

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    const initState: PageTransitionSettingState = {
      pageTrasnition: this.store.getState().ui.pageTransition
    }
    this.reactiveState = this.configureReactiveState(initState)
    this._handleApplyFont = this._handleApplyFont.bind(this)
  }

  subscribeToStore (state: AppState): void {
    try {
      this.reactiveState.state = {
        pageTrasnition: state.ui.pageTransition
      }
    } catch (error) {
      customLogger('Error when updating element state')
    }
  }

  connectedCallback (): void {
    this.render()
  }

  render (): void {
    this._toggleEventHandlers(false)
    this._toggleTransitionIntervall(false)
    updateShadowRootContent(this._root, createTemplate(this.reactiveState.state))
    this._toggleEventHandlers(true)
    this._toggleTransitionIntervall(true)
  }

  private _toggleEventHandlers (on: boolean): void {
    const transitions = this._root.querySelectorAll('.transition-radio')
    transitions.forEach(node => {
      if (!(node instanceof HTMLDivElement)) return
      if (on) node.addEventListener('click', this._handleApplyFont)
      else node.removeEventListener('click', this._handleApplyFont)
    })
  }

  private _handleApplyFont (event: MouseEvent): void {
    if (!(event.target instanceof HTMLDivElement)) {
      return
    }
    const transitionPageName = event.target.id
    if (this._isPageTransitionType(transitionPageName)) {
      this.store.dispatch(createActionUpdatePageTransition(transitionPageName))
    }
  }

  private _isPageTransitionType (value: any): value is PageTransitionType {
    return Object.values(PageTransition).includes(value)
  }

  private _toggleTransitionIntervall (on: boolean): void {
    if (on) {
      const slideTransition = this._root.querySelector('.slide-transition')
      this._transitionIntervall = setInterval(() => {
        slideTransition?.classList.toggle('page-2')
      }, 4000)
    } else {
      clearInterval(this._transitionIntervall)
    }
  }

  disconnectedCallback (): void {
    this._toggleTransitionIntervall(false)
  }
}
