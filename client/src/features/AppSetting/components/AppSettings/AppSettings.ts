import {
  assertElementExist,
  type SettingIcon
} from '@martelleur.online/webcraft'

import { appSettingsTemplate } from './AppSettings.template'
import { AppSettingDialog } from '../AppSettingDialog/AppSettingDialog'

export class AppSettings extends HTMLElement {
  private readonly _root
  private readonly _appSettings: SettingIcon

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    this._root.appendChild(appSettingsTemplate.content.cloneNode(true))
    this._appSettings = assertElementExist(this._root.querySelector<SettingIcon>('setting-icon'))
    this._bindEventHandlers()
  }

  private _bindEventHandlers (): void {
    this._handleSelectAppSettings = this._handleSelectAppSettings.bind(this)
  }

  connectedCallback (): void {
    this._appSettings.addEventListener('click', this._handleSelectAppSettings)
  }

  private _handleSelectAppSettings (): void {
    const settingDialog = new AppSettingDialog()
    document.body.appendChild(settingDialog)
    settingDialog.open()
  }

  disconnectedCallback (): void {
    this._appSettings.removeEventListener('click', this._handleSelectAppSettings)
  }
}

export const nameAppSettingsElement = 'app-settings'

customElements.define('app-settings', AppSettings)
