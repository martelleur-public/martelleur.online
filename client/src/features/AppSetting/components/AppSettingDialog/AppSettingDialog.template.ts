import '@martelleur.online/webcraft'

import '../ThemeSettings/ThemeSettings'
import '../FontSettings/FontSettings'
import '../PageTansitionSetting/PageTransitionSettings'
import { appDefaultContainerStyle } from '@style/app-container-style'

export const appSettingDialogTemplate = document.createElement('template')

appSettingDialogTemplate.innerHTML = `
<responsive-dialog section-lines>
    <h3 slot="title" id="title">App settings</h3>
    <div slot="sub-header" class="dialog-nav">
        <button id="theme-header" class="dialog-sub-header selected">
            Theme
        </button>
        <button id="font-header" class="dialog-sub-header">
            Font
        </button>
        <button id="transition-header" class="dialog-sub-header">
            Page Transition
        </button>
    </div>
    <div slot="body" class="dialog-body-container">
        <div id="theme-body" class="dialog-body">
            <theme-settings></theme-settings>
        </div>
        <div id="font-body" class="dialog-body hide">
            <font-settings></font-settings>
        </div>
        <div id="page-transition-body" class="dialog-body hide">
            <page-transition-settings></page-transition-settings>
        </div>
    </div>
</responsive-dialog>
${appDefaultContainerStyle}
<style>
#title {
    margin: 2px;
}

.dialog-sub-header {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 12px;
    margin: 2px;
    border-radius: 5px;
    cursor: pointer;
    border: none;
    outline: none;
    width: calc(100% / 3);
    min-height: 100%;
    background-color: var(--BACKGROUND_MEDIUM);
    font-family: var(--FONT_PRIMARY);
}

.dialog-sub-header.selected,
.dialog-sub-header:hover {
    background-color: var(--BACKGROUND_LIGHT_X);
    color: var(--TEXT_SECONDARY);
}

.dialog-nav {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: stretch;
}

.dialog-body-container,
.dialog-body {
    width: 100%;
}

.hide {
    display: none;
}
</style>
`
