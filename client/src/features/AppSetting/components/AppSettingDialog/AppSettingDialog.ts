import {
  type ResponsiveDialog,
  assertElementExist
} from '@martelleur.online/webcraft'

import { appSettingDialogTemplate } from './AppSettingDialog.template'
import { customLogger } from '@utility'

export class AppSettingDialog extends HTMLElement {
  private readonly _root: ShadowRoot
  private readonly _dialog: ResponsiveDialog
  private readonly _navigationList: NodeList
  private readonly _fontBody: HTMLDivElement
  private readonly _themeBody: HTMLDivElement
  private readonly _pageTransitionBody: HTMLDivElement

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    this._root.appendChild(appSettingDialogTemplate.content.cloneNode(true))
    this._dialog = assertElementExist(this._root.querySelector<ResponsiveDialog>('responsive-dialog'))
    this._fontBody = assertElementExist(this._root.querySelector('#font-body'))
    this._themeBody = assertElementExist(this._root.querySelector('#theme-body'))
    this._pageTransitionBody = assertElementExist(this._root.querySelector('#page-transition-body'))
    this._navigationList = this._root.querySelectorAll('.dialog-sub-header')
    this._bindEventHandlers()
  }

  private _bindEventHandlers (): void {
    this._handleNavigation = this._handleNavigation.bind(this)
    this.open = this.open.bind(this)
  }

  connectedCallback (): void {
    this._navigationList.forEach(node => {
      if (node instanceof HTMLButtonElement) {
        node.addEventListener('click', this._handleNavigation)
      }
    })
  }

  private _handleNavigation (event: MouseEvent): void {
    if (!(event.target instanceof HTMLButtonElement)) {
      return
    }
    this._updateUIForNavigationHeader(event.target)
    this._updateUIForNavigationBody(event.target)
  }

  private _updateUIForNavigationHeader (selectedNavigation: HTMLButtonElement): void {
    this._navigationList.forEach(item => {
      if (item instanceof HTMLButtonElement) {
        item.classList.remove('selected')
      }
    })
    selectedNavigation.classList.add('selected')
  }

  private _updateUIForNavigationBody (selectedNavigation: HTMLButtonElement): void {
    if (selectedNavigation.id === 'theme-header') {
      this._pageTransitionBody.classList.add('hide')
      this._themeBody.classList.remove('hide')
      this._fontBody.classList.add('hide')
    } else if (selectedNavigation.id === 'font-header') {
      this._pageTransitionBody.classList.add('hide')
      this._themeBody.classList.add('hide')
      this._fontBody.classList.remove('hide')
    } else if (selectedNavigation.id === 'transition-header') {
      this._pageTransitionBody.classList.remove('hide')
      this._themeBody.classList.add('hide')
      this._fontBody.classList.add('hide')
    }
  }

  open (): void {
    this._dialog.open()
  }

  disconnectedCallback (): void {
    customLogger(`${this.constructor.name} dialog disconnected`)
  }
}

export const nameAppSettingDialogElement = 'app-setting-dialog'

window.customElements.define(nameAppSettingDialogElement, AppSettingDialog)
