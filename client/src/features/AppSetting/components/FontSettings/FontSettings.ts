import { DefineCustomElement, updateShadowRootContent } from '@martelleur.online/webcraft'

import { createTemplate } from './FontSettings.template'
import type { FontSettingState } from './FontSettings.state'
import { customLogger } from '@utility'
import { type FontThemeNameType } from '@style/theme-fonts'
import { createActionUpdateFontTheme } from '@state'
import {
  ReactiveStoreSubscriber,
  type AppState,
  type IReactiveState
} from '@store'

export const nameFontSettings = 'font-settings'

@DefineCustomElement(nameFontSettings)
export class FontSettings extends ReactiveStoreSubscriber {
  private readonly _root: ShadowRoot
  private readonly _reactiveState: IReactiveState<FontSettingState>

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    const initState: FontSettingState = {
      fontThemeName: this.store.getState().ui.fontThemeName
    }
    this._reactiveState = this.configureReactiveState(initState)
    this._handleApplyFont = this._handleApplyFont.bind(this)
  }

  subscribeToStore (state: AppState): void {
    try {
      this._reactiveState.state = {
        fontThemeName: state.ui.fontThemeName
      }
    } catch (error) {
      customLogger('Error when updating element state')
    }
  }

  connectedCallback (): void {
    this.render()
  }

  render (): void {
    this._toggleEventHandlers(false)
    updateShadowRootContent(this._root, createTemplate(this._reactiveState.state))
    this._toggleEventHandlers(true)
  }

  private _toggleEventHandlers (on: boolean): void {
    const fonts = this._root.querySelectorAll('.font-radio')
    fonts.forEach(node => {
      if (!(node instanceof HTMLDivElement)) return
      if (on) node.addEventListener('click', this._handleApplyFont)
      else node.removeEventListener('click', this._handleApplyFont)
    })
  }

  private _handleApplyFont (event: MouseEvent): void {
    if (!(event.target instanceof HTMLDivElement)) {
      return
    }
    const fontName = event.target.id as FontThemeNameType
    this.store.dispatch(createActionUpdateFontTheme(fontName))
  }
}
