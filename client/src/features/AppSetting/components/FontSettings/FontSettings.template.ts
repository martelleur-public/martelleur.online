import { appDefaultContainerStyle } from '@style/app-container-style'
import type { FontSettingState } from './FontSettings.state'
import {
  type FontThemeNameType,
  CasualFriendly,
  ClassicElegance,
  FontThemeName,
  ModernClean,
  ProfessionalBusiness,
  TechnicalCode
} from '@style/theme-fonts'

export function createTemplate (state: FontSettingState): HTMLTemplateElement {
  const template = document.createElement('template')

  template.innerHTML = `
  <div class="font-radio" id="${FontThemeName.MODERN_CLEAN}">
    Font 1: ${formatThemeName(FontThemeName.MODERN_CLEAN)}
  </div>
  <div class="font-radio" id="${FontThemeName.CLASSIC_ELEGANCE}">
    Font 2: ${formatThemeName(FontThemeName.CLASSIC_ELEGANCE)}
  </div>
  <div class="font-radio" id="${FontThemeName.PROFESSIONAL_BUSINESS}">
    Font 3: ${formatThemeName(FontThemeName.PROFESSIONAL_BUSINESS)}
  </div>
  <div class="font-radio" id="${FontThemeName.CAUSAL_FRIENDLY}">
    Font 4: ${formatThemeName(FontThemeName.CAUSAL_FRIENDLY)}
  </div>
  <div class="font-radio" id="${FontThemeName.TEHCNICAL_CODE}">
    Font 5: ${formatThemeName(FontThemeName.TEHCNICAL_CODE)}
  </div>
  ${appDefaultContainerStyle}
  <style>
  :host {
    display: flex;
    flex-direction: column;
    width: 100%;
  }

  .font-radio {
    display: flex;
    align-items: center;
    min-height: 54px;
    justify-content: space-between;
    border: 2px solid var(--BORDER_DARK);
    padding: 10px;
    margin: 2px 0px;
    border-radius: 5px;
    background-color: var(--BACKGROUND_MEDIUM);
    cursor: pointer;
  }

  #${state.fontThemeName},
  .font-radio:hover {
    background-color: var(--BACKGROUND_LIGHT_X);
    border-color: var(--BORDER_LIGHT_X);
    color: var(--TEXT_SECONDARY);
  }

  #${FontThemeName.MODERN_CLEAN} {
    font-family: ${ModernClean.FONT_PRIMARY}
  }

  #${FontThemeName.CLASSIC_ELEGANCE} {
    font-family: ${ClassicElegance.FONT_PRIMARY}
  }

  #${FontThemeName.PROFESSIONAL_BUSINESS} {
    font-family: ${ProfessionalBusiness.FONT_PRIMARY}
  }

  #${FontThemeName.CAUSAL_FRIENDLY} {
    font-family: ${CasualFriendly.FONT_PRIMARY}
  }

  #${FontThemeName.TEHCNICAL_CODE} {
    font-family: ${TechnicalCode.FONT_PRIMARY}
  }
  </style>
  `
  return template
}

function formatThemeName (name: FontThemeNameType): string {
  const nameWithoutDash = name.replace('-', ' ')
  return nameWithoutDash.charAt(0).toUpperCase() + nameWithoutDash.slice(1)
}
