import { type FontThemeNameType } from '@style/theme-fonts'

export interface FontSettingState {
  fontThemeName: FontThemeNameType
}
