import '@martelleur.online/webcraft'

import '@components/AppealingButton/AppealingButton'
import { getAppVersion } from '@config'

export const infoDialogTemplate = document.createElement('template')
infoDialogTemplate.innerHTML = `
<responsive-dialog data-test="info-dialog">
    <h3 slot="title">About martelleur.online</h3>
    <div slot="body" class="dialog-main">
        <p>Owner:
            <remote-link
                title="https://github.com/Martelleur/Martelleur/blob/main/README.md" 
                href="https://github.com/Martelleur/Martelleur/blob/main/README.md"
                data-text="Joel Martelleur">
            </remote-link>
        </p>
        <p>Version: ${getAppVersion()}</p>
        <p>🚀 A site where I publish my public libraries and web applications</p>
    </div>
    <div slot="footer" class="dialog-footer">
        <appealing-button
            id="confirm"
            data-text="OK"
            data-test="confirm">
        </appealing-button>
    </div>
</responsive-dialog>
<style>
.dialog-header {
    height: 54px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
}

.dialog-main {
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: start;
}

.dialog-footer {
    display: flex;
    justify-content: center;
    gap: 10px;
    flex-wrap: wrap;
    align-items: center;
}
</style>
`
