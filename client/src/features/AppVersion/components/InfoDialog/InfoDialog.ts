import {
  assertElementExist,
  DefineCustomElement,
  type ResponsiveDialog
} from '@martelleur.online/webcraft'

import { infoDialogTemplate } from './InfoDialog.template'
import { customLogger } from '@utility'

export const nameInfoDialog = 'info-dialog'

@DefineCustomElement(nameInfoDialog)
export class InfoDialog extends HTMLElement {
  private readonly _dialog: ResponsiveDialog
  private readonly _confirm: HTMLButtonElement

  constructor () {
    super()
    const root = this.attachShadow({ mode: 'open' })
    root.appendChild(infoDialogTemplate.content.cloneNode(true))
    this._dialog = assertElementExist(root.querySelector<ResponsiveDialog>('responsive-dialog'))
    this._confirm = assertElementExist(root.querySelector('#confirm'))
    this.bindEventHandlersToThis()
  }

  private bindEventHandlersToThis (): void {
    this._handleConfirm = this._handleConfirm.bind(this)
    this._handleClose = this._handleClose.bind(this)
    this.close = this.close.bind(this)
  }

  connectedCallback (): void {
    this._confirm.addEventListener('click', this._handleConfirm)
  }

  private _handleConfirm (): void {
    this.close()
  }

  private _handleClose (): void {
    this.close()
  }

  open (): void {
    this._dialog.open()
  }

  close (): void {
    this._dialog.close()
    this.remove()
  }

  disconnectedCallback (): void {
    customLogger(`${this.constructor.name} disconnected`)
  }
}
