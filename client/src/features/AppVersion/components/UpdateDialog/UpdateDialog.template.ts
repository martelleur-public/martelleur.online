import '@martelleur.online/webcraft'

import '@components/AppealingButton/AppealingButton'

export const updateDialogTemplate = document.createElement('template')
updateDialogTemplate.innerHTML = `
<responsive-dialog data-test="update-dialog">
    <h3 slot="title">New Version!</h3>
    <div slot="body" class="dialog-main">
        <p>Would you like to update?</p>
    </div>
    <div slot="footer" class="dialog-footer">
        <appealing-button
            id="confirm"
            data-text="YES"
            data-test="confirm">
        </appealing-button>
        <appealing-button
            id="deny"
            data-text="NO"
            data-test="deny">
        </appealing-button>
    </div>
</dialog>
<style>
dialog {
    min-width: 50vw;
    padding: 20px;
    background: var(--BACKGROUND_DARK);
    color: var(--TEXT_PRIMARY);
    border: 3px solid var(--BORDER_LIGHT_X);
    outline: none;
    border-radius: 8px;
}

.dialog-main {
    display: flex;
    justify-content: center;
    align-items: center;
}

.dialog-footer {
    display: flex;
    justify-content: center;
    gap: 10px;
    flex-wrap: wrap;
    align-items: center;
}
</style>
`
