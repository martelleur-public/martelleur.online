import {
  assertElementExist,
  DefineCustomElement,
  type ResponsiveDialog
} from '@martelleur.online/webcraft'

import { updateDialogTemplate } from './UpdateDialog.template'
import { customLogger } from '@utility'
import { ConfirmUpdateEvent } from '@event'

export const nameAppUpdateDialog = 'update-dialog'

@DefineCustomElement(nameAppUpdateDialog)
export class UpdateDialog extends HTMLElement {
  private readonly _dialog: ResponsiveDialog
  private readonly _confirm: HTMLButtonElement
  private readonly _deny: HTMLButtonElement

  constructor () {
    super()
    const root = this.attachShadow({ mode: 'open' })
    root.appendChild(updateDialogTemplate.content.cloneNode(true))
    this._dialog = assertElementExist(root.querySelector('responsive-dialog'))
    this._confirm = assertElementExist(root.querySelector('#confirm'))
    this._deny = assertElementExist(root.querySelector('#deny'))
    this.bindEventHandlers()
  }

  private bindEventHandlers (): void {
    this.handleConfirm = this.handleConfirm.bind(this)
    this.handleDeny = this.handleDeny.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.close = this.close.bind(this)
  }

  connectedCallback (): void {
    this._confirm.addEventListener('click', this.handleConfirm)
    this._deny.addEventListener('click', this.handleDeny)
  }

  handleConfirm (): void {
    this.dispatchEvent(new ConfirmUpdateEvent())
    this.close()
  }

  handleDeny (): void {
    this.close()
  }

  handleClose (): void {
    this.close()
  }

  open (): void {
    this._dialog.open()
  }

  close (): void {
    this._dialog.close()
    this.remove()
  }

  disconnectedCallback (): void {
    customLogger(`${this.constructor.name} dialog disconnected`)
  }
}
