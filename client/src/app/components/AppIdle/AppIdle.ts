import { CreateAndDefineShadowElement } from '@martelleur.online/webcraft'

import { appIdleTemplate as template } from './AppIdle.template'

export const nameAppIdle = 'app-idle'

@CreateAndDefineShadowElement(nameAppIdle, { template })
export class AppIdle extends HTMLElement {}
