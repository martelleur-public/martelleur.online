import '@icons/AppIcon/AppIcon'

export const appIdleTemplate = document.createElement('template')

appIdleTemplate.innerHTML = `
<div class="intro-wrapper"
    data-test="intro-wrapper">
    <div class="intro-icon"
        data-test="intro-icon">
        <app-icon></app-icon>
    </div>
    <div class="intro-title"
        data-test="intro-title">
        martelleur.online
    </div>
</div>
<style>
.intro-wrapper {
    width: 100%;
    height: 100%;
    position: fixed;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background-color: var(--BACKGROUND_DARK);
}
    
.intro-title {
    font-size: 2rem;
    color: var(--TEXT_PRIMARY);
}

.intro-icon {
    width: 150px;
    height: 150px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 20px;
}
</style>
`
