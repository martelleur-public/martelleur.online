import '@icons/AppIcon/AppIcon'
import { AppIntroTime } from '../constants'

export const appIntroTemplate = document.createElement('template')

appIntroTemplate.innerHTML = `
<div class="intro-wrapper"
    data-test="intro-wrapper">
    <div class="intro-icon"
        data-test="intro-icon">
        <app-icon></app-icon>
    </div>
    <div class="intro-title"
        data-test="intro-title">
        martelleur.online
    </div>
</div>
<style>
.intro-wrapper {
    width: 100%;
    height: 100%;
    position: fixed;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background-color: var(--BACKGROUND_DARK);
}
    
.intro-title {
    font-size: 2rem;
    transform: scale(0.2);
    opacity: 0;
    color: var(--TEXT_PRIMARY);
    animation: zoomIn ${AppIntroTime}s ease-out;
}

@keyframes zoomIn {
    from { transform: scale(0.2); opacity: 0.5; }
    to   { transform: scale(1.2); opacity: 1; }
}

.intro-icon {
    width: 150px;
    height: 150px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 20px;
}
</style>
`
