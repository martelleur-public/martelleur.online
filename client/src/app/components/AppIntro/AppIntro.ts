import { CreateAndDefineShadowElement } from '@martelleur.online/webcraft'

import { appIntroTemplate as template } from './AppIntro.template'

export const nameAppIntro = 'app-intro'

@CreateAndDefineShadowElement(nameAppIntro, { template })
export class AppIntro extends HTMLElement {}
