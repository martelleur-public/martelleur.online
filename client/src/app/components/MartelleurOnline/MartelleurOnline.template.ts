import '@layouts/AppLayout/AppLayout'
import { AppIntroTime } from '../constants'
import '../AppIntro/AppIntro'
import '../AppIdle/AppIdle'

export const appTemplate = document.createElement('template')

export const appIdleTemplate = document.createElement('template')

appTemplate.innerHTML = `
<app-intro data-test="app-intro"></app-intro>
<app-idle data-test="app-idle"></app-idle>
<app-layout data-test="app-layout"></app-layout>
<style>
app-intro {
    width: 100%;
    height: 100%;
    animation: fadeOut 0.5s ${AppIntroTime}s forwards;
}

@keyframes fadeOut {
    from { opacity: 1; }
    to { opacity: 0; visibility: hidden; }
}

app-layout,
app-idle {
    width: 100%;
    height: 100%;
    display: none;
}
</style>
`
