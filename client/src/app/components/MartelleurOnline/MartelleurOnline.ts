import {
  DefineCustomElement,
  assertElementExist
} from '@martelleur.online/webcraft'

import { appTemplate } from './MartelleurOnline.template'
import type { AppIdle } from '../AppIdle/AppIdle'
import type { AppIntro } from '../AppIntro/AppIntro'
import { activityEvents, appIdleTimeout } from '../constants'
import { UserActivityEvent } from '@event'
import { StoreProvider } from '@store'
import { updateColors } from '@style/theme-colors'
import { updateFonts } from '@style/theme-fonts'
import { setupServices } from '@service'
import type { AppLayout } from '@layouts'

export const nameMartelleurOnlineElement = 'martelleur-online'

@DefineCustomElement(nameMartelleurOnlineElement)
export class MartelleurOnline extends StoreProvider {
  private readonly _root: ShadowRoot
  private readonly _appLayout: AppLayout
  private readonly _appIntro: AppIntro
  private readonly _appIdle: AppIdle
  private _isUserActive = false
  private _activityTimeout: NodeJS.Timeout

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    this._root.appendChild(appTemplate.content.cloneNode(true))
    this._appLayout = assertElementExist(this._root.querySelector<AppLayout>('app-layout'))
    this._appIntro = assertElementExist(this._root.querySelector<AppIntro>('app-intro'))
    this._appIdle = assertElementExist(this._root.querySelector<AppIdle>('app-idle'))
    this._bindEventHandlersToThis()
    this._activityTimeout = this._getUpdatedActivityTimeout()
  }

  private _bindEventHandlersToThis (): void {
    this._handleAnimationEnd = this._handleAnimationEnd.bind(this)
    this._setUserActive = this._setUserActive.bind(this)
  }

  private _getUpdatedActivityTimeout (): NodeJS.Timeout {
    clearTimeout(this._activityTimeout)
    return setTimeout(() => {
      this._isUserActive = false
      const evenDetail = { isUserActive: this._isUserActive }
      this.dispatchEvent(new UserActivityEvent(evenDetail))
      this._updateUiAccordingToUserActivity(this._isUserActive)
    }, appIdleTimeout)
  }

  connectedCallback (): void {
    this._addAppEventHandlers()
    this._initApp()
  }

  private _initApp (): void {
    const colorThemeName = this.store.getState().ui.colorThemeName
    const fontThemeName = this.store.getState().ui.fontThemeName
    updateColors(colorThemeName)
    updateFonts(fontThemeName)
    setupServices()
  }

  private _addAppEventHandlers (): void {
    this._appIntro.addEventListener('animationend', this._handleAnimationEnd)
    activityEvents.forEach((eventName) => {
      this.addEventListener(eventName, this._setUserActive)
    })
  }

  private _handleAnimationEnd (): void {
    this._appIntro.style.display = 'none'
    this._appLayout.style.display = 'block'
  }

  private _setUserActive (): void {
    const prevStatus = this._isUserActive
    this._isUserActive = true
    if (prevStatus !== this._isUserActive) {
      const evenDetail = { isUserActive: this._isUserActive }
      this.dispatchEvent(new UserActivityEvent(evenDetail))
      this._updateUiAccordingToUserActivity(this._isUserActive)
    }
    this._activityTimeout = this._getUpdatedActivityTimeout()
  }

  private _updateUiAccordingToUserActivity (isActive: boolean): void {
    if (isActive) {
      this._appLayout.style.display = 'block'
      this._appIdle.style.display = 'none'
    } else {
      this._appLayout.style.display = 'none'
      this._appIdle.style.display = 'block'
    }
  }
}
