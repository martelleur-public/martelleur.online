export const AppIntroTime = 2

export const appIdleTimeout = 60000 * 20

export const activityEvents = ['pointermove', 'keydown'] as const
