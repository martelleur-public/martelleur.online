export enum NotificationDecision {
  GRANTED = 'granted',
  DENIED = 'denied',
  DEFAULT = 'default'
}
