import {
  CreateAndDefineShadowElement
} from '@martelleur.online/webcraft'

import { template } from './AppLayout.template'

@CreateAndDefineShadowElement('app-layout', { template })
export class AppLayout extends HTMLElement {}
