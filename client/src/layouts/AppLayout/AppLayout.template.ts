import '@martelleur.online/webcraft'

import '@router'
import '@features/AppNavigation'

export const template = document.createElement('template')

template.innerHTML = `
<basic-layout dynamic-height>
  <app-navigation slot="header"></app-navigation>
  <app-state-router slot="body"></app-state-router>
</basic-layout>
`
