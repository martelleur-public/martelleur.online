export class ConfirmUpdateEvent extends CustomEvent<null> {
  static readonly eventName = 'confirm-update-app'

  constructor () {
    super(ConfirmUpdateEvent.eventName, {
      bubbles: true,
      cancelable: true
    })
  }
}
