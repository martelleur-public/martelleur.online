export function handleCustomEvent<T> (
  eventType: string,
  element: EventTarget,
  callback: (detail: T) => void
): () => void {
  const eventListener = (event: Event): void => {
    const customEvent = event as CustomEvent<T>
    callback(customEvent.detail)
  }
  element.addEventListener(eventType, eventListener)
  const removeEventListener = (): void => {
    element.removeEventListener(eventType, eventListener)
  }
  return removeEventListener
}
