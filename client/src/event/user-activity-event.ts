export class UserActivityEvent extends CustomEvent<UserActivityEventDetail> {
  static readonly eventName = 'on-user-activity'

  constructor (detail: UserActivityEventDetail) {
    super(UserActivityEvent.eventName, {
      bubbles: true,
      cancelable: true,
      detail
    })
  }
}

export interface UserActivityEventDetail {
  isUserActive: boolean
}
