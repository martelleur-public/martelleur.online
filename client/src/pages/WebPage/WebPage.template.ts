import '@components/ImageLink/ImageLink'
import { appDefaultContainerStyle } from '@style/app-container-style'

export const webPageTemplate = document.createElement('template')

webPageTemplate.innerHTML = `
<main id="container">
    <image-link
        title="Library WebCraft"
        href="https://gitlab.com/martelleur-public/martelleur.online/-/blob/main/lib/webcraft/doc/todo/project-webcraft-library.md#project-webcraft-library"
        data-text="Library WebCraft"
        data-image="imageNpmLibrary">
    </image-link>
    <image-link
        title="A simple web chat called MyChat"
        href="https://chat.martelleur.online"
        data-text="MyChat"
        data-image="imageMyChat">
    </image-link>
    <image-link
        title="Github Martelleur"
        href="https://github.com/Martelleur/Martelleur"
        data-text="GitHub Martelleur"
        data-image="imageGitHub">
    </image-link>
    <image-link
        title="Minesweeper game"
        href="https://chat.martelleur.online/game/minesweeper/"
        data-text="Minesweeper"
        data-image="imageMinesweeper">
    </image-link>
</main>
${appDefaultContainerStyle}
<style>
:host, #container {
    display: block;
    width: 100%;
    height: 100%;
}

#container {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    gap: 10px;
    justify-items: center;
    align-items: center;
    box-sizing: border-box;
    padding: 0 10px 10px 10px;
}

@media (max-width: 700px), (max-height: 500px) {
    #container {
        grid-template-columns: repeat(1, 1fr);
    }
}
</style>
`
