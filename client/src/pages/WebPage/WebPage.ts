import { CreateShadowElement, NamedHTMLElement } from '@martelleur.online/webcraft'

import { webPageTemplate as template } from './WebPage.template'

@CreateShadowElement('web-page', { template })
export class WebPage extends NamedHTMLElement {}
