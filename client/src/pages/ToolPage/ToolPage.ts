import { CreateShadowElement, NamedHTMLElement } from '@martelleur.online/webcraft'

import { toolPageTemplate as template } from './ToolPage.template'

@CreateShadowElement('tool-page', { template })
export class ToolPage extends NamedHTMLElement {}
