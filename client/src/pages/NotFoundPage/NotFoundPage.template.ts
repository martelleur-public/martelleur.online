import { appDefaultContainerStyle } from '@style/app-container-style'
// import '@components/RotatingCube/RotatingCube'
// import '@features/AppThreeDimension'

export const notFoundPageTemplate = document.createElement('template')

notFoundPageTemplate.innerHTML = `
<div id="container">
    <div id="page-not-found-container">
        <h1>404</h1>
        <p>Page Not Found</p>
        <!-- <app-three-dimension></app-three-dimension> -->
    </div>
</div>
${appDefaultContainerStyle}
<style>
:host, #container {
    display: block;
    width: 100%;
    height: 100%;
    background: var(--BACKGROUND_LIGHT);
    color: var(--TEXT_PRIMARY);
}

#container {
    display: flex;
    justify-content: center;
    padding-top: 50px;
    align-items: start;
}

#page-not-found-container {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 50%;
    padding: 20px;
    border: 1px solid var(--BORDER_DARK);
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
}

h1 {
    color: #e74c3c;
    font-size: 4em;
}

/* app-three-dimension,
rotating-cube {
    width: 30vw;
    height: 30vw;
} */
</style>
`
