import { CreateShadowElement, NamedHTMLElement } from '@martelleur.online/webcraft'

import { notFoundPageTemplate as template } from './NotFoundPage.template'

@CreateShadowElement('not-found-page', { template })
export class NotFoundPage extends NamedHTMLElement {}
