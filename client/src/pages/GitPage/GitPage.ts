import { CreateShadowElement, NamedHTMLElement } from '@martelleur.online/webcraft'

import { gitPageTemplate as template } from './GitPage.template'

@CreateShadowElement('git-page', { template })
export class GitPage extends NamedHTMLElement {}
