import '@components/ImageLink/ImageLink'
import { appDefaultContainerStyle } from '@style/app-container-style'

export const gitPageTemplate = document.createElement('template')
gitPageTemplate.innerHTML = `
<div id="container">
    <image-link
        title="Github Martelleur"
        href="https://github.com/Martelleur/Martelleur"
        data-text="GitHub Martelleur"
        data-image="imageGitHub">
    </image-link>
    <image-link
        title="Gitlab martelleur.online"
        href="https://gitlab.com/martelleur-public/martelleur.online"
        data-text="GitLab martelleur.online"
        data-image="imageGitLab">
    </image-link>
    <image-link
        title="Library @martelleur/WebCraft"
        href="https://gitlab.com/martelleur-public/martelleur.online/-/blob/main/lib/webcraft/doc/todo/project-webcraft-library.md#project-webcraft-library"
        data-text="Library WebCraft"
        data-image="imageNpmLibrary">
    </image-link>
    <image-link
        title="Gitlab Android Java"
        href="https://gitlab.com/martelleur-public/android-java"
        data-text="GitLab Android Java"
        data-image="imageGitLab">
    </image-link>
</div>
${appDefaultContainerStyle}
<style>
:host, #container {
    display: block;
    width: 100%;
    height: 100%;
}

#container {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    gap: 10px;
    justify-items: center;
    align-items: center;
    box-sizing: border-box;
    padding: 0 10px 10px 10px;
    overflow: hidden;
}

@media (max-width: 700px), (max-height: 500px) {
    #container {
        grid-template-columns: repeat(1, 1fr);
    }
}
</style>
`
