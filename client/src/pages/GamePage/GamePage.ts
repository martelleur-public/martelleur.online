import { CreateShadowElement, NamedHTMLElement } from '@martelleur.online/webcraft'

import { gamePageTemplate as template } from './GamePage.template'

@CreateShadowElement('game-page', { template })
export class GamePage extends NamedHTMLElement {}
