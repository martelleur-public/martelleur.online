import '@components/ImageLink/ImageLink'
import { appDefaultContainerStyle } from '@style/app-container-style'

export const gamePageTemplate = document.createElement('template')
gamePageTemplate.innerHTML = `
<div id="container">
    <image-link
        title="Memory game"
        href="https://chat.martelleur.online/game/memory/"
        data-text="Memory"
        data-image="imageMemory">
    </image-link>
    <image-link
        title="Minesweeper game"
        href="https://chat.martelleur.online/game/minesweeper/"
        data-text="Minesweeper"
        data-image="imageMinesweeper">
    </image-link>
    <image-link
        title="Memory game"
        href="https://chat.martelleur.online/game/memory/"
        data-text="Memory"
        data-image="imageMemory">
    </image-link>
    <image-link
        title="Minesweeper game"
        href="https://chat.martelleur.online/game/minesweeper/"
        data-text="Minesweeper"
        data-image="imageMinesweeper">
    </image-link>
</div>
${appDefaultContainerStyle}
<style>
:host, #container {
    display: block;
    width: 100%;
    height: 100%;
}

#container {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    gap: 10px;
    justify-items: center;
    align-items: center;
    box-sizing: border-box;
    padding: 0 10px 10px 10px;
    overflow: hidden;
}

@media (max-width: 700px), (max-height: 500px) {
    #container {
        grid-template-columns: repeat(1, 1fr);
    }
}
</style>
`
