import { assertElementExist } from '@martelleur.online/webcraft'

import { appIconTemplate } from './AppIcon.template'
import { customLogger } from '@utility'
import * as Constant from '@icons/constant'

export class AppIcon extends HTMLElement {
  private readonly _bigIcon: SVGElement
  private readonly _mediumIcon: SVGElement

  constructor () {
    super()
    const root = this.attachShadow({ mode: 'open' })
    root.appendChild(appIconTemplate.content.cloneNode(true))
    this._bigIcon = assertElementExist(root.querySelector<SVGCircleElement>('#big-icon'))
    this._mediumIcon = assertElementExist(root.querySelector<SVGCircleElement>('#medium-icon'))
  }

  static get observedAttributes (): string[] {
    return [Constant.ICON_SIZE_ATTRIBUTE]
  }

  attributeChangedCallback (name: string, oldValue: string, newValue: string): void {
    switch (name) {
      case Constant.ICON_SIZE_ATTRIBUTE:
        this._updateIconSize(newValue)
        break
      default:
        customLogger(`Not a valid attribute, old value:
          Old attribute value', ${oldValue}
          New attribute value', ${newValue}`)
    }
  }

  _updateIconSize (size: string): void {
    switch (size) {
      case Constant.MEDIUM_ICON_SIZE:
        this._setMediumIcon()
        break
      case Constant.LARGE_ICON_SIZE:
        this._setLargeIcon()
        break
      default:
        customLogger('Not a valid size attribute value')
    }
  }

  _setMediumIcon (): void {
    this._bigIcon.style.display = 'none'
    this._mediumIcon.style.display = 'initial'
  }

  _setLargeIcon (): void {
    this._bigIcon.style.display = 'initial'
    this._mediumIcon.style.display = 'none'
  }
}

export const nameAppIcon = 'app-icon'

window.customElements.define(nameAppIcon, AppIcon)
