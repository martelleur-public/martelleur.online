import { getUserAgentName } from '@martelleur.online/webcraft'

export const appIconTemplate = document.createElement('template')

function getTextYPositionLargeIcon (): number {
  const userAGentName = getUserAgentName()
  return userAGentName === 'Firefox'
    ? 90
    : 75
}

function getTextYPositionMediumIcon (): number {
  const userAGentName = getUserAgentName()
  return userAGentName === 'Firefox'
    ? 26
    : 20
}

appIconTemplate.innerHTML = `
<svg
    id="big-icon"
    width="150"
    height="150"
    xmlns="http://www.w3.org/2000/svg">
    <circle
      cx="75"
      cy="75"
      r="65"
      fill="none"
      stroke="#6f00ff"
      stroke-width="20"
    />
    <text 
        x="75"
        y="${getTextYPositionLargeIcon()}"
        text-anchor="middle"
        alignment-baseline="middle"
        font-family="Arial"
        font-size="48">
        42
    </text>
</svg>
<svg
    id="medium-icon"
    width="64"
    height="64"
    viewBox="-12 -12 64 64"
    xmlns="http://www.w3.org/2000/svg">
    <circle
        cx="20"
        cy="20"
        r="20"
        fill="none"
        stroke="#6f00ff"
        stroke-width="6"
    />
    <text 
        x="20"
        y="${getTextYPositionMediumIcon()}"
        text-anchor="middle"
        alignment-baseline="middle"
        font-family="Arial"
        font-size="18">
        42
    </text>

</svg>
<style>
*, *::before, *::after {
    box-sizing: border-box;
}

#medium-icon {
    display: none;
}

text {
    fill: var(--TEXT_PRIMARY);
    font-weight: bold;
    text-anchor: middle;
    alignment-baseline: central;
}
</style>
`
