export const MEDIUM_ICON_SIZE = 'medium'
export const LARGE_ICON_SIZE = 'large'
export const ICON_SIZE_ATTRIBUTE = 'data-size'
