export const appDefaultContainerStyle = `
<style>
    :host {
        font-family: var(--FONT_PRIMARY);
    },
    *, *::before, *::after {
        box-sizing: border-box;
    }
</style>
`
