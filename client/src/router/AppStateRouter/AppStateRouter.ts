import {
  DefineCustomElement,
  updateShadowRootContent,
  type IReactiveState
} from '@martelleur.online/webcraft'

import { createTemplate } from './AppStateRouter.template'
import type { AppStateRouterState } from './AppStateRouter.state'
import { type AppState, ReactiveStoreSubscriber } from '@store'
import { customLogger } from '@utility'

@DefineCustomElement('app-state-router')
export class AppStateRouter extends ReactiveStoreSubscriber {
  private readonly _root: ShadowRoot
  private readonly reactiveState: IReactiveState<AppStateRouterState>

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    const initState: AppStateRouterState = {
      pageTransition: this.store.getState().ui.pageTransition
    }
    this.reactiveState = this.configureReactiveState(initState)
  }

  subscribeToStore (state: AppState): void {
    try {
      this.reactiveState.state = {
        pageTransition: state.ui.pageTransition
      }
    } catch (error) {
      customLogger('Error when updating element state')
    }
  }

  connectedCallback (): void {
    this.render()
  }

  render (): void {
    updateShadowRootContent(this._root, createTemplate(this.reactiveState.state))
  }
}
