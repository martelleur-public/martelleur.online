import type { PageTransitionType } from '@martelleur.online/webcraft'

export interface AppStateRouterState {
  pageTransition: PageTransitionType
}
