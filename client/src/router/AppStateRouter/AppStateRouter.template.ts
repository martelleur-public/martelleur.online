import '@martelleur.online/webcraft'

import '@pages/NotFoundPage/NotFoundPage'
import '@pages/WebPage/WebPage'
import '@pages/GamePage/GamePage'
import '@pages/GitPage/GitPage'
import '@pages/ToolPage/ToolPage'
import type { AppStateRouterState } from './AppStateRouter.state'

/**
 * @TODO Passing layout to router and route group so route-group
 * elements can share layouts or use unique layouts per group.
 */
export function createTemplate (state: AppStateRouterState): HTMLTemplateElement {
  const template = document.createElement('template')
  template.innerHTML = `
<app-router slot="body" transition="${state.pageTransition}">
  <route-group path="">
    <route-item path="index" component="web-page"></route-item>
    <route-item path="git" component="game-page"></route-item>
    <route-item path="game" component="git-page"></route-item>
    <route-item path="default" component="not-found-page" default></route-item>
  </route-group>
</app-router>
`
  return template
}
