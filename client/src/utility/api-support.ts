export const isNotificationAPISupported = (): boolean => {
  return 'Notification' in window
}
