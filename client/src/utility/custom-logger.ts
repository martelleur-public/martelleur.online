import { getEnvName } from '@config'

export function customLogger (...messages: unknown[]): void {
  if (getEnvName() === 'development') {
    console.log(...messages)
  }
}

export function productionLogger (...messages: unknown[]): void {
  console.log(...messages)
}
