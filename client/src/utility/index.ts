export {
  isNotificationAPISupported
} from './api-support'

export {
  customLogger,
  productionLogger
} from './custom-logger'

export {
  assertElementExist,
  sanitizeHTMLInput,
  ElementDoesNotExistError
} from './html'
