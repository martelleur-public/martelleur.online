import type { PageTransitionType } from '@martelleur.online/webcraft'

import type { ColorThemeNameType } from '@style/theme-colors'
import type { FontThemeNameType } from '@style/theme-fonts'

export const UPDATE_NOTIFICATION_DECISION = 'UPDATE_NOTIFICATION_DECISION'
export const UPDATE_COLOR_THEME_NAME = 'UPDATE_COLOR_THEME_NAME'
export const UPDATE_FONT_THEME_NAME = 'UPDATE_FONT_THEME_NAME'
export const UPDATE_PAGE_TRANSITION = 'UPDATE_PAGE_TRANSITION'
export const UPDATE_IS_SIDEBAR_OPEN = 'UPDATE_IS_SIDEBAR_OPEN'

export interface UiState {
  notifactionDecision: NotificationPermission
  colorThemeName: ColorThemeNameType
  fontThemeName: FontThemeNameType
  sidebarIsOpen: boolean
  pageTransition: PageTransitionType
}

export interface UpdateNotificationDecisionAction {
  type: typeof UPDATE_NOTIFICATION_DECISION
  payload: NotificationPermission
}

export interface UpdateColorThemeAction {
  type: typeof UPDATE_COLOR_THEME_NAME
  payload: ColorThemeNameType
}

export interface UpdateFontThemeAction {
  type: typeof UPDATE_FONT_THEME_NAME
  payload: FontThemeNameType
}

export interface UpdatePageTransition {
  type: typeof UPDATE_PAGE_TRANSITION
  payload: PageTransitionType
}

export interface UpdateIsSidebarOpenAction {
  type: typeof UPDATE_IS_SIDEBAR_OPEN
  payload: boolean
}

export type UiAction =
| UpdateNotificationDecisionAction
| UpdateColorThemeAction
| UpdateFontThemeAction
| UpdatePageTransition
| UpdateIsSidebarOpenAction
