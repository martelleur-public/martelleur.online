import {
  UPDATE_COLOR_THEME_NAME,
  UPDATE_FONT_THEME_NAME,
  UPDATE_IS_SIDEBAR_OPEN,
  UPDATE_NOTIFICATION_DECISION,
  UPDATE_PAGE_TRANSITION,
  type UiAction,
  type UiState
} from './types'

export function uiReducer (state: UiState, action: UiAction): UiState {
  switch (action.type) {
    case UPDATE_NOTIFICATION_DECISION:
      return {
        ...state,
        notifactionDecision: action.payload
      }
    case UPDATE_COLOR_THEME_NAME:
      return {
        ...state,
        colorThemeName: action.payload
      }
    case UPDATE_FONT_THEME_NAME:
      return {
        ...state,
        fontThemeName: action.payload
      }
    case UPDATE_PAGE_TRANSITION:
      return {
        ...state,
        pageTransition: action.payload
      }
    case UPDATE_IS_SIDEBAR_OPEN:
      return {
        ...state,
        sidebarIsOpen: action.payload
      }
    default:
      return state
  }
}
