import type { UiState } from './types'
import { createSettingService } from '@service/service-factory'

const settingService = createSettingService()

/**
 * @TODO Change typo notifactionDecision to notificationDecision.
 */
export const initUiState: UiState = {
  notifactionDecision: settingService.getNotificationDecision(),
  colorThemeName: settingService.getColorThemeName(),
  fontThemeName: settingService.getFontThemeName(),
  pageTransition: settingService.getPageTransition(),
  sidebarIsOpen: false
}
