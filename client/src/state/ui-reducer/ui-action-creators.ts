import type { PageTransitionType } from '@martelleur.online/webcraft'

import type { AppAction } from '@store'
import type { FontThemeNameType } from '@style/theme-fonts'
import type { ColorThemeNameType } from '@style/theme-colors'
import {
  UPDATE_COLOR_THEME_NAME,
  UPDATE_FONT_THEME_NAME,
  UPDATE_IS_SIDEBAR_OPEN,
  UPDATE_NOTIFICATION_DECISION,
  UPDATE_PAGE_TRANSITION
} from './types'

export function createActionUpdateColorTheme (
  colorThemeName: ColorThemeNameType
): AppAction {
  return {
    type: UPDATE_COLOR_THEME_NAME,
    payload: colorThemeName
  }
}

export function createActionUpdateFontTheme (
  fontThemeName: FontThemeNameType
): AppAction {
  return {
    type: UPDATE_FONT_THEME_NAME,
    payload: fontThemeName
  }
}

export function createActionNotificationDecision (
  decision: NotificationPermission
): AppAction {
  return {
    type: UPDATE_NOTIFICATION_DECISION,
    payload: decision
  }
}

export function createActionUpdatePageTransition (
  pageTransition: PageTransitionType
): AppAction {
  return {
    type: UPDATE_PAGE_TRANSITION,
    payload: pageTransition
  }
}

export function createActioUpdateIsSidebarOpen (
  isOpen: boolean
): AppAction {
  return {
    type: UPDATE_IS_SIDEBAR_OPEN,
    payload: isOpen
  }
}
