import type {
  AppAction,
  Middleware,
  AppState,
  MiddlewareStoreApi,
  Next
} from '@store'
import {
  createSettingService,
  createWebPushService
} from '@service/service-factory'
import { customLogger } from '@utility'
import type { SettingService } from '@service/setting-service'
import { type WebPushService } from '@service/web-push-service'
import { NotificationDecision } from '@constants'
import {
  type UpdateColorThemeAction,
  type UpdateFontThemeAction,
  type UpdateNotificationDecisionAction,
  type UpdatePageTransition,
  UPDATE_COLOR_THEME_NAME,
  UPDATE_FONT_THEME_NAME,
  UPDATE_NOTIFICATION_DECISION,
  UPDATE_PAGE_TRANSITION
} from './types'
import {
  createActionUpdateColorTheme,
  createActionUpdateFontTheme,
  createActionNotificationDecision,
  createActionUpdatePageTransition
} from './ui-action-creators'
import { updateColors } from '@style/theme-colors'
import { updateFonts } from '@style/theme-fonts'

class UIReducerMiddleware {
  constructor (
    private readonly settingService: SettingService,
    private readonly webPushService: WebPushService
  ) {
    this.middleware = this.middleware.bind(this)
  }

  middleware: Middleware<AppState, AppAction> = store => next => action => {
    switch (action.type) {
      case (UPDATE_COLOR_THEME_NAME):
        this.handleUpdateColorTheme(action, next)
        break
      case (UPDATE_FONT_THEME_NAME):
        this.handleUpdateFontTheme(action, next)
        break
      case (UPDATE_PAGE_TRANSITION):
        this.handleUpdatePageTransition(action, next)
        break
      case (UPDATE_NOTIFICATION_DECISION):
        this.handleUpdateNotificationDecision(action, next)
        break
      default:
        this.handleDefault(action, store, next)
    }
  }

  handleUpdateColorTheme (
    action: UpdateColorThemeAction,
    next: Next<AppAction>
  ): void {
    customLogger(`${UPDATE_COLOR_THEME_NAME} middleware is used`)
    const selectedThemeName = action.payload
    const updatedThemeName = this.settingService.updateColorThemeName(selectedThemeName)
    const updatedAction = createActionUpdateColorTheme(updatedThemeName)
    updateColors(updatedThemeName)
    next(updatedAction)
  }

  handleUpdateFontTheme (
    action: UpdateFontThemeAction,
    next: Next<AppAction>
  ): void {
    customLogger(`${UPDATE_FONT_THEME_NAME} middleware is used`)
    const selectedFontName = action.payload
    const updatedFontName = this.settingService.updateFontThemeName(selectedFontName)
    const updatedAction = createActionUpdateFontTheme(updatedFontName)
    updateFonts(updatedFontName)
    next(updatedAction)
  }

  handleUpdatePageTransition (
    action: UpdatePageTransition,
    next: Next<AppAction>
  ): void {
    customLogger(`${UPDATE_PAGE_TRANSITION} middleware is used`)
    const selectedPageTransition = action.payload
    const updatedPageTransition = this.settingService.updatePageTransition(selectedPageTransition)
    const updatedAction = createActionUpdatePageTransition(updatedPageTransition)
    next(updatedAction)
  }

  handleUpdateNotificationDecision (
    action: UpdateNotificationDecisionAction,
    next: Next<AppAction>
  ): void {
    customLogger(`${UPDATE_NOTIFICATION_DECISION} middleware is used`)
    this.webPushService.handleNotificationDecision(action.payload).then((res) => {
      customLogger('Subscription ok')
      next(createActionNotificationDecision(res))
    }).catch(err => {
      customLogger(err)
      next(createActionNotificationDecision(NotificationDecision.DENIED))
    })
  }

  handleDefault (
    action: AppAction,
    store: MiddlewareStoreApi<AppState, AppAction>,
    next: Next<AppAction>
  ): void {
    customLogger('UI middleware is ignored. ste')
    customLogger('State: ', store.getState())
    next(action)
  }
}

const settingService = createSettingService()
const webPushService = createWebPushService()
const uiReducerMiddleware = new UIReducerMiddleware(
  settingService,
  webPushService
)

export const uiMiddleware = uiReducerMiddleware.middleware
