export interface DummyState {
  dummy: string
}

export type DummyAction =
  | { type: 'UPDATE_DUMMY', payload: string }
  | { type: 'DUMMY_DUMMY' }

export const initDummyState: DummyState = {
  dummy: 'dummy-state'
}

export function dummyReducer (state: DummyState, action: DummyAction): DummyState {
  switch (action.type) {
    case 'UPDATE_DUMMY':
      return {
        ...state,
        dummy: action.payload
      }
    case 'DUMMY_DUMMY':
      return {
        ...state
      }
    default:
      return state
  }
}
