import { PageTransition, type PageTransitionType } from '@martelleur.online/webcraft'

import type { WebStorageRepository } from '../repository/web-storage-repository'
import { NotificationDecision } from '@constants'
import { ColorThemeName, type ColorThemeNameType } from '@style/theme-colors'
import { FontThemeName, type FontThemeNameType } from '@style/theme-fonts'
import { isNotificationAPISupported } from '@utility'

export class WebStorage implements WebStorageRepository {
  private readonly KEY_NOTIFICATION_DECISION = 'notificationDecision'
  private readonly KEY_COLOR_THEME_NAME = 'colorThemeName'
  private readonly KEY_FONT_THEME_NAME = 'colorFontName'
  private readonly KEY_PAGE_TRANSITION = 'pageTransition'

  constructor () {
    const currentDecision = this.getNotificationDecision()
    localStorage.setItem(this.KEY_NOTIFICATION_DECISION, currentDecision)
  }

  getColorThemeName (): ColorThemeNameType {
    const storedValue = localStorage.getItem(this.KEY_COLOR_THEME_NAME) ?? ''
    const validValues = Object.values(ColorThemeName)
    const isValidValue = this._isValidValue(storedValue, validValues)
    return isValidValue ? storedValue : ColorThemeName.DEFAULT
  }

  _isValidValue<T>(value: any, validValues: T[]): value is T {
    return validValues.includes(value)
  }

  setColorThemeName (themeName: ColorThemeNameType): void {
    localStorage.setItem(this.KEY_COLOR_THEME_NAME, themeName)
  }

  getFontThemeName (): FontThemeNameType {
    const storedValue = localStorage.getItem(this.KEY_FONT_THEME_NAME) ?? ''
    const validValues = Object.values(FontThemeName)
    const isValidValue = this._isValidValue(storedValue, validValues)
    return isValidValue ? storedValue : FontThemeName.DEFAULT
  }

  setFontThemeName (themeName: FontThemeNameType): void {
    localStorage.setItem(this.KEY_FONT_THEME_NAME, themeName)
  }

  getPageTransition (): PageTransitionType {
    const storedValue = localStorage.getItem(this.KEY_PAGE_TRANSITION) ?? ''
    const validValues = Object.values(PageTransition)
    const isValidValue = this._isValidValue(storedValue, validValues)
    return isValidValue ? storedValue : PageTransition.DEFAULT
  }

  setPageTransition (pageTransition: PageTransitionType): void {
    localStorage.setItem(this.KEY_PAGE_TRANSITION, pageTransition)
  }

  setNotificationDecisionToDefault (): void {
    if (isNotificationAPISupported()) {
      localStorage.setItem(this.KEY_NOTIFICATION_DECISION, Notification.permission)
    }
  }

  setNotificationDecisionToGranted (): void {
    if (isNotificationAPISupported()) {
      localStorage.setItem(this.KEY_NOTIFICATION_DECISION, NotificationDecision.GRANTED)
    }
  }

  setNotificationDecisionToDenied (): void {
    if (isNotificationAPISupported()) {
      localStorage.setItem(this.KEY_NOTIFICATION_DECISION, NotificationDecision.DENIED)
    }
  }

  getNotificationDecision (): NotificationPermission {
    const storedValue = localStorage.getItem(this.KEY_NOTIFICATION_DECISION) ?? ''
    const validValues = Object.values(NotificationDecision)
    const isValidValue = isNotificationAPISupported() && this._isValidValue(storedValue, validValues)
    return isValidValue ? storedValue : NotificationDecision.DEFAULT
  }
}
