import type { WebPushRepository } from '../repository/web-push-repository'
import { customLogger } from '@utility'
import { getApiEndpoint } from '@config'

export class WebPushAPI implements WebPushRepository {
  async postSubscription (subscription: PushSubscription): Promise<void> {
    const apinEndpoint = getApiEndpoint()
    const res = await fetch(`${apinEndpoint}/webpush/subscribe`, {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(subscription)
    })
    customLogger('Res: ', res)
  }
}
