import { appDefaultContainerStyle } from '@style/app-container-style'

const appealingButtonTemplate = document.createElement('template')
appealingButtonTemplate.innerHTML = `
<button 
  id="appealing-button"
  data-test="appealing-button">
</button>
${appDefaultContainerStyle}
<style>
#appealing-button {
    color: #FFFFFF;
    padding: 6px 12px;
    font-size: 16px;
    border: none;
    border-radius: 25px;
    box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
    font-weight: bold;
    transition: background-color 0.3s;
    cursor: pointer;
    margin: 20px 0;
    text-align: center;
    background-color: var(--BACKGROUND_ACCENT);
    font-family: var(--FONT_PRIMARY);
}

#appealing-button:hover {
    background: var(--BACKGROUND_ACCENT_HOVER);
}

#appealing-button:focus {
    outline: none;
    box-shadow: 0 0
}
</style>
`

export default appealingButtonTemplate
