import { DefineElement, NamedHTMLElement, assertElementExist } from '@martelleur.online/webcraft'

import appealingButtonTemplate from './AppealingButton.template'
import { customLogger } from '@utility'

export const nameAppealingButtonElement = 'appealing-button'

@DefineElement(nameAppealingButtonElement)
export class AppealingButton extends NamedHTMLElement {
  private readonly _button
  constructor () {
    super()
    const root = this.attachShadow({ mode: 'open' })
    root.appendChild(appealingButtonTemplate.content.cloneNode(true))
    this._button = assertElementExist(root.querySelector<HTMLButtonElement>('#appealing-button'))
  }

  static get observedAttributes (): string[] {
    return ['class', 'data-text']
  }

  attributeChangedCallback (name: string, oldValue: string, newValue: string): void {
    switch (name) {
      case 'class':
        this._button.classList.add(newValue)
        break
      case 'data-text':
        this._button.textContent = newValue
        break
      default:
        customLogger(`Not a valid attribute, old value:
          Old attribute value', ${oldValue}
          New attribute value', ${newValue}`)
    }
  }
}
