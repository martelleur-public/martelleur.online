import { DefineCustomElement, assertElementExist } from '@martelleur.online/webcraft'

import { colorPaletteTemplate } from './ColorPalette.template'
import { customLogger } from '@utility'
import {
  type ColorMode,
  ColorThemeName,
  LightMode,
  DarkMode,
  WarmMode,
  RetroMode,
  ColdMode
} from '@style/theme-colors'

export const nameColorPalette = 'color-palette'

@DefineCustomElement(nameColorPalette)
export class ColorPalette extends HTMLElement {
  private readonly _lightExtra: HTMLDivElement
  private readonly _light: HTMLDivElement
  private readonly _medium: HTMLDivElement
  private readonly _dark: HTMLDivElement

  constructor () {
    super()
    const root = this.attachShadow({ mode: 'open' })
    root.appendChild(colorPaletteTemplate.content.cloneNode(true))
    this._lightExtra = assertElementExist(root.querySelector<HTMLDivElement>('#light-x'))
    this._light = assertElementExist(root.querySelector<HTMLDivElement>('#light'))
    this._medium = assertElementExist(root.querySelector<HTMLDivElement>('#medium'))
    this._dark = assertElementExist(root.querySelector<HTMLDivElement>('#dark'))
  }

  static get observedAttributes (): string[] {
    return ['data-color-theme']
  }

  attributeChangedCallback (name: string, oldValue: string, newValue: string): void {
    switch (name) {
      case 'data-color-theme':
        this._updateColorPalettes(newValue)
        break
      default:
        customLogger(`Not a valid attribute, old value:
          Old attribute value', ${oldValue}
          New attribute value', ${newValue}`)
    }
  }

  private _updateColorPalettes (colorTheme: string): void {
    switch (colorTheme) {
      case ColorThemeName.LIGHT:
        this._updateColorPalette(LightMode)
        break
      case ColorThemeName.DARK:
        this._updateColorPalette(DarkMode)
        break
      case ColorThemeName.WARM:
        this._updateColorPalette(WarmMode)
        break
      case ColorThemeName.COLD:
        this._updateColorPalette(ColdMode)
        break
      case ColorThemeName.RETRO:
        this._updateColorPalette(RetroMode)
        break
      default:
        customLogger('Not a color theme')
    }
  }

  private _updateColorPalette (mode: ColorMode): void {
    this._lightExtra.style.background = mode.BACKGROUND_LIGHT_X
    this._light.style.background = mode.BACKGROUND_LIGHT
    this._medium.style.background = mode.BACKGROUND_MEDIUM
    this._dark.style.background = mode.BACKGROUND_DARK
    this._lightExtra.setAttribute('title', mode.BACKGROUND_LIGHT_X)
    this._light.setAttribute('title', mode.BACKGROUND_LIGHT)
    this._medium.setAttribute('title', mode.BACKGROUND_MEDIUM)
    this._dark.setAttribute('title', mode.BACKGROUND_DARK)
  }
}
