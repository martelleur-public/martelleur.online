export const colorPaletteTemplate = document.createElement('template')

colorPaletteTemplate.innerHTML = `
<div class="color-palette">
    <div id="light-x"></div>
    <div id="light"></div>
    <div id="medium"></div>
    <div id="dark"></div>
</div>
</div>
<style>
.color-palette {
    font-size: 0;
    display: inline-block;
    height: 24px;
    border: 1px solid black;
    margin: 0;
    padding: 0;
}

#light-x, #light, #medium, #dark {
    font-size: 0;
    display: inline-block;
    width: 30px;
    height: 100%;
}

#light-x {
    background: var(--BACKGROUND_LIGHT_X);
}

#light {
    background: var(--BACKGROUND_LIGHT);
}

#medium {
    background: var(--BACKGROUND_MEDIUM);
}

#dark {
    background: var(--BACKGROUND_DARK);
}
</style>
`
