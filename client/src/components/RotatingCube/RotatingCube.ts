import * as THREE from 'three'
import { DefineCustomElement } from '@martelleur.online/webcraft'

import {
  type ColorThemeNameType,
  getCurrentColors
} from '@style/theme-colors'

/**
 * The element is initiated from the element that use
 * it because  of rendering timing issues.
 *
 * @TODO Should be further investigated to see if this is actually true when
 * nesting components that use three.js library. Could very well be
 * inheritance size problem.
 */
export const nameRotatingCube = 'rotating-cube'

@DefineCustomElement(nameRotatingCube)
export class RotatingCube extends HTMLElement {
  private readonly _root: ShadowRoot
  private scene!: THREE.Scene
  private camera!: THREE.PerspectiveCamera
  private renderer!: THREE.WebGLRenderer
  private cube!: THREE.Mesh

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    this._bindEventHandlersToThis()
  }

  private _bindEventHandlersToThis (): void {
    this._startAnimation = this._startAnimation.bind(this)
  }

  public init (width: number, height: number, currentColor: ColorThemeNameType): void {
    this._initUI(width, height, currentColor)
    this._startAnimation()
  }

  private _initUI (width: number, height: number, currentColorName: ColorThemeNameType): void {
    // Create scene, camera, and renderer
    this.scene = new THREE.Scene()
    this.camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000)
    this.renderer = new THREE.WebGLRenderer({ antialias: true })
    this.renderer.setSize(width, height)
    this.renderer.shadowMap.enabled = true // Enable shadow mapping
    this._root.appendChild(this.renderer.domElement)

    // Scene background color
    const currentColors = getCurrentColors(currentColorName)
    this.scene.background = new THREE.Color(currentColors.BACKGROUND_LIGHT)

    // Add a cube
    const geometry = new THREE.BoxGeometry(2, 2, 2) // Cube size
    const material = new THREE.MeshStandardMaterial({ color: currentColors.BACKGROUND_DARK })
    this.cube = new THREE.Mesh(geometry, material)
    this.cube.castShadow = true // Enable cube to cast shadows
    this.scene.add(this.cube)

    // Add a directional light
    const directionalLight = new THREE.DirectionalLight(0xffffff, 1)
    directionalLight.position.set(5, 5, 5)
    directionalLight.castShadow = true // Enable light to cast shadows
    this.scene.add(directionalLight)

    // Optional: Add an ambient light for softer light
    const ambientLight = new THREE.AmbientLight(0x404040) // Soft white light
    this.scene.add(ambientLight)

    this.camera.position.z = 5
  }

  private _startAnimation (): void {
    window.requestAnimationFrame(this._startAnimation)
    // Rotate the cube
    this.cube.rotation.x += 0.01
    this.cube.rotation.y += 0.01
    // Render the scene
    this.renderer.render(this.scene, this.camera)
  }

  public updateSize (width: number, height: number): void {
    this.camera.aspect = width / height
    this.camera.updateProjectionMatrix()
    this.renderer.setSize(width, height)
  }

  public updateColors (newColorName: ColorThemeNameType): void {
    const newColors = getCurrentColors(newColorName)
    this.scene.background = new THREE.Color(newColors.BACKGROUND_LIGHT)
    const material = new THREE.MeshStandardMaterial({ color: newColors.BACKGROUND_DARK })
    this.cube.material = material
  }
}
