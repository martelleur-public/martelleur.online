import '@martelleur.online/webcraft'

import { appDefaultContainerStyle } from '@style/app-container-style'

const imageLinkTemplate = document.createElement('template')
imageLinkTemplate.innerHTML = `
<a
    id="image-link"
    title="A link title"
    href=""
    target="_blank"
    data-test="image-link">
    <span
        id="text-link"
        data-test="text-link">
        <new-tab-icon-white></new-tab-icon-white>
    </span>
</a>
${appDefaultContainerStyle}
<style>
:host {
    width: 100%;
    height: 100%;
}

#image-link {
    background-image: url('../../assets/sw.jpg');
}

#image-link {
    display: flex;
    font-size: 1.4em;
    font-weight: bold;
    color: var(--primary-text);
    text-decoration: none;
    text-decoration: none;
    justify-content: end;
    align-items: end;
    width: 100%;
    height: 100%;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    background-color: var(--primary-background-dark-extra);
    opacity: 0.9;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.7);
    transition: opacity 0.3s ease-in-out, box-shadow 0.3s ease-in-out;
}

#image-link:hover {
    opacity: 1;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 1);
}

#text-link {
    padding: 5px;
}

new-tab-icon-white {
    display: inline-block;
    vertical-align: middle;
    margin-bottom: 5px;
}
</style>
`
export default imageLinkTemplate
