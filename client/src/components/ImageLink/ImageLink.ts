import {
  DefineElement,
  NamedHTMLElement,
  assertElementExist
} from '@martelleur.online/webcraft'

import imageGitHub from './assets/github.jpeg'
import imageGitLab from './assets/gitlab.jpeg'
import imageGitLab2 from './assets/gitlab_2.jpeg'
import imageMemory from './assets/memory.jpeg'
import imageMinesweeper from './assets/minesweeper.jpeg'
import imageNpmLibrary from './assets/npm_library.jpeg'
import imageMyChat from './assets/mychat.png'
import imageLinkTemplate from './ImageLink.template'
import { customLogger } from '@utility'

type ImageKey =
  'imageGitHub' |
  'imageGitLab' |
  'imageGitLab2' |
  'imageMemory' |
  'imageMinesweeper' |
  'imageMyChat' |
  'imageNpmLibrary'

export const imageMap: Record<ImageKey, string> = {
  imageGitHub,
  imageGitLab,
  imageGitLab2,
  imageMemory,
  imageMinesweeper,
  imageMyChat,
  imageNpmLibrary
}

export const nameImageLinkElement = 'image-link'

@DefineElement(nameImageLinkElement)
export class ImageLink extends NamedHTMLElement {
  private readonly _link: HTMLAnchorElement
  private readonly _textContent: HTMLSpanElement

  constructor () {
    super()
    const root = this.attachShadow({ mode: 'open' })
    root.appendChild(imageLinkTemplate.content.cloneNode(true))
    this._link = assertElementExist(root.querySelector<HTMLAnchorElement>('#image-link'))
    this._textContent = assertElementExist(root.querySelector<HTMLAnchorElement>('#text-link'))
  }

  connectedCallback (): void {
    this._link.addEventListener('click', event => {
      customLogger('event.target', event.target)
    })
  }

  static get observedAttributes (): string[] {
    return ['title', 'href', 'data-text', 'data-image']
  }

  attributeChangedCallback (name: string, oldValue: string, newValue: string): void {
    switch (name) {
      case 'title':
        this._link.setAttribute('title', newValue)
        break
      case 'href':
        this._link.setAttribute('href', newValue)
        break
      case 'data-text':
        const textNode = document.createTextNode(newValue)
        this._textContent.insertBefore(textNode, this._textContent.firstChild)
        break
      case 'data-image':
        if (this._isImageKey(newValue)) {
          this._link.style.backgroundImage = `url(${imageMap[newValue]})`
        }
        break
      default:
        customLogger(`Not a valid attribute, old value:
          Old attribute value', ${oldValue}
          New attribute value', ${newValue}`)
    }
  }

  private _isImageKey (key: string): key is ImageKey {
    return Object.prototype.hasOwnProperty.call(imageMap, key)
  }
}
