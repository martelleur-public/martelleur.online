import type { WebPushRepository } from './web-push-repository'
import type { WebStorageRepository } from './web-storage-repository'

import { WebPushAPI, WebStorage } from '@infrastructure'

export function createWebPushRepository (): WebPushRepository {
  const webPushRepository: WebPushRepository = new WebPushAPI()
  return webPushRepository
}

export function createWebStorageRepository (): WebStorageRepository {
  const webStorageRepository: WebStorageRepository = new WebStorage()
  return webStorageRepository
}
