export interface WebPushRepository {
  postSubscription: (subscription: PushSubscription) => Promise<void>
}
