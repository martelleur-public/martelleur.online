import type { PageTransitionType } from '@martelleur.online/webcraft'

import type { ColorThemeNameType } from '@style/theme-colors'
import type { FontThemeNameType } from '@style/theme-fonts'

export interface WebStorageRepository {
  setNotificationDecisionToDefault: () => void
  setNotificationDecisionToGranted: () => void
  setNotificationDecisionToDenied: () => void
  getNotificationDecision: () => NotificationPermission
  getColorThemeName: () => ColorThemeNameType
  setColorThemeName: (themeName: ColorThemeNameType) => void
  getFontThemeName: () => FontThemeNameType
  setFontThemeName: (themeName: FontThemeNameType) => void
  getPageTransition: () => PageTransitionType
  setPageTransition: (pageTransition: PageTransitionType) => void
}
