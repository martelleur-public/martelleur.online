import { customLogger, productionLogger } from '@utility'
import { UpdateDialog } from '@features'
import { getEnvName } from '@config'
import type { MartelleurOnline } from '@app'
import {
  ConfirmUpdateEvent,
  type UserActivityEventDetail,
  handleCustomEvent,
  UserActivityEvent
} from '@event'

/**
 * @TODO Test service worker solution track user activity.
 */
let isUserActive = false
const martelleurElement = document.querySelector('martelleur-online') as MartelleurOnline
const removeUserActivityListener = handleCustomEvent<UserActivityEventDetail>(
  UserActivityEvent.eventName, martelleurElement, (detail) => {
    isUserActive = detail.isUserActive
  }
)

if (getEnvName() === 'production' && 'serviceWorker' in navigator) {
  // Register service worker.
  window.addEventListener('load', () => {
    const workerURL = new URL('./sw-cache-first.js', import.meta.url)
    navigator.serviceWorker
      .register(workerURL, { type: 'module', scope: '/' })
      .then(handleSuccessfulRegistration)
      .catch(handleFailedRegistration)
  })

  function handleSuccessfulRegistration (registration: ServiceWorkerRegistration): void {
    customLogger('Service Worker registered with scope:', registration.scope)

    // Used to reload the page when a new service worker takes control.
    navigator.serviceWorker.addEventListener('controllerchange', () => {
      window.location.reload()
    })

    // Used to return and by so ignore updates if there's no service
    // worker controlling the page, e.g., for a first time visit.
    if (navigator.serviceWorker.controller == null) {
      return
    }

    // Used to prompt a user to update if a new service worker
    // waiting ( new version have been installed but not activated).
    if (registration.waiting != null) {
      promptUserToRefresh(registration.waiting)
    }

    // Used to track service worker installing progress.
    if (registration.installing != null) {
      trackWorkerProgress(registration.installing)
    }

    // Used to track service worker installing progress if a new
    // service worker appear.
    registration.addEventListener('updatefound', () => {
      if (registration.installing != null) {
        trackWorkerProgress(registration.installing)
      }
    })
  }

  // Used to handle registration failure.
  function handleFailedRegistration (err: Error): void {
    customLogger('Service Worker registration failed:', err)
  }

  // Used to tack prompt a user to update to new version if the
  // service worker state is installed.
  function trackWorkerProgress (worker: ServiceWorker): void {
    productionLogger('Active user: ', isUserActive)
    productionLogger('Previous service worker: ', navigator.serviceWorker.controller)
    worker.addEventListener('statechange', () => {
      if (worker.state === 'installed' &&
        isUserActive &&
        navigator.serviceWorker.controller != null) {
        productionLogger('Ask user to update service worker')
        promptUserToRefresh(worker)
        removeUserActivityListener()
      } else if (worker.state === 'installed' &&
        !isUserActive &&
        navigator.serviceWorker.controller != null) {
        productionLogger('Update service worker')
        updateWithoutPrompt(worker)
        removeUserActivityListener()
      } else {
        productionLogger('State service worker: ', worker.state)
      }
    })
  }

  // Used to prompt user to update to new version.
  function promptUserToRefresh (worker: ServiceWorker): void {
    const dialog = new UpdateDialog()
    document.body.appendChild(dialog)
    dialog.addEventListener(ConfirmUpdateEvent.eventName, () => {
      worker.postMessage({ action: 'skipWaiting' })
    })
    dialog.open()
  }

  // Used to update without prompt.
  function updateWithoutPrompt (worker: ServiceWorker): void {
    worker.postMessage({ action: 'skipWaiting' })
  }
}
