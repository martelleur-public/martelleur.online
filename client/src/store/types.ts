import type {
  DummyAction,
  DummyState,
  UiAction,
  UiState
} from '@state'

export type AppAction = UiAction | DummyAction

export interface AppState {
  ui: UiState
  dummy: DummyState
}

/**
@TODO if extending state use following approach

1. Union type of the actions
type Action = UiAction | ProfileAction;

2. Update state with all individual states
interface State {
  ui: UiState;
  profile: ProfileState;
}

3. Explicitly define the reucer types in combineReducers
const rootReducer: Reducer<State, Action> = combineReducers({
  ui: uiReducer: as Reducer<UiState, Action>,
  profile: profileReducer: as Reducer<DummyState, Action>,
});

4. Create the store
const store = new Store<State, Action>(
  rootReducer,
  { ui: initUiState, profile: initProfileState },
  [loggerMiddleware]
);
*/
