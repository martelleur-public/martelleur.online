import { type Middleware, customLogger } from '@martelleur.online/webcraft'

import type { AppAction, AppState } from './types'

export const loggerMiddleware:
Middleware<AppState, AppAction> = store => next => action => {
  customLogger('dispatching', action)
  customLogger('Store state', store.getState())
  next(action)
}
