import {
  createStore,
  combineReducers,
  createStoreSubscriber,
  createStoreProvider,
  createReactiveStoreSubscriber,
  type Reducer
} from '@martelleur.online/webcraft'

import type { AppAction, AppState } from './types'
import { loggerMiddleware } from './logger-middleware'
import {
  type DummyState,
  type UiState,
  dummyReducer,
  initDummyState,
  initUiState,
  uiReducer,
  uiMiddleware
} from '@state'

const rootReducer: Reducer<AppState, AppAction> = combineReducers({
  ui: uiReducer as Reducer<UiState, AppAction>,
  dummy: dummyReducer as Reducer<DummyState, AppAction>
})

const appInitState: AppState = {
  ui: initUiState,
  dummy: initDummyState
}

const appMiddlewares = [
  loggerMiddleware,
  uiMiddleware
]

const store = createStore<AppState, AppAction>(
  rootReducer,
  appInitState,
  appMiddlewares
)

export const StoreSubscriber = createStoreSubscriber<AppState, AppAction>(store)
export const StoreProvider = createStoreProvider<AppState, AppAction>(store)
export const ReactiveStoreSubscriber = createReactiveStoreSubscriber<AppState, AppAction>(store)
export type {
  IReactiveState,
  Middleware,
  MiddlewareStoreApi,
  Next,
  IStore,
  IReactiveHTMLElement
} from '@martelleur.online/webcraft'
export type { AppAction, AppState } from './types'
