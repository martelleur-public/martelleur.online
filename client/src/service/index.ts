import { customLogger } from '@martelleur.online/webcraft'

import { createSettingService, createWebPushService } from './service-factory'

export function setupServices (): void {
  const webPushService = createWebPushService()
  if (webPushService == null) { customLogger('service not started') }
  const settingService = createSettingService()
  if (settingService == null) { customLogger('service not started') }
}
