import { getVapidNotificationPublickKey } from '@config'
import {
  createWebPushRepository,
  createWebStorageRepository
} from '@repository'
import { WebPushService } from './web-push-service'
import { SettingService } from './setting-service'

export function createWebPushService (): WebPushService {
  const webPushRepository = createWebPushRepository()
  const webStorageRepository = createWebStorageRepository()
  const service = new WebPushService(
    webPushRepository,
    webStorageRepository,
    getVapidNotificationPublickKey()
  )
  return service
}

export function createSettingService (): SettingService {
  const webStorageRepository = createWebStorageRepository()
  const service = new SettingService(webStorageRepository)
  return service
}
