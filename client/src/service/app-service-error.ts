import { customLogger } from '@utility'

const errorName = 'App Service Error'

export class AppServiceError extends Error {
  constructor (message: string) {
    super(`${errorName}: ${message}`)
    this.name = errorName
    customLogger(`${errorName}: ${message}`)
  }
}
