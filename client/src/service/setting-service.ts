import type { PageTransitionType } from '@martelleur.online/webcraft'

import type { WebStorageRepository } from '@repository'
import type { ColorThemeNameType } from '@style/theme-colors'
import type { FontThemeNameType } from '@style/theme-fonts'

export class SettingService {
  constructor (
    private readonly _webStorageRepository: WebStorageRepository
  ) {}

  updateColorThemeName (themeName: ColorThemeNameType): ColorThemeNameType {
    this._webStorageRepository.setColorThemeName(themeName)
    return this._webStorageRepository.getColorThemeName()
  }

  updateFontThemeName (themeName: FontThemeNameType): FontThemeNameType {
    this._webStorageRepository.setFontThemeName(themeName)
    return this._webStorageRepository.getFontThemeName()
  }

  updatePageTransition (pageTransition: PageTransitionType): PageTransitionType {
    this._webStorageRepository.setPageTransition(pageTransition)
    return this._webStorageRepository.getPageTransition()
  }

  getNotificationDecision (): NotificationPermission {
    return this._webStorageRepository.getNotificationDecision()
  }

  getColorThemeName (): ColorThemeNameType {
    return this._webStorageRepository.getColorThemeName()
  }

  getFontThemeName (): FontThemeNameType {
    return this._webStorageRepository.getFontThemeName()
  }

  getPageTransition (): PageTransitionType {
    return this._webStorageRepository.getPageTransition()
  }
}
