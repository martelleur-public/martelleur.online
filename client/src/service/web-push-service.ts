import type {
  WebPushRepository,
  WebStorageRepository
} from '@repository'
import { NotificationDecision } from '@constants'
import { isNotificationAPISupported, customLogger } from '@utility'
import { AppServiceError } from './app-service-error'

export class WebPushService {
  constructor (
    private readonly _webPushRepository: WebPushRepository,
    private readonly _webStorageRepository: WebStorageRepository,
    private readonly _webPushPublicKey: string) {
    this._bindThisToClassMethods()
  }

  _bindThisToClassMethods (): void {
    this.handleNotificationDecision = this.handleNotificationDecision.bind(this)
    this._subscribeUser = this._subscribeUser.bind(this)
  }

  /**
   * @throws {AppServiceError}
   */
  async handleNotificationDecision (descision: string): Promise<NotificationPermission> {
    this._checkNotificationAPISupport()
    this._checkUserDenyNotification(descision)

    switch (Notification.permission) {
      case NotificationDecision.DEFAULT:
        const notificationPermission = await Notification.requestPermission()
        return notificationPermission === NotificationDecision.GRANTED
          ? await this._subscribeUser()
          : NotificationDecision.DENIED
      case NotificationDecision.GRANTED:
        return await this._subscribeUser()
      default:
        throw new AppServiceError(`Notification subscription permission is ${Notification.permission}`)
    }
  }

  _checkNotificationAPISupport (): void {
    if (!isNotificationAPISupported()) {
      const msg = 'Notification subscription is not suported'
      throw new AppServiceError(msg)
    }
  }

  _checkUserDenyNotification (descision: string): void {
    if (descision === NotificationDecision.DENIED) {
      const msg = 'Notification subscription is denied'
      throw new AppServiceError(msg)
    }
  }

  async _subscribeUser (): Promise<NotificationPermission> {
    if (!(await this._isServiceWorkerRegistered())) {
      throw new AppServiceError('Notification subscription failed, service worker not registered')
    }
    const registration = await navigator.serviceWorker.ready
    const subscription = await this._subscribeToPushManager(registration)
    return await this._handleSubscription(subscription)
  }

  async _isServiceWorkerRegistered (): Promise<boolean> {
    try {
      const registration = await navigator.serviceWorker.getRegistration()
      const isRegistered = registration != null
      return isRegistered
    } catch (error) {
      customLogger('Error checking service worker registration:', error)
      return false
    }
  }

  async _subscribeToPushManager (registration: ServiceWorkerRegistration): Promise<PushSubscription> {
    return await registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: this._urlBase64ToUint8Array(this._webPushPublicKey)
    })
  }

  /**
   * @TODO Return result from repo.
   */
  async _handleSubscription (subscription: PushSubscription): Promise<NotificationPermission> {
    await this._webPushRepository.postSubscription(subscription)
    this._webStorageRepository.setNotificationDecisionToGranted()
    return this._webStorageRepository.getNotificationDecision()
  }

  /**
   * Used to convert a VAPID public key from a URL-safe base64 string
   * to a Uint8Array, which is required by the Push API for
   * subscribing a user to push notifications.
   */
  _urlBase64ToUint8Array (base64String: string): Uint8Array {
    const padding = '='.repeat((4 - base64String.length % 4) % 4)
    const base64 = (base64String + padding)
      .replace(/-/g, '+')
      .replace(/_/g, '/')
    const rawData = window.atob(base64)
    const outputArray = new Uint8Array(rawData.length)
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i)
    }
    return outputArray
  }
}
