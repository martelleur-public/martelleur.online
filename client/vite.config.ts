import { defineConfig } from 'vite'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    preserveSymlinks: true,
    alias: {
      "@martelleur.online/webcraft": path.resolve(__dirname, "../lib/webcraft/dist/esm/src"),
      "@app": path.resolve(__dirname, "./src/app"),
      "@components": path.resolve(__dirname, "./src/components"),
      "@config": path.resolve(__dirname, "./src/config"),
      "@constants": path.resolve(__dirname, "./src/constants"),
      "@features": path.resolve(__dirname, "./src/features"),
      "@event": path.resolve(__dirname, "./src/event"),
      "@icons": path.resolve(__dirname, "./src/icons"),
      "@infrastructure": path.resolve(__dirname, "./src/infrastructure"),
      "@layouts": path.resolve(__dirname, "./src/layouts"),
      "@lib": path.resolve(__dirname, "./src/lib"),
      "@pages": path.resolve(__dirname, "./src/pages"),
      "@repository": path.resolve(__dirname, "./src/repository"),
      "@router": path.resolve(__dirname, "./src/router"),
      "@service": path.resolve(__dirname, "./src/service"),
      "@state": path.resolve(__dirname, "./src/state"),
      "@store": path.resolve(__dirname, "./src/store"),
      "@style": path.resolve(__dirname, "./src/style"),
      "@sw": path.resolve(__dirname, "./src/sw"),
      "@utility": path.resolve(__dirname, "./src/utility"),
    }
  },
})
