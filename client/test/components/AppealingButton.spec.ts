import { AppealingButton, nameAppealingButtonElement } from '@components/AppealingButton/AppealingButton'
import { assertElementExist } from '@utility/html'

describe('AppealingButton', () => {
  let appealingButton: AppealingButton
  let button: HTMLButtonElement

  beforeAll(() => {
    if (customElements.get('appealing-button') == null) {
      window.customElements.define('appealing-button', AppealingButton)
    }
  })

  beforeEach(async () => {
    appealingButton = document.createElement('appealing-button') as AppealingButton
    document.body.appendChild(appealingButton)
    if (appealingButton == null || appealingButton.shadowRoot == null) {
      throw new Error('AppealingButton element or shadowroot is null')
    }
    button = assertElementExist(appealingButton.shadowRoot.querySelector<HTMLButtonElement>(('[data-test="appealing-button"]')))
  })

  afterEach(() => {
    document.body.removeChild(appealingButton)
  })

  it('should be defined as a custom element appealing-button', () => {
    expect(nameAppealingButtonElement).toEqual('appealing-button')
    expect(customElements.get(nameAppealingButtonElement)).toBe(AppealingButton)
  })

  it('should set text content to the button', async () => {
    appealingButton.setAttribute('data-test', 'appealing-button')
    appealingButton.setAttribute('data-text', 'Test Content')
    expect(button.textContent).toBe('Test Content')
  })

  it('should add class to the button', async () => {
    appealingButton.setAttribute('data-test', 'appealing-button')
    appealingButton.setAttribute('class', 'test-class')
    appealingButton.setAttribute('data-text', 'Test Content')
    expect(button).toHaveClass('test-class')
  })
})
