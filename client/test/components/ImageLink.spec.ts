import {
  ImageLink,
  nameImageLinkElement,
  imageMap
} from '@components/ImageLink/ImageLink'
import { assertElementExist } from '@utility/html'

describe('ImageLink Web Component', () => {
  let imageLink: ImageLink
  let link: HTMLAnchorElement
  let textContent: HTMLSpanElement

  beforeAll(() => {
    if (window.customElements.get(nameImageLinkElement) == null) {
      window.customElements.define(nameImageLinkElement, ImageLink)
    }
  })

  beforeEach(async () => {
    imageLink = document.createElement(nameImageLinkElement) as ImageLink
    document.body.appendChild(imageLink)
    if (imageLink == null || imageLink.shadowRoot == null) {
      throw new Error('AppIntro element or shadowroot is null')
    }
    link = assertElementExist(imageLink.shadowRoot.querySelector('[data-test="image-link"]'))
    textContent = assertElementExist(imageLink.shadowRoot.querySelector('[data-test="text-link"]'))
  })

  it('should be defined as a custom element image-link', () => {
    expect(nameImageLinkElement).toEqual('image-link')
    expect(customElements.get(nameImageLinkElement)).toBe(ImageLink)
  })

  it('should properly set title attribute', () => {
    const newTitle = 'New Title'
    imageLink.setAttribute('title', newTitle)
    expect(link.getAttribute('title')).toBe(newTitle)
  })

  it('should properly set href attribute', () => {
    const newHref = 'https://example.com'
    imageLink.setAttribute('href', newHref)
    expect(link.getAttribute('href')).toBe(newHref)
  })

  it('should properly set data-text attribute', () => {
    const newText = 'New Text Content'
    imageLink.setAttribute('data-text', newText)
    expect(textContent.textContent).toContain(newText)
  })

  it('should properly set data-image attribute', () => {
    const imageKey = 'imageGitHub'
    imageLink.setAttribute('data-image', imageKey)
    const expectedBackgroundImage = `url(${imageMap[imageKey]})`
    expect(link.style.backgroundImage).toBe(expectedBackgroundImage)
  })

  it('should ignore invalid data-image attribute', () => {
    const invalidImageKey = 'invalidImageKey'
    imageLink.setAttribute('data-image', invalidImageKey)
    const initialBackgroundImage = link.style.backgroundImage
    expect(link.style.backgroundImage).toBe(initialBackgroundImage)
  })
})
