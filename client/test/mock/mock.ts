import type { AppAction, AppState, IStore } from '@store'

export const mockWebPushRepository = {
  postSubscription: jest.fn()
}

export const mockWebStorageRepository = {
  setNotificationDecisionToDefault: jest.fn(),
  setNotificationDecisionToGranted: jest.fn(),
  setNotificationDecisionToDenied: jest.fn(),
  getNotificationDecision: jest.fn(),
  getColorThemeName: jest.fn(),
  setColorThemeName: jest.fn(),
  getFontThemeName: jest.fn(),
  setFontThemeName: jest.fn(),
  getPageTransition: jest.fn(),
  setPageTransition: jest.fn()
}

export const mockStore = {
  dispatch: jest.fn(),
  getState: jest.fn(),
  subscribe: jest.fn()
} as unknown as IStore<AppState, AppAction>

export const mockPushSubscription: PushSubscription = {
  endpoint: 'https://example.com/push-endpoint',
  expirationTime: null,
  options: {
    applicationServerKey: new Uint8Array(),
    userVisibleOnly: true
  },
  getKey: () => new Uint8Array(),
  toJSON: () => ({ /* ... JSON representation ... */ })
} as unknown as PushSubscription
