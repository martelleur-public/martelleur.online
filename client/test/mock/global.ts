import { NotificationDecision } from '@constants'

global.Notification = {
  permission: 'default',
  requestPermission: jest.fn(async () => {
    return await Promise.resolve(NotificationDecision.DEFAULT)
  })
} as any

jest.mock('@config', () => ({
  getEnvName: () => 'MOCK_ENV',
  getAppVersion: () => 'APP_MOCK_VERSION',
  getBaseURL: () => 'http://mock',
  getApiEndpoint: () => 'http://mock.api-endpoint',
  getVapidNotificationPublickKey: () => 'PUBLIC-MOCK-KEY'
}))

jest.mock('@martelleur.online/webcraft', () => {
  const originalModule = jest.requireActual('@martelleur.online/webcraft')
  return {
    ...originalModule,
    customLogger: jest.fn().mockImplementation(() => {})
  }
})

export class MockResizeObserver {
  observe = jest.fn()
  unobserve = jest.fn()
  disconnect = jest.fn()
}

global.ResizeObserver = MockResizeObserver
