import { NotificationDecision } from '@constants'
import { createActionNotificationDecision, createActionUpdateColorTheme, createActionUpdateFontTheme } from '@state'
import type { UiAction, UiState } from '@state/ui-reducer/types'
import { createActioUpdateIsSidebarOpen } from '@state/ui-reducer/ui-action-creators'
import { uiReducer } from '@state/ui-reducer/ui-reducer'
import { ColorThemeName } from '@style/theme-colors'
import { FontThemeName } from '@style/theme-fonts'

describe('uiReducer', () => {
  let initialState: UiState

  beforeEach(() => {
    initialState = {
      notifactionDecision: 'default',
      colorThemeName: 'dark',
      fontThemeName: 'modern-clean',
      sidebarIsOpen: false,
      pageTransition: 'dark'
    }
  })

  it('should handle update notifcation/subscription decision', () => {
    const uiState = uiReducer(initialState, createActionNotificationDecision('granted') as UiAction)
    expect(uiState.notifactionDecision).toBe(NotificationDecision.GRANTED)
  })

  it('should handle update color theme name', () => {
    const uiState = uiReducer(initialState, createActionUpdateColorTheme('retro') as UiAction)
    expect(uiState.colorThemeName).toBe(ColorThemeName.RETRO)
  })

  it('should handle update font theme name', () => {
    const uiState = uiReducer(initialState, createActionUpdateFontTheme('modern-clean') as UiAction)
    expect(uiState.fontThemeName).toBe(FontThemeName.MODERN_CLEAN)
  })

  it('should handle update status for sidebar', () => {
    const uiState = uiReducer(initialState, createActioUpdateIsSidebarOpen(true) as UiAction)
    expect(uiState.sidebarIsOpen).toBe(true)
  })
})
