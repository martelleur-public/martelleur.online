import { AppIntro } from '@app/components/AppIntro/AppIntro'
import { assertElementExist } from '@utility/html'

describe('AppIntro', () => {
  let appIntro: AppIntro
  let introWrapper: HTMLDivElement
  let introIcon: HTMLDivElement
  let introTitle: HTMLDivElement
  const elementName = (AppIntro as any).elementName

  beforeEach(async () => {
    appIntro = document.createElement(elementName)
    document.body.appendChild(appIntro)
    if (appIntro == null || appIntro.shadowRoot == null) {
      throw new Error('AppIntro element or shadowroot is null')
    }
    introWrapper = assertElementExist(appIntro.shadowRoot.querySelector('[data-test="intro-wrapper"]'))
    introIcon = assertElementExist(appIntro.shadowRoot.querySelector('[data-test="intro-icon"]'))
    introTitle = assertElementExist(appIntro.shadowRoot.querySelector('[data-test="intro-title"]'))
  })

  afterEach(() => {
    document.body.removeChild(appIntro)
  })

  it('should be defined as a custom element app-intro', () => {
    expect(elementName).toEqual('app-intro')
  })

  it('should render the intro wrapper div', () => {
    expect(introWrapper).toBeInTheDocument()
  })

  it('should render the intro icon div', () => {
    expect(introIcon).toBeInTheDocument()
  })

  it('should render the intro title', () => {
    expect(introTitle).toBeInTheDocument()
    expect(introTitle).toHaveTextContent('martelleur.online')
  })
})
