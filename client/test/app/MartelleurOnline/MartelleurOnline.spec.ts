import { fireEvent } from '@testing-library/dom'
import { assertElementExist } from '@martelleur.online/webcraft'

import {
  MartelleurOnline,
  nameMartelleurOnlineElement
} from '@app/components/MartelleurOnline/MartelleurOnline'
import type { AppIntro } from '@app/components/AppIntro/AppIntro'
import type { AppIdle } from '@app/components/AppIdle/AppIdle'
import type { AppLayout } from '@layouts'
import { activityEvents, appIdleTimeout } from '@app/components/constants'

describe('MartelleurOnline', () => {
  let martelleurOnline: MartelleurOnline
  let appIntro: AppIntro
  let appIdle: AppIdle
  let appLayout: AppLayout
  let addEventListenerMartelleurOnlineSpy: jest.SpyInstance
  let addEventListenerAppIntroSpy: jest.SpyInstance
  const elementName = (MartelleurOnline as any).elementName

  beforeEach(async () => {
    martelleurOnline = document.createElement(elementName) as MartelleurOnline
    if (martelleurOnline == null || martelleurOnline.shadowRoot == null) {
      throw new Error('MartelleurOnline element or shadowroot is null')
    }
    appIntro = assertElementExist(martelleurOnline.shadowRoot.querySelector('[data-test="app-intro"]'))
    appIdle = assertElementExist(martelleurOnline.shadowRoot.querySelector('[data-test="app-idle"]'))
    appLayout = assertElementExist(martelleurOnline.shadowRoot.querySelector('[data-test="app-layout"]'))
    addEventListenerAppIntroSpy = jest.spyOn(appIntro, 'addEventListener')
    addEventListenerMartelleurOnlineSpy = jest.spyOn(martelleurOnline, 'addEventListener')
    document.body.appendChild(martelleurOnline)
  })

  afterEach(() => {
    document.body.removeChild(martelleurOnline)
  })

  it('should be defined as a custom element martelleur-online', () => {
    expect(elementName).toEqual(nameMartelleurOnlineElement)
  })

  it('should render app-intro', () => {
    expect(appIntro).toBeInTheDocument()
  })

  it('should render app-layout', () => {
    expect(appLayout).toBeInTheDocument()
  })

  it('should render app-idle', () => {
    expect(appIdle).toBeInTheDocument()
  })

  it('should setup event listeners for activity events when connected', () => {
    activityEvents.forEach((eventName) => {
      expect(addEventListenerMartelleurOnlineSpy).toHaveBeenCalledWith(
        eventName, expect.any(Function)
      )
    })
  })

  it('should setup event listener for animation end event on app intro when connected', () => {
    expect(addEventListenerAppIntroSpy).toHaveBeenCalledWith(
      'animationend', expect.any(Function)
    )
  })

  it('should display app-layout when animation end for element app-intro', () => {
    fireEvent.animationEnd(appIntro)
    expect(appLayout.style.display).toBe('block')
  })

  it('should hide app-intro when animation end for element app-intro', () => {
    fireEvent.animationEnd(appIntro)
    expect(appIntro.style.display).toBe('none')
  })

  it('should define idle timeout to 20 minutes', () => {
    fireEvent.animationEnd(appIntro)
    const twentyMinutes = 60000 * 20
    expect(appIdleTimeout === twentyMinutes).toBeTruthy()
  })
})
