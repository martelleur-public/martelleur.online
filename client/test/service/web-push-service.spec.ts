import { waitFor } from '@testing-library/dom'

import { WebPushService } from '@service/web-push-service'
import { NotificationDecision } from '@constants'
import {
  mockWebPushRepository,
  mockWebStorageRepository
} from '../mock/mock'

describe.skip('WebPushService', () => {
  let webPushService: WebPushService

  beforeEach(() => {
    Object.defineProperty(global.navigator, 'serviceWorker', {
      value: {
        ready: Promise.resolve({ pushManager: { subscribe: jest.fn() } } as any)
      },
      configurable: true
    })

    webPushService = new WebPushService(
      mockWebPushRepository,
      mockWebStorageRepository,
      'test-key')
  })

  describe('handleNotificationDecision', () => {
    let permissionStatus: string

    beforeEach(() => {
      permissionStatus = NotificationDecision.DEFAULT
      global.Notification = {} as any
      Object.defineProperty(global.Notification, 'permission', {
        get: () => permissionStatus
      })
      global.Notification.requestPermission = jest.fn(async () => {
        return await Promise.resolve('granted')
      })
    })

    it('should request permission and call subscribeUser if permission is granted', async () => {
      const userDecision = 'granted'
      const subscribeUserSpy = jest.spyOn(webPushService, '_subscribeUser')
      await webPushService.handleNotificationDecision(userDecision)
      await waitFor(() => {
        expect(global.Notification.requestPermission).toHaveBeenCalled()
        expect(subscribeUserSpy).toHaveBeenCalled()
      })
    })

    it('should call subscribeUser directly if permission is already granted', async () => {
      permissionStatus = 'granted'
      const userDecision = 'granted'
      const subscribeUserSpy = jest.spyOn(webPushService, '_subscribeUser')
      await webPushService.handleNotificationDecision(userDecision)
      expect(subscribeUserSpy).toHaveBeenCalled()
    })
  })
})
