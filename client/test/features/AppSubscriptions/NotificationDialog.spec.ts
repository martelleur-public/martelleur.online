import {
  NotificationDialog, nameNotificationDialog
} from '@features/AppSubscription/components/NotificationDialog/NotificationDialog'
import { assertElementExist } from '@utility/html'
import type { AppealingButton } from '@components/AppealingButton/AppealingButton'

describe('NotificationDialog', () => {
  let notificationDialog: NotificationDialog
  let accept: AppealingButton
  let deny: AppealingButton
  const elementName = (NotificationDialog as any).elementName

  beforeEach(async () => {
    notificationDialog = document.createElement(elementName) as NotificationDialog
    document.body.appendChild(notificationDialog)
    if (notificationDialog == null || notificationDialog.shadowRoot == null) {
      throw new Error('NotificationDialog element or shadowroot is null')
    }
    accept = assertElementExist(notificationDialog.shadowRoot?.querySelector<AppealingButton>('[data-test="accept-notification"]'))
    deny = assertElementExist(notificationDialog.shadowRoot?.querySelector<AppealingButton>('[data-test="deny-notification"]'))
  })

  afterEach(() => {
    if (document.body.contains(notificationDialog)) {
      document.body.removeChild(notificationDialog)
    }
  })

  it('should be defined as a custom element notification-dialog', () => {
    expect(elementName).toEqual(nameNotificationDialog)
    /**
     * @TODO expect(customElements.get(elementName)).toBe(notificationDialog)
     */
  })

  it('should render the notification prompt in the document', () => {
    expect(notificationDialog).toBeInTheDocument()
  })

  it('should render accept options within the notification prompt', () => {
    expect(accept).toBeInTheDocument()
  })

  it('should render deny option within the notification prompt', () => {
    expect(deny).toBeInTheDocument()
  })
})
