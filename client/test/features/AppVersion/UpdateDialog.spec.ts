import type { ResponsiveDialog } from '@martelleur.online/webcraft'

import { UpdateDialog } from '@features/AppVersion/components/UpdateDialog/UpdateDialog'
import { assertElementExist } from '@utility/html'
import { ConfirmUpdateEvent } from '@event'

describe('AppUpdateDialog', () => {
  let updateDialog: UpdateDialog
  let dialog: ResponsiveDialog
  let confirmButton: HTMLButtonElement
  let denyButton: HTMLButtonElement
  const elementName = (UpdateDialog as any).elementName

  beforeEach(async () => {
    updateDialog = document.createElement(elementName) as UpdateDialog
    document.body.appendChild(updateDialog)
    if (updateDialog == null || updateDialog.shadowRoot == null) {
      throw new Error('UpdateDialog element or shadowroot is null')
    }
    dialog = assertElementExist(updateDialog.shadowRoot.querySelector('[data-test="update-dialog"'))
    dialog.open = jest.fn()
    dialog.close = jest.fn()
    confirmButton = assertElementExist(updateDialog.shadowRoot.querySelector('[data-test="confirm"]'))
    denyButton = assertElementExist(updateDialog.shadowRoot.querySelector('[data-test="deny"]'))
  })

  afterEach(() => {
    if (document.body.contains(updateDialog)) {
      document.body.removeChild(updateDialog)
    }
  })

  it('should be defined as a custom element update-dialog', () => {
    expect(elementName).toEqual('update-dialog')
  })

  it('should render the dialog', () => {
    expect(dialog).not.toBeNull()
  })

  it('should render the confirm button with the correct text', () => {
    expect(confirmButton).not.toBeNull()
    expect(confirmButton.getAttribute('data-text')).toBe('YES')
  })

  it('should render the deny button with the correct text', () => {
    expect(denyButton).not.toBeNull()
    expect(denyButton.getAttribute('data-text')).toBe('NO')
  })

  it.skip('should open the dialog when calling the open method', () => {
    updateDialog.open()
    expect(dialog.open).toHaveBeenCalled()
  })

  it.skip('should close the dialog when calling the close method', () => {
    updateDialog.open()
    updateDialog.close()
    expect(dialog.close).toHaveBeenCalled()
  })

  it('should dispatch the confirm event when the confirm button is clicked', () => {
    const confirmEvent = jest.fn()
    updateDialog.addEventListener(ConfirmUpdateEvent.eventName, confirmEvent)
    updateDialog.open()
    confirmButton.click()
    expect(confirmEvent).toHaveBeenCalled()
  })

  it('should close the dialog when the deny button is clicked', () => {
    updateDialog.open()
    denyButton.click()
    expect(dialog.open).toHaveBeenCalled()
  })
})
