import { WebStorage } from '@infrastructure'
import { NotificationDecision } from '@constants'

describe('WebStorage', () => {
  const KEY_NOTIFICATION_DECISION = 'notificationDecision'
  let webStorage: WebStorage

  beforeEach(() => {
    webStorage = new WebStorage()
    Storage.prototype.getItem = jest.fn()
    Storage.prototype.setItem = jest.fn()
  })

  it('should set the notification decision to default', () => {
    webStorage.setNotificationDecisionToDefault()
    expect(localStorage.setItem).toHaveBeenCalledWith(KEY_NOTIFICATION_DECISION, 'default')
  })

  it('should set the notification decision to granted', () => {
    webStorage.setNotificationDecisionToGranted()
    expect(localStorage.setItem).toHaveBeenCalledWith(KEY_NOTIFICATION_DECISION, NotificationDecision.GRANTED)
  })

  it('should set the notification decision to denied', () => {
    webStorage.setNotificationDecisionToDenied()
    expect(localStorage.setItem).toHaveBeenCalledWith(KEY_NOTIFICATION_DECISION, NotificationDecision.DENIED)
  })

  it('should get the notification decision', () => {
    const mockDecision = NotificationDecision.GRANTED;
    (Storage.prototype.getItem as jest.Mock).mockImplementation((key) => {
      if (key === 'notificationDecision') {
        return mockDecision
      }
    })
    expect(webStorage.getNotificationDecision()).toEqual(mockDecision)
  })

  it('should default to Notification.permission if no value is set in localStorage', () => {
    (localStorage.getItem as jest.Mock).mockReturnValueOnce(null)
    expect(webStorage.getNotificationDecision()).toEqual('default')
  })
})
