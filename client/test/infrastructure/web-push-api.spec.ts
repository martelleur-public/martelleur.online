import { WebPushAPI } from '@infrastructure'
import { getApiEndpoint } from '@config'
import { mockPushSubscription } from '../mock/mock'

describe('WebPushAPI', () => {
  let webPushAPI: WebPushAPI
  let mockFetch: jest.Mock

  beforeEach(() => {
    webPushAPI = new WebPushAPI()
    mockFetch = jest.fn()
    global.fetch = mockFetch
  })

  it('should make a POST request to the correct endpoint with the correct body', async () => {
    const subscription: PushSubscription = mockPushSubscription
    const apiEndpoint = getApiEndpoint()
    mockFetch.mockResolvedValueOnce({ ok: true })
    await webPushAPI.postSubscription(subscription)
    expect(mockFetch).toHaveBeenCalledWith(`${apiEndpoint}/webpush/subscribe`, {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(subscription)
    })
  })
})
