# Martelleur.online

<div align="center">
  <img src="./client/public/icon-192.png" alt="My Icon">
</div>

## About martelleur.online
🚀 This is a repository for the website [martelleur.online](https://martelleur.online) where I publish my public projects, e.g., libraries and web applications. 

🚀 The site [martelleur.online](https://martelleur.online) is built with CSS, HTML, TypeScript, Web Components, Vite, Express.js, Jest, Mysql, and Service Workers. 

💡🌐 The site also uses my own developed frontend library called [WebCraft](https://gitlab.com/martelleur-public/martelleur.online/-/blob/main/lib/webcraft/doc/todo/project-webcraft-library.md#project-webcraft-library) built for web components and frontend frameworks with two main ideas:

1. Web Components is a great reliable standard for creating reusable components.

2. Consistent interface when using a library natively with Web Components or with amazing frameworks like React, Vue, Angular, Svelte, etc gives power and freedom to developers.

The library is under active development but the core components of the library are:

1. **Components:** Reusable Web Components, e.g., responsive layouts and dialogs.

2. **Reactive Store:** A redux-like store with reactive capacity.

3. **SPA Routing:** Routing capacity with visual page transitions for improved UX.

4. **Utility:** Utility functions and TypeScript decorators to accelerate a more maintainable project.

5. **Framework Wrappers (separate wrapper libraries):**
    - ```ReactCraft:``` A wrapper for React, under active development.
    - ```VueCraft:``` A wrapper for Vue, future goal.

## About me

- [https://github.com/Martelleur ](https://github.com/Martelleur )

- Accelerate by slowing down! 💡🌐
