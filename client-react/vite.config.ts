import path from 'path'
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    preserveSymlinks: true,
    alias: {
      "@martelleur.online/reactcraft": path.resolve(__dirname, "../lib/reactcraft/dist/esm/src"),
      "@martelleur.online/webcraft": path.resolve(__dirname, "../lib/webcraft/dist/esm/src"),
      "@app": path.resolve(__dirname, "./src/app"),
      "@assets": path.resolve(__dirname, "./src/assets"),
      "@pages": path.resolve(__dirname, "./src/pages"),
      "@components": path.resolve(__dirname, "./src/components"),
      "@coonfig": path.resolve(__dirname, "./src/coonfig"),
      "@features": path.resolve(__dirname, "./src/features"),
      "@router": path.resolve(__dirname, "./src/router"),
    }
  }
})
