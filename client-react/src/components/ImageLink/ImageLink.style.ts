import styled from 'styled-components'

interface LinkProps {
  $image?: string
}

export const Link = styled.a<LinkProps>`
  display: flex;
  font-size: 1.4em;
  font-weight: bold;
  color: var(--primary-text);
  text-decoration: none;
  justify-content: end;
  align-items: end;
  width: 100%;
  height: 100%;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-color: var(--primary-background-dark-extra);
  opacity: 0.9;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.7);
  transition: opacity 0.3s ease-in-out, box-shadow 0.3s ease-in-out;

  background-image: ${({ $image }) => $image ? `url(${$image})` : 'none'};

  &:hover {
    opacity: 1;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 1);
  }
`

export const Paragraph = styled.p`
  padding: 8px;
  margin: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 6px;
`

export const Span = styled.span`
  padding-bottom: 3px;
`
