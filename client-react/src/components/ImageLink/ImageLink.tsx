import React from 'react'
import { NewTabIconWhite } from '@martelleur.online/reactcraft'

import * as Styled from './ImageLink.style'

interface Props {
  href: string
  title: string
  image: string
  textContent: string
}

export const ImageLink: React.FC<Props> = ({
    href,
    title,
    image,
    textContent
}) => {
  return (
    <>
      <Styled.Link
        $image={image}
        title={title}
        href={href}
        target="_blank">
        <Styled.Paragraph>
          {textContent}
          <Styled.Span><NewTabIconWhite /></Styled.Span>
        </Styled.Paragraph>
      </Styled.Link>
    </>
  )
}