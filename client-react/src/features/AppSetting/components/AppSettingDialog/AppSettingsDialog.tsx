import React from 'react'
import { ResponsiveDialog } from '@martelleur.online/reactcraft'

import { SubHeader } from './SubHeader'

interface Props {
  toogleDialog: () => void
}

export const AppSettingDialog: React.FC<Props> = ({
  toogleDialog
}): JSX.Element => {
  return (
    <>
      <ResponsiveDialog
        sectionLines
        onToogle={toogleDialog}
      >
        <h3 slot="title">App settings</h3>
        <SubHeader slot="sub-header"/>
        <p slot="body">My Dialog Body</p>
      </ResponsiveDialog>
    </>
  )
}
