import styled from 'styled-components'

export const NavigationButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 12px;
  margin: 2px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  outline: none;
  width: calc(100% / 3);
  min-height: 100%;
  background-color: var(--BACKGROUND_MEDIUM);
  font-family: var(--FONT_PRIMARY);
`

export const NavigationContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
`
