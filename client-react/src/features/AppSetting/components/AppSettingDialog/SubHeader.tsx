import React from 'react'

import * as Styled from './AppSettingDialog.style'

interface Props {
  slot: string
}

export const SubHeader: React.FC<Props> = (): JSX.Element => {
  return (
    <Styled.NavigationContainer>
      <Styled.NavigationButton>Theme</Styled.NavigationButton>
      <Styled.NavigationButton>Font</Styled.NavigationButton> 
      <Styled.NavigationButton>Page Transition</Styled.NavigationButton>      
    </Styled.NavigationContainer>
  )
}
