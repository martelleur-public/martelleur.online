import React from 'react'
import { SettingIcon } from '@martelleur.online/reactcraft'

export const AppSettings: React.FC = (): JSX.Element => {
  const style: React.CSSProperties = {
    marginTop: '-5px',
    padding: '10px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }

  return (
    <>
      <SettingIcon style={style}/>
    </>
  )
}
