import React, { useState } from 'react'
import { BasicLayoutSlotType } from '@martelleur.online/webcraft'
import {
  ResponsiveHeader,
  AppLink,
  RemoteLink
} from '@martelleur.online/reactcraft'

import { AppSettings } from '../../../AppSetting'
import { getBaseURL } from '../../../../config'
import { AppSettingDialog } from '../../../AppSetting/components/AppSettingDialog/AppSettingsDialog'

interface Props {
  slot: BasicLayoutSlotType
}

export const AppHeader: React.FC<Props> = ({ slot }): JSX.Element => {
  const baseURL = getBaseURL()
  const [showDialog, setShowDialog] = useState<boolean>(false)

  const toogleDialog = (): void => {
    setShowDialog((prev) => !prev)
  }

  return (
    <>
    {showDialog && <AppSettingDialog toogleDialog={toogleDialog}/>}
    <ResponsiveHeader slot={slot}>
      <span slot="icon"></span>
      <h2 slot="title">martelleur.online</h2>
      <div slot="links">
        <AppLink
          title="Web development"
          href={`${baseURL}/index`}
          dataText="Web">
        </AppLink>
        <AppLink
          title="Git repositories"
          href={`${baseURL}/git`}
          dataText="Git">
        </AppLink>
        <AppLink
          title="Games built with webcomponents"
          href={`${baseURL}/game`}
          dataText="Games">
        </AppLink>
        <AppLink
          title="Web development"
          href={`${baseURL}/a/b/c/alpha/index`}
          dataText="a/b/c/alpha/Web">
        </AppLink>
        <AppLink
          title="Git repositories"
          href={`${baseURL}/a/b/c/alpha/git`}
          dataText="a/b/c/alpha/Git">
        </AppLink>
        <AppLink
          title="Games built with webcomponents"
          href={`${baseURL}/a/b/c/alpha/game`}
          dataText="a/b/c/alpha/Games">
        </AppLink>
        <AppLink
          title="Web development"
          href={`${baseURL}/a/b/c/beta/index`}
          dataText="a/b/c/beta/Web">
        </AppLink>
        <AppLink
          title="Git repositories"
          href={`${baseURL}/a/b/c/beta/git`}
          dataText="a/b/c/beta/Git">
        </AppLink>
        <AppLink
          title="Games built with webcomponents"
          href={`${baseURL}/a/b/c/beta/game`}
          dataText="a/b/c/beta/Games">
        </AppLink>
        <RemoteLink
          title="Library @martelleur/WebCraft"
          href="https://gitlab.com/martelleur-public/martelleur.online/-/blob/main/lib/doc/todo/project-webcraft-library.md#project-webcraft-library"
          dataText="WebCraft">
        </RemoteLink>
      </div>
      <div slot="menu" onClick={toogleDialog}>
        <AppSettings/>
      </div>
    </ResponsiveHeader>
    </>
  )
}

