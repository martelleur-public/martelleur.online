import { BasicLayout } from '@martelleur.online/reactcraft'

import { Router as AppRouter } from './router/AppRouter'
import { AppHeader } from './features'

function App(): JSX.Element {
  return (
    <BasicLayout dynamicHeight={true}>
      <AppHeader slot="header"/>
      <AppRouter slot="body"/>
    </BasicLayout>
  )
}

export default App
