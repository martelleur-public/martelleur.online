export function getEnvName (): string {
  return import.meta.env.VITE_APP_ENV
}

export function getAppVersion (): string {
  return import.meta.env.VITE_APP_VERSION
}

export function getBaseURL (): string {
  return import.meta.env.VITE_BASE_URL
}

export function getApiEndpoint (): string {
  return import.meta.env.VITE_API_ENDPOINT
}

export function getVapidNotificationPublickKey (): string {
  return import.meta.env.VITE_VAPID_NOTIFICATION_PUBLIC_KEY
}
