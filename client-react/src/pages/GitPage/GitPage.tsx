import React from 'react'

import * as Styled from './GitPage.style'
import { ImageLink } from '../../components'
import imageGithub from '../../assets/images/github.jpeg'
import imageGitlab from '../../assets/images/gitlab.jpeg'
import imageNpmLibrary from '../../assets/images/npm_library.jpeg'

interface Props {
  className?: string
}
 
export const GitPage: React.FC<Props> = ({ className }): JSX.Element => {
  return (
    <Styled.PageContainer className={className}>
      <ImageLink
        title="Github Martelleur"
        href="https://github.com/Martelleur/Martelleur"
        textContent="GitHub Martelleur"
        image={imageGithub} />
      <ImageLink
        title="Gitlab martelleur.online"
        href="https://gitlab.com/martelleur-public/martelleur.online"
        textContent="GitLab martelleur.online"
        image={imageGitlab} />
      <ImageLink
        title="Library @martelleur/WebCraft"
        href="https://gitlab.com/martelleur-public/martelleur.online/-/blob/main/lib/doc/todo/project-webcraft-library.md#project-webcraft-library"
        textContent="Library WebCraft"
        image={imageNpmLibrary} />
      <ImageLink
        title="Gitlab Android Java"
        href="https://gitlab.com/martelleur-public/android-java"
        textContent="GitLab Android Java"
        image={imageGitlab} />
    </Styled.PageContainer>
  )
}
