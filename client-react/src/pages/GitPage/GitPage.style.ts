import styled from 'styled-components'

export const PageContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 10px;
  justify-items: center;
  align-items: center;
  box-sizing: border-box;
  padding: 0 10px 10px 10px;
  width: 100%;
  height: 100%;

  @media (max-width: 700px), (max-height: 500px) {
    grid-template-columns: repeat(1, 1fr);
  }
`