import styled from 'styled-components'
import React from 'react'
 
const PageContainer = styled.div`
  display: flex;
  justify-content: center;
  padding-top: 50px;
  align-items: start;
  width: 100%;
  height: 100%;
  background: var(--BACKGROUND_LIGHT);
  color: var(--TEXT_PRIMARY);
`

interface Props {
  className?: string
}
 
export const NotFoundPage: React.FC<Props> = ({ className }): JSX.Element => {
  return (
    <PageContainer className={className}>
        <div>
            <h1>404</h1>
            <p>Page Not Found</p>
        </div>
    </PageContainer>
  )
}
