import React from 'react'

import * as Styled from './GamePage.style'
import { ImageLink } from '../../components'
import imageMemory from '../../assets/images/memory.jpeg'
import imageMinesweeper from '../../assets/images/minesweeper.jpeg'

interface Props {
  className?: string
}
 
export const GamePage: React.FC<Props> = ({ className }): JSX.Element => {
  return (
    <Styled.PageContainer className={className}>
      <ImageLink
        title="Memory game"
        href="https://chat.martelleur.online/game/memory/"
        textContent="Memory"
        image={imageMemory} />
      <ImageLink
        title="Minesweeper game"
        href="https://chat.martelleur.online/game/minesweeper/"
        textContent="Minesweeper"
        image={imageMinesweeper} />
      <ImageLink
        title="Memory game"
        href="https://chat.martelleur.online/game/memory/"
        textContent="Memory"
        image={imageMemory} />
      <ImageLink
        title="Minesweeper game"
        href="https://chat.martelleur.online/game/minesweeper/"
        textContent="Minesweeper"
        image={imageMinesweeper} />
     </Styled.PageContainer>
  )
}
