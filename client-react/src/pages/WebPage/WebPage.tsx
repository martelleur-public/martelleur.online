import React from 'react'

import * as Styled from './WebPage.style'
import imageGitHub from '../../assets/images/github.jpeg'
import imageMinesweeper from '../../assets/images/minesweeper.jpeg'
import imageNpmLibrary from '../../assets/images/npm_library.jpeg'
import imageMyChat from '../../assets/images/mychat.png'
import { ImageLink } from '../../components'

interface Props {
  className?: string
}
 
export const WebPage: React.FC<Props> = ({ className }): JSX.Element => {
  return (
    <Styled.PageContainer className={className}>
      <ImageLink
        title="Library WebCraft"
        href="https://gitlab.com/martelleur-public/martelleur.online/-/blob/main/lib/webcraft/doc/todo/project-webcraft-library.md#project-webcraft-library"
        textContent="Library WebCraft"
        image={imageNpmLibrary} />
      <ImageLink
        title="A simple web chat called MyChat"
        href="https://chat.martelleur.online"
        textContent="MyChat"
        image={imageMyChat} />
      <ImageLink
        title="Github Martelleur"
        href="https://github.com/Martelleur/Martelleur"
        textContent="GitHub Martelleur"
        image={imageGitHub} />
      <ImageLink
        title="Minesweeper game"
        href="https://chat.martelleur.online/game/minesweeper/"
        textContent="Minesweeper"
        image={imageMinesweeper} />
    </Styled.PageContainer>
  )
}
