import { IRouterOptions, PageTransition, RouteMap } from '@martelleur.online/reactcraft'

export const routes: RouteMap = {
  '': '<web-page></web-page>',
  '/': '<web-page></web-page>',
  '/index.html': '<web-page></web-page>',
  '/index': '<web-page></web-page>',
  '/game': '<game-page></game-page>',
  '/git': '<git-page></git-page>',
  '/tool': '<tool-page></tool-page>',
  '/default': '<not-found-page></not-found-page>'
}

export const routerOptions: Partial<IRouterOptions> = {
  defaultPage: routes['/default'],
  transition: PageTransition.DARK,
  elementName: 'app-router'
}
