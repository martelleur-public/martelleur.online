import {
  PageTransition,
  RouteGroup,
  RouteItem,
  AppRouter
} from '@martelleur.online/reactcraft'
import { BasicLayoutSlotType } from '@martelleur.online/webcraft'

import { NotFoundPage } from '../pages/NotFoundPage/NotFoundPage'
import { WebPage } from '../pages/WebPage/WebPage'
import { GitPage } from '../pages/GitPage/GitPage'
import { GamePage } from '../pages/GamePage/GamePage'

interface Props {
  slot: BasicLayoutSlotType
}

export const Router: React.FC<Props> = ({ slot }): JSX.Element => {
  return (
    <AppRouter slot={slot} transition={PageTransition.ROTATION_SHRINK}>
      <RouteGroup path="">
        <RouteItem path={'default'} component={<NotFoundPage />} default/>
        <RouteItem path={'index'} component={<WebPage />}/>
        <RouteItem path={'game'} component={<GamePage />}/>
        <RouteItem path={'git'} component={<GitPage />}/>
      </RouteGroup>
      <RouteGroup path="a">
        <RouteGroup path="b">
          <RouteGroup path="c">
            <RouteGroup path="alpha">
              <RouteItem path="index" component={<WebPage />} />
              <RouteItem path="git" component={<GitPage />} />
              <RouteItem path="game" component={<GamePage />} />
            </RouteGroup>
            <RouteGroup path="beta">
              <RouteItem path="index" component={<WebPage />} />
              <RouteItem path="git" component={<GitPage />} />
              <RouteItem path="game" component={<GamePage />} />
            </RouteGroup>
          </RouteGroup>
        </RouteGroup>
      </RouteGroup>
    </AppRouter>
  )
}
