import sys
import requests
from requests.auth import HTTPBasicAuth

def main():
  (
    notification_user,
    notification_password,
    notification_api_endpoint,
    app_version
  ) = get_args()
  
  response = send_notification_new_version_martelleur_net(
    notification_user,
    notification_password,
    notification_api_endpoint,
    app_version
  )
  print(response.text)

def get_args():
  args = sys.argv
  if len(args) != 5:
    print(f"""Usage: {args[0]} \
<NOTIFICATION_USER> \
<NOTIFICATION_PASSWORD> \
<NOTIFICATION_API_ENDPOINT> \
<APP_VERSION>""")
    sys.exit(1)
  return (
    notification_user := args[1],
    notification_password := args[2],
    notification_api_endpoint := args[3],
    app_version := args[4]
  )
  
def send_notification_new_version_martelleur_net(
    user, password, api_endpoint, app_version):
  json_payload = {
    "title": "martelleur.online",
    "body": "New version martelleur.online v{}".format(app_version),
    "url": "https://martelleur.online/index"
  }
  response = requests.post(
    api_endpoint,
    auth=HTTPBasicAuth(user, password),
    headers={"Content-Type": "application/json"},
    json=json_payload
  )
  return response

if __name__ == "__main__":
    main()
