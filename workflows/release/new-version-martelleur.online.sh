#!/bin/bash

NOTIFICATION_USER="$1"
NOTIFICATION_PASSWORD="$2"
NOTIFICATION_API_ENDPOINT="$3"
APP_VERSION="$4"

main () {
  check_args
  send_notification_new_version_martelleur_net
}

check_args () {
  if [ -z "$NOTIFICATION_USER" ] || \
    [ -z "$NOTIFICATION_PASSWORD" ] || \
    [ -z "$NOTIFICATION_API_ENDPOINT" ] || \
    [ -z "$APP_VERSION" ]; then
    echo "Usage: $0 \
<NOTIFICATION_USER> \
<NOTIFICATION_PASSWORD> \
<NOTIFICATION_API_ENDPOINT> \
<APP_VERSION>"
    exit 1
  fi
}

send_notification_new_version_martelleur_net () {
  JSON_PAYLOAD=$(printf \
  '{
    "title":"martelleur.online",
    "body":"New version martelleur.online v%s",
    "url":"https://martelleur.online/index"
  }' "$APP_VERSION")

  curl -X POST \
    -u ${NOTIFICATION_USER}:${NOTIFICATION_PASSWORD} \
    -H "Content-Type: application/json" \
    -d "$JSON_PAYLOAD" \
    ${NOTIFICATION_API_ENDPOINT}
}

main
