import type * as CSS from 'csstype'

export interface CSSStyleProperties extends CSS.Properties, CSS.PropertiesHyphen {}
