import { render, screen, cleanup } from '@testing-library/react'
import { BasicLayout as CustomBasicLayout } from '@martelleur.online/webcraft'

import React from 'react'
import { BasicLayout } from '@layouts/BasicLayout'

describe('BasicLayout', () => {
  const testCases = [
    { description: 'With true attributes', dynamicHeight: true },
    { description: 'With false attributes', dynamicHeight: false },
    { description: 'With no attributes', dynamicHeight: undefined }
  ]

  const childContent = {
    header: 'My Dialog SubHeader',
    body: 'My Dialog Body',
    footer: 'My Dialog Footer'
  }

  testCases.forEach(({ description, dynamicHeight }) => {
    describe(description, () => {
      let layout: HTMLElement

      beforeEach(() => {
        const layoutElement = React.createElement(
          BasicLayout,
          {
            dynamicHeight,
          },
          React.createElement('div', { slot: 'header' }, childContent.header),
          React.createElement('div', { slot: 'body' }, childContent.body),
          React.createElement('div', { slot: 'footer' }, childContent.footer)
        )
        render(layoutElement)
        layout = screen.getByTestId(CustomBasicLayout.elementName)
      })

      afterEach(() => {
        cleanup()
      })

      it(`Should be a react wrapper for custom element ${CustomBasicLayout.elementName}`, () => {
        expect(layout).toBeInTheDocument()
      })

      it.each([
        { attribute: CustomBasicLayout.elementAttributes.DYNAMIC_HEIGHT, value: dynamicHeight },
      ])(
        `Should wrap custom component ${CustomBasicLayout.elementName} attributes`,
        ({ attribute, value }) => {
          if (value) {
            expect(layout).toHaveAttribute(attribute, `${value}`)
          } else {
            expect(layout).not.toHaveAttribute(attribute)
          }
        }
      )

      it.each([
        { selector: '[slot=header]' },
        { selector: '[slot=body]' },
        { selector: '[slot=footer]' }
      ])(
        `Should render child elements in slots in custom element ${CustomBasicLayout.elementName}`,
        ({ selector }) => {
          expect(layout.querySelector(selector)).toBeInTheDocument()
        }
      )

      it.each([
        { selector: '[slot=header]', text: childContent.header },
        { selector: '[slot=body]', text: childContent.body },
        { selector: '[slot=footer]', text: childContent.footer }
      ])(
        `Should render child element text content in slots in custom element ${CustomBasicLayout.elementName}`,
        ({ selector, text }) => {
          expect(layout.querySelector(selector)).toHaveTextContent(text)
        }
      )
    })
  })
})
