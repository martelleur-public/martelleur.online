import React from 'react'
import { cleanup, render, screen } from '@testing-library/react'
import {
  ResponsiveHeader as CustomResponsiveHeader
} from '@martelleur.online/webcraft'

import { ResponsiveHeader } from '@layouts/ResponsiveHeader'

describe('ResponsiveHeader', () => {
  let layout: HTMLElement

  const childContent = {
    icon: 'My Icon',
    title: 'My Title',
    links: 'My Footer',
    menu: 'My Menu'
  }

  const layoutElement = React.createElement(
    ResponsiveHeader,
    {
      slot: 'header',
    },
    React.createElement('div', { slot: 'icon' }, childContent.icon),
    React.createElement('div', { slot: 'title' }, childContent.title),
    React.createElement('div', { slot: 'links' }, childContent.links),
    React.createElement('div', { slot: 'menu' }, childContent.menu)
  )

  beforeEach(() => {
    render(layoutElement)
    layout = screen.getByTestId(CustomResponsiveHeader.elementName)
  })

  afterEach(() => {
    cleanup()
  })

  it(`Should be a react wrapper for custom element ${CustomResponsiveHeader.elementName}`, () => {
    expect(layout).toBeInTheDocument()
  })

  it.each([
    { selector: '[slot=icon]' },
    { selector: '[slot=title]' },
    { selector: '[slot=links]' },
    { selector: '[slot=menu]' }
  ])(
    `Should render child elements in slots in custom element ${CustomResponsiveHeader.elementName}`,
    ({ selector }) => {
      expect(layout.querySelector(selector)).toBeInTheDocument()
    }
  )

  it.each([
    { selector: '[slot=icon]', text: childContent.icon },
    { selector: '[slot=title]', text: childContent.title },
    { selector: '[slot=links]', text: childContent.links },
    { selector: '[slot=menu]', text: childContent.menu },
  ])(
    `Should render child element text content in slots in custom element ${CustomResponsiveHeader.elementName}`,
    ({ selector, text }) => {
      expect(layout.querySelector(selector)).toHaveTextContent(text)
    }
  )
})
