import React from 'react'
import { cleanup, render } from '@testing-library/react'
import {
  RouteGroup as CustomRouteGroup,
  RouterElementError
} from '@martelleur.online/webcraft'

import {
  createElementNameFromUrlPath,
  isValidCustomElementName
} from '@router/utility'

import { AppRouter, PageTransition } from '@router/AppRouter'
import { RouteItem } from '@router/RouteItem'
import { RouteGroup } from '@router/RouteGroup'

describe(('RouteItem'), () => {
  const defaultPage = false
  const component = React.createElement('div')

  afterEach(() => {
    cleanup()
  })

  it.each([
    { path: 'pageone', expectedName: 'pageone-page' },
    { path: 'pageTwo', expectedName: 'pagetwo-page' },
    { path: 'PaGeThree', expectedName: 'pagethree-page' },
    { path: 'PAGE', expectedName: 'page-page' },
    { path: 'p', expectedName: 'p-page' }
  ])(
    'Should define a custom element with name <path>-page',
    ({ path, expectedName }) => {
      const routeItem = React.createElement(RouteItem, { path, component, default: defaultPage })
      const routeGroup = React.createElement(
        RouteGroup,
        { path: ''},
        routeItem
      )
      const transition = PageTransition.NONE
      render(React.createElement(AppRouter, { transition }, routeGroup ))
      const elementName = createElementNameFromUrlPath(path)
      const isElementDefined = customElements.get(createElementNameFromUrlPath(path)) !== undefined
      expect(elementName).toBe(expectedName)
      expect(isElementDefined).toBeTruthy()
    }
  )

  it.each([
    { path: '.' },
    { path: '1' },
    { path: ';' },
    { path: '-' },
  ])(
    'Should not define to element with invalid cutom elements names',
    ({ path }) => {
      const elementName = createElementNameFromUrlPath(path)
      const isValidName = isValidCustomElementName(elementName)
      expect(isValidName).toBeFalsy()
      expect(() => {
        const routeItem = React.createElement(RouteItem, { path, component, default: defaultPage })
        const routeGroup = React.createElement(
          RouteGroup,
          { path: ''},
          routeItem
        )
        const transition = PageTransition.NONE
        render(React.createElement(AppRouter, { transition }, routeGroup ))
      }).toThrow()
    }
  )

  /**
   * Does throw but is catched by react create element ???
   */
  it.skip(`Should throw ${RouterElementError.errorName}
    when not inside a ${CustomRouteGroup.elementName} element`, () => {
    const mockPath = 'mock-Path'
    const renderRoutItemOutsideRouteGroup = (): void => {
      const routeItem = React.createElement(RouteItem, {
        path: mockPath, component, default: defaultPage
      })
      render(routeItem)
    }
    expect(renderRoutItemOutsideRouteGroup).toThrow()
  })
})
