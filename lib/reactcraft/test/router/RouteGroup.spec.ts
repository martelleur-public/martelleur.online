import React from 'react'
import { cleanup, render, screen } from '@testing-library/react'
import {
  RouteGroup as CustomRouteGroup
} from '@martelleur.online/webcraft'

import { AppRouter, PageTransition } from '@router/AppRouter'
import { RouteItem } from '@router/RouteItem'
import { RouteGroup } from '@router/RouteGroup'

describe(('RouteGroup'), () => {
  const defaultPage = false
  const component = React.createElement('div')

  afterEach(() => {
    cleanup()
  })

  it.skip(`Should be a wrapper of custom element ${CustomRouteGroup.elementName}`, () => {
    const path = 'mockPath'
    const routeItem = React.createElement(RouteItem, {
      path,
      component,
      default: defaultPage
    })
    const expectedRouteGroup = React.createElement(
      RouteGroup,
      { path: ''},
      routeItem
    )
    const transition = PageTransition.NONE
    render(React.createElement(AppRouter, { transition }, expectedRouteGroup ))
    const actualRouteGroup = screen.getByTestId(CustomRouteGroup.elementName)
    expect(expectedRouteGroup).toBe(actualRouteGroup)
  })
})
