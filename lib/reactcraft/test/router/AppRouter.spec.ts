import React from 'react'
import { cleanup, render, screen } from '@testing-library/react'
import {
  PageTransition,
  AppRouter as CustomAppRouter
} from '@martelleur.online/webcraft'

import { AppRouter } from '@router/AppRouter'
import { RouteItem } from '@router/RouteItem'
import { RouteGroup, RouteGroupChildType } from '@router/RouteGroup'

jest.mock('@martelleur.online/webcraft', () => ({
  ...jest.requireActual('@martelleur.online/webcraft'),
  createRouterElement: jest.fn()
}))

describe('AppRouter', () => {
  let router: HTMLElement
  const elementName = CustomAppRouter.elementName
  const transition = PageTransition.NONE
  const routeItems: RouteGroupChildType = [
    React.createElement(RouteItem, {
      default: true,
      path: 'pageone',
      component: React.createElement('div', {})
    }),
    React.createElement(RouteItem, {
      path: 'pagetwo',
      component: React.createElement('div', {})
    }),
    React.createElement(RouteItem, {
      path: 'pagethree',
      component: React.createElement('div', {})
    })
  ]
  const routeGroup = React.createElement(
    RouteGroup,
    { path: ''},
    routeItems
  )

  beforeEach(() => {
    render(React.createElement(AppRouter, { transition }, routeGroup ))
    router = screen.getByTestId(elementName)
  })

  afterEach(() => {
    cleanup()
  })

  it(`Should be a react wrapper for custom element ${elementName}`, () => {
    expect(router).toBeInTheDocument()
  })
})
