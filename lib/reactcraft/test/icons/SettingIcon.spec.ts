import React from 'react'
import { cleanup, render, screen } from '@testing-library/react'
import {
  SettingIcon as CustomSettingIcon
} from '@martelleur.online/webcraft'

import { SettingIcon } from '@icons/SettingIcon'
import { CSSStyleProperties } from '../types'

describe('SettingIcon', () => {
  let layout: HTMLElement
  const styleReact: React.CSSProperties = {
    marginTop: '-10px',
    paddingLeft: '10px',
    paddingRight: '10px'
  }
  const styleCSS: CSSStyleProperties = {
    'margin-top': '-10px',
    'padding-left': '10px',
    'padding-right': '10px'
  }

  beforeEach(() => {
    render(React.createElement(SettingIcon, { style: styleReact }))
    layout = screen.getByTestId(CustomSettingIcon.elementName)
  })

  afterEach(() => {
    cleanup()
  })

  it(`Should be a react wrapper for custom element ${CustomSettingIcon.elementName}`, () => {
    expect(layout).toBeInTheDocument()
  })

  it('Should be be stylable by passing react style object', () => {
    expect(layout).toHaveAttribute(
      'style',
      `${getCssString(styleCSS)}`
    )
  })
})

function getCssString (cssStyle: CSSStyleProperties): string {
  let cssString = ''
  for (const [key, value] of Object.entries(cssStyle)) {
    const space = ' '
    cssString += `${key}: ${value};${space}` 
  }
  const cssStringWithoutLastSpace = cssString.slice(0, -1)
  return cssStringWithoutLastSpace
}
