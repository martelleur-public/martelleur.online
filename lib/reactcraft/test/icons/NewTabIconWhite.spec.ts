import React from 'react'
import { cleanup, render, screen } from '@testing-library/react'
import {
  NewTabIconWhite as CustomNewTabIconWhite
} from '@martelleur.online/webcraft'

import { NewTabIconWhite } from '@icons/NewTabIconWhite'

describe('NweTabIconWhite', () => {
  let layout: HTMLElement
  beforeEach(() => {
    render(React.createElement(NewTabIconWhite))
    layout = screen.getByTestId(CustomNewTabIconWhite.elementName)
  })

  afterEach(() => {
    cleanup()
  })

  it(`Should be a react wrapper for custom element ${CustomNewTabIconWhite.elementName}`, () => {
    expect(layout).toBeInTheDocument()
  })
})
