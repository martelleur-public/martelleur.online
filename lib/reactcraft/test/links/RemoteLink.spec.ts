import { cleanup, render, screen } from '@testing-library/react'
import { RemoteLink as CustomRemoteLink } from '@martelleur.online/webcraft'

import React from 'react'
import { RemoteLink } from '@links/RemoteLink'

describe('RemoteLink', () => {
  let remoteLink: HTMLElement
  const href = 'https://mock.com'
  const title = 'Mock link'
  const dataText = 'Mock text'

  beforeEach(() => {
    render(React.createElement(RemoteLink, { href, title, dataText }))
    remoteLink = screen.getByTestId(CustomRemoteLink.elementName)
  })

  afterEach(() => {
    cleanup()
  })

  it(`Should be a react wrapper for custom element ${CustomRemoteLink.elementName}`, () => {
    expect(remoteLink).toBeInTheDocument()
  })

  it.each([
    { attribute: CustomRemoteLink.elementAttributes.DATA_TEXT, value: dataText },
    { attribute: CustomRemoteLink.elementAttributes.HREF, value: href },
    { attribute: CustomRemoteLink.elementAttributes.TITLE, value: title }
  ])(
    `Should wrap custom component ${CustomRemoteLink.elementName} attributes`,
    ({ attribute, value }) => {
      expect(remoteLink).toHaveAttribute(attribute, value)
    }
  )
})
