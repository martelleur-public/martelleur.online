import React from 'react'
import { cleanup, render, screen } from '@testing-library/react'
import { AppLink as CustomAppLink } from '@martelleur.online/webcraft'

import { AppLink } from '@links/AppLink'

describe('AppLink', () => {
  let appLink: HTMLElement
  const href = 'https://mock.com'
  const title = 'Mock link'
  const dataText = 'Mock text'

  beforeEach(() => {
    render(React.createElement(AppLink, { href, title, dataText }))
    appLink = screen.getByTestId(CustomAppLink.elementName)
  })

  afterEach(() => {
    cleanup()
  })

  it(`Should be a react wrapper for custom element ${CustomAppLink.elementName}`, () => {
    expect(appLink).toBeInTheDocument()
  })

  it.each([
    { attribute: CustomAppLink.elementAttributes.DATA_TEXT, value: dataText },
    { attribute: CustomAppLink.elementAttributes.HREF, value: href },
    { attribute: CustomAppLink.elementAttributes.TITLE, value: title }
  ])(
    `Should wrap custom component ${CustomAppLink.elementName} attributes`,
    ({ attribute, value }) => {
      expect(appLink).toHaveAttribute(attribute, value)
    }
  )
})
