import { render, screen } from '@testing-library/react'
import { ResponsiveDialog as CustomResponsiveDialog } from '@martelleur.online/webcraft'

import React from 'react'
import { ResponsiveDialog } from '@dialogs'

describe('ResponsiveDialog', () => {
  const testCases = [
    { description: 'With true attributes', sectionLines: true, onToogle: () => {} },
    { description: 'With false attributes', sectionLines: false, onToogle: () => {} },
    { description: 'With undefined attributes', sectionLines: undefined, onToogle: () => {} },
  ]

  const childContent = {
    title: 'My Dialog Title',
    subHeader: 'My Dialog SubHeader',
    body: 'My Dialog Body',
    footer: 'My Dialog Footer'
  }

  testCases.forEach(({ description, sectionLines, onToogle }) => {
    describe(description, () => {
      let dialog: HTMLElement

      beforeEach(() => {
        const openMock = jest.fn(() => {})
        jest.spyOn(CustomResponsiveDialog.prototype, 'open').mockImplementation(openMock)
        const dialogElement = React.createElement(
          ResponsiveDialog,
          { 
            sectionLines,
            onToogle
          },
          React.createElement('p', { slot: 'title' }, childContent.title),
          React.createElement('p', { slot: 'sub-header' }, childContent.subHeader),
          React.createElement('p', { slot: 'body' }, childContent.body),
          React.createElement('p', { slot: 'footer' }, childContent.footer)
        )
        render(dialogElement)
        dialog = screen.getByTestId(CustomResponsiveDialog.elementName)
      })
  
      afterEach(() => {
        /**
        * Use  document.body.removeChild(dialog) insteade of clean() due
        * custom dialog element is rendered by document create element not react
        */
        const dialog = document.querySelector('responsive-dialog')
        if (dialog != null) {
          document.body.removeChild(dialog)
        }
      })
    
      it(`Should be a react wrapper for custom element ${CustomResponsiveDialog.elementName}`, () => {
        expect(dialog).toBeInTheDocument()
      })
    
      it.each([
        { attribute: CustomResponsiveDialog.elementAttributes.SECTION_LINES, value: sectionLines }
      ])(
        `Should wrap custom component ${CustomResponsiveDialog.elementName} attributes`,
        ({ attribute, value }) => {
          if (value) {
            expect(dialog).toHaveAttribute(attribute, `${value}`)
          } else {
            expect(dialog).not.toHaveAttribute(attribute)
          }
        }
      )

      it.each([
        { selector: '[slot=title]' },
        { selector: '[slot=sub-header]' },
        { selector: '[slot=body]' },
        { selector: '[slot=footer]' }
      ])(
        `Should render child elements in slots in custom element ${CustomResponsiveDialog.elementName}`,
        ({ selector }) => {
          expect(dialog.querySelector(selector)).toBeInTheDocument()
        }
      )

      it.each([
        { selector: '[slot=title]', text: childContent.title },
        { selector: '[slot=sub-header]', text: childContent.subHeader},
        { selector: '[slot=body]', text: childContent.body },
        { selector: '[slot=footer]', text: childContent.footer }
      ])(
        `Should render child element text content in slots in custom element ${CustomResponsiveDialog.elementName}`,
        ({ selector, text }) => {
          expect(dialog.querySelector(selector)).toHaveTextContent(text)
        }
      )
    })
  })
})
