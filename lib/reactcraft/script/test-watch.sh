#!/bin/bash

main () {
  set -ex
  run_test
}

run_test () {
  jest --clearCache
  jest --watch
}

main
