#!/bin/bash

main () {
  clean_build
  configure_env
  build_library
}

clean_build () {
  rimraf dist
}

configure_env () {
  export NODE_ENV=development  
  node ./script/setup-config.mjs
}

build_library () {
  # Need to compile typescript first before watching
  # Ref: https://www.npmjs.com/package/tsc-alias#add-it-to-your-build-scripts-in-packagejson
  tsc --project tsconfig.esm.json
  tsc --project tsconfig.cjs.json

  concurrently \
    "tsc --project tsconfig.cjs.json --watch" \
    "tsc-alias --project tsconfig.cjs.json --watch" \
    "tsc --project tsconfig.esm.json --watch" \
    "tsc-alias --project tsconfig.esm.json --watch"
}

main
