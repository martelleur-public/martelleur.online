#!/bin/bash

main () {
  clean_build
  build_library_cjs
  build_library_esm
}

clean_build () {
  rimraf dist
}

build_library_cjs () {
  tsc --project tsconfig.cjs.json
  tsc-alias --project tsconfig.cjs.json
}

build_library_esm () {
  tsc --project tsconfig.esm.json
  tsc-alias --project tsconfig.esm.json
}

main
