import React from 'react'

import {
  BasicLayoutSlotType,
  BasicLayout as CustomBasicLayout
} from '@martelleur.online/webcraft'

interface ChildProps {
  slot: BasicLayoutSlotType
}

type ChildElementType = 
  React.ReactElement<ChildProps>[] |
  React.ReactElement<ChildProps>

interface BasicLayoutProps {
  dynamicHeight?: boolean
  children?: ChildElementType
}

export const BasicLayout: React.FC<BasicLayoutProps> = (props) => {
  const attributes = {
    ...(props.dynamicHeight ? { 'dynamic-height': true } : {}),
    ['data-testid']: CustomBasicLayout.elementName
  }

  return React.createElement(CustomBasicLayout.elementName,
    attributes,
    props.children
  )
}
