import React from 'react'

import {
  BasicLayoutSlotType,
  ResponsiveHeader as CustomResponsiveHeader,
  DistributeListenerEvent,
  ResponsiveHeaderSlotType
} from '@martelleur.online/webcraft'

interface ChildProps {
  slot: ResponsiveHeaderSlotType
  // eventType: string
  // eventCallback: () => void
}

type ChildElementType = 
  React.ReactElement<ChildProps>[] |
  React.ReactElement<ChildProps>

interface ResponsiveHeaderProps {
  slot: BasicLayoutSlotType
  style?: React.CSSProperties,
  children?: ChildElementType
}

export const ResponsiveHeader: React.FC<ResponsiveHeaderProps> = (props) => {
  const attributes = {
    slot: props.slot,
    style: { width: '100%' },
    ['data-testid']: CustomResponsiveHeader.elementName
  }

  React.useEffect(() => {
    if (props.children == null) {
      return
    }
    React.Children.forEach(props.children, (child) => {
      if (child?.props == null) {
        return
      }
      const listenerData = extractListenerDataReactEvent(child.props)
      if (listenerData != null) {
        const event = new DistributeListenerEvent({
          slot: child.props.slot,
          eventType: listenerData.eventType,
          eventCallback: listenerData.eventCallback
        })
        document.dispatchEvent(event)
      }
    })
  })

  /**
   * @TODO Improve ugly maybe unreliable solution
   * Ugly but could works. Reliable?
   * Insteade maybe covert all slot element to custme elements and then add
   * them as children...
   * 
   * Check if the prop is an event handler (e.g., onClick, onMouseEnter, etc.)
   * And then extract the real event name.
   */
  const extractListenerDataReactEvent = (childProps: ChildProps):
  {
    eventType: string
    eventCallback: () => void
  } | null => {
    let extractedListenerData = null
    Object.entries(childProps).forEach(([propName, propValue]) => {
      if (propName.startsWith('on') && isThirdCharacterCapital(propName)) {
        const eventType = propName.substring(2).toLowerCase()
        const eventCallback = propValue
        extractedListenerData = {
          eventType,
          eventCallback
        }
      }
    })
    return extractedListenerData
  }

  const isThirdCharacterCapital = (str: string): boolean => {
    if (str.slice(0, 2) === 'on' && str.length >= 3) {
      return str[2] === str[2].toUpperCase()
    }
    return false
  }

  return React.createElement(
    CustomResponsiveHeader.elementName,
    attributes,
    props.children
  )
}
