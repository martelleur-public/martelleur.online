import React from 'react'
import { createRoot } from 'react-dom/client'

import {
  ResponsiveDialog as CustomResponsiveDialog,
  ResponsiveDialogCloseEvent,
  ResponsiveDialogAttributeType,
  ResponsiveDialogSlotType
} from '@martelleur.online/webcraft'

interface ChildProps {
  slot: ResponsiveDialogSlotType
}

type ChildElementType = 
  React.ReactElement<ChildProps>[] |
  React.ReactElement<ChildProps>

interface ResponsiveDialogProps {
  sectionLines?: boolean
  children?: ChildElementType
  onToogle: () => void
}

export const ResponsiveDialog: React.FC<ResponsiveDialogProps> = ({
  sectionLines,
  children,
  onToogle
}) => {
  const dialogRef = React.useRef<CustomResponsiveDialog | null>(null)
  const attributes = getCustomAttributes(sectionLines)

  React.useEffect(() => {
    const toogle = () => { onToogle() }
    document.addEventListener(ResponsiveDialogCloseEvent.eventName, toogle)
    return () => {
      document.removeEventListener(ResponsiveDialogCloseEvent.eventName, toogle)
    }
  }, [onToogle])

  React.useEffect(() => {
    if (document.querySelector(CustomResponsiveDialog.elementName) == null) {
      const dialog = new CustomResponsiveDialog()
      dialogRef.current = dialog
      updateDialogElementAttributes(attributes, dialog)
      openDialog(dialog)
    }
  }, [attributes])

  React.useEffect(() => {
    if (children == null) {
      return
    }
    React.Children.forEach(children, (child) => {
      if (dialogRef.current == null) {
        return
      }
      addChildToParentSlot(dialogRef.current, child)
    })
  }, [children])

  return null
}

function getCustomAttributes (
  sectionLines?: boolean
): ResponsiveDialogAttributeType {
  return {
    ...(sectionLines != null && sectionLines === true ? { 'section-lines': true } : {})
  }
}

function updateDialogElementAttributes (
  attributes: ResponsiveDialogAttributeType,
  dialog: CustomResponsiveDialog
): void {
  for (const [attribute, value] of Object.entries(attributes)) {
    if (value === true) {
      dialog.setAttribute(attribute, `${value}`)
    }
  }
  dialog.setAttribute('data-testid', CustomResponsiveDialog.elementName)
}

function openDialog (dialog: CustomResponsiveDialog) {
  document.body.appendChild(dialog)
  dialog.open()
}

function addChildToParentSlot (
  parent: CustomResponsiveDialog,
  child?: React.ReactElement<{ slot: string}>): void {
  if (child == null) {
    return
  }
  const slotName = child.props.slot
  if (isChildInSlot(slotName, parent)) {
    return
  }
  const childContainer = document.createElement('div')
  childContainer.setAttribute('slot', slotName)
  childContainer.style.width = '100%'
  createRoot(childContainer).render(child)
  parent.appendChild(childContainer)
}

function isChildInSlot (
  slotName: string,
  parent: CustomResponsiveDialog): boolean {
  const child = parent.querySelector(`[slot=${slotName}]`)
  return child != null
}
