import React from 'react'

import { RemoteLink as CustomRemoteLink } from '@martelleur.online/webcraft'

interface RemoteLinkProps {
  href: string
  title: string
  dataText: string
}

export const RemoteLink: React.FC<RemoteLinkProps> = (props) => {
  return React.createElement(CustomRemoteLink.elementName, {
    href: props.href,
    title: props.title,
    ['data-text']: props.dataText,
    ['data-testid']: CustomRemoteLink.elementName,
  })
}
