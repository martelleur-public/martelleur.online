import React from 'react'

import { AppLink as CustomAppLink } from '@martelleur.online/webcraft'

interface AppLinkProps {
  href: string
  title: string
  dataText: string
}

export const AppLink: React.FC<AppLinkProps> = (props) => {
  return React.createElement(CustomAppLink.elementName, {
    href: props.href,
    title: props.title,
    ['data-text']: props.dataText,
    ['data-testid']: CustomAppLink.elementName
  })
}
