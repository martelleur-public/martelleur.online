import React from 'react'
import { NewTabIconWhite as CustomNewTabIconWhite } from '@martelleur.online/webcraft'

export const NewTabIconWhite: React.FC = () => {
  return React.createElement(
    CustomNewTabIconWhite.elementName,
    { ['data-testid']: CustomNewTabIconWhite.elementName }
  )
}
