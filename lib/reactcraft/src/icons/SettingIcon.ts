import React from 'react'
import {
  SettingIcon as CustomSettingIcon
} from '@martelleur.online/webcraft'

interface SettingIconProps {
  style?: React.CSSProperties
}

export const SettingIcon: React.FC<SettingIconProps> = (props):
React.ReactElement<{ style?: React.CSSProperties }> => {
  const attributes = {
    ...(props.style != null ? { style: props.style } : {}),
    ['data-testid']: CustomSettingIcon.elementName
  }

  return React.createElement(
    CustomSettingIcon.elementName,
    attributes
  )
}
