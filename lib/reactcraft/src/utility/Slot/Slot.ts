import React from 'react'

interface SlotProps {
  name: string
  children?: React.ReactElement[] | React.ReactElement
  style?: React.CSSProperties
}

export const Slot: React.FC<SlotProps> = ({ name, children, style }) => {
  const defaultStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 0,
    padding: 0,
    width: '100%',
    height: '100%',
    boxSizing: 'border-box',
  }

  const mergedStyle = {
    ...defaultStyle,
    ...style
  }
  
  return React.createElement(
    'div',
    { 
      slot: name,
      style: mergedStyle
    },
    children
  )
}
