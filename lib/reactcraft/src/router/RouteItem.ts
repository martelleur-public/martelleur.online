import React from 'react'
import { createRoot } from 'react-dom/client'
import {
  customLogger,
  RouteItem as CustomRouteItem
} from '@martelleur.online/webcraft'

import { createElementNameFromUrlPath, isValidCustomElementName } from './utility'

export interface RouteItemProps {
  path: string
  component: React.ReactElement,
  default?: boolean
}

export const RouteItem: React.FC<RouteItemProps> = (props) => {
  const elementName = createElementNameFromUrlPath(props.path)

  if (!isValidCustomElementName(elementName)) {
    throw new Error(`Path must construct a valid custom element name ${elementName}`)
  }
  
  createAndDefineRouteElement(elementName, (root) => {
    root.classList.add('page-container')
    createRoot(root).render(props.component)
  })

  return React.createElement(CustomRouteItem.elementName, {
    path: props.path,
    component: elementName,
    ['data-testid']: CustomRouteItem.elementName
  })
}

function createAndDefineRouteElement (
    elementName: string,
    renderFunction: (root: HTMLElement) => void) {
    class RoutePage extends HTMLElement {
      constructor () {
        super()
      }
  
      connectedCallback (): void {
        renderFunction(this)
      }
    }
  
    if (customElements.get(elementName)) {
      customLogger(`${elementName} is already defined.`)
    } else {
      customElements.define(elementName, RoutePage)
    }
  }
  
