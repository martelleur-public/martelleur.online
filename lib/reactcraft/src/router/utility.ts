export function createElementTagFromUrlPath (path: string): string {
  const elementName = createElementNameFromUrlPath(path)
  return `<${elementName}></${elementName}>`
}

export function createElementNameFromUrlPath (path: string): string {
  const validPath = getValidPath(path)
  return `${validPath}-page`
}

function getValidPath (path: string): string {
  let validPath = path.toLowerCase()
  if (path === '' || path === '/') {
    validPath = 'index'
  }
  return validPath
}

export function isValidCustomElementName(name: string): boolean {
  const pattern = /^[a-z][a-z0-9]*(-[a-z0-9]+)*$/
  return pattern.test(name)
}
