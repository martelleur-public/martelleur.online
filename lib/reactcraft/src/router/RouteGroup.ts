import React from 'react'

import { RouteGroup as CustomRouteGroup } from '@martelleur.online/webcraft'
import { RouteItemProps } from './RouteItem'

export type RouteGroupChildType = 
  React.ReactElement<RouteItemProps>[] |
  React.ReactElement<RouteItemProps> |
  React.ReactElement<RouteGroupProps>[] |
  React.ReactElement<RouteGroupProps>

export interface RouteGroupProps {
  path: string
  children?: RouteGroupChildType
}

export const RouteGroup: React.FC<RouteGroupProps> = ({ children = [], path }) => {
  return React.createElement(CustomRouteGroup.elementName, {
    path,
    ['data-testid']: CustomRouteGroup.elementName,
    children
  })
}
