import React from 'react'
import {
  PageTransition,
  PageTransitionType,
  AppRouter as CustomAppRouter
} from '@martelleur.online/webcraft'
import { RouteGroupProps } from './RouteGroup'

export type AppRouterChildType = 
  React.ReactElement<RouteGroupProps>[] |
  React.ReactElement<RouteGroupProps>

interface RouterProps {
  slot?: string
  transition?: PageTransitionType,
  children?: AppRouterChildType
}

export { PageTransition }

export const AppRouter: React.FC<RouterProps> = ({
  children = [],
  transition,
  slot
}) => {
  const elementName = CustomAppRouter.elementName

  const attributes = {
    slot,
    transition,
    ['data-testid' ]: elementName
  }

  return (
    React.createElement(
      elementName,
      attributes,
      children
    )
  )
}
