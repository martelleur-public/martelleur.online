/**
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

import type { JestConfigWithTsJest } from 'ts-jest'

const config: JestConfigWithTsJest = {
  preset: 'ts-jest',
  clearMocks: true,
  restoreMocks: true,
  testEnvironment: 'jsdom',
  testPathIgnorePatterns: [
    "/node_modules/",
    "/dist/"
  ],
  transform: {
    '^.+\\.tsx?$': [
      'ts-jest',
      {
        tsconfig: 'tsconfig.json',
      },
    ],
  },
  setupFilesAfterEnv: ['<rootDir>/test/jest.setup.ts'],
  moduleNameMapper: {
    "^@dialogs$": "<rootDir>/src/dialogs/index",
    "^@icons$": "<rootDir>/src/icons/index",
    "^@layouts$": "<rootDir>/src/layouts/index",
    "^@links$": "<rootDir>/src/links/index",
    "^@router$": "<rootDir>/src/router/index",
    "^@dialogs/(.*)$": "<rootDir>/src/dialogs/$1",
    "^@icons/(.*)$": "<rootDir>/src/icons/$1",
    "^@layouts/(.*)$": "<rootDir>/src/layouts/$1",
    "^@links/(.*)$": "<rootDir>/src/links/$1",
    "^@router/(.*)$": "<rootDir>/src/router/$1",
  }
}

export default config
