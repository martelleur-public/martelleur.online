import fs from 'fs'

// Determine the environment
const environment = process.env.NODE_ENV ?? 'development'

// Copy the correct config file
const configFile = `config.${environment}.ts`
fs.copyFileSync(`src/configs/${configFile}`, 'src/configs/config.ts')

console.log(`Setup config for ${environment} environment.`)
