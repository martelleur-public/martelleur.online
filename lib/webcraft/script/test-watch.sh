#!/bin/bash

main () {
  configure_env
  run_test
}

configure_env () {
  export NODE_ENV=test
  node ./script/setup-config.mjs    
}

run_test () {
  jest --clearCache
  jest --watch
}

main
