/**
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

import type { JestConfigWithTsJest } from 'ts-jest'

const config: JestConfigWithTsJest = {
  preset: 'ts-jest',
  clearMocks: true,
  restoreMocks: true,
  testEnvironment: 'jsdom',
  testPathIgnorePatterns: [
    "/node_modules/",
    "/dist/",
    "/src/configs"
  ],
  transform: {
    '^.+\\.ts$': [
      'ts-jest',
      {
        tsconfig: 'tsconfig.json',
      },
    ],
  },
  setupFilesAfterEnv: ['<rootDir>/test/jest.setup.ts'],
  moduleNameMapper: {
    "^@components$": "<rootDir>/src/components/index",
    "^@configs$": "<rootDir>/src/configs/index",
    "^@router$": "<rootDir>/src/router/index",
    "^@store$": "<rootDir>/src/store/index",
    "^@styles$": "<rootDir>/src/styles/index",
    "^@themes$": "<rootDir>/src/themes/index",
    "^@utility$": "<rootDir>/src/utility/index",
    "^@components/(.*)$": "<rootDir>/src/components/$1",
    "^@configs/(.*)$": "<rootDir>/src/configs/$1",
    "^@router/(.*)$": "<rootDir>/src/router/$1",
    "^@store/(.*)$": "<rootDir>/src/store/$1",
    "^@styles/(.*)$": "<rootDir>/src/styles/$1",
    "^@themes/(.*)$": "<rootDir>/src/themes/$1",
    "^@utility/(.*)$": "<rootDir>/src/utility/$1",
  }
}

export default config
