import { config } from '../configs/config'

export function customLogger (...messages: unknown[]): void {
  if (config.environment === 'development') {
    console.log(...messages)
  }
}

export function testLogger (...messages: unknown[]): void {
  if (config.environment === 'test') {
    console.log(...messages)
  }
}

export function productionLogger (...messages: unknown[]): void {
  if (config.environment === 'production') {
    console.log(...messages)
  }
}
