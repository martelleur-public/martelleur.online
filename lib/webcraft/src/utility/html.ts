import { customLogger } from './custom-logger'

export function assertElementExist<T> (element: T | null): T {
  if (element === null) {
    customLogger(element)
    throw new ElementDoesNotExistError('Expected the element to exist, but it does not.')
  }
  return element
}

export class ElementDoesNotExistError extends Error {
  constructor (message: string) {
    super(message)
    this.name = 'ElementDoesNotExistError'
  }
}

export function sanitizeHTMLInput (data: unknown): string {
  let stringData: string

  if (typeof data === 'string') {
    stringData = data
  } else if (data === null || data === undefined) {
    stringData = ''
  } else if (typeof data === 'object') {
    stringData = JSON.stringify(data)
  } else {
    stringData = String(data)
  }

  const div = document.createElement('div')
  div.appendChild(document.createTextNode(stringData))
  return div.innerHTML
}

export function updateShadowRootContent (
  root: ShadowRoot,
  element: HTMLElement
): void {
  while (root.firstElementChild != null) {
    root.removeChild(root.firstElementChild)
  }
  if (element instanceof HTMLTemplateElement) {
    root.appendChild(element.content.cloneNode(true))
  } else {
    root.appendChild(element)
  }
}
