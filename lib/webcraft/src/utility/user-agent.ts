type UserAgentName =
| 'Firefox'
| 'Chrome'
| 'Safari'
| 'Unknown'

export function getUserAgentName (): UserAgentName {
  const userAgent = navigator.userAgent
  if (userAgent.includes('Firefox')) {
    return 'Firefox'
  } else if (userAgent.includes('Chrome')) {
    return 'Chrome'
  } else if (userAgent.includes('Safari')) {
    return 'Safari'
  } else {
    return 'Unknown'
  }
}
