import { customLogger } from './custom-logger'

type CustomElementDecorator = (constructor: CustomElementConstructor) => void

interface CustomElementConstructorWithName extends CustomElementConstructor {
  elementName: string
}

type ElementDecorator = (constructor: CustomElementConstructorWithName) => void

/**
 * @DEPRECATDED Use DefineElement instead and extend web component
 * class with type NamedHTMLElement imported from this library insteade of
 * type HTMLElement.
 */
export function DefineCustomElement (elementName: string): CustomElementDecorator {
  const decorator = function (constructor: CustomElementConstructor): void {
    (constructor as any).elementName = elementName
    if (customElements.get(elementName)) {
      customLogger(`${elementName} have already been defined`)
    } else {
      customElements.define(elementName, constructor)
    }
  }
  return decorator
}

/**
 * Decorator for custome elements that automatically
 * define the element with a given name.
 */
export function DefineElement (elementName: string): ElementDecorator {
  const decorator = function (
    constructor: CustomElementConstructorWithName
  ): void {
    constructor.elementName = elementName
    if (customElements.get(elementName)) {
      customLogger(`${elementName} have already been defined`)
    } else {
      customElements.define(elementName, constructor)
    }
  }
  return decorator
}

interface ShadowRootOptions {
  mode?: 'open' | 'closed'
  template: HTMLTemplateElement
}

/**
 * @DEPRECATDED Use CreateShadowElement instead and extend web component
 * class with type NamedHTMLElement imported from this library insteade of
 * type HTMLElement
 */
export function CreateAndDefineShadowElement (
  elementName: string,
  shadowRootOptions: ShadowRootOptions
): CustomElementDecorator {
  const decorator = function (constructor: CustomElementConstructor): void {
    (constructor as any).elementName = elementName

    if (customElements.get(elementName)) {
      customLogger(`${elementName} have already been defined`)
    } else {
      customElements.define(elementName, class extends constructor {
        constructor (...args: any[]) {
          super(...args)
          const root = this.attachShadow({ mode: shadowRootOptions.mode ?? 'open' })
          root.appendChild(shadowRootOptions.template.content.cloneNode(true))
        }
      })
    }
  }
  return decorator
}

/**
 * Decorator for custome elements that automatically
 * create with an attached shadow host and define the element.
 */
export function CreateShadowElement (
  elementName: string,
  shadowRootOptions: ShadowRootOptions
): ElementDecorator {
  const decorator = function (
    constructor: CustomElementConstructorWithName
  ): void {
    constructor.elementName = elementName

    if (customElements.get(elementName)) {
      customLogger(`${elementName} have already been defined`)
    } else {
      customElements.define(elementName, class extends constructor {
        constructor (...args: any[]) {
          super(...args)
          const root = this.attachShadow({ mode: shadowRootOptions.mode ?? 'open' })
          root.appendChild(shadowRootOptions.template.content.cloneNode(true))
        }
      })
    }
  }
  return decorator
}
