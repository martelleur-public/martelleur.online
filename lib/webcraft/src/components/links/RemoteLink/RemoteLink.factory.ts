import { ViewModelFactory } from '@components/core'
import { IRemoteLink, IRemoteLinkVM } from './RemoteLink.type'
import { RemoteLinkVM } from './RemoteLink.vm'

export const createViewModel:
ViewModelFactory<IRemoteLink, IRemoteLinkVM> =
(remoteLink: IRemoteLink): IRemoteLinkVM => {
  return new RemoteLinkVM(remoteLink)
}
