import { NewTabIcon } from '@components/icons'

export interface IRemoteLink extends HTMLElement {
  link: HTMLAnchorElement
  container: HTMLDivElement
  newTabIcon: NewTabIcon
}

export interface IRemoteLinkVM {
  setTitleAttribute: (newValue: string) => void
  setHrefAttribute: (newValue: string) => void
  setDataTextAttribute: (newValue: string) => void
  handleEnterHover: () => void
  handleExitHover: () => void
}
