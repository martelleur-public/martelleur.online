import { IRemoteLink, IRemoteLinkVM } from './RemoteLink.type'

export class RemoteLinkVM implements IRemoteLinkVM {
  constructor (private readonly view: IRemoteLink) {
    this.handleEnterHover = this.handleEnterHover.bind(this)
    this.handleExitHover = this.handleExitHover.bind(this)
  }

  setTitleAttribute (newValue: string): void {
    this.view.link.setAttribute('title', newValue)
  }

  setHrefAttribute (newValue: string): void {
    this.view.link.setAttribute('href', newValue)
  }

  setDataTextAttribute (newValue: string): void {
    const textNode = document.createTextNode(newValue)
    this.view.link.insertBefore(textNode, this.view.link.firstChild)
  }

  handleEnterHover (): void {
    this.view.newTabIcon.setSelected()
  }

  handleExitHover (): void {
    this.view.newTabIcon.removeSelected()
  }
}
