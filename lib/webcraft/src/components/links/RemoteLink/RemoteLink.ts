import { type NewTabIcon } from '../../icons'
import {
  type IBaseVM,
  NamedHTMLElement,
  createBaseViewModel
} from '@components/core'
import { remoteLinkTemplate } from './RemoteLink.template'
import {
  DefineElement,
  assertElementExist,
} from '@utility'
import { attributeRemoteLink as attributes } from './RemoteLink.attribute'
import { IRemoteLink, IRemoteLinkVM } from './RemoteLink.type'
import { createViewModel } from './RemoteLink.factory'

export const nameRemoteLinkElement = 'remote-link'

@DefineElement(nameRemoteLinkElement)
export class RemoteLink
  extends NamedHTMLElement
  implements IRemoteLink {
  static readonly elementAttributes = attributes
  readonly link: HTMLAnchorElement
  readonly container: HTMLDivElement
  readonly newTabIcon: NewTabIcon
  private readonly _root: ShadowRoot
  private readonly _vm: IRemoteLinkVM
  private readonly _baseVm: IBaseVM

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    this._root.appendChild(remoteLinkTemplate.content.cloneNode(true))
    this.link = assertElementExist(this._root.querySelector<HTMLAnchorElement>('#remote-link'))
    this.container = assertElementExist(this._root.querySelector<HTMLDivElement>('#container'))
    this.newTabIcon = assertElementExist(this._root.querySelector<NewTabIcon>('new-tab-icon'))
    this._vm = createViewModel(this)
    this._baseVm = createBaseViewModel()
  }

  static get observedAttributes (): string[] {
    return Object.values(attributes)
  }

  attributeChangedCallback (
    name: string,
    oldValue: string,
    newValue: string
  ): void {
    if (this._baseVm.isAttributeValueSame(newValue, oldValue)) return
    switch (name) {
    case attributes.TITLE: this._vm.setTitleAttribute(newValue); break
    case attributes.HREF: this._vm.setHrefAttribute(newValue); break
    case attributes.DATA_TEXT: this._vm.setDataTextAttribute(newValue); break
    default: this._baseVm.handleDefaultAttributeChg(newValue, oldValue)
    }
  }

  connectedCallback (): void {
    this.container.addEventListener('mouseenter', this._vm.handleEnterHover)
    this.container.addEventListener('mouseleave', this._vm.handleExitHover)
  }
}
