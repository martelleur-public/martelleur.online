import '@components/icons'

export const remoteLinkTemplate = document.createElement('template')
remoteLinkTemplate.innerHTML = `
<div id="container">
    <a
        id="remote-link"
        title="Link title"
        href=""
        target="_blank"
        data-test="remote-link">
        <new-tab-icon></new-tab-icon>
    </a>
    <div id="link-decorator"></div>
</div>
<style>
*, *::before, *::after {
    box-sizing: border-box;
}

:host {
    font-family: var(--FONT_PRIMARY);
}

#container {
    display: inline-flex;
    flex-direction: column;
    justify-content: center;
    align-items: start;
    width: fit-content;
}

#link-decorator {
    width: 0;
    height: 2px;
    background-color: var(--TEXT_HOVER);
    transition: width 0.2s ease-out;
    margin: 0 4px;
}

a {
    padding: 1px 8px;
    text-decoration: none;
    color: var(--TEXT_PRIMARY);
}

a:hover {
    text-decoration: none;
    color: var(--TEXT_HOVER);
}

a:hover + #link-decorator {
    width: calc(100% - 8px);
}

new-tab-icon {
    display: inline-block;
    vertical-align: middle;
    margin-bottom: 6px;
}
</style>
`
