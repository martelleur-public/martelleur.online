export const attributeAppLink = {
  DATA_TEXT: 'data-text',
  HREF: 'href',
  TITLE: 'title'
} as const
