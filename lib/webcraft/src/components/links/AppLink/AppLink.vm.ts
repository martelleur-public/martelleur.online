import { IAppLinkVM, type IAppLink } from './AppLink.type'

export class AppLinkVM implements IAppLinkVM {
  constructor (private readonly view: IAppLink) {}

  setTitleAttribute (newValue: string): void {
    this.view.link.setAttribute('title', newValue)
  }

  setHrefAttribute (newValue: string): void {
    this.view.link.setAttribute('href', newValue)
  }

  setDataTextAttribute (newValue: string): void {
    this.view.link.setAttribute('data-text', newValue)
  }
}
