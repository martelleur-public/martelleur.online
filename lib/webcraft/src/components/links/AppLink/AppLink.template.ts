import '@router/components/RouterLink/RouterLink'

export const appLinkTemplate = document.createElement('template')
appLinkTemplate.innerHTML = `
<div id="container">
    <router-link
        id="router-link"
        title="Link title"
        href=""
        data-text="app-link"
        data-test="router-link">
    </router-link>
    <div id="link-decorator"></div>
</div>
<style>
*, *::before, *::after {
    box-sizing: border-box;
}

:host {
    font-family: var(--FONT_PRIMARY);
}

#container {
    display: inline-flex;
    flex-direction: column;
    justify-content: center;
    align-items: start;
    width: fit-content;
}

router-link {
    padding: 8px;
}

#link-decorator {
    width: 0;
    height: 2px;
    background-color: var(--TEXT_HOVER, #6f00ff);
    transition: width 0.2s ease-out;
    margin: 0 4px;
}

router-link:hover + #link-decorator {
    width: calc(100% - 8px);
}
</style>
`
