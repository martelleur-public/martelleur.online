import { ViewModelFactory } from '@components/core'
import { IAppLink, IAppLinkVM } from './AppLink.type'
import { AppLinkVM } from './AppLink.vm'

export const createViewModel:
ViewModelFactory<IAppLink, IAppLinkVM> = (view: IAppLink):
IAppLinkVM => {
  return new AppLinkVM(view)
}
