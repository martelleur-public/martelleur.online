import { RouterLink } from '@router'

export interface IAppLink extends HTMLElement {
  link: RouterLink
}

export interface IAppLinkVM {
  setTitleAttribute: (newValue: string) => void
  setHrefAttribute: (newValue: string) => void
  setDataTextAttribute: (newValue: string) => void
}
