import {
  DefineElement,
  assertElementExist
} from '@utility'
import type { RouterLink } from '@router'
import { appLinkTemplate } from './AppLink.template'
import { attributeAppLink as attributes } from './AppLink.attribute'
import {
  type IBaseVM,
  NamedHTMLElement,
  createBaseViewModel
} from '@components/core'
import { IAppLinkVM, type IAppLink } from './AppLink.type'
import { createViewModel } from './AppLink.factory'

export const nameAppLinkElement = 'app-link'

@DefineElement(nameAppLinkElement)
export class AppLink
  extends NamedHTMLElement
  implements IAppLink {
  static readonly elementAttributes = attributes
  readonly link: RouterLink
  private readonly _root: ShadowRoot
  private readonly _vm: IAppLinkVM
  private readonly _baseVm: IBaseVM

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    this._root.appendChild(appLinkTemplate.content.cloneNode(true))
    this.link = assertElementExist(this._root.querySelector<RouterLink>('#router-link'))
    this._vm = createViewModel(this)
    this._baseVm = createBaseViewModel()
  }

  static get observedAttributes (): string[] {
    return Object.values(attributes)
  }

  attributeChangedCallback (
    name: string,
    oldValue: string,
    newValue: string
  ): void {
    if (this._baseVm.isAttributeValueSame(newValue, oldValue)) return
    switch (name) {
    case attributes.TITLE: this._vm.setTitleAttribute(newValue); break
    case attributes.HREF: this._vm.setHrefAttribute(newValue); break
    case attributes.DATA_TEXT: this._vm.setDataTextAttribute(newValue); break
    default: this._baseVm.handleDefaultAttributeChg(newValue, oldValue)
    }
  }
}
