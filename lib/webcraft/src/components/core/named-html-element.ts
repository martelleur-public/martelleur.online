/**
 * Use with decorator DefineElement
 * and CreateShadowElement.
 *
 * @remarks @DEPREACTE Works in client, client test, and lib
 * environment but not in library testing environment.
 */

export class NamedHTMLElement extends HTMLElement {
  static readonly elementName: string
}
