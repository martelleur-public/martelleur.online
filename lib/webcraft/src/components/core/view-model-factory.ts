export interface ViewModelFactory<View, ViewModel> {
  (view: View): ViewModel
}
