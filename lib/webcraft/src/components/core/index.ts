export { NamedHTMLElement } from './named-html-element'
export { IBaseVM } from './base-vm'
export { createBaseViewModel } from './base-vm-factory'
export { ViewModelFactory } from './view-model-factory'
