import { customLogger } from '@utility'

export interface IBaseVM {
  isAttributeValueSame: (newValue: string, oldValue: string) => boolean
  handleDefaultAttributeChg: (newValue: string, oldValue: string) => void
}

export class BaseVM implements IBaseVM {
  isAttributeValueSame (newValue: string, oldValue: string): boolean {
    return newValue === oldValue
  }

  handleDefaultAttributeChg (newValue: string, oldValue: string): void {
    customLogger(`Not a valid attribute, old value:
      Old attribute value', ${oldValue}
      New attribute value', ${newValue}`)
  }
}
