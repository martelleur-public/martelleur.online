import { BaseVM, IBaseVM } from './base-vm'

export function createBaseViewModel (): IBaseVM {
  return new BaseVM()
}
