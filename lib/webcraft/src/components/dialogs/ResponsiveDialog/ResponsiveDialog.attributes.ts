export const attributeResponsiveDialog = {
  SECTION_LINES: 'section-lines'
} as const

export interface ResponsiveDialogAttributeType {
  [key: string]: boolean | undefined
}
