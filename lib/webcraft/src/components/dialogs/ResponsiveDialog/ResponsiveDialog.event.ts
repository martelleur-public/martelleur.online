export class ResponsiveDialogCloseEvent extends CustomEvent<null> {
  static readonly eventName = 'on-close-responsive-dialog'

  constructor () {
    super(ResponsiveDialogCloseEvent.eventName, {
      bubbles: true,
      cancelable: true
    })
  }
}
