import '../../icons/CloseIcon/CloseIcon'
import { responsiveDialogSlot as Slot } from './ResponsiveDialog.slots'

export const responsiveDialogTemplate = document.createElement('template')

responsiveDialogTemplate.innerHTML = `
<dialog class="scroll">
    <div class="header">
        <slot name="${Slot.TITLE}">Add title...</slot>
        <close-icon
            id="close"
            data-test="close"></close-icon>
    </div>
    <div class="${Slot.SUB_HEADER} hide">
        <slot name="${Slot.SUB_HEADER}">Add sub header...</slot>
    </div>
    <hr />
    <div class="${Slot.BODY} hide">
        <slot name="${Slot.BODY}">Add body...</slot>
    </div>
    <hr />
    <div class="${Slot.FOOTER} hide">
        <slot name="${Slot.FOOTER}">Add footer...</slot>
    </div>
</dialog>
<style>
:host {
    display: block;
    color: var(--TEXT_PRIMARY);
    background-color: var(--BACKGROUND_LIGHT_X);
}

dialog {
    min-width: 50vw;
    max-width: 90vw;
    max-height: 70vh;
    max-height: 70vh;
    padding: 0 20px 20px 20px;
    background: var(--BACKGROUND_DARK);
    color: var(--TEXT_PRIMARY);
    border: 3px solid var(--BORDER_LIGHT_X);
    outline: none;
    border-radius: 8px;
}

hr {
    border: 1px solid var(--BORDER_LIGHT_X, white);
    display: var(--section-lines, none);
}

.hide {
    display: none !important;
}

.header {
    position: sticky;
    top: 0;
    left: 0;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    height: 64px;
    background: var(--BACKGROUND_DARK);
    border-bottom: 2px solid var(--BORDER_LIGHT_X, white);
    margin-bottom: 8px;
}

.${Slot.BODY}
.${Slot.SUB_HEADER},
.${Slot.FOOTER} {
    width: 100%;
}

.${Slot.BODY} {
    max-height: calc(70vh - 74px - 4px);
}

.scroll::-webkit-scrollbar {
    width: 16px; /* Width of the vertical scrollbar */
    height: 16px; /* Width of horizontal scrollbar */
}

.scroll::-webkit-scrollbar-track {
    background: var(--BACKGROUND_LIGHT_X, white); /* Color of the track  */
}

.scroll::-webkit-scrollbar-thumb {
    background: var(--BACKGROUND_LIGHT, black); /* Color of the scroll handle */
    border-radius: 8px; /* Roundness of the scroll handle */
}

.scroll::-webkit-scrollbar-thumb:hover {
    background: var(--BACKGROUND_MEDIUM, black); /* Color of the scroll handle when hovered over */
}

@media (max-width: 700px) {
    dialog {
        width: 80vw;
    }
}
</style>
`
