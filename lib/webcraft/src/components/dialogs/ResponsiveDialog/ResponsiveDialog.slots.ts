export const responsiveDialogSlot = {
  TITLE: 'title',
  SUB_HEADER: 'sub-header',
  BODY: 'body',
  FOOTER: 'footer'
} as const

export type ResponsiveDialogSlotType = typeof responsiveDialogSlot[
  keyof typeof responsiveDialogSlot
]
