import {
  customLogger,
  DefineElement,
  assertElementExist
} from '@utility'

import { type CloseIcon } from '../../icons/CloseIcon/CloseIcon'
import { responsiveDialogTemplate } from './ResponsiveDialog.template'
import { attributeResponsiveDialog as attribute } from './ResponsiveDialog.attributes'
import { ResponsiveDialogCloseEvent } from './ResponsiveDialog.event'

export const nameResponsiveDialog = 'responsive-dialog'

@DefineElement(nameResponsiveDialog)
export class ResponsiveDialog extends HTMLElement {
  static readonly elementName = nameResponsiveDialog
  static readonly elementAttributes = attribute
  private readonly _root: ShadowRoot
  private readonly _dialog: HTMLDialogElement
  private readonly _close: CloseIcon
  private readonly _slots: NodeListOf<HTMLSlotElement>

  constructor () {
    super()
    this._root = this.attachShadow({ mode: 'open' })
    this._root.appendChild(responsiveDialogTemplate.content.cloneNode(true))
    this._dialog = assertElementExist(this._root.querySelector('dialog'))
    this._close = assertElementExist(this._root.querySelector('#close'))
    this._slots = this._root.querySelectorAll('slot')
    this._bindEventHandlers()
  }

  private _bindEventHandlers (): void {
    this._handleClose = this._handleClose.bind(this)
    this._handleDisplaySlot = this._handleDisplaySlot.bind(this)
    this.close = this.close.bind(this)
    this.open = this.open.bind(this)
  }

  static get observedAttributes (): string[] {
    return Object.values(attribute)
  }

  attributeChangedCallback (name: string, oldValue: string, newValue: string): void {
    switch (name) {
    case attribute.SECTION_LINES:
      this._setHrLinesVisisbility()
      break
    default:
      customLogger(`Not a valid attribute, old value:
        Old attribute value', ${oldValue}
        New attribute value', ${newValue}`)
    }
  }

  _setHrLinesVisisbility (): void {
    if (this.hasAttribute(attribute.SECTION_LINES)) {
      this.style.setProperty(`--${attribute.SECTION_LINES}`, 'block')
    } else {
      this.style.setProperty(`--${attribute.SECTION_LINES}`, 'none')
    }
  }

  connectedCallback (): void {
    this._close.addEventListener('click', this._handleClose)
    this._slots.forEach(slot => {
      slot.addEventListener('slotchange', () => {
        this._handleDisplaySlot(slot)
      })
    })
  }

  private _handleDisplaySlot (slot: HTMLSlotElement): void {
    const slotContainer = this._root.querySelector(`.${slot.getAttribute('name')}`)
    slotContainer?.classList.remove('hide')
  }

  private _handleClose (): void {
    this.close()
  }

  open (): void {
    this._dialog.showModal()
  }

  close (): void {
    const event = new ResponsiveDialogCloseEvent()
    window.document.dispatchEvent(event)
    this._dialog.close()
    this.remove()
  }
}
