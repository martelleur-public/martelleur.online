import { CreateAndDefineShadowElement } from '@utility'

import { hamburgerIconTemplate as template } from './HamburgerIcon.template'

export const nameHamburgerIcon = 'hamburger-icon'

@CreateAndDefineShadowElement(nameHamburgerIcon, { template })
export class HamburgerIcon extends HTMLElement {}
