export const hamburgerIconTemplate = document.createElement('template')
hamburgerIconTemplate.innerHTML = `
<svg
  id="hamburger-icon-light"
  width="28"
  height="36"
  xmlns="http://www.w3.org/2000/svg"
  viewBox="0 0 448 512">
  <!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
  <path
    fill="var(--TEXT_PRIMARY)"
    d="M0 96C0 78.3 14.3 64 32 64H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 128 0 113.7 0 96zM0 256c0-17.7 14.3-32 32-32H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32zM448 416c0 17.7-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32H416c17.7 0 32 14.3 32 32z"
  />
</svg>
<style>
#hamburger-icon-light {
    display: flex;
    justify-content: center;
    align-items: center;
    background: var(--BACKGROUND_DARK);
    box-sizing: border-box;
    transform: scale(1.5);
    padding: 4px;
}

#hamburger-icon-light:hover {
    background-color: var(--BACKGROUND_LIGHT);
    cursor: pointer;
}
</style>
`
