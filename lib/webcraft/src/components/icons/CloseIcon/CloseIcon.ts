import { CreateAndDefineShadowElement } from '@utility'

import { closeIconTemplate as template } from './CloseIcon.template'

export const nameCloseIcon = 'close-icon'

@CreateAndDefineShadowElement(nameCloseIcon, { template })
export class CloseIcon extends HTMLElement {}
