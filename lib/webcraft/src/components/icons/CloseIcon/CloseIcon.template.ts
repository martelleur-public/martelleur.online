export const closeIconTemplate = document.createElement('template')
closeIconTemplate.innerHTML = `
<svg
  id="close-icon"
  xmlns="http://www.w3.org/2000/svg"
  width="28"
  height="36"
  viewBox="0 0 384 512">
  <!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
  <path
    fill="var(--TEXT_PRIMARY)"
    d="M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z"
  />
</svg>
<style>
#close-icon {
    display: flex;
    justify-content: center;
    align-items: center;
    background: var(--BACKGROUND_DARK);
    box-sizing: border-box;
    transform: scale(1.5);
    padding: 4px;
}

#close-icon:hover {
    background-color: var(--BACKGROUND_LIGHT);
    cursor: pointer;
}
</style>
`
