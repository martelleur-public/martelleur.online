import { CreateShadowElement } from '@utility'

import { newTabTemplate as template } from './NewTabIconWhite.template'
import { NamedHTMLElement } from '../../core/named-html-element'

export const nameNewTabIconWhite = 'new-tab-icon-white'

@CreateShadowElement(nameNewTabIconWhite, { template })
export class NewTabIconWhite extends NamedHTMLElement {}
