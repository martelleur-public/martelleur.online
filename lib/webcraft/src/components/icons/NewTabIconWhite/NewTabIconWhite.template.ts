export const newTabTemplate = document.createElement('template')
newTabTemplate.innerHTML = `
<svg
    id="new-tab-icon"
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24">
    <!-- Top Line -->
    <line x1="5" y1="6" x2="12" y2="6" stroke="white" stroke-width="2"/>

    <!-- Right Line -->
    <line x1="18" y1="12" x2="18" y2="21" stroke="white" stroke-width="2"/>

    <!-- Bottom Line -->
    <line x1="5" y1="20" x2="17" y2="20" stroke="white" stroke-width="2"/>

    <!-- Left Line -->
    <line x1="4" y1="5" x2="4" y2="21" stroke="white" stroke-width="2"/>

    <!-- Arrow shaft-->
    <line x1="8" y1="16" x2="20" y2="4" stroke="white" stroke-width="2"/>
    
    <!-- Arrow head left side-->
    <line x1="14" y1="4" x2="20.5" y2="4" stroke="white" stroke-width="2"/>
    
    <!-- Arrow head right side-->
    <line x1="20" y1="3.5" x2="20" y2="10" stroke="white" stroke-width="2"/>
</svg>
<style>
#new-tab-icon {
    display: inline-block;
    vertical-align: middle;
    margin-bottom: 0px;
}
</style>
`
