import { DefineCustomElement, assertElementExist } from '@utility'

import { newTabTemplate } from './NewTabIcon.template'

export const nameNewTabIcon = 'new-tab-icon'

@DefineCustomElement(nameNewTabIcon)
export class NewTabIcon extends HTMLElement {
  private readonly _icon: SVGElement
  constructor () {
    super()
    const root = this.attachShadow({ mode: 'open' })
    root.appendChild(newTabTemplate.content.cloneNode(true))
    this._icon = assertElementExist(root.querySelector<SVGElement>('#new-tab-icon'))
  }

  setSelected (): void {
    this._icon.classList.add('selected')
  }

  removeSelected (): void {
    this._icon.classList.remove('selected')
  }
}
