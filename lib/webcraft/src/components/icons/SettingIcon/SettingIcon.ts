import { CreateShadowElement } from '@utility'

import { settingIconTemplate as template } from './SettingIcon.template'
import { NamedHTMLElement } from '../../core'

export const nameSettingIcon = 'setting-icon'

@CreateShadowElement(nameSettingIcon, { template })
export class SettingIcon extends NamedHTMLElement {}
