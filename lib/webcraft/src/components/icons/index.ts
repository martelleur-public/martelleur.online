export { CloseIcon } from './CloseIcon/CloseIcon'
export { HamburgerIcon } from './HamburgerIcon/HamburgerIcon'
export { SettingIcon } from './SettingIcon/SettingIcon'
export { NewTabIcon } from './NewTabIcon/NewTabIcon'
export { NewTabIconWhite } from './NewTabIconWhite/NewTabIconWhite'
