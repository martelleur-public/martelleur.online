export const responsiveHeaderSlot = {
  ICON: 'icon',
  TITLE: 'title',
  LINKS: 'links',
  ITEM: 'item',
  MENU: 'menu'
} as const

export type ResponsiveHeaderSlotType = typeof responsiveHeaderSlot[
  keyof typeof responsiveHeaderSlot
]
