import { ResponsiveHeaderSlotType } from "./ResponsiveHeader.slots"

export interface DistributeListenerEventDeail {
  slot: ResponsiveHeaderSlotType
  eventType: string
  eventCallback: () => void
}

export class DistributeListenerEvent extends CustomEvent<DistributeListenerEventDeail> {
  static readonly eventName = 'on-distribute-listener'

  constructor (detail: DistributeListenerEventDeail) {
    super(DistributeListenerEvent.eventName, {
      bubbles: true,
      composed: true,
      cancelable: true,
      detail: detail
    })
  }
}
