import type {
  ResponsiveLeftSidebar
} from '../ResponsiveLeftSidebar/ResponsiveLeftSidebar'
import { DistributeListenerEvent } from './ResponsiveHeader.event'

export interface IResponsiveHeader extends HTMLElement {
  root: ShadowRoot
  menuToggle: HTMLInputElement
  navigationSmallScreen: HTMLDivElement
  titleContainer: HTMLDivElement
  sidebarContainer: HTMLDivElement
  sidebar: ResponsiveLeftSidebar
  slotLinks: HTMLSlotElement
  slotItem: HTMLSlotElement
  slotTitle: HTMLSlotElement
  slotIcon: HTMLSlotElement
  slotMenu: HTMLSlotElement
}

export interface IResponsiveHeaderVM extends ResizeObserver {
  sidebarWidth: number
  linksWidthDefaultBreakPoint: number
  handleResize(entries: ResizeObserverEntry[]): void
  getPreviouseContainerQuerryStyle(): HTMLStyleElement | undefined
  handleLinksSlotChange(event: Event): void
  observeSizeOfAssignedElements(assignedElements: HTMLElement[]): void
  handleToggleMenu(event: Event): void
  updateUIForSidebarState(isSidebarOpen: boolean): void
  handleSidebarOpen(event: MouseEvent): void
  preventEventOutsideSidebarBoundaries(event: MouseEvent): void
  redistributeSlotsToSidebar(): void
  redistributeSlotToSidebar(slot: HTMLSlotElement, updateStyle?: (element: HTMLElement) => void): void
  createContainerQueryStyle (breakpointSidebarNavigation: number): string
  createDefaultContainerQueryStyle (): string
}

export interface IResponsiveHeaderReactVM {
  handleRedistributeEventHandler(event: Event): void
}
