import '@components/icons/HamburgerIcon/HamburgerIcon'
import '@components/icons/CloseIcon/CloseIcon'
import '../ResponsiveLeftSidebar/ResponsiveLeftSidebar'
import { IResponsiveHeaderVM } from './ResponsiveHeader.type'

export function createTemplate (viewModel: IResponsiveHeaderVM): HTMLTemplateElement {
  const template = document.createElement('template')
  template.innerHTML = `
<div class="container">
  <div class="title-container">
    <div><slot name="icon"></slot></div>
    <div><slot name="title"></slot></div>
  </div>
  <div class="link-container">
    <div class="small-screen">
      <input type="checkbox" id="menu-toggle">
      <label class="hamburger-icon" for="menu-toggle" aria-label="Open Menu">
        <hamburger-icon></hamburger-icon>
      </label>
      <label class="close-icon" for="menu-toggle" aria-label="Close Menu">
        <close-icon></close-icon>
      </label>
    </div>
    <div class="big-screen">
      <slot name="links"></slot>
      <div class="menu">
        <slot name="item"></slot>
      </div>
      <div class="menu">
        <slot name="menu"></slot>
      </div>
    </div>
  </div>
  <aside class="sidebar">
    <responsive-left-sidebar></responsive-left-sidebar>
  </aside>
</div>
<style>
*, *::before, *::after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

:host, .container {
  height: 100%;
  width: 100%;
}

.container {
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
}

.title-container {
  min-width: 20%;
  width: 20%;
  flex-grow: 0;
  height: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-left: 16px;
}

.link-container {
  container-type: inline-size;
  container-name: link-container;
  width: 70%;
  max-width: 70%;
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  padding-right: 16px;
}

.sidebar {
  position: fixed;
  z-index: 1000;
  height: 100vh;
  transition: left 0.3s;
  top: 0;
  left: -300px;
  width: ${viewModel.sidebarWidth}px;
}

.big-screen {
  display: flex;
  width: 100%;
  max-height: 100%;
  flex-wrap: nowrap;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  gap: 12px;  
}

.sidebar-links {
  display: flex;
  flex-direction: column;
  gap: 34px;
}

/* Prevents text in big screen slots from wrapping to a new line */
.big-screen > slot[name=links] {
  white-space: nowrap;
}

.close-icon {
  display: none;
}

#menu-toggle {
  display: none;
}

#menu-toggle:checked ~ .hamburger-icon {
  display: none;
}

#menu-toggle:checked ~ .close-icon {
  display: block;
}

.small-screen {
  display: none;
}
</style>
<style>
${viewModel.createDefaultContainerQueryStyle()}
</style>
`
  return template
}
