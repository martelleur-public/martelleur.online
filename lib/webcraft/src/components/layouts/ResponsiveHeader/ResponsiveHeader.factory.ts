import { IResponsiveHeader, IResponsiveHeaderReactVM, IResponsiveHeaderVM } from './ResponsiveHeader.type'
import { ResponsiveHeaderVM } from './ResponsiveHeader.vm'

export function createViewModel (view: IResponsiveHeader):
IResponsiveHeaderVM & IResponsiveHeaderReactVM {
  return new ResponsiveHeaderVM(view)
}
