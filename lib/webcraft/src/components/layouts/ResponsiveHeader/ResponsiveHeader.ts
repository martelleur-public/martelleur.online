import { DefineElement, assertElementExist, customLogger } from '@utility'
import { NamedHTMLElement } from '@components/core'
import { createTemplate } from './ResponsiveHeader.template'
import type { ResponsiveLeftSidebar } from '../ResponsiveLeftSidebar/ResponsiveLeftSidebar'
import { IResponsiveHeader, IResponsiveHeaderReactVM, IResponsiveHeaderVM } from './ResponsiveHeader.type'
import { createViewModel } from './ResponsiveHeader.factory'
import { DistributeListenerEvent } from './ResponsiveHeader.event'

@DefineElement('responsive-header')
export class ResponsiveHeader
  extends NamedHTMLElement
  implements IResponsiveHeader {
  readonly root: ShadowRoot
  readonly menuToggle: HTMLInputElement
  readonly sidebarContainer: HTMLDivElement
  readonly navigationSmallScreen: HTMLDivElement
  readonly titleContainer: HTMLDivElement
  readonly sidebar: ResponsiveLeftSidebar
  readonly slotLinks: HTMLSlotElement
  readonly slotItem: HTMLSlotElement
  readonly slotTitle: HTMLSlotElement
  readonly slotIcon: HTMLSlotElement
  readonly slotMenu: HTMLSlotElement
  private readonly _vm: IResponsiveHeaderVM & IResponsiveHeaderReactVM

  constructor () {
    super()
    this.root = this.attachShadow({ mode: 'open' })
    this._vm = createViewModel(this)
    this.root.appendChild(createTemplate(this._vm).content.cloneNode(true))
    this.menuToggle = assertElementExist(this.root.querySelector<HTMLInputElement>('#menu-toggle'))
    this.sidebarContainer = assertElementExist(this.root.querySelector<HTMLDivElement>('.sidebar'))
    this.navigationSmallScreen = assertElementExist(this.root.querySelector<HTMLDivElement>('.small-screen'))
    this.titleContainer = assertElementExist(this.root.querySelector<HTMLDivElement>('.title-container'))
    this.sidebar = assertElementExist(this.root.querySelector<ResponsiveLeftSidebar>('responsive-left-sidebar'))
    this.slotLinks = assertElementExist(this.root.querySelector<HTMLSlotElement>('slot[name="links"]'))
    this.slotItem = assertElementExist(this.root.querySelector<HTMLSlotElement>('slot[name="item"]'))
    this.slotTitle = assertElementExist(this.root.querySelector<HTMLSlotElement>('slot[name="title"]'))
    this.slotIcon = assertElementExist(this.root.querySelector<HTMLSlotElement>('slot[name="icon"]'))
    this.slotMenu = assertElementExist(this.root.querySelector<HTMLSlotElement>('slot[name="menu"]'))
  }

  connectedCallback (): void {
    this._vm.redistributeSlotsToSidebar()
    this.slotLinks.addEventListener('slotchange', this._vm.handleLinksSlotChange)
    this.menuToggle.addEventListener('change', this._vm.handleToggleMenu)
    document.addEventListener('click', this._vm.handleSidebarOpen)
    document.addEventListener(DistributeListenerEvent.eventName, this._vm.handleRedistributeEventHandler)
  }

  disconnectedCallback (): void {
    document.removeEventListener('click', this._vm.handleSidebarOpen)
    document.removeEventListener(DistributeListenerEvent.eventName, this._vm.handleRedistributeEventHandler)
    this._vm.disconnect()
  }
}
