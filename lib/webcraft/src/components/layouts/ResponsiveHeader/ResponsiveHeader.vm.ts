import { DistributeListenerEvent, DistributeListenerEventDeail } from './ResponsiveHeader.event'
import { IResponsiveHeader, IResponsiveHeaderReactVM, IResponsiveHeaderVM } from './ResponsiveHeader.type'

export class ResponsiveHeaderVM
  extends ResizeObserver
  implements IResponsiveHeaderVM, IResponsiveHeaderReactVM {
  readonly sidebarWidth = 300
  readonly linksWidthDefaultBreakPoint = 700
  readonly _redistributedSidebarElements: HTMLElement[] = []

  constructor (
    private readonly view: IResponsiveHeader) {
    super((entries) => this.handleResize(entries))
    this.handleResize = this.handleResize.bind(this)
    this.handleLinksSlotChange = this.handleLinksSlotChange.bind(this)
    this.handleToggleMenu = this.handleToggleMenu.bind(this)
    this.handleSidebarOpen = this.handleSidebarOpen.bind(this)
    this.handleRedistributeEventHandler = this.handleRedistributeEventHandler.bind(this)
  }

  handleResize (entries: ResizeObserverEntry[]): void {
    for (const entry of entries) {
      if (entry.contentRect.width > this.linksWidthDefaultBreakPoint) {
        const prevContainerQueryStyle = this.getPreviouseContainerQuerryStyle()
        const newContainerQueryStyle = document.createElement('style')
        newContainerQueryStyle.textContent = this.createContainerQueryStyle(
          entry.contentRect.width + 50
        )
        prevContainerQueryStyle?.remove()
        this.view.root.appendChild(newContainerQueryStyle)
      }
    }
  }

  getPreviouseContainerQuerryStyle (): HTMLStyleElement | undefined {
    return Array.from(this.view.root.querySelectorAll('style')).find(style => {
      return style.textContent != null && style.textContent.includes('@container link-container')
    })
  }

  handleLinksSlotChange (event: Event): void {
    const slot = event.target
    if (!(slot instanceof HTMLSlotElement)) {
      return
    }
    const assignedNodes = slot.assignedNodes({ flatten: true })
    const assignedElements = assignedNodes.filter(node => {
      return node instanceof HTMLElement
    }) as HTMLElement[]
    this.observeSizeOfAssignedElements(assignedElements)
  }

  observeSizeOfAssignedElements (assignedElements: HTMLElement[]): void {
    this.disconnect()
    assignedElements.forEach((assignedElement) => {
      this.observe(assignedElement)
    })
  }

  handleToggleMenu (event: Event): void {
    const target = event.target
    if (!(target instanceof HTMLInputElement)) {
      return
    }
    this.view.menuToggle.checked = target.checked
    this.updateUIForSidebarState(this.view.menuToggle.checked)
  }

  updateUIForSidebarState (isSidebarOpen: boolean): void {
    this.view.sidebarContainer.style.left = isSidebarOpen
      ? '0'
      : `-${this.sidebarWidth}px`
    this.view.titleContainer.style.visibility = isSidebarOpen
      ? 'hidden'
      : 'visible'
  }

  handleSidebarOpen (event: MouseEvent): void {
    if (!this.view.menuToggle.checked) {
      return
    }
    this.preventEventOutsideSidebarBoundaries(event)
  }

  handleRedistributeEventHandler (event: Event): void {
    if (!(event instanceof DistributeListenerEvent)) return
    this.redistributeSlotEventListener(this.view.slotMenu, event.detail)
  }

  redistributeSlotEventListener (
    slot: HTMLSlotElement,
    eventDetail: DistributeListenerEventDeail
  ): void {
    const assignedNodes = slot.assignedNodes({ flatten: true })
    const assignedElements = assignedNodes.filter(node => node.nodeType === Node.ELEMENT_NODE)
    assignedElements.forEach((assignedElement) => {
      if (!(assignedElement instanceof HTMLElement)) {
        return
      }
      assignedElement.addEventListener(eventDetail.eventType, eventDetail.eventCallback)
      this._redistributedSidebarElements.forEach(element => {
        if (element.getAttribute('slot') !== eventDetail.slot) {
          return
        }
        element.addEventListener(eventDetail.eventType, eventDetail.eventCallback)
      })
    })
  }

  preventEventOutsideSidebarBoundaries (event: MouseEvent): void {
    const sideBarIsIncluded = event.composedPath().includes(
      this.view.sidebarContainer
    )
    const navigationSmallScreenIsIncluded = event.composedPath().includes(
      this.view.navigationSmallScreen
    )
    const withinBoundaries = sideBarIsIncluded || navigationSmallScreenIsIncluded
    if (!withinBoundaries) {
      event.preventDefault()
    }
  }

  redistributeSlotsToSidebar (): void {
    this.redistributeSlotToSidebar(this.view.slotLinks, (element) => {
      element.classList.add('sidebar-links')
    })
    this.redistributeSlotToSidebar(this.view.slotItem)
    this.redistributeSlotToSidebar(this.view.slotTitle)
    this.redistributeSlotToSidebar(this.view.slotIcon)
    this.redistributeSlotToSidebar(this.view.slotMenu)
  }

  redistributeSlotToSidebar (
    slot: HTMLSlotElement,
    updateStyle?: (element: HTMLElement) => void
  ): void {
    const assignedNodes = slot.assignedNodes({ flatten: true })
    const assignedElements = assignedNodes.filter(node => node.nodeType === Node.ELEMENT_NODE)
    assignedElements.forEach((assignedElement) => {
      if (!(assignedElement instanceof HTMLElement)) {
        return
      }
      const assignedElementCopy = assignedElement.cloneNode(true)
      if (!(assignedElementCopy instanceof HTMLElement)) {
        return
      }
      assignedElementCopy.setAttribute('slot', `${slot.name}`)
      updateStyle != null && updateStyle(assignedElementCopy)
      this.view.sidebar.appendChild(assignedElementCopy)
      this._redistributedSidebarElements.push(assignedElementCopy)
    })
  }

  createContainerQueryStyle (breakpointSidebarNavigation: number): string {
    return `
@container link-container (max-width: ${breakpointSidebarNavigation}px) {
  .big-screen {
    display: none;
  }
  
  .small-screen {
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
  }
}`
  }

  createDefaultContainerQueryStyle (): string {
    return this.createContainerQueryStyle(this.linksWidthDefaultBreakPoint)
  }
}

/**
 * @TODO
 */
// createContainerQueryStyle (breakpointSidebarNavigation: number): CSS? {
//   const sheet = new CSSStyleSheet()
//   const cssContent = `
//   @container link-container (max-width: ${breakpointSidebarNavigation}px) {
//     .big-screen {
//       display: none;
//     }

//     .small-screen {
//       display: flex;
//       flex-direction: row;
//       justify-content: flex-end;
//     }
//   }`
//   return sheet.replaceSync(cssContent)
// }
