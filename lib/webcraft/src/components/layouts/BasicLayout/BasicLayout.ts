import { DefineElement } from '@utility'

import { template } from './BasicLayout.template'
import { basicLayoutAttributes as attributes } from './BasicLayout.attribute'
import {
  type IBaseVM,
  NamedHTMLElement,
  createBaseViewModel
} from '@components/core'
import { IBasicLayout, IBasicLayoutVM } from './BasicLayout.type'
import { createViewModel } from './BasicLayout.factory'

@DefineElement('basic-layout')
export class BasicLayout
  extends NamedHTMLElement
  implements IBasicLayout {
  static readonly elementAttributes = attributes
  readonly slots: NodeListOf<HTMLSlotElement>
  readonly root: ShadowRoot
  private readonly _vm: IBasicLayoutVM
  private readonly _baseVm: IBaseVM

  constructor () {
    super()
    this.root = this.attachShadow({ mode: 'open' })
    this.root.appendChild(template.content.cloneNode(true))
    this.slots = this.root.querySelectorAll('slot')
    this._vm = createViewModel(this)
    this._baseVm = createBaseViewModel()
  }

  static get observedAttributes (): string[] {
    return Object.values(attributes)
  }

  attributeChangedCallback (
    name: string,
    oldValue: string,
    newValue: string
  ): void {
    if (this._baseVm.isAttributeValueSame(newValue, oldValue)) return
    switch (name) {
    case attributes.DYNAMIC_HEIGHT: this._vm.handleDynamicHeightAttribute(); break
    default: this._baseVm.handleDefaultAttributeChg(newValue, oldValue)
    }
  }

  connectedCallback (): void {
    this.slots.forEach(slot => {
      slot.addEventListener('slotchange', this._vm.handleSlotChange)
    })
  }
}
