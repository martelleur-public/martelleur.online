import { basicLayoutAttributes } from './BasicLayout.attribute'
import { IBasicLayout, IBasicLayoutVM } from './BasicLayout.type'

export class BasicLayoutVM implements IBasicLayoutVM {
  constructor (private readonly view: IBasicLayout) {
    this.handleSlotChange = this.handleSlotChange.bind(this)
  }

  handleDynamicHeightAttribute (): void {
    const attribute = basicLayoutAttributes.DYNAMIC_HEIGHT
    if (this.view.hasAttribute(attribute)) {
      this.view.style.setProperty(`--${attribute}`, '100%')
    } else {
      this.view.style.setProperty(`--${attribute}`, '100vh')
    }
  }

  handleSlotChange (event: Event): void {
    const slot = event.target
    if (!(slot instanceof HTMLSlotElement)) {
      return
    }
    this.handleDisplaySlotContainer(slot)
  }

  handleDisplaySlotContainer (slot: HTMLSlotElement): void {
    const slotContainer = this.view.root.querySelector(`.${slot.getAttribute('name')}`)
    slotContainer?.classList.remove('hide')
  }
}
