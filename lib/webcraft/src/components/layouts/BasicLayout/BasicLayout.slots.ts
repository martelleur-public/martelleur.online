export const basicLayoutSlot = {
  HEADER: 'header',
  BODY: 'body',
  FOOTER: 'footer',
} as const

export type BasicLayoutSlotType = typeof basicLayoutSlot[
  keyof typeof basicLayoutSlot
]
