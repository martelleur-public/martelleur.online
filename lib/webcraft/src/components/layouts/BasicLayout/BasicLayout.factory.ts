import { IBasicLayout, IBasicLayoutVM } from './BasicLayout.type'
import { BasicLayoutVM } from './BasicLayout.vm'

export function createViewModel (view: IBasicLayout): IBasicLayoutVM {
  return new BasicLayoutVM(view)
}
