import { basicLayoutSlot as slot } from './BasicLayout.slots'

export const template = document.createElement('template')
template.innerHTML = `
<div class="container">
  <header class="${slot.HEADER} hide">
    <slot name="${slot.HEADER}">Header Content...</slot>
  </header>
  <main class="${slot.BODY} hide">  
    <slot name="${slot.BODY}">Body Content...</slot>
  </main>
  <footer class="${slot.FOOTER} hide">
    <slot name="${slot.FOOTER}">Footer Content...</slot>
  </footer>
</div>
<style>
*, *::before, *::after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

:host {
  display: block;
  height: var(--dynamic-height, 100vh);
  width: 100vw;
  color: var(--TEXT_PRIMARY);
  background-color: var(--BACKGROUND_LIGHT_X);
}

.hide {
  display: none !important;
}

.container {
  display: flex;
  flex-direction: column;
  height: 100%;
}

.${slot.BODY} {
  flex-grow: 1; /* Allows content to fill available space */
  overflow: auto;
  background-color: var(--BACKGROUND_LIGHT_X);
}

.${slot.HEADER}, .${slot.FOOTER} {
  height: 74px;
  background-color: var(--BACKGROUND_DARK);
  color: var(--TEXT_PRIMARY);
  flex-shrink: 0; /* Keeps header and footer from shrinking */
}

.${slot.HEADER} {
  position: sticky;
  top: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  z-index: 1000;
  border-bottom: 3px solid var(--BORDER_LIGHT_X);
}

.${slot.FOOTER} {
  margin-top: auto; /* Pushes the footer to the bottom */
}

</style>
`
