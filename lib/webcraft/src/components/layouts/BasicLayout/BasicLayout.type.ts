export interface IBasicLayout extends HTMLElement {
  slots: NodeListOf<HTMLSlotElement>
  root: ShadowRoot
}

export interface IBasicLayoutVM {
  handleDynamicHeightAttribute: () => void
  handleSlotChange: (event: Event) => void
  handleDisplaySlotContainer: (slot: HTMLSlotElement) => void
}
