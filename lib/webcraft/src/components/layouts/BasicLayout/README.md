# Basic Layout Description

A basic layout with static height and width filling the whole viewport. The layout contains three main parts; Header, Content, and Footer

## Header

Static height: 54px. Contain a title and link area. If the links part is less the 200 px a hamburger menu is displayed which then opens a sidebar with the links.

## Footer

Static height: 54px

## Content

Content fills the available space between the header and footer. If the content overflows a scroll is added.
