import { CreateAndDefineShadowElement } from '@utility'
import { template } from './ResponsiveLeftSidebar.template'

@CreateAndDefineShadowElement('responsive-left-sidebar', { template })
export class ResponsiveLeftSidebar extends HTMLElement {}
