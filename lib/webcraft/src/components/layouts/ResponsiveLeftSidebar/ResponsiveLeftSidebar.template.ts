export const template = document.createElement('template')
template.innerHTML = `
<div class="container">
  <div class="header">
    <slot name="title"></slot>
    <slot name="icon"></slot>
  </div>
  <div class="body">
    <slot name="item" class="item"></slot>
    <slot name="links"></slot>
  </div>
  <div class="footer">
    <slot name="menu"></slot>
  </div>
</div>
<style>
*, *::before, *::after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

:host, .container {
  height: 100%;
  width: 100%;
}

.container {
  display: flex;
  flex-direction: column;
  background-color: var(--BACKGROUND_MEDIUM);
  color: var(--TEXT_PRIMARY);
  border-right: 3px solid var(--BORDER_LIGHT_X);
}

.header, .footer {
  flex-shrink: 0; /* Keeps header and footer from shrinking */
  background: var(--BACKGROUND_DARK);
  font-family: var(--FONT_SECONDARY);
}

.body {
  display: flex;
  flex-direction: column;
  flex-grow: 1; /* Allows content to fill available space */
  overflow: auto;
  padding: 24px 12px;
}

.item {
  display: flex;
  flex-direction: row;
  justify-content: center;
}

.header {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 164px;
}

.footer {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 54px;
  margin-top: auto; /* Pushes the footer to the bottom */
}
</style>
`
