export const defaultContainerStyle = `
<style>
    *, *::before, *::after {
        box-sizing: border-box;
        font-family: var(--FONT_PRIMARY);
    }
</style>
`
