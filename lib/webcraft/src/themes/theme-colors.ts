export const ColorThemeName = {
  LIGHT: 'light',
  DARK: 'dark',
  WARM: 'warm',
  COLD: 'cold',
  RETRO: 'retro',
  DEFAULT: 'dark'
} as const

export type ColorThemeNameType = typeof ColorThemeName[
  keyof typeof ColorThemeName
]

export interface ColorMode {
  COLOR_THEME: ColorThemeNameType
  BACKGROUND_ACCENT: string
  BACKGROUND_ACCENT_HOVER: string
  BACKGROUND_LIGHT_X: string
  BACKGROUND_LIGHT: string
  BACKGROUND_MEDIUM: string
  BACKGROUND_DARK: string
  TEXT_PRIMARY: string
  TEXT_SECONDARY: string
  TEXT_HOVER: string
  TEXT_ACTIVE: string
  BORDER_LIGHT_X: string
  BORDER_DARK: string
}

export const DarkMode: ColorMode = {
  COLOR_THEME: 'dark',
  BACKGROUND_ACCENT: '#FF5733',
  BACKGROUND_ACCENT_HOVER: '#FF6F50',
  BACKGROUND_LIGHT_X: '#635985',
  BACKGROUND_LIGHT: '#443C68',
  BACKGROUND_MEDIUM: '#393053',
  BACKGROUND_DARK: '#18122B',
  TEXT_PRIMARY: '#FFFFFF',
  TEXT_SECONDARY: '#FFFFFF',
  TEXT_HOVER: '#008CBA',
  TEXT_ACTIVE: '#008CBA',
  BORDER_LIGHT_X: '#635985',
  BORDER_DARK: '#18122B'
}

export const LightMode: ColorMode = {
  COLOR_THEME: 'light',
  BACKGROUND_ACCENT: '#FF5733',
  BACKGROUND_ACCENT_HOVER: '#FF6F50',
  BACKGROUND_LIGHT_X: '#F8F6F4',
  BACKGROUND_LIGHT: '#E3F4F4',
  BACKGROUND_MEDIUM: '#D2E9E9',
  BACKGROUND_DARK: '#C4DFDF',
  TEXT_PRIMARY: '#000000',
  TEXT_SECONDARY: '#000000',
  TEXT_HOVER: '#6f00ff',
  TEXT_ACTIVE: '#6f00ff',
  BORDER_LIGHT_X: '#F8F6F4',
  BORDER_DARK: '#C4DFDF'
}

export const WarmMode: ColorMode = {
  COLOR_THEME: 'warm',
  BACKGROUND_ACCENT: '#FF5733',
  BACKGROUND_ACCENT_HOVER: '#FF6F50',
  BACKGROUND_LIGHT_X: '#F05941',
  BACKGROUND_LIGHT: '#BE3144',
  BACKGROUND_MEDIUM: '#872341',
  BACKGROUND_DARK: '#22092C',
  TEXT_PRIMARY: '#FFFFFF',
  TEXT_SECONDARY: '#FFFFFF',
  TEXT_HOVER: '#008CBA',
  TEXT_ACTIVE: '#008CBA',
  BORDER_LIGHT_X: '#F05941',
  BORDER_DARK: '#22092C'
}

export const ColdMode: ColorMode = {
  COLOR_THEME: 'cold',
  BACKGROUND_ACCENT: '#FF5733',
  BACKGROUND_ACCENT_HOVER: '#FF6F50',
  BACKGROUND_LIGHT_X: '#97FEED',
  BACKGROUND_LIGHT: '#35A29F',
  BACKGROUND_MEDIUM: '#0B666A',
  BACKGROUND_DARK: '#071952',
  TEXT_PRIMARY: '#FFFFFF',
  TEXT_SECONDARY: '#000000',
  TEXT_HOVER: '#008CBA',
  TEXT_ACTIVE: '#008CBA',
  BORDER_LIGHT_X: '#97FEED',
  BORDER_DARK: '#071952'
}

export const RetroMode: ColorMode = {
  COLOR_THEME: 'retro',
  BACKGROUND_ACCENT: '#FF5733',
  BACKGROUND_ACCENT_HOVER: '#FF6F50',
  BACKGROUND_LIGHT_X: '#F79540',
  BACKGROUND_LIGHT: '#FC4F00',
  BACKGROUND_MEDIUM: '#B71375',
  BACKGROUND_DARK: '#8B1874',
  TEXT_PRIMARY: '#FFFFFF',
  TEXT_SECONDARY: '#FFFFFF',
  TEXT_HOVER: '#008CBA',
  TEXT_ACTIVE: '#008CBA',
  BORDER_LIGHT_X: '#F79540',
  BORDER_DARK: '#8B1874'
}

export function updateColors (themeName: ColorThemeNameType): void {
  const root = document.documentElement
  switch (themeName) {
    case ColorThemeName.DARK:
      for (const [key, value] of Object.entries(DarkMode)) {
        root.style.setProperty(`--${key}`, value)
      }
      break
    case ColorThemeName.LIGHT:
      for (const [key, value] of Object.entries(LightMode)) {
        root.style.setProperty(`--${key}`, value)
      }
      break
    case ColorThemeName.WARM:
      for (const [key, value] of Object.entries(WarmMode)) {
        root.style.setProperty(`--${key}`, value)
      }
      break
    case ColorThemeName.COLD:
      for (const [key, value] of Object.entries(ColdMode)) {
        root.style.setProperty(`--${key}`, value)
      }
      break
    case ColorThemeName.RETRO:
      for (const [key, value] of Object.entries(RetroMode)) {
        root.style.setProperty(`--${key}`, value)
      }
      break
    default:
      for (const [key, value] of Object.entries(DarkMode)) {
        root.style.setProperty(`--${key}`, value)
      }
  }
}

export function getCurrentColors (themeName: ColorThemeNameType): ColorMode {
  switch (themeName) {
    case ColorThemeName.DARK:
      return DarkMode
    case ColorThemeName.LIGHT:
      return LightMode
    case ColorThemeName.WARM:
      return WarmMode
    case ColorThemeName.COLD:
      return ColdMode
    case ColorThemeName.RETRO:
      return RetroMode
    default:
      return DarkMode
  }
}
