export const FontThemeName = {
  MODERN_CLEAN: 'modern-clean',
  CLASSIC_ELEGANCE: 'classic-elegance',
  PROFESSIONAL_BUSINESS: 'professional-business',
  CAUSAL_FRIENDLY: 'causal-friendly',
  TEHCNICAL_CODE: 'technical-code',
  DEFAULT: 'modern-clean'
} as const

export type FontThemeNameType = typeof FontThemeName[
  keyof typeof FontThemeName
]

export interface FontMode {
  FONT_NAME: FontThemeNameType
  FONT_PRIMARY: string
  FONT_SECONDARY: string
}

export const DefaultFont: FontMode = {
  FONT_NAME: 'modern-clean',
  FONT_PRIMARY: 'Helvetica, Arial, sans-serif',
  FONT_SECONDARY: 'Gill Sans, sans-serif'
}

export const ModernClean: FontMode = {
  FONT_NAME: 'modern-clean',
  FONT_PRIMARY: 'Helvetica, Arial, sans-serif',
  FONT_SECONDARY: 'Gill Sans, sans-serif'
}

export const ClassicElegance: FontMode = {
  FONT_NAME: 'classic-elegance',
  FONT_PRIMARY: 'Georgia, serif',
  FONT_SECONDARY: 'Palatino, \'Palatino Linotype\', serif'
}

export const ProfessionalBusiness: FontMode = {
  FONT_NAME: 'professional-business',
  FONT_PRIMARY: 'Calibri, Candara, Segoe, sans-serif',
  FONT_SECONDARY: 'Arial, sans-serif'
}

export const CasualFriendly: FontMode = {
  FONT_NAME: 'causal-friendly',
  FONT_PRIMARY: 'Comic Sans MS, cursive, sans-serif',
  FONT_SECONDARY: 'Tahoma, Geneva, sans-serif'
}

export const TechnicalCode: FontMode = {
  FONT_NAME: 'technical-code',
  FONT_PRIMARY: 'Courier New, Courier, monospace',
  FONT_SECONDARY: 'Consolas, \'Lucida Console\', monospace'
}

export function updateFonts (fontName: FontThemeNameType): void {
  const root = document.documentElement
  switch (fontName) {
    case FontThemeName.MODERN_CLEAN:
      for (const [key, value] of Object.entries(ModernClean)) {
        root.style.setProperty(`--${key}`, value)
      }
      break
    case FontThemeName.CLASSIC_ELEGANCE:
      for (const [key, value] of Object.entries(ClassicElegance)) {
        root.style.setProperty(`--${key}`, value)
      }
      break
    case FontThemeName.PROFESSIONAL_BUSINESS:
      for (const [key, value] of Object.entries(ProfessionalBusiness)) {
        root.style.setProperty(`--${key}`, value)
      }
      break
    case FontThemeName.CAUSAL_FRIENDLY:
      for (const [key, value] of Object.entries(CasualFriendly)) {
        root.style.setProperty(`--${key}`, value)
      }
      break
    case FontThemeName.TEHCNICAL_CODE:
      for (const [key, value] of Object.entries(TechnicalCode)) {
        root.style.setProperty(`--${key}`, value)
      }
      break
    default:
      for (const [key, value] of Object.entries(DefaultFont)) {
        root.style.setProperty(`--${key}`, value)
      }
  }
}
