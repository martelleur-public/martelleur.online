import { RouterElementError } from './RouterElementError'

export function assertElementIsInsideParent (
  element: HTMLElement,
  parentTagName: string
): void {
  let parentElement = element.parentElement
  let parentIsFound = false
  while (parentElement) {
    if (parentElement.tagName.toLowerCase() === parentTagName) {
      parentIsFound = true
      return
    }
    parentElement = parentElement.parentElement
  }
  if (!parentIsFound) {
    const errMsg = `Element ${element.tagName.toLowerCase()} must be inside element ${parentTagName.toLowerCase()}.`
    throw new RouterElementError(errMsg)
  }
}
