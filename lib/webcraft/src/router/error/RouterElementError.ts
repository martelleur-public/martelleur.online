export class RouterElementError extends Error {
  static readonly errorName = 'RouterElementError'
  constructor (message: string) {
    super(`[${RouterElementError.errorName}] ${message}`)
  }
}
