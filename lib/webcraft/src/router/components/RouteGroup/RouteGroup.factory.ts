import { ViewModelFactory } from '@components/core'
import type { IRouteGroup, IRouteGroupVM } from './RouteGroup.type'
import { RouteGroupVM } from './RouteGroup.vm'

export const createViewModel:
ViewModelFactory<IRouteGroup, IRouteGroupVM> = (element: IRouteGroup):
IRouteGroupVM => {
  return new RouteGroupVM(element)
}
