import { RouteMap } from '@router/types'

export interface IRouteGroup extends HTMLElement {
  root: ShadowRoot
  slotElement: HTMLSlotElement
  isConcreteInstance: (element: IRouteGroup) => boolean
}

export interface IRouteGroupVM {
  routes: RouteMap
  path: string
  handlePathAttributeChg(newValue: string): void
  handleSlotChange(): void
}
