export const groupRouteTemplate = document.createElement('template')
groupRouteTemplate.innerHTML = `
<style>
*, :host, ::after, ::before {
    display: none;
}
</style>
<slot></slot>
`
