import { NewRoutesEvent } from '@router/events/new-routes-event'
import { RouteItem } from '../RouteItem/RouteItem'
import type { IRouteGroup, IRouteGroupVM } from './RouteGroup.type'
import type { RouteMap } from '@router'

export class RouteGroupVM implements IRouteGroupVM {
  routes: RouteMap = {}
  path = ''

  constructor (private readonly view: IRouteGroup) {
    this.handleSlotChange = this.handleSlotChange.bind(this)
  }

  handlePathAttributeChg (newValue: string): void {
    this.path = newValue
  }

  handleSlotChange (): void {
    const assignedNodes = this.view.slotElement.assignedNodes({ flatten: true })
    const compositeParentPath = this.getCompositeParentPaths(this.view)
    const routeGroupPath = compositeParentPath === ''
      ? this.path
      : `${compositeParentPath}/${this.path}`
    this.routes = this.createRoutes(assignedNodes, routeGroupPath)
    const newRoutesEvent = new NewRoutesEvent(this.routes)
    this.view.root.dispatchEvent(newRoutesEvent)
  }

  private getCompositeParentPaths (element: IRouteGroup): string {
    if (this.view.isConcreteInstance(element)) {
      const parentElement = element.parentElement as IRouteGroup
      const parentPath = parentElement?.getAttribute('path') ?? ''
      return `${this.getCompositeParentPaths(parentElement)}/${parentPath}`
    }
    return ''
  }

  private createRoutes (assignedNodes: Node[], routeGroupPath: string): RouteMap {
    let routes: RouteMap = {}
    for ( const node of assignedNodes) {
      if (!(node instanceof RouteItem)) continue
      routes = {
        ...routes,
        ...this.createRoute(node, routeGroupPath)
      }
    }
    return routes
  }

  private createRoute (routeItem: RouteItem, routeGroupPath: string): RouteMap {
    const routes: RouteMap = {}
    const routeItemPath = routeItem.getAttribute('path')
    const component = routeItem.getAttribute('component')
    if (routeItemPath == null || component == null) {
      return routes
    }
    if (routeItemPath === 'index' || routeItemPath === '/' || routeItemPath === '') {
      routes[`${routeGroupPath}`] = this.constructHTMLTag(component)
      routes[`${routeGroupPath}/`] = this.constructHTMLTag(component)
      routes[`${routeGroupPath}/index`] = this.constructHTMLTag(component)
      routes[`${routeGroupPath}/${routeItemPath}`] = this.constructHTMLTag(component)
    } else {
      routes[`${routeGroupPath}/${routeItemPath}`] = this.constructHTMLTag(component)
    }
    return routes
  }

  private constructHTMLTag (componentName: string): string {
    return `<${componentName}></${componentName}>`
  }
}
