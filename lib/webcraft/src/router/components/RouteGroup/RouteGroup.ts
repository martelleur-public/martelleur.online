import { IBaseVM, NamedHTMLElement, createBaseViewModel } from '@components'
import { DefineElement, assertElementExist } from '@utility'
import { groupRouteTemplate } from './RouteGroup.template'
import { routeGroupAttributes as attributes } from './RouteGroup.attributes'
import { AppRouter } from '../AppRouter/AppRouter'
import { assertElementIsInsideParent } from '@router/error'
import { IRouteGroup, IRouteGroupVM } from './RouteGroup.type'
import { createViewModel } from './RouteGroup.factory'

@DefineElement('route-group')
export class RouteGroup
  extends NamedHTMLElement
  implements IRouteGroup {
  static readonly elementAttributes = attributes
  private readonly _vm: IRouteGroupVM
  private readonly _baseVm: IBaseVM
  readonly root: ShadowRoot
  readonly slotElement: HTMLSlotElement

  constructor () {
    super()
    this.root = this.attachShadow({ mode: 'open' })
    this.root.appendChild(groupRouteTemplate.content.cloneNode(true))
    this.slotElement = assertElementExist(this.root.querySelector('slot'))
    this._vm = createViewModel(this)
    this._baseVm = createBaseViewModel()
  }

  static get observedAttributes (): string[] {
    return Object.values(RouteGroup.elementAttributes)
  }

  attributeChangedCallback (
    name: string,
    oldValue: string,
    newValue: string
  ): void {
    this._baseVm.isAttributeValueSame(newValue, oldValue)
    switch (name) {
    case attributes.PATH: this._vm.handlePathAttributeChg(newValue); break
    default: this._baseVm.handleDefaultAttributeChg(newValue, oldValue)
    }
  }

  connectedCallback (): void {
    assertElementIsInsideParent(this, AppRouter.elementName)
    this.slotElement.addEventListener('slotchange', this._vm.handleSlotChange)
  }

  isConcreteInstance (element: IRouteGroup): boolean {
    return element.parentElement instanceof RouteGroup
  }
}
