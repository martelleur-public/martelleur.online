import { PageTransitionType } from './page-transition'

export interface IAppRouter extends HTMLElement {
  pageContainer: HTMLDivElement
}

export type RouteMap = Record<string, string>

export interface IAppRouterVM {
  routes: RouteMap
  transition: PageTransitionType
  defaultPage: string
  setPageTransitionAttribute: (newValue: string, oldValue: string) => void
  handleNewRoutes: (event: Event) => void
  handlePageTransition: () => void
  render: () => void
  renderDarkTransition: () => void
  renderSlideTransition: () => void
  renderShrinkTransition: (rotation: boolean) => void
  renderNoneTransition: () => void
}
