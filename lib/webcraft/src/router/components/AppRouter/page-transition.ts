export const PageTransition = {
  DEFAULT: 'dark',
  DARK: 'dark',
  SLIDE: 'slide',
  NONE: 'none',
  SHRINK: 'shrink',
  ROTATION_SHRINK: 'rotation-shrink'
} as const

export type PageTransitionType = typeof PageTransition[
  keyof typeof PageTransition
]
