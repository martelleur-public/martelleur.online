import { ViewModelFactory } from '@components/core'
import { IAppRouter, IAppRouterVM } from './AppRouter.type'
import { AppRouterVM } from './AppRouter.vm'

export const createViewModel:
ViewModelFactory<IAppRouter, IAppRouterVM> = (element: IAppRouter):
IAppRouterVM => {
  return new AppRouterVM(element)
}
