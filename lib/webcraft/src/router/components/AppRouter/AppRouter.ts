import { NewRoutesEvent } from '@router/events/new-routes-event'
import { IBaseVM, NamedHTMLElement, createBaseViewModel } from '../../../components'
import { DefineElement, assertElementExist } from '../../../utility'
import { appRouterAttribute as a } from './AppRouter.attributes'
import { createViewModel } from './AppRouter.factory'
import { createAppRouterTemplate } from './AppRouter.template'
import { IAppRouter, IAppRouterVM } from './AppRouter.type'
import { AppRouterEvent } from '@router/events/app-router-event'

/**
 * Used to create a router element in a declerative approach.
 * Together with RouteItem and RouteGroup a user can create a
 * router with HTML syntax to make the router and the routes more
 * explicit, manageable, and readable.
 */
@DefineElement('app-router')
export class AppRouter
  extends NamedHTMLElement
  implements IAppRouter {
  static readonly elementAttributes = a
  private readonly _vm: IAppRouterVM
  private readonly _baseVm: IBaseVM
  pageContainer!: HTMLDivElement

  constructor () {
    super()
    this._vm = createViewModel(this)
    this._baseVm = createBaseViewModel()
  }

  static get observedAttributes (): string[] {
    return Object.values(AppRouter.elementAttributes)
  }

  attributeChangedCallback (
    name: string,
    oldValue: string,
    newValue: string
  ): void {
    if (this._baseVm.isAttributeValueSame(newValue, oldValue)) return
    switch (name) {
    case a.TRANSITION: this._vm.setPageTransitionAttribute(newValue, oldValue); break
    default: this._baseVm.handleDefaultAttributeChg(newValue, oldValue)
    }
  }

  connectedCallback (): void {
    this.appendChild(createAppRouterTemplate(this._vm.defaultPage).content.cloneNode(true))
    this.pageContainer = assertElementExist(this.querySelector<HTMLDivElement>('.page-container'))
    this.classList.add('page-container')
    document.addEventListener(NewRoutesEvent.eventName, this._vm.handleNewRoutes)
    window.addEventListener('popstate', this._vm.handlePageTransition)
    window.addEventListener(AppRouterEvent.eventName, this._vm.handlePageTransition)
  }

  disconnectedCallback (): void {
    document.removeEventListener(NewRoutesEvent.eventName, this._vm.handleNewRoutes)
    window.removeEventListener('popstate', this._vm.handlePageTransition)
    window.removeEventListener(AppRouterEvent.eventName, this._vm.handlePageTransition)
  }
}
