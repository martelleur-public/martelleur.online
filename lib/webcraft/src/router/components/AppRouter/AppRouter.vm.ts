import { IAppRouter, IAppRouterVM, RouteMap } from './AppRouter.type'
import { PageTransition, PageTransitionType } from './page-transition'
import { NewRoutesEvent } from '@router/events/new-routes-event'

export class AppRouterVM implements IAppRouterVM {
  routes: RouteMap = {}
  transition: PageTransitionType = PageTransition.DEFAULT
  defaultPage: string = document.createElement('div').outerHTML
  private _currentTransition = false

  constructor (private readonly view: IAppRouter) {
    this.handleNewRoutes = this.handleNewRoutes.bind(this)
    this.handlePageTransition = this.handlePageTransition.bind(this)
  }

  setPageTransitionAttribute (newValue: string, oldValue: string): void {
    if (this._isPageTransitionType(newValue)) {
      this.transition = newValue
    } else if (this._isPageTransitionType(oldValue)) {
      this.transition = oldValue
    } else {
      this.transition = PageTransition.DEFAULT
    }
  }

  private _isPageTransitionType (value: any): value is PageTransitionType {
    return Object.values(PageTransition).includes(value)
  }

  handleNewRoutes (event: Event): void {
    if (event instanceof NewRoutesEvent) {
      const newRoutes = event.detail
      this.routes = { ...this.routes, ...newRoutes }
      if (this.routes['/default'] != null &&
        this.routes['/default'] !== this.defaultPage) {
        this.defaultPage = this.routes['/default']
      }
      this.render()
    }
  }

  render (): void {
    const path = window.location.pathname
    const pageTag = this.routes[path] ?? this.defaultPage
    this._clearPageContainer()
    const newPage = document.createElement(this._extractElementName(pageTag) ?? '')
    this.view.pageContainer.appendChild(newPage)
  }

  private _clearPageContainer (): void {
    while (this.view.pageContainer.firstChild != null) {
      this.view.pageContainer.removeChild(this.view.pageContainer.firstChild)
    }
  }

  private _extractElementName (elementTag: string): string | null {
    const match = elementTag.match(/^<([^\s>]+)/)
    return match ? match[1] : null
  }

  handlePageTransition (): void {
    switch (this.transition) {
    case PageTransition.DARK: this.renderDarkTransition(); break
    case PageTransition.SLIDE: this.renderSlideTransition(); break
    case PageTransition.SHRINK: this.renderShrinkTransition(); break
    case PageTransition.ROTATION_SHRINK: this.renderShrinkTransition(true); break
    default: this.renderNoneTransition()
    }
  }

  renderDarkTransition (): void {
    this._clearPageContainer()
    this.view.pageContainer.classList.add('darken')
    setTimeout(() => {
      this.render()
      this.view.pageContainer.classList.remove('darken')
      this.view.pageContainer.classList.add('lighten')
      setTimeout(() => {
        this.view.pageContainer.classList.remove('lighten')
      }, 300)
    }, 300)
  }

  renderShrinkTransition (roation = false): void {
    this.view.pageContainer.classList.add('pre-transition')
    this.view.pageContainer.classList.add('shrink')
    if (roation) { this.view.pageContainer.style.transform = 'rotate(360deg)' }
    setTimeout(() => {
      this.render()
      this.view.pageContainer.classList.remove('shrink')
      this.view.pageContainer.classList.add('grow')
      if (roation) { this.view.pageContainer.style.transform = 'rotate(-360deg)' }
      setTimeout(() => {
        this.view.pageContainer.classList.remove('grow')
      }, 500)
    }, 500)
  }

  renderSlideTransition (): void {
    if (this._currentTransition) {
      return
    }
    this._currentTransition = true

    const path = window.location.pathname
    const prevPage = this.view.pageContainer.firstElementChild
    if (!(prevPage instanceof HTMLElement)) {
      return
    }

    const nextPageTag = this.routes[path] ?? this.defaultPage
    const nextPageElementName = this._extractElementName(nextPageTag)
    if (nextPageElementName === null) {
      return
    }

    this.view.setAttribute('style', 'width: 200%')
    const nextPage = document.createElement(nextPageElementName)
    this.view.pageContainer.appendChild(nextPage)
    this._applySlideTransitionAfterRerendering(prevPage)
  }

  private _applySlideTransitionAfterRerendering (prevPage: HTMLElement): void {
    requestAnimationFrame(() => {
      this.view.pageContainer.classList.add('slide')
      this.view.pageContainer.style.transform = 'translateX(-50%)'
      setTimeout(() => {
        this.view.pageContainer.classList.remove('slide')
        this.view.pageContainer.removeChild(prevPage)
        this.view.setAttribute('style', 'width: 100%')
        this.view.pageContainer.style.transform = 'translateX(0)'
        this._currentTransition = false
      }, 700)
    })
  }

  renderNoneTransition (): void {
    this.render()
  }
}
