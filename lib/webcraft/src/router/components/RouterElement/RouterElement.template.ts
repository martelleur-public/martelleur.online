export function createRouterTemplate (defaultPage: string): HTMLTemplateElement {
  const template = document.createElement('template')
  template.innerHTML = `
<div class="page-container">
  ${defaultPage}
</div>
<style>
/**
 * Selectors may not be reliable when using without shadow host.
 */
*, *::before, *::after {
  box-sizing: border-box;
  overflow-x: hidden;
}

.page-container {
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
}

.darken {
  background-color: rgba(0, 0, 0, 1);
  transition: background-color 0.3s ease;
}

.lighten {
  background-color: rgba(0, 0, 0, 0);
  transition: background-color 0.3s ease;
}

.shrink {
  width: 0%;
  height: 0%;
  border-radius: 50%;
  transition-property: width, height, border-radius, transform;
  transition-duration: 0.4s, 0.4s, 0.4s, 0.4s;
  transition-timing-function: ease, ease, ease, ease;
}

.grow {
  width: 100%;
  height: 100%;
  border-radius: 0%;
  transition-property: width, height, border-radius, transform;
  transition-duration: 0.4s, 0.4s, 0.4s, 0.4s;
  transition-timing-function: ease, ease, ease, ease;
}

.slide {
  transition: transform 0.7s ease;
}
</style>
`
  return template
}
