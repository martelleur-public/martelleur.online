import { PageTransitionType } from '../AppRouter/page-transition'

/**
 * Represents a mapping from route paths to corresponding component names or identifiers.
 * This type is used to define the relationship between paths in the application's URL space
 * and the components that should be rendered for those paths.
 */
// export type RouteMap = Record<string, string>

export interface RouterElement extends HTMLElement {
  render (): void
}

export interface IRouterElementVM {
  transition: PageTransitionType
  setTansitionAtrribute: (newValue: string, oldValue: string) => void
}

export interface IRouterOptions {
  defaultPage: string,
  transition: PageTransitionType,
  elementName: string
  shadowHost: boolean
}
