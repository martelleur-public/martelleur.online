// import { assertElementExist, customLogger } from '@utility'
// import { AppRouterEvent } from '../../events/app-router-event'
// import type {
//   RouteMap,
//   RouterElement,
//   IRouterOptions,
//   IRouterElementVM
// } from './RouterElement.type'
// import { createRouterTemplate } from './RouterElement.template'
// import { routerElementAttribute as a } from './RouterElement.attributes'
// import { PageTransitionType } from './page-transition'
// import { defaultRouterOptions } from './default-router-options'
// import { NewRoutesEvent } from '@router/events/new-routes-event'
// import { IBaseVM, createBaseViewModel } from '@components'
// import { createViewModel } from './RouterElement.factory'

// /**
//  * Creates a new ```RouterElement``` class that extends HTMLElement with routing capabilities.
//  * The returned class is capable of rendering content based on the current route and
//  * automatically updates when the route changes.
//  *
//  * @param routes - A `RouteMap` object that defines the mapping between route paths and page content.
//  * @param options - A optional configuration object that can be used to configure the router:
//  *          defaultPage: A string used to set default page as a string
//  *             Defaul value: '\<div\>\</div>\'.
//  *          transition: A string used to set page transition
//  *             Valid values: 'dark', 'slide', 'shrink', 'rotate-shrink', and 'none'.
//  *             Default values: 'dark'.
//  *          elementName: A string used to set element name
//  *             Valid values: Must conform to custom elements name.
//  *             Default value: 'app-router'
//  *
//  * @returns A class that can be instantiated to create a router element. The class is a custom
//  *          HTMLElement that implements the RouterElement interface.
//  */
// export function createRouterElement (
//   routes: RouteMap,
//   options: Partial<IRouterOptions> = {}
// ): new () => RouterElement {
//   class RouterElement
//     extends HTMLElement
//     implements RouterElement {
//     static readonly options = { ...defaultRouterOptions, ...options } as const
//     static readonly elementAttributes = a
//     private readonly _vm: IRouterElementVM
//     private readonly _baseVm: IBaseVM
//     private _routes = routes
//     private _pageContainer!: HTMLDivElement
//     private _currentTransition = false

//     constructor () {
//       super()
//       this._bindEventHandlersToThis()
//       this._baseVm = createBaseViewModel()
//       this._vm = createViewModel(this, RouterElement.options.transition)
//     }

//     private _bindEventHandlersToThis (): void {
//       this._handlePageTransition = this._handlePageTransition.bind(this)
//       this._handleNewRoutes = this._handleNewRoutes.bind(this)
//       this.render = this.render.bind(this)
//     }

//     static get observedAttributes (): string[] {
//       return Object.values(RouterElement.elementAttributes)
//     }

//     attributeChangedCallback (
//       name: string,
//       oldValue: string,
//       newValue: string
//     ): void {
//       if (this._baseVm.isAttributeValueSame(newValue, oldValue)) return
//       switch (name) {
//       case a.TRANSITION: this._vm.setTansitionAtrribute(newValue, oldValue); break
//       default: this._baseVm.handleDefaultAttributeChg(newValue, oldValue)
//       }
//     }

//     connectedCallback (): void {
//       this.appendChild(createRouterTemplate(RouterElement.options.defaultPage).content.cloneNode(true))
//       this._pageContainer = assertElementExist(this.querySelector<HTMLDivElement>('.page-container'))
//       this.classList.add('page-container')
//       // document.addEventListener(NewRoutesEvent.eventName, this._handleNewRoutes)
//       // window.addEventListener('popstate', this._handlePageTransition)
//       // window.addEventListener(AppRouterEvent.eventName, this._handlePageTransition)
//       // this.render()
//     }

//     private _handleNewRoutes (event: Event): void {
//       if (event instanceof NewRoutesEvent) {
//         const newRoutes = event.detail
//         this._routes = { ...this._routes, ...newRoutes }
//         if (this._routes['/default'] != null &&
//           this._routes['/default'] !== RouterElement.options.defaultPage) {
//           RouterElement.options.defaultPage = this._routes['/default']
//         }
//         this.render()
//       }
//     }

//     render (): void {
//       const path = window.location.pathname
//       const pageTag = this._routes[path] ?? RouterElement.options.defaultPage
//       this._clearPageContainer()
//       const newPage = document.createElement(this._extractElementName(pageTag) ?? '')
//       this._pageContainer.appendChild(newPage)
//     }

//     private _clearPageContainer (): void {
//       while (this._pageContainer.firstChild != null) {
//         this._pageContainer.removeChild(this._pageContainer.firstChild)
//       }
//     }

//     private _extractElementName (elementTag: string): string | null {
//       const match = elementTag.match(/^<([^\s>]+)/)
//       return match ? match[1] : null
//     }

//     private _handlePageTransition (): void {
//       switch (this._vm.transition) {
//       case 'dark': this._renderDarkTransition(); break
//       case 'slide': this._renderSlideTransition(); break
//       case 'shrink': this._renderShrinkTransition(); break
//       case 'rotation-shrink': this._renderShrinkTransition(true); break
//       default: this._renderNoneTransition()
//       }
//     }

//     private _renderDarkTransition (): void {
//       this._clearPageContainer()
//       this._pageContainer.classList.add('darken')
//       setTimeout(() => {
//         this.render()
//         this._pageContainer.classList.remove('darken')
//         this._pageContainer.classList.add('lighten')
//         setTimeout(() => {
//           this._pageContainer.classList.remove('lighten')
//         }, 300)
//       }, 300)
//     }

//     private _renderShrinkTransition (roation = false): void {
//       this._pageContainer.classList.add('shrink')
//       if (roation) { this._pageContainer.style.transform = 'rotate(360deg)' }
//       setTimeout(() => {
//         this.render()
//         this._pageContainer.classList.remove('shrink')
//         this._pageContainer.classList.add('grow')
//         if (roation) { this._pageContainer.style.transform = 'rotate(-360deg)' }
//         setTimeout(() => {
//           this._pageContainer.classList.remove('grow')
//         }, 500)
//       }, 500)
//     }

//     private _renderSlideTransition (): void {
//       if (this._currentTransition) {
//         return
//       }
//       this._currentTransition = true

//       const path = window.location.pathname
//       const previousePage = this._pageContainer.firstElementChild
//       if (!(previousePage instanceof HTMLElement)) {
//         return
//       }

//       const nextPageTag = this._routes[path] ?? RouterElement.options.defaultPage
//       const nextPageElementName = this._extractElementName(nextPageTag)
//       if (nextPageElementName === null) {
//         return
//       }

//       this.setAttribute('style', 'width: 200%')
//       const nextPage = document.createElement(nextPageElementName)
//       this._activateSlideTransition(previousePage, nextPage)
//     }

//     private _activateSlideTransition (previousePage: HTMLElement, nextPage: HTMLElement): void {
//       this._pageContainer.appendChild(nextPage)
//       // Wait for the next frame to allow the browser to render next page
//       requestAnimationFrame(() => {
//         this._pageContainer.classList.add('slide')
//         this._pageContainer.style.transform = 'translateX(-50%)'
//         setTimeout(() => {
//           this._pageContainer.classList.remove('slide')
//           this._pageContainer.removeChild(previousePage)
//           this.setAttribute('style', 'width: 100%')
//           this._pageContainer.style.transform = 'translateX(0)'
//           this._currentTransition = false
//         }, 700)
//       })
//     }

//     private _renderNoneTransition (): void {
//       this.render()
//     }

//     disconnectedCallback (): void {
//       window.removeEventListener('popstate', this._handlePageTransition)
//       document.removeEventListener(NewRoutesEvent.eventName, this._handleNewRoutes)
//       window.removeEventListener(AppRouterEvent.eventName, this._handlePageTransition)
//     }
//   }

//   if (customElements.get(RouterElement.options.elementName)) {
//     customLogger(`${RouterElement.options.elementName} is already defined.`)
//   } else {
//     customElements.define(RouterElement.options.elementName, RouterElement)
//   }
//   return RouterElement
// }
