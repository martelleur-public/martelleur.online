import { IRouterElementVM, RouterElement } from './RouterElement.type'
import { RouterElementVM } from './RouterElement.vm'
import { PageTransitionType } from '../AppRouter/page-transition'

export const createViewModel = (
  element: RouterElement,
  transition: PageTransitionType
): IRouterElementVM => {
  return new RouterElementVM(element, transition)
}
