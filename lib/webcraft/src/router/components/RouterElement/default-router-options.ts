import { IRouterOptions } from './RouterElement.type'
import { PageTransition } from '../AppRouter/page-transition'

export const defaultRouterOptions: IRouterOptions = {
  defaultPage: '<div></div>',
  transition: PageTransition.DARK,
  elementName: 'inner-router',
  shadowHost: false
}
