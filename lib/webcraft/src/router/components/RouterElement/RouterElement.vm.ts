import { IRouterElementVM, RouterElement } from './RouterElement.type'
import { PageTransition, PageTransitionType } from '../AppRouter/page-transition'

export class RouterElementVM implements IRouterElementVM {
  transition: PageTransitionType
  _view: RouterElement

  constructor (view: RouterElement, transition: PageTransitionType) {
    this._view = view
    this.transition = transition
  }

  setTansitionAtrribute (newValue: string, oldValue: string): void {
    if (this._isPageTransitionType(newValue)) {
      this.transition = newValue
    } else if (this._isPageTransitionType(oldValue)) {
      this.transition = oldValue
    } else {
      this.transition = PageTransition.DEFAULT
    }
  }

  private _isPageTransitionType (value: any): value is PageTransitionType {
    return Object.values(PageTransition).includes(value)
  }
}
