import { ViewModelFactory } from '@components/core'
import type { IRouteItem, IRouteItemVM } from './RouteItem.type'
import { RouteItemVM } from './RouteItem.vm'

export const createViewModel:
ViewModelFactory<IRouteItem, IRouteItemVM> = (element: IRouteItem):
IRouteItemVM => {
  return new RouteItemVM(element)
}
