export interface IRouteItem extends HTMLElement {}

export interface IRouteItemVM {
  isDefaultRoute: boolean
  setPathAttribute: (value: string) => void
  setComponentAttribute: (value: string) => void
  handleAttributeDefault: () => void
}
