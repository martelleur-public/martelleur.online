import { routeItemAttributes } from './RouteItem.attributes'
import type { IRouteItem, IRouteItemVM } from './RouteItem.type'

export class RouteItemVM implements IRouteItemVM {
  isDefaultRoute = false

  constructor (private readonly view: IRouteItem) {}

  setPathAttribute (value: string): void {
    this.view.setAttribute(routeItemAttributes.PATH, value)
  }

  setComponentAttribute (value: string): void {
    this.view.setAttribute(routeItemAttributes.COMPONENT, value)
  }

  handleAttributeDefault (): void {
    this.isDefaultRoute = this.view.hasAttribute(routeItemAttributes.DEFAULT)
  }
}
