export const routeItemAttributes = {
  PATH: 'path',
  COMPONENT: 'component',
  DEFAULT: 'default'
} as const
