import {
  type IBaseVM,
  NamedHTMLElement,
  createBaseViewModel
} from '@components'
import { DefineElement } from '@utility'
import { routeItemAttributes as attributes } from './RouteItem.attributes'
import { assertElementIsInsideParent } from '@router/error'
import { RouteGroup } from '../RouteGroup/RouteGroup'
import { IRouteItem, IRouteItemVM } from './RouteItem.type'
import { createViewModel } from './RouteItem.factory'

@DefineElement('route-item')
export class RouteItem
  extends NamedHTMLElement
  implements IRouteItem {
  static readonly elementAttributes = attributes
  private _baseVM: IBaseVM
  private _vm: IRouteItemVM

  constructor () {
    super()
    this._baseVM = createBaseViewModel()
    this._vm = createViewModel(this)
  }

  static get observedAttributes (): string[] {
    return Object.values(attributes)
  }

  attributeChangedCallback (
    name: string,
    oldValue: string,
    newValue: string
  ): void {
    if (this._baseVM.isAttributeValueSame(newValue, oldValue)) return
    switch (name) {
    case attributes.PATH: this._vm.setPathAttribute(newValue); break
    case attributes.COMPONENT: this._vm.setComponentAttribute(newValue); break
    case attributes.DEFAULT: this._vm.handleAttributeDefault(); break
    default: this._baseVM.handleDefaultAttributeChg(newValue, oldValue)
    }
  }

  connectedCallback (): void {
    assertElementIsInsideParent(this, RouteGroup.elementName)
  }
}
