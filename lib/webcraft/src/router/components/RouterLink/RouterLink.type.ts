export interface IRouterLink extends HTMLElement {
  readonly link: HTMLAnchorElement
}

export interface IRouterLinkVM {
  activeLink: boolean
  setTitleAttribute: (newValue: string) => void
  setHrefAttribute: (newValue: string) => void
  setDataTextAttribute: (newValue: string) => void
  handleRouting: (event: MouseEvent) => void
  handleUpdateActiveLink: () => void
}
