export const attributeRouterLink = {
  DATA_TEXT: 'data-text',
  HREF: 'href',
  TITLE: 'title'
} as const
