import { ViewModelFactory } from '@components/core'
import { IRouterLink, IRouterLinkVM } from './RouterLink.type'
import { RouterLinkVM } from './RouterLink.vm'

export const createViewModel:
ViewModelFactory<IRouterLink, IRouterLinkVM> =
(routerLink: IRouterLink): IRouterLinkVM => {
  return new RouterLinkVM(routerLink)
}
