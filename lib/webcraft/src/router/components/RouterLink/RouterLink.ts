import { DefineElement, assertElementExist } from '@utility'
import { routerLinkTemplate } from './RouterLinkTemplate'
import { AppRouterEvent } from '../../events/app-router-event'
import { attributeRouterLink as attributes} from './RouterLink.attribute'
import { IRouterLink, IRouterLinkVM } from './RouterLink.type'
import { IBaseVM, NamedHTMLElement, createBaseViewModel } from '@components'
import { createViewModel } from './RouterLink.factory'

/**
 * Represents a custom HTML element for routing links within a single-page application (SPA).
 * The `RouterLink` extends `HTMLElement` to provide enhanced functionality for navigating between routes.
 * It listens for clicks on anchor elements to update the browser's history without reloading the page.
 */

export const nameRouterLinkElement = 'router-link'

@DefineElement(nameRouterLinkElement)
export class RouterLink
  extends NamedHTMLElement
  implements IRouterLink {
  static readonly elementName = nameRouterLinkElement
  static readonly elementAttributes = attributes
  readonly link: HTMLAnchorElement
  private readonly _vm: IRouterLinkVM
  private readonly _baseVm: IBaseVM

  constructor () {
    super()
    const root = this.attachShadow({ mode: 'open' })
    root.appendChild(routerLinkTemplate.content.cloneNode(true))
    this.link = assertElementExist(root.querySelector<HTMLAnchorElement>('#router-link'))
    this._vm = createViewModel(this)
    this._baseVm = createBaseViewModel()
  }

  static get observedAttributes (): string[] {
    return Object.values(attributes)
  }

  attributeChangedCallback (
    name: string,
    oldValue: string,
    newValue: string
  ): void {
    if (this._baseVm.isAttributeValueSame(newValue, oldValue)) return
    switch (name) {
    case attributes.TITLE: this._vm.setTitleAttribute(newValue); break
    case attributes.HREF: this._vm.setHrefAttribute(newValue); break
    case attributes.DATA_TEXT: this._vm.setDataTextAttribute(newValue); break
    default: this._baseVm.handleDefaultAttributeChg(newValue, oldValue)
    }
  }

  connectedCallback (): void {
    this._vm.handleUpdateActiveLink()
    window.addEventListener(AppRouterEvent.eventName, this._vm.handleUpdateActiveLink)
    this.link.addEventListener('click', this._vm.handleRouting)
  }

  disconnectedCallback (): void {
    window.removeEventListener(AppRouterEvent.eventName, this._vm.handleUpdateActiveLink)
  }
}
