import { AppRouterEvent } from '@router/events/app-router-event'
import type { IRouterLink, IRouterLinkVM } from './RouterLink.type'

export class RouterLinkVM implements IRouterLinkVM {
  private _activeLink = false

  constructor (private readonly view: IRouterLink) {
    this.handleUpdateActiveLink = this.handleUpdateActiveLink.bind(this)
    this.handleRouting = this.handleRouting.bind(this)
  }

  get activeLink (): boolean {
    return this._activeLink
  }

  set activeLink (value: boolean) {
    this._activeLink = value
    /**
     * Use time out to fix timing issue when router link component
     * are nested to update active link classes.
     */
    setTimeout(() => {
      if (this._activeLink) this.view.link.classList.add('active-link')
      else this.view.link.classList.remove('active-link')
    }, 0)
  }

  setTitleAttribute (newValue: string): void {
    this.view.link.setAttribute('title', newValue)
  }

  setHrefAttribute (newValue: string): void {
    this.view.link.setAttribute('href', newValue)
  }

  setDataTextAttribute (newValue: string): void {
    this.view.link.textContent = newValue
  }

  handleRouting (event: MouseEvent): void {
    event.preventDefault()
    const target = event.target
    if (!(target instanceof HTMLAnchorElement)) {
      return
    }
    const href = target.getAttribute('href')
    href != null && this.updateWindowHistory(href)
    this.dispatchAppRouterEvent()
  }

  updateWindowHistory (href: string): void {
    history.pushState({}, '', href)
  }

  dispatchAppRouterEvent (): void {
    const appRouterEvent = new AppRouterEvent()
    dispatchEvent(appRouterEvent)
  }

  handleUpdateActiveLink (): void {
    this.updateActiveLink()
  }

  updateActiveLink (): void {
    const currentHref = location.href
    const linkHref = `${this.view.link.getAttribute('href') ?? ''}`
    this.activeLink = linkHref === currentHref
  }
}
