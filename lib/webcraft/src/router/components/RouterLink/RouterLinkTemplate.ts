/**
 * @TODO How to use css style varibales if component is a library
 * CSS Color variables must be injected as arguments to the library.
 *
 * Use Container query?
 */

export const routerLinkTemplate = document.createElement('template')
routerLinkTemplate.innerHTML = `
<a
    id="router-link"
    title="Add title"
    href="Add href"
    data-test="router-link">
    Add text
</a>
<style>
a {
    box-sizing: border-box;
    padding: 0;
    margin: 0;
    text-decoration: none;
    color: var(--TEXT_PRIMARY, #000000);
}

a:hover,
a:active {
    color: var(--TEXT_ACTIVE, #6f00ff);
}

.active-link {
    color: var(--TEXT_ACTIVE, #6f00ff);
}
</style>
`
