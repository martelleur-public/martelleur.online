export class AppRouterEvent extends CustomEvent<null> {
  static readonly eventName = 'on-app-routing'

  constructor () {
    super(AppRouterEvent.eventName, {
      bubbles: true,
      cancelable: true
    })
  }
}
