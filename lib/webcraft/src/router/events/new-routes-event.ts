import type { RouteMap } from '@router'

export class NewRoutesEvent extends CustomEvent<RouteMap> {
  static readonly eventName = 'on-new-routes'

  constructor (detail: RouteMap) {
    super(NewRoutesEvent.eventName, {
      bubbles: true,
      composed: true,
      cancelable: true,
      detail: detail
    })
  }
}
