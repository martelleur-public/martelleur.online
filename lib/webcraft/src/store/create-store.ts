import { Store } from './store'
import type {
  IStore,
  Middleware,
  Reducer
} from './types'

/**
 * Creates a store that holds the state tree. The store is responsible for
 * managing the state with the help of reducers and optional middlewares.
 *
 * @param reducer - A reducer function that returns the next state tree, given
 * the current state tree and an action to handle.
 * @param initialState - The initial state of the store.
 * @param middlewares - An optional array of middleware functions that are
 * applied to the store.
 * @returns The store that holds the complete state of your app. This store's
 * API includes methods to update the state, register listeners, and dispatch actions.
 *
 * @typeParam S - The type of state managed by this store.
 * @typeParam A - The type of actions that can be dispatched to this store, must have a 'type' property.
 */
export function createStore<S, A extends { type: string }> (
  reducer: Reducer<S, A>,
  initialState: S,
  middlewares: Array<Middleware<S, A>> = []
): IStore<S, A> {
  return new Store(reducer, initialState, middlewares)
}
