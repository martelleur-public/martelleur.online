import { customLogger } from '@utility'
import type { IStore } from '@store'
import type { IStoreSubscriber } from '../types'

export function createStoreSubscriber<S, A extends { type: string }> (
  store: IStore<S, A>
): new () => HTMLElement & IStoreSubscriber<S, A> {
  return class StoreSubscriber
    extends HTMLElement
    implements IStoreSubscriber<S, A> {
    private readonly _store: IStore<S, A>
    readonly unsubscribeToStore: () => void

    constructor () {
      super()
      this._store = store
      this.unsubscribeToStore = store.subscribe(
        this.subscribeToStore.bind(this),
        this.constructor.name
      )
    }

    get store (): IStore<S, A> {
      return this._store
    }

    subscribeToStore (storeState: S): void {
      customLogger('state: ', storeState)
      throw new Error('subscribeToStore not implemented should be implemented in child class')
    }

    disconnectedCallback (): void {
      this.unsubscribeToStore()
    }
  }
}
