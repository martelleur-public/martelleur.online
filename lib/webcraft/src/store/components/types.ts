import type { IStore } from '../types'

export interface IStoreSubscriber<S, A extends { type: string }> {
  store: IStore<S, A>
  subscribeToStore (storeState: S): void
  unsubscribeToStore (): void
  disconnectedCallback (): void
}

export interface IStoreProvider<S, A extends { type: string }> {
  store: IStore<S, A>
}

export interface IReactiveState<R> {
  state: R
  isEqual: (value: IReactiveState<R>) => boolean
}

export interface IReactiveHTMLElement {
  configureReactiveState: <R extends object> (
    reactiveState: R,
    factory?: () => IReactiveState<R>
  ) => IReactiveState<R>
  render(): void
}
