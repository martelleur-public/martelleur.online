import type { IReactiveState } from '../types'

class ReactiveState<R extends object> implements IReactiveState<R> {
  state: R

  constructor (state: R) {
    this.state = state
  }

  isEqual (value: IReactiveState<R>): boolean {
    if (typeof value !== 'object' || value == null || !('state' in value)) {
      return false
    }
    return this._isDeepEqual(this.state, value.state)
  }

  private _isDeepEqual (objA: any, objB: any): boolean {
    if (objA === objB) {
      return true
    }

    if (typeof objA !== 'object' ||
      objA === null ||
      typeof objB !== 'object' ||
      objB === null) {
      return false
    }

    const keysA = Object.keys(objA)
    const keysB = Object.keys(objB)

    if (keysA.length !== keysB.length) {
      return false
    }

    for (const key of keysA) {
      if (!keysB.includes(key) || !this._isDeepEqual(objA[key], objB[key])) {
        return false
      }
    }

    return true
  }
}

export function createReactiveState<R extends object> (state: R): IReactiveState<R> {
  return new ReactiveState<R>(state)
}
