import { customLogger } from '../../../utility'
import { createReactiveState } from './create-reactive-state'
import type { IReactiveState, IReactiveHTMLElement } from '../types'

/**
 * @TODO Rename configureReactiveState to setReactiveState or useReactiveState.
 */
export class ReactiveHTMLElement
  extends HTMLElement
  implements IReactiveHTMLElement {
  configureReactiveState<R extends object> (initState: R): IReactiveState<R> {
    const target = createReactiveState<R>(initState)
    return new Proxy(target, {
      set: (
        target: IReactiveState<R>,
        property: keyof IReactiveState<R>,
        value: R,
        receiver: any): boolean => {
        customLogger('Proxy trap set used:')
        customLogger('Target:', target)
        customLogger('Target[property]:', target[property])
        customLogger('Value:', value)
        customLogger('Receiver:', receiver)
        if (target.isEqual(createReactiveState<R>(value))) {
          return false // If equal the trap of the set method return false
        }
        const result = Reflect.set(target, property, value, receiver)
        if (result) { // Only call render if the property was successfully set
          this.render() // Call render
        }
        return result
      }
    })
  }

  render (): void {
    throw new Error('Render not implemented should be implemented in child class')
  }

  disconnectedCallback (): void {
    customLogger(`${this.constructor.name} removed from the document`)
  }
}
