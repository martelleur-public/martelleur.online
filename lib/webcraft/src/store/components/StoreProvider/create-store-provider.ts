import type { IStore } from '@store'
import type { IStoreProvider } from '../types'

export function createStoreProvider<S, A extends { type: string }> (
  store: IStore<S, A>
): new () => HTMLElement & IStoreProvider<S, A> {
  return class StoreSubscriber
    extends HTMLElement
    implements IStoreProvider<S, A> {
    private readonly _store: IStore<S, A>

    constructor () {
      super()
      this._store = store
    }

    get store (): IStore<S, A> {
      return this._store
    }
  }
}
