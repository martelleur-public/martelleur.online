import type {
  Dispatch,
  Middleware,
  MiddlewareStoreApi
} from './types'

/**
 * Applies middleware to the dispatch function of a store.
 *
 * Middleware is a higher-order function that composes a dispatch function to return a new dispatch function.
 * It is useful for logging actions, performing side effects like asynchronous requests, manipulating actions,
 * or delaying actions. applyMiddleware enhances the store's dispatch function with the given middlewares.
 *
 * @param {Dispatch<A>} baseDispatch - The original dispatch function of the store.
 * @param {() => S} getState - A function that returns the current state of the store.
 * @param {Array<Middleware<S, A>>} middlewares - An array of middleware functions to be applied to the dispatch function.
 *
 * @returns {Dispatch<A>} A dispatch function that has been enhanced by the provided middlewares.
 *
 * @typeParam S - The state object type.
 * @typeParam A - The action object type, must have a 'type' property.
 */
export function applyMiddleware<S, A extends { type: string }> (
  baseDispatch: Dispatch<A>,
  getState: () => S,
  middlewares: Array<Middleware<S, A>>
): Dispatch<A> {
  const middlewareStoreApi: MiddlewareStoreApi<S, A> = {
    getState: getState,
    dispatch: (action: A) => { baseDispatch(action) }
  }

  const middlewareChain = middlewares.map(middleware => {
    return middleware(middlewareStoreApi)
  })

  let composedDispatch = baseDispatch
  for (let i = middlewareChain.length - 1; i >= 0; i--) {
    const currentMiddleware = middlewareChain[i]
    composedDispatch = currentMiddleware(composedDispatch)
  }

  return composedDispatch
}
