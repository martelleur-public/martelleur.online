import type { Reducer } from './types'

/**
 * Creates a single reducer function from an object whose values are different
 * reducing functions into a single state object, where each key corresponds to a
 * piece of the state controlled by that reducer. The resulting reducer calls
 * every child reducer, and gathers their results into a single state object.
 *
 * The shape of the state object matches the keys of the passed `reducers` object.
 *
 * @typeParam S - The state object type that the `combineReducers` is managing.
 * @typeParam A - The action object type, must have a 'type' property.
 *
 * @param reducers - An object where keys correspond to different pieces of state
 *                   controlled by their respective reducers. Each reducer function
 *                   is responsible for managing its own part of the global state.
 *                   The state produced by combineReducers() namespaces the states
 *                   of each reducer under their keys as passed to combineReducers()
 *
 * @returns A reducer function that invokes every reducer inside the passed object,
 *          and builds a state object with the same shape.
 */
export function combineReducers<S, A extends { type: string }> (
  reducers: {
    [K in keyof S]: Reducer<S[K], A>
  }
): Reducer<S, A> {
  return function combined (state: S, action: A): S {
    let hasChanged = false
    const nextState: Partial<S> = {}

    for (const key in reducers) {
      if (Object.prototype.hasOwnProperty.call(reducers, key)) {
        const reducer = reducers[key as keyof S]
        const previousStateForKey = state[key as keyof S]
        const nextStateForKey = reducer(previousStateForKey, action)

        if (nextStateForKey !== previousStateForKey) {
          hasChanged = true
        }

        nextState[key as keyof S] = nextStateForKey
      }
    }
    return hasChanged ? { ...state, ...nextState } : state
  }
}
