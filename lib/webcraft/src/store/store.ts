import { customLogger } from '../utility'
import type {
  IStore,
  Middleware,
  Dispatch,
  Reducer
} from './types'
import { applyMiddleware } from './apply-middleware'

/**
 * Implements a simple Redux-like store with state management and middleware support.
 * Allows for state to be updated via dispatching actions, and listeners to be notified on state changes.
 *
 * @typeParam S - The state object type that the store will manage.
 * @typeParam A - The action object type, which must have a 'type' property.
 */
export class Store<S, A extends { type: string }> implements IStore<S, A> {
  private readonly reducer: Reducer<S, A>
  private state: S
  private readonly listeners: Array<(state: S) => void>
  private readonly listenerNames: string[] = []
  private readonly dispatchWithMiddleware: (action: A) => void

  /**
   * Creates an instance of the Store.
   *
   * @param reducer - The reducer function that determines how the state is updated with actions.
   * @param initialState - The initial state of the store.
   * @param middlewares - An optional array of middleware functions to be applied to the dispatch function.
   */
  constructor (reducer: Reducer<S, A>, initialState: S, middlewares: Array<Middleware<S, A>> = []) {
    this.reducer = reducer
    this.state = initialState
    this.listeners = []
    const baseDispatch = this.getBaseDispatch()
    this.dispatchWithMiddleware = applyMiddleware(
      baseDispatch, this.getState.bind(this), middlewares
    )
  }

  /**
   * The base dispatch function that updates the state and notifies listeners.
   * @returns A dispatch function that can be called with an action.
   */
  private getBaseDispatch (): Dispatch<A> {
    const baseDispatch: Dispatch<A> = (action: A): void => {
      this.state = this.reducer(this.state, action)
      this.listeners.forEach(listener => { listener(this.state) })
    }
    return baseDispatch
  }

  /**
   * Dispatches an action to the store.
   * This is the public dispatch method that will apply any middleware before
   * calling the base dispatch function.
   *
   * @param action - The action to be dispatched.
   */
  dispatch (action: A): void {
    this.dispatchWithMiddleware(action)
  }

  /**
   * Returns the current state of the store.
   * @returns The current state.
   */
  getState (): S {
    return this.state
  }

  /**
   * Adds a listener that will be invoked on every state update.
   *
   * @param listener - The callback to be invoked on state update.
   * @param listenerName - An optional name for the listener for easier identification.
   * @returns A function to unsubscribe the listener.
   */
  subscribe (listener: (state: S) => void, listenerName?: string): () => void {
    this.listeners.push(listener)
    const updatedListenerName = listenerName ?? 'Anonymous listener name'
    this.listenerNames.push(updatedListenerName)
    customLogger(`New listener: ${updatedListenerName}`)
    customLogger('All listeners:')
    this.listenerNames.forEach(listenerName => {
      customLogger(listenerName)
    })
    return () => {
      const index = this.listeners.indexOf(listener)
      if (index > -1) {
        customLogger(`Listener ${this.listenerNames[index]} unsubscribed from store`)
        this.listeners.splice(index, 1)
        this.listenerNames.splice(index, 1)
      }
    }
  }
}
