/**
 * Represents the store with state management capabilities.
 * Allows dispatching actions, subscribing to state changes, and getting the current state.
 *
 * @typeParam S - The state object type that the store will manage.
 * @typeParam A - The action object type, which must have a 'type' property.
 */
export interface IStore<S, A extends { type: string }> {
  dispatch: (action: A) => void
  getState: () => S
  subscribe: (listener: (state: S) => void, listenerName?: string) => () => void
}

/**
 * Defines a listener function type that receives the state.
 *
 * @typeParam S - The state object type.
 */
export type Listener<S> = (state: S) => void

/**
 * Represents a function that takes an action and does not return anything.
 *
 * @typeParam A - The action object type.
 */
export type Next<A> = (action: A) => void

/**
 * Dispatch function type, used to dispatch actions.
 *
 * @typeParam A - The action object type.
 */
export type Dispatch<A> = (action: A) => void

/**
 * Reducer function type, used to specify how the state changes in response to actions.
 *
 * @typeParam S - The state object type.
 * @typeParam A - The action object type.
 */
export type Reducer<S, A> = (state: S, action: A) => S

/**
 * Represents the API available to middleware, allowing state retrieval and action dispatching.
 *
 * @typeParam S - The state object type.
 * @typeParam A - The action object type.
 */
export interface MiddlewareStoreApi<S, A extends { type: string }> {
  getState: () => S
  dispatch: (action: A) => void
}

/**
 * Middleware function type, used to create middleware for the store.
 * Middleware can intercept actions, modify them, delay them, or even replace them.
 *
 * @typeParam S - The state object type.
 * @typeParam A - The action object type.
 */
export type Middleware<S, A extends { type: string }> =
  (store: MiddlewareStoreApi<S, A>) =>
  (next: Next<A>) =>
  (action: A) => void
