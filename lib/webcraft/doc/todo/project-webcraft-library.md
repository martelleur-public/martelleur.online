# Project WebCraft Library

The library, currently named ```WebCraft```, is created with the two ideas:

1. Web Components is great for reusable front-end development.
2. Consistent interface when using a library natively with Web Components or with amazing frameworks like React, Vue, Angular, Svelte, etc gives power and freedom to developers.

## Main Components

The library consist of four main components plus UI framework wrappers that will be published in separate libraries:

### Component 1: A Reactive Redux-like store with reactive capacity.

### Component 2: SPA Routing with page transitions for improved UX.

- **Example routing with Web Components using library WebCraft:**
```html
<!-- 
Component "web-page", "game-page", "git-page", "not-found-page" is defined
custom elements used for pages.
-->
<app-router transition="${PageTransition.DARK}">
  <route-group path="">
    <route-item path="index" component="home-page"></route-item>
    <route-item path="about" component="about-page"></route-item>
    <route-item default component="not-found-page"></route-item>
  </route-group>
  <route-group path="utility">
    <route-item path="index" component="utility-page"></route-item>
    <route-item path="search" component="game-page"></route-item>
  </route-group>
</app-router>
```

- **Example routing in React using wrapper library ReactCraft:**
```jsx
/**
 * Component WebPage, GamePage, GitPage, NotFoundPage is JSX elements
 * used for pages.
 */
<AppRouter transition={PageTransition.DARK}>
  <RouteGroup path="">
    <RouteItem path="index" component={<HomePage />}/>
    <RouteItem path="about" component={<AboutPage />}/>
    <RouteItem default component={<NotFoundPage />}/>
  </RouteGroup>
  <RouteGroup path="utility">
    <RouteItem path="index" component={<UtilityPage />}/>
    <RouteItem path="search" component={<SearchPage />}/>
  </RouteGroup>
</AppRouter>
```

### Components 3: Reusable Components, e.g., responsive layouts and dialogs.

- **Reusable layout with Web Components using library WebCraft:**
```html
<responsive-layout dynamic-height>
    <app-header slot="header"></app-header>
    <app-router slot="body"></app-router>
    <app-footer slot="footer"></app-footer>
</responsive-layout>
`
```

- **Reusable layout React using wrapper library ReactCraft:**
```jsx
<ResponsiveLayout dynamicHeight>
    <AppHeader slot="header"/>
    <AppRouter slot="body"/>
    <AppFooter slot="footer"/>
</ResponsiveLayout>
```

### 4. Utility functions and decorators to accelerate a more maintainable project.

### 5. Framework Wrapper Libraries
    - ```ReactCraft:``` A wrapper for React, under active development.
    - ```VueCraft:``` A wrapper for Vue, future goal.

## Current Status

- The library ```WebCraft``` is used in production on the site [martelleur.online](https://martelleur.online) and the wrapper library ```ReactCraft``` is tested in development mode for the same site.

## Current Goal

- Provide clear, complete, and compelling documentation for the libraries ```WebCraft``` and ```ReactCraft```.

- Publish the libraries under the names `martelleur/webcraft` and `martelleur/reactcraft`

## Library Project Backlog

1. Name consideration
    - @martelleur/webcraft or @fourtytwo/webcraft
    - @martelleur/webweld or @fourtytwo/webweld

2. README.md
    - Overview: A concise introduction to the library. What it does, its purpose, and its target audience.
    - Installation Instructions: Step-by-step instructions on how to install the library.
    - Usage: Examples of how to use your project, including code snippets or command-line examples.
    - License: Information about the project's license, usually a short snippet with a link to the full license text.

3. Documentation Folder or Wiki, including:
    - API Documentation: Detailed descriptions of APIs, including parameters, return types, and examples. Use Typedoc with markdown and code snippets
    - Architecture: Information on the project's architecture, design decisions, and high-level overview.
    - FAQs: Answers to frequently asked questions.
    - Tutorials and Guides: Step-by-step tutorials or guides for more complex uses of your project.

4. LICENSE
    - License Text: The full license text under which your project is made available. Use MIT, Apache 2.0, or GNU GPL.

5. CHANGELOG.md
    - Version History: A record of all notable changes made to the project, organized by version. This includes new features, bug fixes, and breaking changes.

6. Create a separate GitLab or GitHub project.

7. Publishing package: Publish package to npm with scoped name, name the package @<username>/<package_name>.

8. Continue to use and test the library with current projects, e.g. ```martelleur.online```.


## Future Contributing Files

1. CONTRIBUTING.md
    - Contribution Guidelines: Detailed information on how others can contribute to the project, including coding standards, pull request guidelines, and the process for submitting issues.
    - Community Standards: Expectations for behavior within the project's community, referencing a code of conduct if you have one.

2. CODE_OF_CONDUCT.md
    - Community Guidelines: A code of conduct outlines the expectations for behavior and interaction within your project's community. It also provides details on how to report unacceptable behavior.
