## Reusable layout components

Use Web Components to create reusable layouts for websites.

## Reusable Layout using Web Components with Slot elements as placeholders
In this example, a basic layout component is defined with slots for a header, content, and footer. You can then use this layout component throughout your website, changing the content of each slot as needed.
Ref: [slot element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/slot)

```html
<!-- Define the Web Component -->
<script>
  class BasicLayout extends HTMLElement {
    constructor() {
      super();
      const template = document.createElement('template');

      template.innerHTML = `
        <style>
          .container {
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 20px;
          }
        </style>
        <div class="container">
          <slot name="header"></slot>
          <slot name="content"></slot>
          <slot name="footer"></slot>
        </div>
      `;

      this.attachShadow({ mode: 'open' }).appendChild(template.content.cloneNode(true));
    }
  }

  customElements.define('basic-layout', BasicLayout);
</script>

<!-- Use the Web Component -->
<basic-layout>
  <header slot="header">Header Content</header>
  <div slot="content">Main Content</div>
  <footer slot="footer">Footer Content</footer>
</basic-layout>
```

## 5 Website Layouts

1. The Z-Pattern Layout: Ideal for simple or minimalist websites, this layout guides the user’s eye in a Z pattern, starting from the top-left corner (logo), moving horizontally to the top-right (navigation), diagonally to the bottom-left (main content), and finally horizontally again to the bottom-right (call-to-action or additional content). It's great for landing pages or single-page websites.

2. The F-Pattern Layout: Suited for websites with lots of content, like blogs or news sites. The user’s eye naturally moves in an F pattern, scanning from left to right at the top, moving down a bit, and then scanning again, creating areas for headings, subheadings, and content blocks. This layout optimizes for readability and scanning.

3. Full-Screen Photo Layout: Used predominantly in portfolios, photography sites, or high-impact landing pages. The layout features a large, full-screen photo or video as the background with minimal text overlays for headings or calls to action. It's effective for capturing attention and setting a mood or tone.

4. Card-Based Layout (Grid): Perfect for e-commerce sites, blogs, or portfolios where different sections or items need to be showcased. Content is organized into cards (rectangular blocks), which are easy to scan and interact with. This layout is highly flexible and responsive, adapting well to different screen sizes.

5. Magazine Layout: Mimics the layout of traditional magazines, utilizing multiple columns, featured images, pull quotes, and varied typographic styles to create a dynamic, engaging reading experience. It's best for editorial websites, online magazines, or news sites that want to offer a rich content experience.






