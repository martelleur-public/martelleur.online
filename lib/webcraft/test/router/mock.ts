import {
  RouteMap,
  IRouterOptions,
  PageTransition
} from '../../src/router'

export const mockRoutes: RouteMap = {
  '': '<web-page></web-page>',
  '/': '<web-page></web-page>',
  '/index.html': '<web-page></web-page>',
  '/index': '<web-page></web-page>',
  '/game': '<game-page></game-page>',
  '/git': '<git-page></git-page>',
  '/tool': '<tool-page></tool-page>',
  '/default': '<not-found-page></not-found-page>'
}

export const mockRouterOptions: Partial<IRouterOptions> = {
  defaultPage: mockRoutes['/default'],
  transition: PageTransition.NONE,
  elementName: 'inner-router',
  shadowHost: true
}
