import { RouteGroup, RouteItem, RouteMap } from '@router'
import { createViewModel } from '@router/components/RouteGroup/RouteGroup.factory'
import type {
  IRouteGroup,
  IRouteGroupVM
} from '@router/components/RouteGroup/RouteGroup.type'
import { RouteGroupVM } from '@router/components/RouteGroup/RouteGroup.vm'
import { NewRoutesEvent } from '@router/events/new-routes-event'

describe('RouterGroupVM', () => {
  let routeGroupVM: IRouteGroupVM
  let routeGroup: IRouteGroup

  beforeEach(() => {
    routeGroup = new RouteGroup()
    routeGroupVM = createViewModel(routeGroup)
  })

  it('should have a factory method that create a concret instance', () => {
    expect(routeGroupVM).toBeInstanceOf(RouteGroupVM)
  })

  it(`should update ${RouteGroup.elementAttributes.PATH} attribute`, () => {
    const newValue = 'new path'
    routeGroupVM.handlePathAttributeChg(newValue)
    expect(
      routeGroupVM.path
    ).toBe(newValue)
  })

  it('should dispatch events with composite route map', () => {
    const dispatchEventSpy = jest.spyOn(EventTarget.prototype, 'dispatchEvent')
    if (window.customElements.get(RouteItem.elementName) == null) {
      window.customElements.define(RouteItem.elementName, RouteItem)
    }
    const mockRoutes: RouteMap = {
      '/path1': '<page-one></page-one>',
      '/path2': '<page-two></page-two>',
      '/path3': '<page-three></page-three>'
    }
    const mockRouteAttributes = {
      'path1': 'page-one',
      'path2': 'page-two',
      'path3': 'page-three'
    }
    const mockRouteElements = Object.entries(mockRouteAttributes).map(([path, component]) => {
      const route = document.createElement(RouteItem.elementName)
      route.setAttribute(RouteItem.elementAttributes.PATH, path)
      route.setAttribute(RouteItem.elementAttributes.COMPONENT, component)
      return route
    })

    mockRouteElements.forEach(element => {
      routeGroup.appendChild(element)
    })

    const expectedEvent = new NewRoutesEvent(mockRoutes)

    routeGroupVM.handleSlotChange()

    expect(
      JSON.stringify(routeGroupVM.routes)
    ).toBe(JSON.stringify(mockRoutes))
    expect(dispatchEventSpy).toHaveBeenCalledWith(expectedEvent)
  })
})
