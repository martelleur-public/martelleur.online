import { createViewModel } from '@router/components/RouteGroup/RouteGroup.factory'
import { AppRouter, RouteGroup, RouteItem } from '@router'
import { RouterElementError } from '@router/error'
import { IRouteGroupVM } from '@router/components/RouteGroup/RouteGroup.type'

jest.mock('@router/components/RouteGroup/RouteGroup.factory', () => ({
  createViewModel: jest.fn()
}))

const mockRouteGroupVM: jest.Mocked<IRouteGroupVM> = {
  routes: {'/mock-path': '<mock-component></mock-component>'},
  path: 'mock-path',
  handlePathAttributeChg: jest.fn(),
  handleSlotChange: jest.fn()
}

describe('RouteGroup', () => {
  let routeItem: RouteItem
  let routeGroup: RouteGroup
  let appRouter: AppRouter

  beforeAll(() => {
    if (window.customElements.get(RouteItem.elementName) == null) {
      window.customElements.define(RouteItem.elementName, RouteItem)
    }
    if (window.customElements.get(RouteGroup.elementName) == null) {
      window.customElements.define(RouteGroup.elementName, RouteGroup)
    }
    if (window.customElements.get(AppRouter.elementName) == null) {
      window.customElements.define(AppRouter.elementName, AppRouter)
    }
  })

  beforeEach(() => {
    (createViewModel as jest.Mock).mockReturnValue(mockRouteGroupVM)
    routeItem = document.createElement(RouteItem.elementName) as RouteItem
    routeGroup = document.createElement(RouteGroup.elementName) as RouteGroup
    appRouter = document.createElement(AppRouter.elementName) as AppRouter
    document.body.appendChild(appRouter)
    appRouter.appendChild(routeGroup)
    routeGroup.appendChild(routeItem)
    if (routeItem == null) {
      throw new Error(`${RouteItem.elementName} element`)
    }
    if (routeGroup == null || routeGroup.shadowRoot == null) {
      throw new Error(`${RouteGroup.elementName} element or shadowroot is null`)
    }
    if (appRouter == null) {
      throw new Error(`${AppRouter.elementName} element is null`)
    }
  })

  afterEach(() => {
    document.body.removeChild(appRouter)
  })

  it(`should call handle path operation 
    when ${RouteGroup.elementAttributes.PATH} attribute change`, () => {
    const operation = mockRouteGroupVM.handlePathAttributeChg
    const newValue = 'New Path'
    routeGroup.setAttribute(RouteGroup.elementAttributes.PATH, newValue)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newValue)
  })

  it(`should call slots change operation
    when connected to the dom with children`, () => {
    const operation = mockRouteGroupVM.handleSlotChange
    expect(operation).toHaveBeenCalledTimes(1)
  })

  it(`should throw router error if not inside parent ${AppRouter.elementName}
    when connected to the dom`, () => {
    const routeItem = document.createElement(RouteGroup.elementName) as RouteGroup
    const routeItemConnectedCallback = (): void => {
      routeItem.connectedCallback()
    }
    expect(routeItemConnectedCallback).toThrow(RouterElementError)
  })
})
