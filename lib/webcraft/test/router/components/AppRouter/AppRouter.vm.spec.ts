import { AppRouter, PageTransition } from '@router'
import { createViewModel } from '@router/components/AppRouter/AppRouter.factory'
import type {
  IAppRouter,
  IAppRouterVM,
  RouteMap
} from '@router/components/AppRouter/AppRouter.type'
import { AppRouterVM } from '@router/components/AppRouter/AppRouter.vm'
import { NewRoutesEvent } from '@router/events/new-routes-event'

describe('AppRouterVM', () => {
  let appRouterVM: IAppRouterVM
  let appRouter: IAppRouter

  beforeAll(() => {
    if (window.customElements.get(AppRouter.elementName) == null) {
      window.customElements.define(AppRouter.elementName, AppRouter)
    }
  })

  beforeEach(() => {
    appRouter = document.createElement(AppRouter.elementName) as IAppRouter
    document.body.appendChild(appRouter)
    appRouterVM = createViewModel(appRouter)
    appRouter.pageContainer.appendChild(document.createElement('div'))
  })

  it('should have a factory method that create a concret instance', () => {
    expect(appRouterVM).toBeInstanceOf(AppRouterVM)
  })

  it(`should update state of routes to new routes
    when handle new routes is called with a new routes event
    with routes in event detail`, () => {
    const mockRoutes: RouteMap = {
      '/page-one': '<page-one></page-one>',
      '/page-two': '<page-two></page-two>',
      '/page-three': '<page-three></page-three>',
      '/default': '<page-three></page-three>'
    }
    const event = new NewRoutesEvent(mockRoutes)
    appRouterVM.handleNewRoutes(event)
    expect(appRouterVM.routes).toStrictEqual(mockRoutes)
    expect(appRouterVM.defaultPage).toStrictEqual(mockRoutes['/default'])
    expect
  })

  it(`should call render operation
    when new routes is set`, () => {
    const mockRoutes: RouteMap = {
      '/page-one': '<page-one></page-one>',
      '/page-two': '<page-two></page-two>',
      '/page-three': '<page-three></page-three>',
      '/default': '<page-three></page-three>'
    }
    const event = new NewRoutesEvent(mockRoutes)
    const operationSpy = jest.spyOn(appRouterVM, 'render')
    appRouterVM.handleNewRoutes(event)
    expect(operationSpy).toHaveBeenCalledTimes(1)
    expect
  })

  test.each([
    { op: 'renderDarkTransition' as const, args: [], trans: PageTransition.DARK },
    { op: 'renderSlideTransition' as const, args: [], trans: PageTransition.SLIDE },
    { op: 'renderShrinkTransition' as const, args: [], trans: PageTransition.SHRINK },
    { op: 'renderShrinkTransition' as const, args: [true], trans: PageTransition.ROTATION_SHRINK },
    { op: 'renderNoneTransition' as const, args: [], trans: PageTransition.NONE }
  ])(
    `should call specified transition operation controlled by current transition
    for transition %s`,
    ({ trans, op, args }) => {
      appRouterVM.transition = trans
      const operationSpy = jest.spyOn(appRouterVM, op)
      appRouterVM.handlePageTransition()
      expect(operationSpy).toHaveBeenCalledTimes(1)
      expect(operationSpy).toHaveBeenCalledWith(...args)
    }
  )
})
