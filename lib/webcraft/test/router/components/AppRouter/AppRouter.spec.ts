import { createViewModel } from '@router/components/AppRouter/AppRouter.factory'
import { AppRouter, PageTransition } from '@router'
import type {
  IAppRouter,
  IAppRouterVM,
  RouteMap
} from '@router/components/AppRouter/AppRouter.type'
import { NewRoutesEvent } from '@router/events/new-routes-event'
import { appRouterAttribute } from '@router/components/AppRouter/AppRouter.attributes'
import { AppRouterEvent } from '@router/events/app-router-event'

jest.mock('@router/components/AppRouter/AppRouter.factory', () => ({
  createViewModel: jest.fn()
}))

const mockAppRouterVM: jest.Mocked<IAppRouterVM> = {
  routes: {'/mock-path': '<mock-component></mock-component>'},
  transition: PageTransition.DEFAULT,
  defaultPage: document.createElement('div').outerHTML,
  setPageTransitionAttribute: jest.fn(),
  handleNewRoutes: jest.fn(),
  handlePageTransition: jest.fn(),
  render: jest.fn(),
  renderDarkTransition: jest.fn(),
  renderSlideTransition: jest.fn(),
  renderShrinkTransition: jest.fn(),
  renderNoneTransition: jest.fn()
}

describe('AppRouter', () => {
  let appRouter: IAppRouter

  beforeAll(() => {
    if (window.customElements.get(AppRouter.elementName) == null) {
      window.customElements.define(AppRouter.elementName, AppRouter)
    }
  })

  beforeEach(() => {
    (createViewModel as jest.Mock).mockReturnValue(mockAppRouterVM)
    appRouter = document.createElement(AppRouter.elementName) as IAppRouter
    document.body.appendChild(appRouter)
    if (appRouter == null) {
      throw new Error(`${AppRouter.elementName} element is null`)
    }
  })

  afterEach(() => {
    while (document.body.firstChild != null) {
      document.body.removeChild(document.body.firstChild)
    }
  })

  it(`should call handle page transition operation 
    when ${AppRouter.elementAttributes.TRANSITION} attribute change`, () => {
    const operation = mockAppRouterVM.setPageTransitionAttribute
    const newPageTransition = PageTransition.ROTATION_SHRINK
    const oldPageTransition = PageTransition.DARK
    appRouter.setAttribute(appRouterAttribute.TRANSITION, oldPageTransition)
    appRouter.setAttribute(appRouterAttribute.TRANSITION, newPageTransition)
    expect(operation).toHaveBeenCalledTimes(2)
    expect(operation).toHaveBeenCalledWith(newPageTransition, oldPageTransition)
  })

  it(`should call handle new routes operation
    when event ${NewRoutesEvent.eventName} is dispatched on document`, () => {
    const operation = mockAppRouterVM.handleNewRoutes
    const mockRoutes: RouteMap = {}
    document.dispatchEvent(new NewRoutesEvent(mockRoutes))
    expect(operation).toHaveBeenCalledTimes(1)
  })

  it(`should call handle page transition operation
    when event ${AppRouterEvent.eventName} is dispatched on global window`, () => {
    const operation = mockAppRouterVM.handlePageTransition
    global.dispatchEvent(new AppRouterEvent())
    expect(operation).toHaveBeenCalledTimes(1)
  })

  it(`should call handle page transition operation
    when popstate event is dispatched on global window`, () => {
    const operation = mockAppRouterVM.handlePageTransition
    global.dispatchEvent(new Event('popstate'))
    expect(operation).toHaveBeenCalledTimes(1)
  })

  it.each([
    {
      eventName: NewRoutesEvent.eventName,
      operation: mockAppRouterVM.handleNewRoutes,
      spyElement: document
    },
    {
      eventName: AppRouterEvent.eventName,
      operation: mockAppRouterVM.handlePageTransition,
      spyElement: global
    },
    {
      eventName: 'popstate',
      operation: mockAppRouterVM.handlePageTransition,
      spyElement: global
    }
  ])('should clean up event listener when removed from document',
    ({eventName, operation, spyElement}) => {
      const spy = jest.spyOn(spyElement, 'removeEventListener')
      document.body.removeChild(appRouter)
      expect(spy).toHaveBeenCalledWith(eventName, operation)
    }
  )
})
