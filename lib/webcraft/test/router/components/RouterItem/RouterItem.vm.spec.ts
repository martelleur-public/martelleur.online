import { RouteItem } from '@router'
import { routeItemAttributes } from '@router/components/RouteItem/RouteItem.attributes'
import { createViewModel } from '@router/components/RouteItem/RouteItem.factory'
import type {
  IRouteItem,
  IRouteItemVM
} from '@router/components/RouteItem/RouteItem.type'
import { RouteItemVM } from '@router/components/RouteItem/RouteItem.vm'

describe('RouterItemVM', () => {
  let routeItemVM: IRouteItemVM
  let routeItem: IRouteItem

  beforeEach(() => {
    routeItem = new RouteItem()
    routeItemVM = createViewModel(routeItem)
  })

  it('should have a factory method that create a concret instance', () => {
    expect(routeItemVM).toBeInstanceOf(RouteItemVM)
  })

  it(`should update ${routeItemAttributes.PATH}`, () => {
    const newValue = 'New Path'
    routeItemVM.setPathAttribute(newValue)
    expect(routeItem.getAttribute(routeItemAttributes.PATH)).toBe(newValue)
  })

  it(`should update ${routeItemAttributes.COMPONENT}`, () => {
    const newValue = 'component-name'
    routeItemVM.setComponentAttribute(newValue)
    expect(routeItem.getAttribute(routeItemAttributes.COMPONENT)).toBe(newValue)
  })

  it(`should update ${routeItemAttributes.DEFAULT}`, () => {
    routeItem.setAttribute(routeItemAttributes.DEFAULT, '')
    routeItemVM.handleAttributeDefault()
    expect(routeItemVM.isDefaultRoute).toBeTruthy()
  })
})
