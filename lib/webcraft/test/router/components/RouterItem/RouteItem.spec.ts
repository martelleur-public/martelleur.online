import { createViewModel } from '@router/components/RouteItem/RouteItem.factory'
import { IRouteItemVM } from '@router/components/RouteItem/RouteItem.type'
import { AppRouter, RouteGroup, RouteItem } from '@router'
import { RouterElementError } from '@router/error'

jest.mock('@router/components/RouteItem/RouteItem.factory', () => ({
  createViewModel: jest.fn()
}))

describe('RouteItem', () => {
  let routeItem: RouteItem
  let routeGroup: RouteGroup
  let appRouter: AppRouter
  const mockRouteItemVM: jest.Mocked<IRouteItemVM> = {
    isDefaultRoute: false,
    setComponentAttribute: jest.fn(),
    setPathAttribute: jest.fn(),
    handleAttributeDefault: jest.fn()
  }

  beforeAll(() => {
    if (window.customElements.get(RouteItem.elementName) == null) {
      window.customElements.define(RouteItem.elementName, RouteItem)
    }
    if (window.customElements.get(RouteGroup.elementName) == null) {
      window.customElements.define(RouteGroup.elementName, RouteGroup)
    }
    if (window.customElements.get(AppRouter.elementName) == null) {
      window.customElements.define(AppRouter.elementName, AppRouter)
    }
  })

  beforeEach(() => {
    (createViewModel as jest.Mock).mockReturnValue(mockRouteItemVM)
    routeItem = document.createElement(RouteItem.elementName) as RouteItem
    routeGroup = document.createElement(RouteGroup.elementName) as RouteGroup
    appRouter = document.createElement(AppRouter.elementName) as AppRouter
    document.body.appendChild(appRouter)
    appRouter.appendChild(routeGroup)
    routeGroup.appendChild(routeItem)
    if (routeItem == null) {
      throw new Error(`${RouteItem.elementName} element`)
    }
    if (routeGroup == null || routeGroup.shadowRoot == null) {
      throw new Error(`${RouteGroup.elementName} element or shadowroot is null`)
    }
    if (appRouter == null) {
      throw new Error(`${AppRouter.elementName} element is null`)
    }
  })

  afterEach(() => {
    document.body.removeChild(appRouter)
  })

  it(`should call set path operation
    when attribute ${RouteItem.elementAttributes.PATH} change`, () => {
    const operation = mockRouteItemVM.setPathAttribute
    const newValue = 'New Path'
    routeItem.setAttribute(RouteItem.elementAttributes.PATH, newValue)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newValue)
  })

  it(`should call set component operation
    when attribute ${RouteItem.elementAttributes.COMPONENT} change`, () => {
    const operation = mockRouteItemVM.setComponentAttribute
    const newValue = 'component-element'
    routeItem.setAttribute(RouteItem.elementAttributes.COMPONENT, newValue)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newValue)
  })

  it(`should call operation handle attribute default
    when attribute ${RouteItem.elementAttributes.DEFAULT} change`, () => {
    const operation = mockRouteItemVM.handleAttributeDefault
    const newValue = ''
    routeItem.setAttribute(RouteItem.elementAttributes.DEFAULT, newValue)
    expect(operation).toHaveBeenCalledTimes(1)
  })

  it(`should throw router error if not inside parent ${RouteGroup.elementName}
    when connected to th dom`, () => {
    const routeItem = document.createElement(RouteItem.elementName) as RouteItem
    const routeItemConnectedCallback = (): void => {
      routeItem.connectedCallback()
    }
    expect(routeItemConnectedCallback).toThrow(RouterElementError)
  })
})
