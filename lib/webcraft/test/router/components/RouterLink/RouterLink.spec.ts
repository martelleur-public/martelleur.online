import { fireEvent } from '@testing-library/dom'

import {
  RouterLink,
  nameRouterLinkElement
} from '@router/components/RouterLink/RouterLink'
import { IRouterLinkVM } from '@router/components/RouterLink/RouterLink.type'
import { createViewModel } from '@router/components/RouterLink/RouterLink.factory'
import { attributeRouterLink } from '@router/components/RouterLink/RouterLink.attribute'
import { AppRouterEvent } from '@router/events/app-router-event'

jest.mock('@router/components/RouterLink/RouterLink.factory', () => ({
  createViewModel: jest.fn()
}))

describe('RouterLink', () => {
  let routerLink: RouterLink
  const mockRouterLinkVM: jest.Mocked<IRouterLinkVM> = {
    activeLink: false,
    setTitleAttribute: jest.fn(),
    setHrefAttribute:  jest.fn(),
    setDataTextAttribute:  jest.fn(),
    handleRouting: jest.fn(),
    handleUpdateActiveLink:  jest.fn(),
  }

  beforeAll(() => {
    if (window.customElements.get('router-link') == null) {
      window.customElements.define('router-link', RouterLink)
    }
  })

  beforeEach(async () => {
    (createViewModel as jest.Mock).mockReturnValue(mockRouterLinkVM)
    routerLink = document.createElement('router-link') as RouterLink
    document.body.appendChild(routerLink)
    if (routerLink == null || routerLink.shadowRoot == null) {
      throw new Error('AppLink element or shadowroot is null')
    }
  })

  afterEach(() => {
    document.body.removeChild(routerLink)
  })

  it('should be defined as a custom element router-link', () => {
    expect(nameRouterLinkElement).toEqual('router-link')
    expect(customElements.get(nameRouterLinkElement)).toBe(RouterLink)
  })

  it(`should call set title operation
    when attribute ${attributeRouterLink.TITLE} change`, () => {
    const operation = mockRouterLinkVM.setTitleAttribute
    const newValue = 'New Title'
    routerLink.setAttribute(attributeRouterLink.TITLE, newValue)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newValue)
  })

  it(`should call set href operation
    when attribute ${attributeRouterLink.HREF} change`, () => {
    const operation = mockRouterLinkVM.setHrefAttribute
    const newValue = 'https://new'
    routerLink.setAttribute(attributeRouterLink.HREF, newValue)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newValue)
  })

  it(`should call set data text operation
    when attribute ${attributeRouterLink.DATA_TEXT} change`, () => {
    const operation = mockRouterLinkVM.setDataTextAttribute
    const newValue = 'New Text'
    routerLink.setAttribute(attributeRouterLink.DATA_TEXT, newValue)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newValue)
  })

  it(`should call update active link operation 
    when connected to the dom`, () => {
    const operation = mockRouterLinkVM.handleUpdateActiveLink
    expect(operation).toHaveBeenCalledTimes(1)
  })

  it(`should call update active link operation 
    when ${AppRouterEvent.eventName} is dispatched on target window`, () => {
    const operation = mockRouterLinkVM.handleUpdateActiveLink
    global.dispatchEvent(new CustomEvent(AppRouterEvent.eventName))
    expect(operation).toHaveBeenCalledTimes(2)
  })

  it(`should call handle  routing operation 
    when ${AppRouterEvent.eventName} is dispatched on target link`, () => {
    const operation = mockRouterLinkVM.handleRouting
    fireEvent.click(routerLink.link)
    expect(operation).toHaveBeenCalledTimes(1)
  })
})
