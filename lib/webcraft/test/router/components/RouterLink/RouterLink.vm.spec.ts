import { RouterLink } from '@router'
import { attributeRouterLink } from '@router/components/RouterLink/RouterLink.attribute'
import { createViewModel } from '@router/components/RouterLink/RouterLink.factory'
import type {
  IRouterLink,
  IRouterLinkVM
} from '@router/components/RouterLink/RouterLink.type'
import { RouterLinkVM } from '@router/components/RouterLink/RouterLink.vm'

describe('RouterLinkVM', () => {
  let routerLinkVM: IRouterLinkVM
  let routerLink: IRouterLink

  beforeAll(() => {
    const originalHistory = { ...global.history }
    const originalLocation = { ...global.location }
    const dispatchEventMock = jest.fn()
    const historyPushStateMock = jest.fn()
    const locationHrefMock = 'http://mock'
    Object.defineProperty(global, 'dispatchEvent', {
      value: dispatchEventMock
    })
    Object.defineProperty(global, 'history', {
      value: {
        ...originalHistory,
        pushState: historyPushStateMock
      },
    })
    Object.defineProperty(global, 'location', {
      value: {
        ...originalLocation,
        href: locationHrefMock
      },
    })
  })

  beforeEach(() => {
    routerLink = new RouterLink()
    routerLinkVM = createViewModel(routerLink)
  })

  it('should have a factory method that create a concret instance', () => {
    expect(routerLinkVM).toBeInstanceOf(RouterLinkVM)
  })

  it(`should update ${attributeRouterLink.TITLE} attribute`, () => {
    const newValue = 'New Title'
    routerLinkVM.setTitleAttribute(newValue)
    expect(
      routerLink.link.getAttribute(attributeRouterLink.TITLE)
    ).toBe(newValue)
  })

  it(`should update ${attributeRouterLink.HREF} attribute`, () => {
    const newValue = 'https://example.com'
    routerLinkVM.setHrefAttribute(newValue)
    expect(
      routerLink.link.getAttribute(attributeRouterLink.HREF)
    ).toBe(newValue)
  })

  it(`should update ${attributeRouterLink.DATA_TEXT} attribute`, () => {
    const newValue = 'New Text'
    routerLinkVM.setDataTextAttribute(newValue)
    expect(
      routerLink.link.textContent
    ).toBe(newValue)
  })

  it(`should dispatch router event
    when event target is anchor element
    [Spy on global dispatch]` , () => {
    const event = new MouseEvent('click')
    const anchorElement = document.createElement('a')
    const mockHref = 'http://mock'
    anchorElement.setAttribute('href', mockHref)
    const dispatchEventMock = jest.spyOn(global, 'dispatchEvent').mockImplementation(() => true)
    Object.defineProperty(event, 'target', { value: anchorElement })
    routerLinkVM.handleRouting(event)
    expect(global.history.pushState).toHaveBeenCalledWith({}, '', mockHref)
    expect(dispatchEventMock).toHaveBeenCalledTimes(1)
  })

  it(`should dispatch router event
    when event target is anchor element
    [Test on global dispatch]` , () => {
    const event = new MouseEvent('click')
    const anchorElement = document.createElement('a')
    const mockHref = 'http://mock'
    anchorElement.setAttribute('href', mockHref)
    Object.defineProperty(event, 'target', { value: anchorElement })
    routerLinkVM.handleRouting(event)
    expect(global.history.pushState).toHaveBeenCalledWith({}, '', mockHref)
    expect(global.dispatchEvent).toHaveBeenCalledTimes(1)
  })

  it(`should dispatch router event
    when event target is not anchor element` , () => {
    const event = new MouseEvent('click')
    const notAnchorElement = document.createElement('p')
    const mockHref = 'http://mock'
    notAnchorElement.setAttribute('href', mockHref)
    const dispatchEventMock = jest.spyOn(global, 'dispatchEvent').mockImplementation(() => true)
    Object.defineProperty(event, 'target', { value: notAnchorElement })
    routerLinkVM.handleRouting(event)
    expect(history.pushState).toHaveBeenCalledTimes(0)
    expect(dispatchEventMock).toHaveBeenCalledTimes(0)
  })

  it(`should update link to active
    when path is same as window location path` , () => {
    const mockHrefSameAsLocationHref = global.location.href
    routerLinkVM.setHrefAttribute(mockHrefSameAsLocationHref)
    routerLinkVM.handleUpdateActiveLink()
    expect(routerLinkVM.activeLink).toBeTruthy()
  })

  it(`should update link to not active
    when path is not same as window location path` , () => {
    const mockHrefNotSameAsLocationHref = global.location.href + '/extra'
    routerLinkVM.setHrefAttribute(mockHrefNotSameAsLocationHref)
    routerLinkVM.handleUpdateActiveLink()
    expect(routerLinkVM.activeLink).toBeFalsy()
  })
})
