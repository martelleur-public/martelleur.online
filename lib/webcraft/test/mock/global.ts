export class MockResizeObserver {
  observe = jest.fn()
  unobserve = jest.fn()
  disconnect = jest.fn()
}

global.ResizeObserver = MockResizeObserver

global.requestAnimationFrame = jest.fn((callback) => {
  callback(0)
  return 0
})
