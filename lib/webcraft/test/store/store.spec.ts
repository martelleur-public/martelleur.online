import {
  createStore,
  type IStore,
  type Middleware
} from '@store'
import { customLogger } from '@utility'
import type { TestAction, TestState } from './test-types'
import { mockReducer } from './mock'

describe('Store', () => {
  let store: IStore<TestState, TestAction>

  beforeEach(() => {
    store = createStore(mockReducer, { count: 0 })
  })

  it('should initialize with the given state', () => {
    expect(store.getState()).toEqual({ count: 0 })
  })

  it('should handle dispatched actions', () => {
    store.dispatch({ type: 'INCREMENT_ONE' })
    expect(store.getState()).toEqual({ count: 1 })
    store.dispatch({ type: 'DECREMENT_ONE' })
    expect(store.getState()).toEqual({ count: 0 })
  })

  it('should notify subscribers of state changes', () => {
    const listener = jest.fn()
    store.subscribe(listener)
    store.dispatch({ type: 'INCREMENT_ONE' })
    expect(listener).toHaveBeenCalledWith({ count: 1 })
  })

  it('should allow subscribers to unsubscribe', () => {
    const listener = jest.fn()
    const unsubscribe = store.subscribe(listener)
    unsubscribe()
    store.dispatch({ type: 'INCREMENT_ONE' })
    expect(listener).not.toHaveBeenCalled()
  })

  it('should work with middlewares', () => {
    const loggerMiddleware: Middleware<TestState, TestAction> = storeApi => next => action => {
      customLogger('Test logging middleware: before dispatch', storeApi.getState())
      next(action)
      customLogger('Test logging middleware: after dispatch', storeApi.getState())
    }
    store = createStore(mockReducer, { count: 0 }, [loggerMiddleware])
    store.dispatch({ type: 'INCREMENT_ONE' })
    expect(store.getState()).toEqual({ count: 1 })
  })
})
