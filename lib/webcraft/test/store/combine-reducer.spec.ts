import { combineReducers } from '@store'

describe('combineReducers', () => {
  function reducerOneMock (
    state = { count: 0 },
    action: { type: string }): { count: number } {
    switch (action.type) {
    case 'INCREMENT_ONE':
      return { count: state.count + 1 }
    default:
      return state
    }
  }

  function reducerTwoMock (
    state = { value: 'default' },
    action: { type: string, payload?: string }): { value: string } {
    switch (action.type) {
    case 'UPDATE_VALUE':
      return { value: action.payload ?? 'updated' }
    default:
      return state
    }
  }

  interface State {
    one: ReturnType<typeof reducerOneMock>
    two: ReturnType<typeof reducerTwoMock>
  }

  const rootReducer = combineReducers<State, { type: string, payload?: string }>({
    one: reducerOneMock,
    two: reducerTwoMock
  })

  it('should return the initial state', () => {
    const initState: State = { one: { count: 0 }, two: { value: 'default' } }
    const result = rootReducer(initState, { type: 'UNKNOWN' })
    expect(result).toEqual(initState)
  })

  it('should handle actions for reducerOne', () => {
    const state: State = { one: { count: 0 }, two: { value: 'default' } }
    const result = rootReducer(state, { type: 'INCREMENT_ONE' })
    expect(result).toEqual({ one: { count: 1 }, two: { value: 'default' } })
  })

  it('should handle actions for reducerTwo', () => {
    const state: State = { one: { count: 0 }, two: { value: 'default' } }
    const result = rootReducer(state, { type: 'UPDATE_VALUE', payload: 'new-value' })
    expect(result).toEqual({ one: { count: 0 }, two: { value: 'new-value' } })
  })

  it('should return the same state if no changes occurred', () => {
    const state: State = { one: { count: 0 }, two: { value: 'default' } }
    const result = rootReducer(state, { type: 'UNKNOWN' })
    expect(result).toBe(state)
  })
})
