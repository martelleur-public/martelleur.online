export type TestAction =
  | { type: 'INCREMENT_ONE' }
  | { type: 'INCREMENT_MULTIPLE', payload: number }
  | { type: 'DECREMENT_ONE' }
  | { type: 'DECREMENT_MULTIPLE', payload: number }

export interface TestState {
  count: number
}

export interface ReactiveElementTestState {
  testValue: string
  option?: unknown
}
