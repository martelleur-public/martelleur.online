import type { IStore, Middleware } from '@store'
import { applyMiddleware } from '@store/apply-middleware'
import { customLogger } from '@utility'
import type { TestAction, TestState } from './test-types'
import { mockStore } from './mock'

describe('applyMiddleware', () => {
  let store: IStore<TestState, TestAction>
  const sideEffectMock1 = jest.fn()
  const sideEffectMock2 = jest.fn()
  const baseDispatch = jest.fn(action => action)
  const mockMiddleware1:
  Middleware<TestState, TestAction> = store => next => action => {
    customLogger('store', store)
    sideEffectMock1()
    next(action)
  }
  const mockMiddleware2:
  Middleware<TestState, TestAction> = store => next => action => {
    customLogger('store', store)
    sideEffectMock2()
    const modifiedAction = { ...action, modified: true }
    next(modifiedAction)
  }
  const mockMiddleware3:
  Middleware<TestState, TestAction> = store => next => action => {
    customLogger('store', next)
    customLogger('action', action)
    customLogger('store', store)
    store.dispatch({ type: 'INCREMENT_MULTIPLE', payload: 10 })
  }

  beforeEach(() => {
    store = mockStore
  })

  it('should enhance dispatch with middlewares before dispatching the action', () => {
    const enhancedDispatch = applyMiddleware(
      baseDispatch, store.getState, [mockMiddleware1, mockMiddleware2]
    )
    const testAction: TestAction = { type: 'DECREMENT_ONE' }
    enhancedDispatch(testAction)
    expect(sideEffectMock1).toHaveBeenCalledTimes(1)
    expect(sideEffectMock2).toHaveBeenCalledTimes(1)
    expect(baseDispatch).toHaveBeenCalledWith({ ...testAction, modified: true })
  })

  it('should enable middleware to update actions', () => {
    const enhancedDispatch = applyMiddleware(
      baseDispatch, store.getState, [mockMiddleware2]
    )
    const testAction: TestAction = { type: 'DECREMENT_ONE' }
    enhancedDispatch(testAction)
    expect(sideEffectMock1).toHaveBeenCalledTimes(0)
    expect(sideEffectMock2).toHaveBeenCalledTimes(1)
    expect(baseDispatch).toHaveBeenCalledWith({ ...testAction, modified: true })
  })

  it('should enable middleware to dispatch new actions', () => {
    const enhancedDispatch = applyMiddleware(
      baseDispatch, store.getState, [mockMiddleware3]
    )
    const testAction: TestAction = { type: 'DECREMENT_ONE' }
    enhancedDispatch(testAction)
    expect(baseDispatch).toHaveBeenCalledWith({ type: 'INCREMENT_MULTIPLE', payload: 10 })
  })
})
