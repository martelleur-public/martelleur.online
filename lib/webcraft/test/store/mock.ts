import { ReactiveHTMLElement } from '@store/components/ReactiveHTMLElement/ReactiveHTMLElement'
import type { IStore } from '@store'

import type { TestState, TestAction } from './test-types'

export const mockReducer = (state: TestState, action: TestAction): TestState => {
  switch (action.type) {
  case 'INCREMENT_ONE':
    return { count: state.count + 1 }
  case 'DECREMENT_ONE':
    return { count: state.count - 1 }
  case 'INCREMENT_MULTIPLE':
    return { count: state.count + action.payload }
  case 'DECREMENT_MULTIPLE':
    return { count: state.count - action.payload }
  default:
    return state
  }
}

export const mockSubscribe:
jest.Mock<() => void, [callback: any]> = jest.fn((callback) => {
  callback()
  return () => {}
})

export const mockStore: IStore<TestState, TestAction> = {
  dispatch: jest.fn(),
  getState: jest.fn(),
  subscribe: mockSubscribe
}

export const reactiveMockRender = jest.fn()

export class ReactiveMockTestElement extends ReactiveHTMLElement {
  render = reactiveMockRender
}
