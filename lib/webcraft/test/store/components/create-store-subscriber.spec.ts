import { createStoreSubscriber, type IStoreSubscriber } from '@store/components'
import type { TestAction, TestState } from '../test-types'
import { mockStore, mockSubscribe } from '../mock'

describe('createStoreSubscriber', () => {
  const unsubscribeSpy = jest.fn()
  type StoreSubscriberType = new () => HTMLElement & IStoreSubscriber<TestState, TestAction>
  type StoreSubscriberElement = HTMLElement & IStoreSubscriber<TestState, TestAction>
  let StoreSubscriber: StoreSubscriberType
  let storeSubscriberElement: StoreSubscriberElement

  beforeAll(() => {
    StoreSubscriber = createStoreSubscriber<TestState, TestAction>(mockStore)
    if (customElements.get('store-subscriber') == null) {
      window.customElements.define('store-subscriber', StoreSubscriber)
    }
  })

  beforeEach(async () => {
    mockSubscribe.mockReturnValue(unsubscribeSpy)
    storeSubscriberElement = document.createElement('store-subscriber') as StoreSubscriberElement
    document.body.appendChild(storeSubscriberElement)
    if (storeSubscriberElement == null) {
      throw new Error('Store subscriber element is null')
    }
  })

  afterEach(() => {
    document.body.removeChild(storeSubscriberElement)
  })

  it('can create an instance of the custom element', () => {
    const element = document.createElement('store-subscriber')
    expect(element).toBeDefined()
  })

  it('should return a StoreSubscriber class that subscribes to the store', () => {
    expect(mockSubscribe).toHaveBeenCalled()
    expect(storeSubscriberElement.store).toBe(mockStore)
  })

  it('should unsubscribe from the store on disconnectedCallback', () => {
    storeSubscriberElement.disconnectedCallback()
    expect(unsubscribeSpy).toHaveBeenCalledTimes(1)
  })
})
