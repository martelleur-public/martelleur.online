import { ReactiveHTMLElement } from '@store/components/ReactiveHTMLElement/ReactiveHTMLElement'
import { createReactiveState } from '@store/components/ReactiveHTMLElement/create-reactive-state'
import { ReactiveMockTestElement, reactiveMockRender } from '../mock'
import type { ReactiveElementTestState } from '../test-types'

import { customLogger } from '@utility'

jest.mock('@utility', () => ({
  customLogger: jest.fn()
}))

jest.mock('@store/components/ReactiveHTMLElement/create-reactive-state', () => ({
  createReactiveState: jest.fn()
}))

describe('ReaciveHTMLElement', () => {
  let reactiveHTMLElement: ReactiveHTMLElement
  let reactiveTestElement: ReactiveMockTestElement
  const mockCreateReactiveState = createReactiveState as jest.MockedFunction<typeof createReactiveState>

  beforeAll(() => {
    if (customElements.get('reactive-html-element') == null) {
      window.customElements.define('reactive-html-element', ReactiveHTMLElement)
    }
    if (customElements.get('reactive-test-element') == null) {
      window.customElements.define('reactive-test-element', ReactiveMockTestElement)
    }
  })

  beforeEach(async () => {
    reactiveHTMLElement = document.createElement('reactive-html-element') as ReactiveHTMLElement
    document.body.appendChild(reactiveHTMLElement)
    if (reactiveHTMLElement == null) {
      throw new Error('ReactiveHTMLElement is null')
    }
    reactiveTestElement = document.createElement('reactive-test-element') as ReactiveMockTestElement
    document.body.appendChild(reactiveTestElement)
    if (reactiveHTMLElement == null) {
      throw new Error('ReactiveTestElement is null')
    }
  })

  afterEach(() => {
    document.body.removeChild(reactiveHTMLElement)
  })

  it('should call customLogger when disconnectedCallback is called', () => {
    reactiveHTMLElement.disconnectedCallback()
    expect(jest.requireMock('@utility').customLogger).toHaveBeenCalled()
  })

  it('should not throw error if reactive state is updated with new value', () => {
    const initialState: ReactiveElementTestState = { testValue: 'initial' }
    const updatedState: ReactiveElementTestState = { testValue: 'updated' }
    mockCreateReactiveState.mockImplementation((state) => ({
      state,
      isEqual: jest.fn().mockReturnValue(false)
    }))

    const reactiveState = reactiveTestElement.configureReactiveState(initialState)
    const updateState = (): void => {
      reactiveState.state = updatedState
    }

    expect(updateState).not.toThrow(TypeError)
  })

  it('should call render once when reactive state is updated', () => {
    const initialState: ReactiveElementTestState = { testValue: 'initial' }
    const updatedState: ReactiveElementTestState = { testValue: 'updated' }
    mockCreateReactiveState.mockImplementation((state) => ({
      state,
      isEqual: jest.fn().mockReturnValue(false)
    }))

    const reactiveState = reactiveTestElement.configureReactiveState(initialState)
    reactiveState.state = updatedState

    expect(reactiveMockRender).toHaveBeenCalledTimes(1)
  })

  it('should throw error if reactive state is updated with same value', () => {
    const initialState: ReactiveElementTestState = { testValue: 'initial' }
    const updatedState: ReactiveElementTestState = { testValue: 'initial' }
    mockCreateReactiveState.mockImplementation((state) => ({
      state,
      isEqual: jest.fn().mockReturnValue(true)
    }))

    const reactiveState = reactiveTestElement.configureReactiveState(initialState)
    const updateState = (): void => {
      reactiveState.state = updatedState
    }

    expect(updateState).toThrow(TypeError)
  })

  it('should not call render when reactive state is updated with same value', () => {
    const initialState: ReactiveElementTestState = { testValue: 'initial' }
    const updatedState: ReactiveElementTestState = { testValue: 'initial' }
    mockCreateReactiveState.mockImplementation((state) => ({
      state,
      isEqual: jest.fn().mockReturnValue(true)
    }))

    const reactiveState = reactiveTestElement.configureReactiveState(initialState)
    try {
      reactiveState.state = updatedState
    } catch (error) {
      customLogger(error)
    }

    expect(reactiveMockRender).toHaveBeenCalledTimes(0)
  })
})
