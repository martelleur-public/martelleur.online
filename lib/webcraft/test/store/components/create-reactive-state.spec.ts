import type { IReactiveState } from '@store/components'
import { createReactiveState } from '@store/components/ReactiveHTMLElement/create-reactive-state'
import type { ReactiveElementTestState } from '../test-types'

describe('createReactiveState', () => {
  const testState1: ReactiveElementTestState = {
    testValue: 'testState'
  }
  const testState2: ReactiveElementTestState = {
    testValue: 'testState',
    option: {
      mockOption: 'mock-option'
    }
  }
  const testState3: ReactiveElementTestState = {
    testValue: 'testState',
    option: {
      mockOption: 'mock-option'
    }
  }
  const testState4: ReactiveElementTestState = {
    testValue: 'testState',
    option: {
      mockOption: 'mockoption'
    }
  }
  let reactiveState1: IReactiveState<ReactiveElementTestState>
  let reactiveState2: IReactiveState<ReactiveElementTestState>
  let reactiveState3: IReactiveState<ReactiveElementTestState>
  let reactiveState4: IReactiveState<ReactiveElementTestState>

  beforeEach(async () => {
    reactiveState1 = createReactiveState(testState1)
    reactiveState2 = createReactiveState(testState2)
    reactiveState3 = createReactiveState(testState3)
    reactiveState4 = createReactiveState(testState4)
  })

  it('should be a container for state', () => {
    expect(reactiveState1).toHaveProperty('state', testState1)
  })

  it('should check state for shallow equality', () => {
    expect(reactiveState1.isEqual(reactiveState2)).toBeFalsy()
    expect(reactiveState1.isEqual(reactiveState1)).toBeTruthy()
    expect(reactiveState2.isEqual(reactiveState2)).toBeTruthy()
  })

  it('should check state for deep equality', () => {
    expect(reactiveState2.isEqual(reactiveState3)).toBeTruthy()
    expect(reactiveState2.isEqual(reactiveState4)).toBeFalsy()
  })
})
