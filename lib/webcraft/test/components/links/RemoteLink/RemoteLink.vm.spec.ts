import { RemoteLinkVM } from '@components/links/RemoteLink/RemoteLink.vm'
import { RemoteLink } from '@components'
import { IRemoteLink, IRemoteLinkVM } from '@components/links/RemoteLink/RemoteLink.type'
import { createViewModel } from '@components/links/RemoteLink/RemoteLink.factory'

describe('RemoteLinkVM', () => {
  let remoteLinkVM: IRemoteLinkVM
  let remoteLink: IRemoteLink

  beforeEach(() => {
    remoteLink = new RemoteLink()
    remoteLinkVM = createViewModel(remoteLink)
  })

  it('should have a factory method that create a concret instance', () => {
    expect(remoteLinkVM).toBeInstanceOf(RemoteLinkVM)
  })

  it(`should update ${RemoteLink.elementAttributes.TITLE} attribute`, () => {
    const newValue = 'New Title'
    remoteLinkVM.setTitleAttribute(newValue)
    expect(
      remoteLink.link.getAttribute(RemoteLink.elementAttributes.TITLE)
    ).toBe(newValue)
  })

  it(`should update ${RemoteLink.elementAttributes.HREF} attribute`, () => {
    const newValue = 'https://example.com'
    remoteLinkVM.setHrefAttribute(newValue)
    expect(
      remoteLink.link.getAttribute(RemoteLink.elementAttributes.HREF)
    ).toBe(newValue)
  })

  it(`should update ${RemoteLink.elementAttributes.DATA_TEXT} attribute`, () => {
    const newValue = 'New Text'
    remoteLinkVM.setDataTextAttribute(newValue)
    expect(remoteLink.link.textContent).toContain(newValue)
  })
})
