import {
  RemoteLink,
  nameRemoteLinkElement
} from '@components/links/RemoteLink/RemoteLink'
import { IRemoteLink, IRemoteLinkVM } from '@components/links/RemoteLink/RemoteLink.type'
import { createViewModel } from '@components/links/RemoteLink/RemoteLink.factory'
import { attributeRemoteLink } from '@components/links/RemoteLink/RemoteLink.attribute'
import { fireEvent } from '@testing-library/dom'

jest.mock('@components/links/RemoteLink/RemoteLink.factory', () => ({
  createViewModel: jest.fn()
}))

describe('RemoteLink', () => {
  let remoteLink: IRemoteLink

  const mockRemoteLinkVM: jest.Mocked<IRemoteLinkVM> = {
    setTitleAttribute: jest.fn(),
    setHrefAttribute: jest.fn(),
    setDataTextAttribute: jest.fn(),
    handleEnterHover: jest.fn(),
    handleExitHover: jest.fn()
  }

  beforeAll(() => {
    if (window.customElements.get('remote-link') == null) {
      window.customElements.define('remote-link', RemoteLink)
    }
  })

  beforeEach(async () => {
    (createViewModel as jest.Mock).mockReturnValue(mockRemoteLinkVM)
    remoteLink = document.createElement('remote-link') as RemoteLink
    document.body.appendChild(remoteLink)
    if (remoteLink == null || remoteLink.shadowRoot == null) {
      throw new Error('remoteLink element or shadowroot is null')
    }
  })

  afterEach(() => {
    if (document.body.contains(remoteLink)) {
      document.body.removeChild(remoteLink)
    }
  })

  it('should be defined as a custom element remote-link', () => {
    expect(nameRemoteLinkElement).toEqual('remote-link')
    expect(customElements.get(nameRemoteLinkElement)).toBe(RemoteLink)
  })

  it(`should call handle title attribute change operation
    when ${attributeRemoteLink.TITLE} attribute change`, () => {
    const newTitle = 'New Title'
    const operation = mockRemoteLinkVM.setTitleAttribute
    remoteLink.setAttribute(attributeRemoteLink.TITLE, newTitle)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newTitle)
  })

  it(`should call handle href attribute change operation
    when ${attributeRemoteLink.HREF} attribute change`, () => {
    const newHref = 'http://new'
    const operation = mockRemoteLinkVM.setHrefAttribute
    remoteLink.setAttribute(attributeRemoteLink.HREF, newHref)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newHref)
  })

  it(`should call handle data text attribute change operation
    when ${attributeRemoteLink.DATA_TEXT} attribute change`, () => {
    const newText = 'New Text'
    const operation = mockRemoteLinkVM.setDataTextAttribute
    remoteLink.setAttribute(attributeRemoteLink.DATA_TEXT, newText)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newText)
  })

  it(`should call event handler operation for entering the link container
    when enter event is dispatched`, () => {
    const operation = mockRemoteLinkVM.handleEnterHover
    fireEvent(remoteLink.container, new Event('mouseenter'))
    expect(operation).toHaveBeenCalledTimes(1)
  })

  it(`should call event handler operation for leaving the link container
    when leaving event is dispatched`, () => {
    const operation = mockRemoteLinkVM.handleExitHover
    fireEvent(remoteLink.container, new Event('mouseleave'))
    expect(operation).toHaveBeenCalledTimes(1)
  })
})
