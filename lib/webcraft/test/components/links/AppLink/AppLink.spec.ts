import { AppLink, nameAppLinkElement } from '@components/links/AppLink/AppLink'
import { IAppLink, IAppLinkVM } from '@components/links/AppLink/AppLink.type'
import { createViewModel } from '@components/links/AppLink/AppLink.factory'
import { attributeAppLink } from '@components/links/AppLink/AppLink.attribute'

jest.mock('@components/links/AppLink/AppLink.factory', () => ({
  createViewModel: jest.fn()
}))

describe('AppLink', () => {
  let appLink: IAppLink

  const mockAppLinkVM: jest.Mocked<IAppLinkVM> = {
    setTitleAttribute: jest.fn(),
    setHrefAttribute: jest.fn(),
    setDataTextAttribute: jest.fn()
  }

  beforeAll(() => {
    if (window.customElements.get('app-link') == null) {
      window.customElements.define('app-link', AppLink)
    }
  })

  beforeEach(async () => {
    (createViewModel as jest.Mock).mockReturnValue(mockAppLinkVM)
    appLink = document.createElement('app-link') as AppLink
    document.body.appendChild(appLink)
    if (appLink == null || appLink.shadowRoot == null) {
      throw new Error('AppLink element or shadowroot is null')
    }
  })

  afterEach(() => {
    document.body.removeChild(appLink)
  })

  it('should be defined as a custom element app-link', () => {
    expect(nameAppLinkElement).toEqual('app-link')
    expect(customElements.get(nameAppLinkElement)).toBe(AppLink)
  })

  it(`should call handle title attribute change operation
    when ${attributeAppLink.TITLE} attribute change`, () => {
    const newTitle = 'New Title'
    const operation = mockAppLinkVM.setTitleAttribute
    appLink.setAttribute(attributeAppLink.TITLE, newTitle)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newTitle)
  })

  it(`should call handle href attribute change operation
    when ${attributeAppLink.HREF} attribute change`, () => {
    const newHref = 'http://new'
    const operation = mockAppLinkVM.setHrefAttribute
    appLink.setAttribute(attributeAppLink.HREF, newHref)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newHref)
  })

  it(`should call handle data text attribute change operation
    when ${attributeAppLink.DATA_TEXT} attribute change`, () => {
    const newText = 'New Text'
    const operation = mockAppLinkVM.setDataTextAttribute
    appLink.setAttribute(attributeAppLink.DATA_TEXT, newText)
    expect(operation).toHaveBeenCalledTimes(1)
    expect(operation).toHaveBeenCalledWith(newText)
  })
})
