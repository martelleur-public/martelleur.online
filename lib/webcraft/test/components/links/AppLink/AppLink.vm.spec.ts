import { AppLinkVM } from '@components/links/AppLink/AppLink.vm'
import { AppLink } from '@components/links/AppLink/AppLink'
import { IAppLink, IAppLinkVM } from '@components/links/AppLink/AppLink.type'
import { createViewModel } from '@components/links/AppLink/AppLink.factory'

describe('AppLinkVM', () => {
  let appLinkVM: IAppLinkVM
  let appLink: IAppLink

  beforeEach(() => {
    appLink = new AppLink()
    appLinkVM = createViewModel(appLink)
  })

  it('should have a factory method that create a concret instance', () => {
    expect(appLinkVM).toBeInstanceOf(AppLinkVM)
  })

  it(`should update ${AppLink.elementAttributes.TITLE} attribute`, () => {
    const newValue = 'New Title'
    appLinkVM.setTitleAttribute(newValue)
    expect(
      appLink.link.getAttribute(AppLink.elementAttributes.TITLE)
    ).toBe(newValue)
  })

  it(`should update ${AppLink.elementAttributes.HREF} attribute`, () => {
    const newValue = 'https://example.com'
    appLinkVM.setHrefAttribute(newValue)
    expect(
      appLink.link.getAttribute(AppLink.elementAttributes.HREF)
    ).toBe(newValue)
  })

  it(`should update ${AppLink.elementAttributes.DATA_TEXT} attribute`, () => {
    const newValue = 'New Text'
    appLinkVM.setDataTextAttribute(newValue)
    expect(
      appLink.link.getAttribute(AppLink.elementAttributes.DATA_TEXT)
    ).toBe(newValue)
  })
})
