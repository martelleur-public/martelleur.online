import { BasicLayoutVM } from '@components/layouts/BasicLayout/BasicLayout.vm'
import { BasicLayout } from '@components/layouts/BasicLayout/BasicLayout'
import { IBasicLayout, IBasicLayoutVM } from '@components/layouts/BasicLayout/BasicLayout.type'
import { basicLayoutSlot } from '@components/layouts/BasicLayout/BasicLayout.slots'
import { createViewModel } from '@components/layouts/BasicLayout/BasicLayout.factory'

describe('BasicLayoutVM', () => {
  let basicLayoutVM: IBasicLayoutVM
  let basicLayout: IBasicLayout

  beforeEach(() => {
    basicLayout = new BasicLayout()
    basicLayoutVM = createViewModel(basicLayout)
  })

  it('should have a factory method that create a concret instance', () => {
    expect(basicLayoutVM).toBeInstanceOf(BasicLayoutVM)
  })

  it(`should update style property ${BasicLayout.elementAttributes.DYNAMIC_HEIGHT}
    to width 100% when attribute ${BasicLayout.elementAttributes.DYNAMIC_HEIGHT}
    is present`, () => {
    basicLayout.setAttribute(BasicLayout.elementAttributes.DYNAMIC_HEIGHT, '')
    basicLayoutVM.handleDynamicHeightAttribute()
    const actually = basicLayout.style.getPropertyValue(
      `--${BasicLayout.elementAttributes.DYNAMIC_HEIGHT}`
    )
    expect(actually).toBe('100%')
  })

  it(`should update style property ${BasicLayout.elementAttributes.DYNAMIC_HEIGHT}
    to width 100vh when attribute ${BasicLayout.elementAttributes.DYNAMIC_HEIGHT}
    is not present`, () => {
    basicLayoutVM.handleDynamicHeightAttribute()
    const actually = basicLayout.style.getPropertyValue(
      `--${BasicLayout.elementAttributes.DYNAMIC_HEIGHT}`
    )
    expect(actually).toBe('100vh')
  })

  it.each([
    { slotName: basicLayoutSlot.HEADER },
    { slotName: basicLayoutSlot.BODY },
    { slotName: basicLayoutSlot.FOOTER }
  ])('should display slot container', ({ slotName }) => {
    const mockSlot = document.createElement('slot')
    mockSlot.setAttribute('name', slotName)
    basicLayoutVM.handleDisplaySlotContainer(mockSlot)
    const slotContainer = basicLayout.root.querySelector(`.${slotName}`)
    expect(slotContainer?.classList.contains('hide')).toBe(false)
  })
})
