import { ResponsiveHeader } from '@components'
import { responsiveHeaderSlot } from '@components/layouts/ResponsiveHeader/ResponsiveHeader.slots'
import { IResponsiveHeader, IResponsiveHeaderVM } from '@components/layouts/ResponsiveHeader/ResponsiveHeader.type'
import { ResponsiveHeaderVM } from '@components/layouts/ResponsiveHeader/ResponsiveHeader.vm'
import { createViewModel } from '@components/layouts/ResponsiveHeader/ResponsiveHeader.factory'

describe(('ResponsiveHeaderVM'), () => {
  let responsiveHeaderVM: IResponsiveHeaderVM
  let responsiveHeader: IResponsiveHeader

  beforeEach(() => {
    responsiveHeader = new ResponsiveHeader()
    responsiveHeaderVM = createViewModel(responsiveHeader)
  })

  it('should have a factory method that create a concret instance', () => {
    expect(responsiveHeaderVM).toBeInstanceOf(ResponsiveHeaderVM)
  })

  it('should extend resize observer', () => {
    expect(responsiveHeaderVM).toBeInstanceOf(global.ResizeObserver)
  })

  it(`should observe size of assigned elements of slot ${responsiveHeaderSlot.LINKS}`, () => {
    const assignedElement = document.createElement('div')
    responsiveHeaderVM.observeSizeOfAssignedElements([assignedElement])
    expect(responsiveHeaderVM.disconnect).toHaveBeenCalled()
    expect(responsiveHeaderVM.observe).toHaveBeenCalledWith(assignedElement)
  })

  it.each(
    Object.values(responsiveHeaderSlot).map(slotName => ({ slotName }))
  )('should redistribute slots to sidebar',
    ({ slotName }) => {
      const assignedElement = document.createElement('p')
      assignedElement.textContent = 'Mock Text'
      assignedElement.setAttribute('slot', slotName)
      responsiveHeader.appendChild(assignedElement)
      responsiveHeaderVM.redistributeSlotsToSidebar()
      const actualElement = responsiveHeader.sidebar.querySelector(
        `[slot=${slotName}]`
      )
      if (actualElement?.classList.contains('sidebar-links')) {
        actualElement.removeAttribute('class')
      }
      const actualHTML = actualElement?.outerHTML
      const expectedHTML = assignedElement.outerHTML
      expect(actualHTML).toContain(expectedHTML)
    }
  )

  it('should display sidebar when sidebar menu is selected', () => {
    const sidebarMenuSelected = true
    responsiveHeaderVM.updateUIForSidebarState(sidebarMenuSelected)
    expect(responsiveHeader.sidebarContainer.style.left).toBe('0px')
  })

  it('should hide sidebar when sidebar menu is closed', () => {
    const sidebarMenuSelected = false
    responsiveHeaderVM.updateUIForSidebarState(sidebarMenuSelected)
    expect(responsiveHeader.sidebarContainer.style.left).toBe('-300px')
  })

  it('should hide header title when sidebar menu is selected', () => {
    const sidebarMenuSelected = true
    responsiveHeaderVM.updateUIForSidebarState(sidebarMenuSelected)
    expect(responsiveHeader.titleContainer.style.visibility).toBe('hidden')
  })

  it('should display header title when sidebar menu is closed', () => {
    const sidebarMenuSelected = false
    responsiveHeaderVM.updateUIForSidebarState(sidebarMenuSelected)
    expect(responsiveHeader.titleContainer.style.visibility).toBe('visible')
  })

  it('should call prevent click operation when sidebar is selected', () => {
    responsiveHeader.menuToggle.checked = true
    const spyPreventEvent = jest.spyOn(
      responsiveHeaderVM,
      'preventEventOutsideSidebarBoundaries'
    )
    const event = new MouseEvent('click')
    responsiveHeaderVM.handleSidebarOpen(event)
    expect(spyPreventEvent).toHaveBeenCalledTimes(1)
  })

  it('should not call prevent click operation when sidebar is not selected', () => {
    responsiveHeader.menuToggle.checked = false
    const spyPreventEvent = jest.spyOn(
      responsiveHeaderVM,
      'preventEventOutsideSidebarBoundaries'
    )
    const event = new MouseEvent('click')
    responsiveHeaderVM.handleSidebarOpen(event)
    expect(spyPreventEvent).toHaveBeenCalledTimes(0)
  })

  it('should call display sidebar operation with true when menu is selected', () => {
    const menuMock = document.createElement('input')
    menuMock.checked = true
    const eventMock: Partial<Event> = { target: menuMock }
    const spyDisplaySidebarOperation = jest.spyOn(
      responsiveHeaderVM,
      'updateUIForSidebarState'
    )
    const event = eventMock as Event
    responsiveHeaderVM.handleToggleMenu(event)
    expect(spyDisplaySidebarOperation).toHaveBeenCalledWith(true)
  })

  it('should call display sidebar operation with false when menu is not selected', () => {
    const menuMock = document.createElement('input')
    menuMock.checked = false
    const eventMock: Partial<Event> = { target: menuMock }
    const spyDisplaySidebarOperation = jest.spyOn(
      responsiveHeaderVM,
      'updateUIForSidebarState'
    )
    const event = eventMock as Event
    responsiveHeaderVM.handleToggleMenu(event)
    expect(spyDisplaySidebarOperation).toHaveBeenCalledWith(false)
  })
})
