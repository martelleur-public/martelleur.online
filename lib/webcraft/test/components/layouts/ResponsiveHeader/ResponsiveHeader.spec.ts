import { fireEvent } from '@testing-library/dom'

import { ResponsiveHeader } from '@components'
import { IResponsiveHeaderVM } from '@components/layouts/ResponsiveHeader/ResponsiveHeader.type'
import { createViewModel } from '@components/layouts/ResponsiveHeader/ResponsiveHeader.factory'

jest.mock('@components/layouts/ResponsiveHeader/ResponsiveHeader.factory', () => ({
  createViewModel: jest.fn()
}))

const mockSidebarWidth = 1000
const mockLinksWidthDefaultBreakPoint = 1000

const mockResponsiveHeaderVM: jest.Mocked<IResponsiveHeaderVM> = {
  sidebarWidth: mockSidebarWidth,
  linksWidthDefaultBreakPoint: mockLinksWidthDefaultBreakPoint,
  handleResize: jest.fn(),
  getPreviouseContainerQuerryStyle: jest.fn(),
  handleLinksSlotChange: jest.fn(),
  observeSizeOfAssignedElements: jest.fn(),
  handleToggleMenu: jest.fn(),
  updateUIForSidebarState: jest.fn(),
  handleSidebarOpen: jest.fn(),
  preventEventOutsideSidebarBoundaries: jest.fn(),
  redistributeSlotsToSidebar: jest.fn(),
  redistributeSlotToSidebar: jest.fn(),
  observe: jest.fn(),
  unobserve: jest.fn(),
  disconnect: jest.fn(),
  createContainerQueryStyle: jest.fn(),
  createDefaultContainerQueryStyle: jest.fn()
}

describe('ResponsiveHeader', () => {
  let header: ResponsiveHeader

  beforeAll(() => {
    if (window.customElements.get(ResponsiveHeader.elementName) == null) {
      window.customElements.define(ResponsiveHeader.elementName, ResponsiveHeader)
    }
  })

  beforeEach(async () => {
    (createViewModel as jest.Mock).mockReturnValue(mockResponsiveHeaderVM)
    header = document.createElement(ResponsiveHeader.elementName) as ResponsiveHeader
    document.body.appendChild(header)
    if (header == null || header.shadowRoot == null) {
      throw new Error(`${ResponsiveHeader.elementName} element or shadowroot is null`)
    }
  })

  afterEach(() => {
    document.body.removeChild(header)
  })

  it('should call link slots handler once when slotchange event occurs', () => {
    const specifiedHandler = mockResponsiveHeaderVM.handleLinksSlotChange
    fireEvent(header.slotLinks, new Event('slotchange'))
    expect(specifiedHandler).toHaveBeenCalledTimes(1)
  })

  it('should call menu toogle handler once when chane event occurs', () => {
    const specifiedHandler = mockResponsiveHeaderVM.handleToggleMenu
    fireEvent.change(header.menuToggle)
    expect(specifiedHandler).toHaveBeenCalledTimes(1)
  })

  it('should call siddebar open handler once when click event occurs', () => {
    const specifiedHandler = mockResponsiveHeaderVM.handleSidebarOpen
    fireEvent.click(document)
    expect(specifiedHandler).toHaveBeenCalledTimes(1)
  })
})
