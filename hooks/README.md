# Use git hooks to update the version of the application

Run the following command to set up git hooks and commit template for this project.

- ```git config core.hooksPath hooks/git```


- ```git config commit.template hooks/git/commit-template.md```
