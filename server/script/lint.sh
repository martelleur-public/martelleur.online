#!/bin/bash

main () {
  run_lint
}

run_lint () {
  eslint 'src/**/*.{js,ts}' 'test/**/*.{js,ts}'
}

main
