#!/bin/bash

main () {
  set -xe
  pre_build
  run_dev_server
}

pre_build () {
  if [ -d "target/server" ]; then
    rm -Rf target/server
  fi
  mkdir -p target/server/src
  cp -r src/assets target/server/src
}

run_dev_server () {
  tsc --project tsconfig.build.json
  concurrently \
    "tsc --project tsconfig.build.json --watch" \
    "NODE_ENV=development nodemon \
    -r dotenv/config \
    target/server/src/server.js \
    dotenv_config_path=.env.dev"
}

main
