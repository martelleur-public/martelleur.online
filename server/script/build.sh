#!/bin/bash

main () {
  set -e
  pre_build
  build_server  
}

pre_build () {
  if [ -d "target/server" ]; then
    rm -Rf target/server
  fi
  mkdir -p target/server/src
  cp -r src/assets target/server/src
}

build_server () {
  tsc --project tsconfig.build.json
}

main