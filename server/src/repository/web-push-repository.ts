export interface WebPushRepository {
  addSubscription: (subscription: WebPushSubscription) => Promise<void>
  getSubscribers: () => Promise<WebPushSubscription[]>
}
