import type { WebPushRepository } from '../repository/web-push-repository'

export class WebPushService {
  constructor (private readonly _repository: WebPushRepository) {}

  async addSubscription (subscription: WebPushSubscription): Promise<void> {
    await this._repository.addSubscription(subscription)
  }

  async getSubscribers (): Promise<WebPushSubscription[]> {
    return await this._repository.getSubscribers()
  }
}
