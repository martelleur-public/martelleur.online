import { performance } from 'perf_hooks'

import type { Response, Request, NextFunction } from 'express'

export class CPUHeavyController {
  measureCpuTask = (req: Request, res: Response, next: NextFunction): void => {
    const start = performance.now()
    const result = this.cpuHeavyTask(45)
    const end = performance.now()

    const resultMsg = `CPU-Heavy Task: Fibonacci(35) = ${result}`
    const timeMsg = `Time taken: ${(end - start).toFixed(2)}ms`
    console.log(resultMsg)
    console.log(timeMsg)
    res.status(200).json({
      time: timeMsg,
      result: resultMsg
    })
  }

  cpuHeavyTask (n: number): number {
    if (n < 2) {
      return n
    }
    return this.cpuHeavyTask(n - 1) + this.cpuHeavyTask(n - 2)
  }
}
