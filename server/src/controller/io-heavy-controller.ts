import { performance } from 'perf_hooks'
import path from 'path'
import fs from 'fs'

import type { Response, Request, NextFunction } from 'express'
import { HttpError } from '../errors/http-error'

export class IOHeavyController {
  measureIoTask = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const start = performance.now()
    try {
      const data = await this._ioHeavyTask('test-file-xl.txt')
      const end = performance.now()

      const resultMsg = `I/O-Heavy Task: Read ${data.length} characters`
      const timeMsg = `Time taken: ${(end - start).toFixed(2)}ms`
      console.log(resultMsg)
      console.log(timeMsg)
      res.status(200).json({
        time: timeMsg,
        result: resultMsg
      })
    } catch (err) {
      next(new HttpError('Failed load file and calculate elapsed time', 500))
    }
  }

  /**
   * Add basic sanitization and validation due sast report concerning
   * path traversal attacks when constructing a path dynamically.
   *
   * The issue is only critical when path is constructed from untrusted
   * input. If untrusted input path can created to get acces of other system
   * resources. Ref: https://owasp.org/www-community/attacks/Path_Traversal.
   */
  _ioHeavyTask = (filename: string): Promise<string> => {
    const sanitizedFilename = this._getSanitizedFilename(filename)
    const filePath = path.resolve(__dirname, '..', 'assets', sanitizedFilename)
    const dirPath = path.resolve(__dirname, '..', 'assets')
    if (!this._isFileInDir(filePath, dirPath)) {
      return Promise.reject(new Error('Invalid file path'))
    }
    return new Promise((resolve, reject) => {
      fs.readFile(filePath, 'utf-8', (err, data) => {
        if (err) reject(err)
        resolve(data)
      })
    })
  }

  _getSanitizedFilename = (filename: string): string => {
    return filename.replace(/\.\.(\/|\\)/g, '')
  }

  _isFileInDir = (filePath: string, dirPath: string): boolean => {
    return filePath.startsWith(dirPath)
  }
}
