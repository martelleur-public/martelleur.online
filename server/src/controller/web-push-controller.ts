import type { Response, Request, NextFunction } from 'express'
import webpush from 'web-push'

import type { WebPushService } from '../service/web-push-service'
import getEnvironment from '../utility/environment'
import { HttpError } from '../errors/http-error'
import { WebPushKeyVO } from '../model/web-push-key'
import { WebPushSubscriptionVO } from '../model/web-push-subscription'
import { WebPushNotificationVO } from '../model/web-push-notification'

const environment = getEnvironment()
webpush.setVapidDetails(
  `mailto:${environment.SITE_MAIL}`,
  environment.WEBPUSH_PUBLIC_KEY,
  environment.WEBPUSH_PRIVATE_KEY
)

export class WebPushController {
  constructor (private readonly _service: WebPushService) {}

  subscribe (req: Request, res: Response, next: NextFunction): void {
    const webPushKey: WebPushKey = new WebPushKeyVO(
      req.body.keys.p256dh, req.body.keys.auth)
    const webPushSubscription: WebPushSubscription = new WebPushSubscriptionVO(
      req.body.endpoint, webPushKey)
    this._service.addSubscription(webPushSubscription)
      .then(() => {
        res.status(201).json({ data: 'granted' })
      }).catch(() => {
        next(new HttpError('Failed subscribing to notifications', 400))
      })
  }

  getSubscribers (req: Request, res: Response, next: NextFunction): void {
    this._service.getSubscribers()
      .then(subscriptionsResult => {
        res.locals.subscriptions = subscriptionsResult
        next()
      }).catch(() => {
        next(new HttpError('Failed posting notification to subscribers', 400))
      })
  }

  postNotificationToSubscribers (req: Request, res: Response, next: NextFunction): void {
    const validatedNotification: WebPushNotification = new WebPushNotificationVO(
      req.body.title, req.body.body, req.body.url)
    const notificationDTO: NotificationDTO = {
      title: validatedNotification.title,
      body: validatedNotification.body,
      url: validatedNotification.url
    }
    const notificationPayload = { notification: notificationDTO }
    const subscriptions: WebPushSubscription[] = res.locals.subscriptions
    Promise.all(subscriptions.map(async subscription => {
      return await webpush.sendNotification(
        subscription,
        JSON.stringify(notificationPayload)
      )
    })).then(() => {
      res.status(200)
        .json({ message: 'Notification sent successfully.' })
    }).catch((err) => {
      console.log('err', err)
      next(new HttpError('Failed posting notification to subscribers', 400))
    })
  }
}
