function createMsg (message?: string, sqlMessage?: string): string {
  if (message !== undefined && sqlMessage === undefined) {
    return `Error: ${message}`
  } else if (message === undefined && sqlMessage !== undefined) {
    return `SQL Message: ${sqlMessage}`
  } else if (message !== undefined && sqlMessage !== undefined) {
    return `Error: ${message}.\nSQL Message: ${sqlMessage}`
  } else {
    return 'Sql unknown error.'
  }
}

export default class DatabaseError extends Error {
  readonly userMessage?: string

  constructor (
    message?: string,
    sqlMessage?: string,
    userMessage?: string) {
    super(createMsg(message, sqlMessage))
    this.userMessage = userMessage
    this.name = 'DatbaseError'
  }
}
