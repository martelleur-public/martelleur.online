import express from 'express'
import type { Router, Request, Response, NextFunction } from 'express'

import { IOHeavyController } from '../controller/io-heavy-controller'

export const router: Router = express.Router()
const controller = new IOHeavyController()

router.get('/timelargefile', (req: Request, res: Response, next: NextFunction) => {
  controller.measureIoTask(req, res, next)
})
