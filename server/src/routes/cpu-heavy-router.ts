import express from 'express'
import type { Router, Request, Response, NextFunction } from 'express'

import { CPUHeavyController } from '../controller/cpu-heavy-controller'

export const router: Router = express.Router()
const controller = new CPUHeavyController()

router.get('/timefibonacci', (req: Request, res: Response, next: NextFunction) => {
  controller.measureCpuTask(req, res, next)
})
