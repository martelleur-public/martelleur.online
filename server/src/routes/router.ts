import express from 'express'
import type { Router, Request, Response } from 'express'
import { router as webPushRouter } from './web-push-router'
import { router as ioHeavyRouter } from './io-heavy-router'
import { router as cpuHeavyRouter } from './cpu-heavy-router'

/***
 * Module: router.ts
 * Description: Main router off the application.
 */

export const router: Router = express.Router()
router.use('/webpush', webPushRouter)
router.use('*', (req: Request, res: Response) => { res.redirect('/') })

/**
 * Use for perfommance experiment.
 */
if (process.env.NODE_ENV === 'development') {
  router.use('/io', ioHeavyRouter)
  router.use('/cpu', cpuHeavyRouter)
}
