import express from 'express'
import type { Router, Request, Response, NextFunction } from 'express'
import basicAuth from 'express-basic-auth'

import type { WebPushRepository } from '../repository/web-push-repository'
import { WebPushService } from '../service/web-push-service'
import { WebPushController } from '../controller/web-push-controller'
import { WebPushStore } from '../infrastructure/web-push-store'
import getEnvironment from '../utility/environment'

export const router: Router = express.Router()
const repository: WebPushRepository = new WebPushStore()
const service = new WebPushService(repository)
const controller = new WebPushController(service)
const environment = getEnvironment()

router.use('/sendnotification', basicAuth({
  users: { [environment.NOTIFICATION_USER]: environment.NOTIFICATION_PASSWORD },
  challenge: true,
  realm: 'Notification area'
}))

router.post('/subscribe', (req: Request, res: Response, next: NextFunction) => {
  controller.subscribe(req, res, next)
})

/**
 * Route to trigger a push notification that can be triggerd with
 */
router.post('/sendnotification',
  (req: Request, res: Response, next: NextFunction) => {
    controller.getSubscribers(req, res, next)
  },
  (req: Request, res: Response, next: NextFunction) => {
    controller.postNotificationToSubscribers(req, res, next)
  }
)
