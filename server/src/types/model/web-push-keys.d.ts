interface WebPushKey {
  p256dh: string
  auth: string
}
