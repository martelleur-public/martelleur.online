interface WebPushNotification {
  readonly title: string
  readonly body: string
  readonly url: string
}
