interface WebPushSubscription {
  endpoint: string
  keys: WebPushKey
}
