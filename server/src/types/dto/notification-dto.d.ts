interface NotificationDTO {
  title: string
  body: string
  url: string
}
