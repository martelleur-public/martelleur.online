interface Environment {
  WEBPUSH_PUBLIC_KEY: string
  WEBPUSH_PRIVATE_KEY: string
  SITE_MAIL: string
  NOTIFICATION_USER: string
  NOTIFICATION_PASSWORD: string
  MYSQL_HOST: string
  MYSQL_PORT: number
  MYSQL_USERNAME: string
  MYSQL_PASSWORD: string
  MYSQL_DATABASE: string
  PORT: number
  METRIC_SERVICE_PORT: number
  METRIC_SERVICE_HOST: string
  METRIC_INTERGRITY_KEY: string
  METRIC_CONFIDENTIALITY_KEY: string
}
