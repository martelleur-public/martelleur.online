export default function getEnvironment (): Environment {
  const WEBPUSH_PUBLIC_KEY = process.env.WEBPUSH_PUBLIC_KEY
  const WEBPUSH_PRIVATE_KEY = process.env.WEBPUSH_PRIVATE_KEY
  const SITE_MAIL = process.env.SITE_MAIL
  const NOTIFICATION_USER = process.env.NOTIFICATION_USER
  const NOTIFICATION_PASSWORD = process.env.NOTIFICATION_PASSWORD
  const MYSQL_HOST = process.env.MYSQL_HOST
  const MYSQL_PORT = Number(process.env.MYSQL_PORT)
  const MYSQL_USERNAME = process.env.MYSQL_USERNAME
  const MYSQL_PASSWORD = process.env.MYSQL_PASSWORD
  const MYSQL_DATABASE = process.env.MYSQL_DATABASE
  const PORT = Number(process.env.PORT)
  const METRIC_SERVICE_HOST = process.env.METRIC_SERVICE_HOST
  const METRIC_SERVICE_PORT = Number(process.env.METRIC_SERVICE_PORT)
  const METRIC_INTERGRITY_KEY = process.env.METRIC_INTERGRITY_KEY
  const METRIC_CONFIDENTIALITY_KEY = process.env.METRIC_CONFIDENTIALITY_KEY

  if (WEBPUSH_PUBLIC_KEY == null ||
    WEBPUSH_PRIVATE_KEY == null ||
    SITE_MAIL == null ||
    NOTIFICATION_USER == null ||
    NOTIFICATION_PASSWORD == null ||
    MYSQL_HOST == null ||
    MYSQL_PORT == null ||
    MYSQL_USERNAME == null ||
    MYSQL_PASSWORD == null ||
    MYSQL_DATABASE == null ||
    PORT == null ||
    METRIC_SERVICE_HOST == null ||
    METRIC_SERVICE_PORT == null ||
    METRIC_INTERGRITY_KEY == null ||
    METRIC_CONFIDENTIALITY_KEY == null) {
    throw new Error('Variables are missing in environment variables')
  }

  return {
    WEBPUSH_PUBLIC_KEY,
    WEBPUSH_PRIVATE_KEY,
    SITE_MAIL,
    NOTIFICATION_USER,
    NOTIFICATION_PASSWORD,
    MYSQL_HOST,
    MYSQL_PORT,
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    MYSQL_DATABASE,
    PORT,
    METRIC_SERVICE_HOST,
    METRIC_SERVICE_PORT,
    METRIC_INTERGRITY_KEY,
    METRIC_CONFIDENTIALITY_KEY
  }
}
