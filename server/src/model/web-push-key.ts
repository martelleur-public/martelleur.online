/**
 * @TODO Check standard if these values are ok.
 */
export class WebPushKeyVO implements WebPushKey {
  constructor (
    readonly p256dh: string,
    readonly auth: string) {
    if (!this._isValidBase64(p256dh) || p256dh.length < 10 || p256dh.length > 100) {
      throw new WebPushKeyError(WebPushKeyErrorMsg.INVALID_P256DH_KEY)
    }
    if (!this._isValidBase64(auth) || auth.length < 10 || auth.length > 50) {
      throw new WebPushKeyError(WebPushKeyErrorMsg.INVALID_AUTH_KEY)
    }
    this.p256dh = p256dh
    this.auth = auth
  }

  /**
   * Check if string is valid Base64
   * Only alphanumeric characters, underscore, and hyphen.
   */
  _isValidBase64 (str: string): boolean {
    const base64UrlPattern = /^[A-Za-z0-9_-]+$/
    return base64UrlPattern.test(str)
  }
}

export class WebPushKeyError extends Error {
  constructor (message: string) {
    super(message)
    this.name = 'WebPushKeyError'
  }
}

export class WebPushKeyErrorMsg {
  static readonly INVALID_P256DH_KEY = 'Invalid p256dh key'
  static readonly INVALID_AUTH_KEY = 'Invalid auth key'
}
