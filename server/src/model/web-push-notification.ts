export class WebPushNotificationVO implements WebPushNotification {
  constructor (
    readonly title: string,
    readonly body: string,
    readonly url: string) {
    if (title.length < 5 || title.length > 50) {
      throw new NotificationError(NotificationErrorMsg.INVALID_TITLE)
    }
    if (body.length < 5 || body.length > 120) {
      throw new NotificationError(NotificationErrorMsg.INVALID_BODY)
    }
    if (url.length < 5 || url.length > 255 || !this._isValidURL(url)) {
      throw new NotificationError(NotificationErrorMsg.INVALID_URL)
    }
  }

  _isValidURL (str: string): boolean {
    const pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3})|' + // OR ip (v4) address
      'localhost)' + // OR 'localhost'
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i') // fragment locator
    return !!pattern.test(str)
  }
}

export class NotificationError extends Error {
  constructor (message: string) {
    super(message)
    this.name = 'WebPushKeyError'
  }
}

export class NotificationErrorMsg {
  static readonly INVALID_TITLE = 'Invalid title attribute'
  static readonly INVALID_BODY = 'Invalid body attribute'
  static readonly INVALID_URL = 'Invalid url attribute'
}
