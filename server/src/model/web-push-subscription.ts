/**
 * @TODO Check standard if these values are ok.
 */
export class WebPushSubscriptionVO implements WebPushSubscription {
  constructor (
    readonly endpoint: string,
    readonly keys: WebPushKey) {
    if (endpoint.length < 10 || endpoint.length > 512) {
      throw new WebPushSubscriptionError(WebPushSubscriptionErrorMsg.INVALID_ENDPOINT)
    }
    this.endpoint = endpoint
  }
}

export class WebPushSubscriptionError extends Error {
  constructor (message: string) {
    super(message)
    this.name = 'WebPushSubscriptionError'
  }
}

export class WebPushSubscriptionErrorMsg {
  static readonly INVALID_ENDPOINT = 'Invalid endpoint'
}
