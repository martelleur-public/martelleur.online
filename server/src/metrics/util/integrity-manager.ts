import crypto from 'crypto'

export function createHMAC (key: string, message: string): string {
  const hmac = crypto.createHmac('sha256', key)
  hmac.update(message)
  return hmac.digest('hex')
}
