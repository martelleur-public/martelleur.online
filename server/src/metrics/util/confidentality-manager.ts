import crypto from 'crypto'
import { Buffer } from 'buffer'

import getEnvironment from '../../utility/environment'

/**
 * See crypto.getCiphers() for full list of supported algorithms.
 */
type AES256Algorithm =
'aes-256-cbc' |
'aes-256-cbc-hmac-sha1' |
'aes-256-cbc-hmac-sha256' |
'aes-256-ccm' |
'aes-256-cfb' |
'aes-256-cfb1' |
'aes-256-cfb8' |
'aes-256-ctr' |
'aes-256-ecb' |
'aes-256-gcm' |
'aes-256-ocb' |
'aes-256-ofb' |
'aes-256-xts'

export class ConfidentialityManager {
  constructor (readonly algorithm: AES256Algorithm) {}

  encrypt (plaintext: string): {
    ciphertext: string
    initializationVector: string
    authTag: string
  } {
    const key = this._convertBase64StringTo32BitBufferKey()
    const initializationVector = crypto.randomBytes(12)
    const cipher = crypto.createCipheriv('aes-256-gcm', key, initializationVector)

    let encrypted = cipher.update(plaintext, 'utf8', 'hex')
    encrypted += cipher.final('hex')

    const authTag = cipher.getAuthTag().toString('hex')

    return {
      ciphertext: encrypted,
      initializationVector: initializationVector.toString('hex'),
      authTag: authTag
    }
  }

  encryptMultipleItems (plaintexts: string[]): {
    ciphertexts: string[]
    initializationVector: string
    authTags: string[]
  } {
    const key = this._convertBase64StringTo32BitBufferKey()
    const initializationVector = crypto.randomBytes(12)

    const authTags: string[] = []
    const encryptedTexts = plaintexts.map(plaintext => {
      const cipher = crypto.createCipheriv('aes-256-gcm', key, initializationVector)
      let encrypted = cipher.update(plaintext, 'utf8', 'hex')
      encrypted += cipher.final('hex')
      authTags.push(cipher.getAuthTag().toString('hex'))
      return encrypted
    })

    return {
      ciphertexts: encryptedTexts,
      initializationVector: initializationVector.toString('hex'),
      authTags: authTags
    }
  }

  _convertBase64StringTo32BitBufferKey (): Buffer {
    const keyBase64 = getEnvironment().METRIC_CONFIDENTIALITY_KEY
    const keyBuffer = Buffer.from(keyBase64, 'base64')
    const zero32BitBuffer = Buffer.alloc(32)
    keyBuffer.copy(zero32BitBuffer, 0, 0, Math.min(32, keyBuffer.length))
    return keyBuffer
  }

  _convert32BitKeyToBase64 (key: Buffer): string {
    return key.toString('base64')
  }

  _generate32BitKeyForAES256Algorithm (): Buffer {
    const key = crypto.randomBytes(32)
    const keyBase64: string = key.toString('base64')
    console.log(`key keyBase64: ${keyBase64}`)
    return key
  }

  decrypt (
    ciphertext: string,
    initializationVector: string,
    authTag: string): string {
    const key = this._convertBase64StringTo32BitBufferKey()
    const decipher = crypto.createDecipheriv(
      'aes-256-gcm',
      key,
      Buffer.from(initializationVector, 'hex'))
    decipher.setAuthTag(Buffer.from(authTag, 'hex'))
    let decrypted = decipher.update(ciphertext, 'hex', 'utf8')
    decrypted += decipher.final('utf8')
    return decrypted
  }
}
