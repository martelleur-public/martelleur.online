import os from 'os'

export class MemoryUsageCalculator {
  getMemoryUsageInPercantage = (): number => {
    const totalMemory = os.totalmem()
    const freeMemory = os.freemem()
    const usedMemory = totalMemory - freeMemory
    return (usedMemory / totalMemory) * 100
  }
}
