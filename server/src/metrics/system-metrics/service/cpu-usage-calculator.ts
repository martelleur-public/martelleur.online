import * as os from 'os'

interface CpuInfo {
  user: number
  nice: number
  sys: number
  idle: number
}

/**
 * Compare snapshots CPU info
 * to calculate CPU usage.
 *
 * If CPU user, nice, and sys times have increased a lot while CPU idle
 * time has not increased much, the CPU has been busy.
 *
 * If CPU idle time has increased a lot compared to CPU user, nice, and
 * sys times, then the CPU has been mostly idle.
 */
export class CpuUsageCalculator {
  private cpuUsage: number | null = null
  private cpuTimeoutId: NodeJS.Timeout | null = null
  private readonly cpuIntervalId: NodeJS.Timeout | null = null

  constructor () {
    this.cpuIntervalId = setInterval(this.runCpuUsageCalculator, 2000)
  }

  runCpuUsageCalculator = (): void => {
    const startSnapshot = this.cpuSnapshot()

    this.cpuTimeoutId = setTimeout(() => {
      const endSnapshot = this.cpuSnapshot()
      this.cpuUsage = this.calculateCpuUsage(startSnapshot, endSnapshot)
    }, 1000)
  }

  cpuSnapshot = (): CpuInfo[] => {
    try {
      return os.cpus().map(cpu => ({
        user: cpu.times.user,
        nice: cpu.times.nice,
        sys: cpu.times.sys,
        idle: cpu.times.idle
      }))
    } catch (error) {
      console.error('Error while getting CPU snapshot:', error)
      return []
    }
  }

  calculateCpuUsage = (startTimes: CpuInfo[], endTimes: CpuInfo[]): number | null => {
    if (startTimes.length === 0 || endTimes.length === 0 || startTimes.length !== endTimes.length) {
      console.error('Invalid snapshots provided for calculating CPU usage.')
      return null
    }

    let totalIdle = 0
    let totalTick = 0

    for (let i = 0; i < startTimes.length; i++) {
      const start = startTimes[i]
      const end = endTimes[i]

      const idleDifference = end.idle - start.idle
      const totalDifference = (end.user - start.user) + (end.nice - start.nice) + (end.sys - start.sys) + idleDifference

      totalIdle += idleDifference
      totalTick += totalDifference
    }

    return 1 - totalIdle / totalTick
  }

  getCpuUsageInPercentage = (): number | null => {
    if (this.cpuUsage == null) {
      return null
    } else {
      // console.log(`CPU Usage: ${(this.cpuUsage * 100).toFixed(2)}%`)
      return Number((this.cpuUsage * 100).toFixed(2))
    }
  }

  clearCPUTimers = (): void => {
    if (this.cpuTimeoutId !== null) {
      clearTimeout(this.cpuTimeoutId)
    }
    if (this.cpuIntervalId !== null) {
      clearInterval(this.cpuIntervalId)
    }
  }
}
