import dgram from 'dgram'

import type { CpuUsageCalculator } from './cpu-usage-calculator'
import type { MemoryUsageCalculator } from './memory-usage-calculator'
import { SystemMetricBodyDTO } from '../protocol/system-metric-body-dto'
import { SystemMetricHeaderDTO } from '../protocol/system-metric-header-dto'
import { ConfidentialityManager, createHMAC } from '../../util'
import { SystemMetricMetaDTO } from '../protocol/system-metric-meta-dto'

export class SystemMetricPublisher {
  private readonly client = dgram.createSocket('udp4')
  private readonly publishIntervalId: NodeJS.Timeout | null = null
  private readonly cpuUsageCalculator: CpuUsageCalculator
  private readonly memoryUsageCalculator: MemoryUsageCalculator
  private readonly metricServiceHost: string
  private readonly metricServicePort: number
  private readonly metricIntegrityKey: string

  constructor (
    cpuUsageCalculator: CpuUsageCalculator,
    memoryUsageCalculator: MemoryUsageCalculator,
    metricServiceHost: string,
    metricServicePort: number,
    metricIntegrityKey: string) {
    this.publishIntervalId = setInterval(this.sendMetrics.bind(this), 10000)
    this.cpuUsageCalculator = cpuUsageCalculator
    this.memoryUsageCalculator = memoryUsageCalculator
    this.metricServiceHost = metricServiceHost
    this.metricServicePort = metricServicePort
    this.metricIntegrityKey = metricIntegrityKey
  }

  sendMetrics (): void {
    const body = this._createProtocolBody()
    if (body == null) return
    const cm = new ConfidentialityManager('aes-256-gcm')
    const header = this._createProtocolHeader()
    const { ciphertexts, initializationVector, authTags } = cm.encryptMultipleItems(
      [JSON.stringify(header), JSON.stringify(body)]
    )
    const [headerCipher, bodyCipher] = ciphertexts
    const meta = this._createProtocolMeta(header, body, initializationVector, authTags)
    const data = JSON.stringify({
      meta, header: headerCipher, body: bodyCipher
    })

    this.client.send(data, this.metricServicePort, this.metricServiceHost, (err) => {
      if (err != null) {
        console.error('An error occurred:', err)
        this.client.close()
        return
      }
      console.log(data)
    })
  }

  _createProtocolBody (): SystemMetricBodyDTO | null {
    const cpuUsagePercentage = this.cpuUsageCalculator.getCpuUsageInPercentage()
    if (cpuUsagePercentage == null) {
      return null
    }
    return new SystemMetricBodyDTO(
      cpuUsagePercentage,
      this.memoryUsageCalculator.getMemoryUsageInPercantage(),
      this.getDateNowInNanoSeconds()
    )
  }

  _createProtocolHeader (): SystemMetricHeaderDTO {
    return new SystemMetricHeaderDTO(
      'mmp', 'post', 'system_metrics'
    )
  }

  _createProtocolMeta (
    header: SystemMetricHeaderDTO,
    body: SystemMetricBodyDTO,
    initializationVector: string,
    authTags: string[]): SystemMetricMetaDTO {
    const headerSortedStr = JSON.stringify(header, Object.keys(header).sort())
    const bodySortedStr = JSON.stringify(body, Object.keys(body).sort())
    const hmacDigest = createHMAC(
      this.metricIntegrityKey,
      headerSortedStr + bodySortedStr
    )
    const timestamp = new Date().getTime()
    return new SystemMetricMetaDTO(timestamp, hmacDigest, initializationVector, authTags)
  }

  getDateNowInNanoSeconds (): number {
    return Date.now() * 1000000
  }

  clearPublishTimers (): void {
    if (this.publishIntervalId !== null) {
      clearInterval(this.publishIntervalId)
    }
    this.cpuUsageCalculator.clearCPUTimers()
  }
}
