import { SystemMetricPublisher } from './service/system-metric-publisher'
import { CpuUsageCalculator } from './service/cpu-usage-calculator'
import { MemoryUsageCalculator } from './service/memory-usage-calculator'
import getEnvironment from '../../utility/environment'

const env = getEnvironment()
const cpuUsageCalculator = new CpuUsageCalculator()
const memoryUsageCalculator = new MemoryUsageCalculator()
export const systemMetricPublisher = new SystemMetricPublisher(
  cpuUsageCalculator,
  memoryUsageCalculator,
  env.METRIC_SERVICE_HOST,
  env.METRIC_SERVICE_PORT,
  env.METRIC_INTERGRITY_KEY
)
