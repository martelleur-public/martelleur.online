export class SystemMetricBodyDTO {
  constructor (
    readonly cpu_usage: number,
    readonly memory_usage: number,
    readonly timestamp: number) {
    this._validateMetric()
  }

  _validateMetric (): void {
    if (this.cpu_usage > 100 || this.cpu_usage < 0) {
      throw new SystemMetricError(SystemMetricErrorMsg.INVALID_CPU_USAGE_METRIC)
    }

    if (this.memory_usage > 100 || this.memory_usage < 0) {
      throw new SystemMetricError(SystemMetricErrorMsg.INVALID_MEMORY_USAGE_METRIC)
    }

    if (this.timestamp < 0) {
      throw new SystemMetricError(SystemMetricErrorMsg.INVALID_TIMESTAMP)
    }
  }
}

export class SystemMetricError extends Error {
  constructor (message: string) {
    super(message)
    this.name = 'SystemMetricError'
  }
}

export class SystemMetricErrorMsg {
  static readonly INVALID_CPU_USAGE_METRIC = 'Invalid cpu usage metric, metric should be in percentage (0-100)'
  static readonly INVALID_MEMORY_USAGE_METRIC = 'Invalid memory usage metric, metric should be in percentage (0-100)'
  static readonly INVALID_TIMESTAMP = 'Invalid timestamp, metric should be bigger then 0'
}
