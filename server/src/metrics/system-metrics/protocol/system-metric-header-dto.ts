type SystemMetricHeaderScheme = 'mmp' | 'MMP'
type SystemMetricHeaderMethod = 'post' | 'get'
type SystemMetricHeaderType = 'system_metrics'

export class SystemMetricHeaderDTO {
  constructor (
    readonly name: SystemMetricHeaderScheme,
    readonly method: SystemMetricHeaderMethod,
    readonly type: SystemMetricHeaderType) {}
}
