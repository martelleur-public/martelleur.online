export class SystemMetricMetaDTO {
  constructor (
    readonly timestamp: number,
    readonly hmac_digest: string,
    readonly initialization_vector: string,
    readonly auth_tags: string[]) {}
}
