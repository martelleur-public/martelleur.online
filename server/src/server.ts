import path from 'path'

import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'

import { router } from './routes/router'
import { dbHealthCheck, closeMysqlConnection } from './infrastructure/mysql-connection'
import { errorMiddleware } from './middleware/error-middleware'
import getEnvironment from './utility/environment'
import { systemMetricPublisher } from './metrics/system-metrics/'

const main = async (): Promise<void> => {
  const app = express()

  app.use(bodyParser.json())
  app.use(errorMiddleware)

  const staticDirectory = path.join(__dirname, '../../client')
  const indexHTML = path.join(__dirname, '../../client/index.html')
  switch (process.env.NODE_ENV) {
    case 'production':
      app.use('/', express.static(staticDirectory))
      app.set('trust proxy', 1)
      break
    case 'staging':
      app.use('/', express.static(staticDirectory))
      app.set('trust proxy', 1)
      break
    case 'test':
      app.use('/', express.static(staticDirectory))
      app.set('trust proxy', 1)
      break
    default:
      console.log('Environment used: ', process.env.NODE_ENV)
      app.use(cors({
        credentials: true,
        methods: ['GET', 'POST', 'PUSH', 'DELETE'],
        origin: [
          'https://content-mammal-previously.ngrok-free.app',
          'http://localhost:5173',
          'http://127.0.0.1:5173',
          'http://127.0.0.1:5173',
          'http://192.168.0.39:5173',
          'http://192.168.0.30:5173',
          'http://192.168.0.35:5173'
        ]
      }))
      break
  }

  app.use('/api', router)
  app.get('*', (req, res) => { res.sendFile(indexHTML) })

  const port = getEnvironment().PORT
  app.listen(port, () => {
    console.log(`Server started on port ${port}`)
  })
}

main().catch(console.error)

const cleanup = async (exitCode = 0): Promise<void> => {
  try {
    clearInterval(dbHealthCheck)
    systemMetricPublisher.clearPublishTimers()
    await closeMysqlConnection()
    console.log('Cleanup completed successfully')
    process.exit(exitCode)
  } catch (error) {
    console.error('Error during cleanup: ', error)
    process.exit(1)
  }
}

/**
 * Docker will try to restart on failure (error code != 0).
 */
process.on('SIGINT', () => cleanup(0)) // Kill -2.
process.on('SIGTERM', () => cleanup(0)) // Kill -15.
// https://nodejs.org/api/process.html#event-uncaughtexception.
process.on('uncaughtException', (error) => {
  console.error('Unhandled exception:', error)
  cleanup(1)
})
// https://nodejs.org/api/process.html#event-unhandledrejection.
process.on('unhandledRejection', (error, promise) => {
  console.error('Unhandled promise rejection at:', promise, 'reason:', error)
  cleanup(1)
})
