import type { Request, Response, NextFunction } from 'express'

import type { HttpError } from '../errors/http-error'

export const errorMiddleware = (
  error: HttpError,
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  const status = error.statusCode ?? 500
  const message = error.message ?? 'Something went wrong'
  if (process.env.NODE_ENV !== 'production') {
    console.error('Middleware error: ', error)
  }
  res.status(status).json({
    status: 'error',
    statusCode: status,
    message: message
  })
}
