import { createPool } from 'mysql2/promise'
import type { Pool } from 'mysql2/promise'

/**
 * Module: mysql-connection.ts
 * Description: An module used to manage
 * MySQL connection.
 *
 * @TODO Test restart db in development by navigate to db directory and run:
 * 1) docker compose stop <container_name>
 * 2) docker compose restart <container_name>
 */

let mySQLConnection: Pool
const createMysqlConnection = async (): Promise<void> => {
  const dbConfig = {
    host: process.env.MYSQL_HOST,
    port: Number(process.env.MYSQL_PORT),
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    connectionLimit: 10
  }
  try {
    console.log('Creating a MySQL connection pool...')
    mySQLConnection = createPool(dbConfig)
    // Check the connection.
    const [rows] = await mySQLConnection.query('SELECT 1')
    console.log('Connected to MySQL successfully:', rows)
    console.log(`MySQL server listening: ${dbConfig.host}:${dbConfig.port}`)
  } catch (error) {
    console.error('Failed to create a MySQL connection pool. Error: ', error)
  }
}

(async () => {
  try {
    await createMysqlConnection()
  } catch (error) {
    console.error('Failed to establish MySQL connection at start up: ', error)
    process.exit(1)
  }
})().catch((error) => {
  console.error('Unhandled error:', error)
})

export const closeMysqlConnection = async (): Promise<void> => {
  try {
    console.log('Closing MySQL connection pool...')
    if (mySQLConnection) {
      await mySQLConnection.end()
      console.log('Successfully closed the MySQL connection pool.')
    } else {
      console.log('No active MySQL connection pool to close.')
    }
  } catch (error) {
    console.error('Failed to close the MySQL connection pool. Error: ', error)
  }
}

const delay = async (ms: number): Promise<void> => {
  await new Promise(resolve => setTimeout(resolve, ms))
}

const intervall = process.env.NODE_ENV === 'production'
  ? 60 * 1000
  : 6 * 1000

export const dbHealthCheck = setInterval(async () => {
  try {
    console.error('Run database health check.')
    await mySQLConnection.query('SELECT 1')
  } catch (error) {
    console.error('Database health check failed. Error: ', error)
    try {
      await delay(5000)
      if (mySQLConnection) {
        await closeMysqlConnection()
      }
      await createMysqlConnection()
    } catch (error) {
      console.error('Restart database failed. Error: ', error)
    }
  }
}, intervall)

export { mySQLConnection as default }
