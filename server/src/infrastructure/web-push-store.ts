import connection from './mysql-connection'
import DatabaseError from '../errors/database-error'
import type { WebPushRepository } from '../repository/web-push-repository'
import type { MysqlExecuteResult } from './mysql-orm'
import { WebPushKeyVO } from '../model/web-push-key'
import { WebPushSubscriptionVO } from '../model/web-push-subscription'

export class WebPushStore implements WebPushRepository {
  async addSubscription (subscription: WebPushSubscription): Promise<void> {
    try {
      const inserSubscriptionWithUpdate = `
        INSERT INTO web_push_subscription (endpoint, p256dh, auth) 
        VALUES (?, ?, ?) 
        ON DUPLICATE KEY UPDATE 
        p256dh = VALUES(p256dh), 
        auth = VALUES(auth),
        updated_at = CURRENT_TIMESTAMP`
      const args = [
        subscription.endpoint,
        subscription.keys.p256dh,
        subscription.keys.auth
      ]
      await connection.execute(inserSubscriptionWithUpdate, args)
    } catch (e: any) {
      throw new DatabaseError(e.message, e.sqlMessage)
    }
  }

  async getSubscribers (): Promise<WebPushSubscription[]> {
    const selectSubscriptions =
      'SELECT endpoint, p256dh, auth FROM `web_push_subscription`'
    const [selectSubscriptionsResult] = await connection.execute(selectSubscriptions)
    return this._mapSubscribersResultToModel(selectSubscriptionsResult)
  }

  _mapSubscribersResultToModel (result: MysqlExecuteResult): WebPushSubscription[] {
    if (Array.isArray(result)) {
      return result.map((item: any) => {
        const keys: WebPushKey = new WebPushKeyVO(
          item.p256dh, item.auth)
        const subscription: WebPushSubscription = new WebPushSubscriptionVO(
          item.endpoint, keys)
        return subscription
      })
    } else {
      const message = 'Could not map mysql room result.'
      throw new DatabaseError(message, message)
    }
  }
}
