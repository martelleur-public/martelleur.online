import type {
  OkPacket,
  RowDataPacket,
  ResultSetHeader,
  ProcedureCallPacket
} from 'mysql2'

export type MysqlExecuteResult = OkPacket | RowDataPacket[] | ResultSetHeader[] | RowDataPacket[][] | OkPacket[] | ProcedureCallPacket
