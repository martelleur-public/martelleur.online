import { CpuUsageCalculator } from '../../src/metrics/system-metrics/service/cpu-usage-calculator'
import * as os from 'os'

jest.useFakeTimers()
jest.mock('os')

describe('CpuUsageCalculator', () => {
  let cpuUsageCalculator: CpuUsageCalculator

  beforeAll(() => {
    jest.clearAllMocks()
    jest.restoreAllMocks()
  })

  beforeEach(() => {
    cpuUsageCalculator = new CpuUsageCalculator()
  })

  afterEach(() => {
    cpuUsageCalculator.clearCPUTimers()
    jest.clearAllTimers()
  })

  const mockCpus = (time: number, numberOfCpus: number): void => {
    const cpuArray = Array.from({ length: numberOfCpus }, () => ({
      model: 'testModel',
      speed: 1000,
      times: {
        user: time,
        nice: time,
        sys: time,
        idle: time,
        irq: time
      }
    }))
    jest.spyOn(os, 'cpus').mockReturnValue(cpuArray)
  }

  it('should take CPU snapshots', () => {
    mockCpus(100, 4)
    const snapshot = cpuUsageCalculator.cpuSnapshot()
    expect(snapshot
      .every(item => {
        return item.user === 100 && item.nice === 100 && item.sys === 100 && item.idle === 100
      })
    ).toBe(true)
  })

  it('should calculate CPU usage correctly', () => {
    const startSnapshot = [
      { user: 100, nice: 100, sys: 100, idle: 100 }
    ]

    const endSnapshot = [
      { user: 200, nice: 200, sys: 200, idle: 150 }
    ]

    const usage = cpuUsageCalculator.calculateCpuUsage(startSnapshot, endSnapshot)
    expect(usage).toBeCloseTo(0.8571428571428572, 5)
  })

  it('should periodically calculate CPU usage', async () => {
    mockCpus(1000, 4)
    jest.runOnlyPendingTimers() // Trigger the initial runCpuUsageCalculator
    mockCpus(1100, 4) // Changed to 1100, to simulate time has passed
    jest.advanceTimersByTime(1000) // Trigger the setTimeout inside runCpuUsageCalculator

    const usage = cpuUsageCalculator.getCpuUsageInPercentage()

    expect(usage).toBeCloseTo(75, 5)
  })
})
