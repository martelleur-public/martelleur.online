import {
  SystemMetricHeaderDTO
} from '../../src/metrics/system-metrics/protocol/system-metric-header-dto'

describe('SystemMetricHeaderDTO', () => {
  describe('constructor', () => {
    // Arrange
    const validScheme = 'mmp'
    const validMethod = 'post'
    const validType = 'system_metrics'

    it('should successfully create an system metric header instance with valid metrics', () => {
      // Act
      const systemMetricHeaderInstance = new SystemMetricHeaderDTO(
        validScheme,
        validMethod,
        validType
      )

      // Assert
      expect(systemMetricHeaderInstance.name).toEqual(validScheme)
      expect(systemMetricHeaderInstance.method).toEqual(validMethod)
      expect(systemMetricHeaderInstance.type).toEqual(validType)
    })
  })
})
