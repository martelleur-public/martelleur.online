import * as os from 'os'

import { MemoryUsageCalculator } from '../../src/metrics/system-metrics/service/memory-usage-calculator'

jest.mock('os')

describe('MemoryUsageCalculator', () => {
  let memoryUsageCalculator: MemoryUsageCalculator

  beforeEach(() => {
    memoryUsageCalculator = new MemoryUsageCalculator()
  })

  afterEach(() => {
    jest.clearAllMocks()
    jest.restoreAllMocks()
  })

  it.each([
    [1000, 500, 50],
    [100, 50, 50],
    [100, 0, 100]
  ])('should calculate memory usage percentage correctly for total memory %i and free memory %i)',
    (totalMemory, freeMemory, expectedMemoryUsage) => {
      jest.spyOn(os, 'totalmem').mockReturnValue(totalMemory)
      jest.spyOn(os, 'freemem').mockReturnValue(freeMemory)

      const result = memoryUsageCalculator.getMemoryUsageInPercantage()

      expect(result).toBe(expectedMemoryUsage)
    })
})
