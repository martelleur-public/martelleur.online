import {
  SystemMetricMetaDTO
} from '../../src/metrics/system-metrics/protocol/system-metric-meta-dto'

describe('SystemMetricMetaDTO', () => {
  describe('constructor', () => {
    // Arrange
    const validTimestamp = 1
    const validHmacDigest = 'mockDigest'
    const validInitializationVector = 'validInitializationVectorMock'
    const validAuthTags = ['validAuthTagsOne', 'validAuthTagsTwo']

    it('should successfully create an system metric meta instance with valid timestamp', () => {
      // Act
      const systemMetricHeaderInstance = new SystemMetricMetaDTO(
        validTimestamp,
        validHmacDigest,
        validInitializationVector,
        validAuthTags
      )

      // Assert
      expect(systemMetricHeaderInstance.timestamp).toEqual(validTimestamp)
    })

    it('should successfully create an system metric meta instance with valid hmac digest', () => {
      // Act
      const systemMetricHeaderInstance = new SystemMetricMetaDTO(
        validTimestamp,
        validHmacDigest,
        validInitializationVector,
        validAuthTags
      )

      // Assert
      expect(systemMetricHeaderInstance.timestamp).toEqual(validTimestamp)
      expect(systemMetricHeaderInstance.hmac_digest).toEqual(validHmacDigest)
    })
  })
})
