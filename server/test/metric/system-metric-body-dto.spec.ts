import {
  SystemMetricBodyDTO,
  SystemMetricError,
  SystemMetricErrorMsg
} from '../../src/metrics/system-metrics/protocol/system-metric-body-dto'

/**
 * @TODO Parameterise input to specifications.
 */
describe('SystemMetricBodyDTO', () => {
  afterEach(() => {
    jest.clearAllMocks()
    jest.restoreAllMocks()
  })

  describe('constructor', () => {
    // Arrange
    const validCPUUsage = 50
    const validMemoryUsage = 50
    const validTimestamp = 1

    it('should successfully create a system metric body instance with valid metrics', () => {
      // Arrange
      const validationSpy = jest.spyOn(SystemMetricBodyDTO.prototype, '_validateMetric')

      // Act
      const systemMetricBodyInstance = new SystemMetricBodyDTO(
        validCPUUsage,
        validMemoryUsage,
        validTimestamp
      )

      // Assert
      expect(validationSpy).toBeCalled()
      expect(systemMetricBodyInstance.cpu_usage).toEqual(validCPUUsage)
      expect(systemMetricBodyInstance.memory_usage).toEqual(validMemoryUsage)
      expect(systemMetricBodyInstance.timestamp).toEqual(validTimestamp)
    })

    it.each([
      [101],
      [-1],
      [-0.0000000000000000000000000000001]
    ])('should throw an error for invalid cpu usage %i', invalidCPUUsage => {
      // Act
      const systemMetricBodyInstanceCreator = (): SystemMetricBodyDTO => {
        return new SystemMetricBodyDTO(
          invalidCPUUsage,
          validMemoryUsage,
          validTimestamp
        )
      }

      // Assert
      expect(systemMetricBodyInstanceCreator).toThrow(SystemMetricError)
      expect(systemMetricBodyInstanceCreator).toThrow(SystemMetricErrorMsg.INVALID_CPU_USAGE_METRIC)
    })

    it.each([
      [101],
      [-1],
      [-0.0000000000000000000000000000001]
    ])('should throw an error for invalid memory usage %i', (invalidMemoryUsage) => {
      // Act
      const systemMetricBodyInstanceCreator = (): SystemMetricBodyDTO => {
        return new SystemMetricBodyDTO(
          validCPUUsage,
          invalidMemoryUsage,
          validTimestamp)
      }

      // Assert
      expect(systemMetricBodyInstanceCreator).toThrow(SystemMetricError)
      expect(systemMetricBodyInstanceCreator).toThrow(SystemMetricErrorMsg.INVALID_MEMORY_USAGE_METRIC)
    })

    it('should throw an error for invalid timestamp', () => {
      // Arrange
      const invalidTimestamp = -1

      // Act
      const systemMetricBodyInstanceCreator = (): SystemMetricBodyDTO => {
        return new SystemMetricBodyDTO(
          validCPUUsage,
          validMemoryUsage,
          invalidTimestamp
        )
      }

      // Assert
      expect(systemMetricBodyInstanceCreator).toThrow(SystemMetricError)
      expect(systemMetricBodyInstanceCreator).toThrow(SystemMetricErrorMsg.INVALID_TIMESTAMP)
    })
  })
})
