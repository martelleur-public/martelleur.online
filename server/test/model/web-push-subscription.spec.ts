import { WebPushSubscriptionVO, WebPushSubscriptionError, WebPushSubscriptionErrorMsg } from '../../src/model/web-push-subscription'
import { WebPushKeyVO } from '../../src/model/web-push-key'

describe('WebPushSubscriptionVO', () => {
  describe('constructor', () => {
    // Arrange
    const validEndpoint = 'https://valid-endpoint.example.com'
    const validKeys = new WebPushKeyVO('ValidP256dh_123', 'ValidAuth_456')

    it('should successfully create an instance with valid endpoint and keys', () => {
      // Act
      const instance = new WebPushSubscriptionVO(validEndpoint, validKeys)
      // Assert
      expect(instance.endpoint).toEqual(validEndpoint)
      expect(instance.keys).toEqual(validKeys)
    })

    it('should throw an error for invalid endpoint length', () => {
      // Arrange
      const shortEndpoint = 'short'
      // Act & Assert
      expect(() => new WebPushSubscriptionVO(shortEndpoint, validKeys)).toThrow(WebPushSubscriptionError)
      expect(() => new WebPushSubscriptionVO(shortEndpoint, validKeys)).toThrow(WebPushSubscriptionErrorMsg.INVALID_ENDPOINT)
    })
  })
})
