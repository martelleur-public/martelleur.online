import { WebPushKeyVO, WebPushKeyError, WebPushKeyErrorMsg } from '../../src/model/web-push-key'

describe('WebPushKeyVO', () => {
  describe('constructor', () => {
    // Arrange
    const validP256dh = 'ValidP256dh_123'
    const validAuth = 'ValidAuth_456'

    it('should successfully create an instance with valid keys', () => {
      // Act
      const instance = new WebPushKeyVO(validP256dh, validAuth)
      // Assert
      expect(instance.p256dh).toEqual(validP256dh)
      expect(instance.auth).toEqual(validAuth)
    })

    it('should throw an error for invalid p256dh', () => {
      // Arrange
      const invalidP256dh = '9chars$$$'
      // Act & Assert
      expect(() => new WebPushKeyVO(invalidP256dh, validAuth)).toThrow(WebPushKeyError)
      expect(() => new WebPushKeyVO(invalidP256dh, validAuth)).toThrow(WebPushKeyErrorMsg.INVALID_P256DH_KEY)
    })

    it('should throw an error for invalid auth', () => {
      // Arrange
      const invalidAuth = '9char$$$'
      // Act & Assert
      expect(() => new WebPushKeyVO(validP256dh, invalidAuth)).toThrow(WebPushKeyError)
      expect(() => new WebPushKeyVO(validP256dh, invalidAuth)).toThrow(WebPushKeyErrorMsg.INVALID_AUTH_KEY)
    })
  })

  describe('_isValidBase64', () => {
    // Arrange
    const instance = new WebPushKeyVO('ValidKey1_123', 'ValidKey2_456')
    it('should return true for valid Base64 strings', () => {
      // Act
      const result = instance._isValidBase64('ValidKey_789')
      // Assert
      expect(result).toBeTruthy()
    })

    it('should return false for invalid Base64 strings', () => {
      // Act
      const result = instance._isValidBase64('Invalid$$$')
      // Assert
      expect(result).toBeFalsy()
    })
  })
})
