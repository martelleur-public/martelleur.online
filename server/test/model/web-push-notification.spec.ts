import {
  WebPushNotificationVO,
  NotificationErrorMsg
} from '../../src/model/web-push-notification'

describe('NotificationVO', () => {
  describe('constructor', () => {
    it('should correctly instantiate the NotificationVO with valid data', () => {
      // Arrange
      const title = 'Valid Title'
      const body = 'Valid Body'
      const url = 'https://validurl.com'
      // Act
      const notification = new WebPushNotificationVO(title, body, url)
      // Assert
      expect(notification.title).toBe(title)
      expect(notification.body).toBe(body)
      expect(notification.url).toBe(url)
    })

    it('should correctly instantiate the NotificationVO using localhost as domain', () => {
      // Arrange
      const title = 'Valid Title'
      const body = 'Valid Body'
      const url = 'http://localhost'
      // Act
      const notification = new WebPushNotificationVO(title, body, url)
      // Assert
      expect(notification.title).toBe(title)
      expect(notification.body).toBe(body)
      expect(notification.url).toBe(url)
    })

    it('should throw error with short title', () => {
      // Arrange
      const title = 'Ttl'
      const body = 'Valid Body'
      const url = 'https://validurl.com'
      // Act & Assert
      expect(() => new WebPushNotificationVO(title, body, url)).toThrowError(NotificationErrorMsg.INVALID_TITLE)
    })

    it('should throw error with long title', () => {
      // Arrange
      const title = 'T'.repeat(51)
      const body = 'Valid Body'
      const url = 'https://validurl.com'
      // Act & Assert
      expect(() => new WebPushNotificationVO(title, body, url)).toThrowError(NotificationErrorMsg.INVALID_TITLE)
    })

    it('should throw error with short body', () => {
      // Arrange
      const title = 'Valid Title'
      const body = 'Body'
      const url = 'https://validurl.com'
      // Act & Assert
      expect(() => new WebPushNotificationVO(title, body, url)).toThrowError(NotificationErrorMsg.INVALID_BODY)
    })

    it('should throw error with long body', () => {
      // Arrange
      const title = 'Valid Title'
      const body = 'B'.repeat(121)
      const url = 'https://validurl.com'

      // Act & Assert
      expect(() => new WebPushNotificationVO(title, body, url)).toThrowError(NotificationErrorMsg.INVALID_BODY)
    })

    it('should throw error with short url', () => {
      // Arrange
      const title = 'Valid Title'
      const body = 'Valid Body'
      const url = 'htt'
      // Act & Assert
      expect(() => new WebPushNotificationVO(title, body, url)).toThrowError(NotificationErrorMsg.INVALID_URL)
    })

    it('should throw error with long url', () => {
      // Arrange
      const title = 'Valid Title'
      const body = 'Valid Body'
      const url = 'https://' + 'a'.repeat(250)
      // Act & Assert
      expect(() => new WebPushNotificationVO(title, body, url)).toThrowError(NotificationErrorMsg.INVALID_URL)
    })

    it('should throw error with invalid url', () => {
      // Arrange
      const title = 'Valid Title'
      const body = 'Valid Body'
      const url = 'invalidurl'
      // Act & Assert
      expect(() => new WebPushNotificationVO(title, body, url)).toThrowError(NotificationErrorMsg.INVALID_URL)
    })

    it('should throw error with invalid scheme', () => {
      // Arrange
      const title = 'Valid Title'
      const body = 'Valid Body'
      const url = 'file://localhost:3000'
      // Act & Assert
      expect(() => new WebPushNotificationVO(title, body, url)).toThrowError(NotificationErrorMsg.INVALID_URL)
    })
  })
})
