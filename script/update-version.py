import sys
import json

def main():
    version_placeholder_files = [ 
        'client/dist/manifest.json',
        'client/dist/sw-cache-first.js']
    
    json_files = [
        'package.json',
        'client/package.json',
        'server/package.json']
    
    version_number = get_version_number()
    
    print(f'Updating version number in project files to version: \"{version_number}\".')

    for file in version_placeholder_files:
        replace_placeholder_with_version(file, version_number)

    for file in json_files:
        update_version_in_json(file, version_number)

def get_version_number():
    if len(sys.argv) < 2:
        print(f'Usage: {sys.argv[0]} <APP_VERSION>')
        sys.exit(1)
    return sys.argv[1]

def replace_placeholder_with_version(filename, version_number):
    with open(filename, 'r') as f:
        content = f.read().replace('/*VERSION_PLACEHOLDER*/', version_number)

    with open(filename, 'w') as f:
        f.write(content)

def update_version_in_json(filename, version_number):
    with open(filename, 'r') as f:
        content = json.load(f)
        content['version'] = version_number

    with open(filename, 'w') as f:
        json.dump(content, f, indent=2)

if __name__ == '__main__':
    main()