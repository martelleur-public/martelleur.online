import re

def main(ci_file = '.gitlab-ci.yml'):
    content = getCurrentCiContent(ci_file)
    match = getCurrentAppVersionMatch(content)
    if match:
        new_content = replaceVersionWithIncrementedMinorVersion(
            match, content)
        updateCiFile(ci_file, new_content)
    else:
        print(f'APP_VERSION not found in {ci_file}')

def getCurrentCiContent(ci_file):
    with open(ci_file, 'r') as file:
        content = file.read()
    return content

def getCurrentAppVersionMatch(content):
    return re.search(r'(APP_VERSION: )([0-9.]+)', content)

def replaceVersionWithIncrementedMinorVersion(match, content):
    old_version_str = match.group(2)
    old_version_parts = old_version_str.split('.')
    old_version_parts[-1] = str(int(old_version_parts[-1]) + 1)
    new_version_str = '.'.join(old_version_parts)
    return content.replace(f'APP_VERSION: {old_version_str}', f'APP_VERSION: {new_version_str}')

def updateCiFile(ci_file, content):
    with open(ci_file, 'w') as file:
        file.write(content)

if __name__ == '__main__':
    main()