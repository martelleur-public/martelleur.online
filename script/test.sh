#!/bin/bash

main () {
  set -e
  install_dependencies
  build_lib
  test_packages
}

install_dependencies () {
  npm install --workspace=lib/webcraft
  npm install --workspace=lib/reactcraft
  npm install --workspace=server
  npm install --workspace=client
  npm install --workspace=client-react
  npm install
}

build_lib () {
  npm run build:prod --workspace=lib/webcraft
  npm run build:prod --workspace=lib/reactcraft
}

test_packages () {
  npm run test --workspace=lib/webcraft
  npm run test --workspace=lib/reactcraft
  npm run test --workspace=server
  npm run test --workspace=client
  npm run test --workspace=client-react
}

main
