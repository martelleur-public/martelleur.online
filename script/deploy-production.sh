#!/bin/bash

# Variables.
APP_NAME=$1
DB_NAME=$2
APP_VERSION=$3
DB_VERSION=$4
TOKEN=$5
PW=$6
REGISTRY=$7
DEPLOYMENT_DIR=deployment/production
DOCKER_DIR=docker
ROLE_FILE_DIR=deployment/production/roles/martelleur.online/files

main () {
  set -e
  echo "Deploying application for production..."
  check_args
  prepare_container_dir
  run_deployment
}

check_args () {
  if [ -z "$APP_NAME" ] || \
    [ -z "$DB_NAME" ] || \
    [ -z "$APP_VERSION" ] || \
    [ -z "$DB_VERSION" ] || \
    [ -z "$TOKEN" ] || \
    [ -z "$PW" ] ||
    [ -z "$REGISTRY" ]; then
    echo "Usage: $0 \
<APP_NAME> \
<DB_NAME> \
<APP_VERSION> \
<DB_VERSION> \
<TOKEN> \
<PW> \
<REGISTRY>"
    exit 1
  fi
}

prepare_container_dir () {  
  tar -czvf ${DEPLOYMENT_DIR}/${DOCKER_DIR}.tar.gz -C ${DEPLOYMENT_DIR} ${DOCKER_DIR}/
  if [ -f "${ROLE_FILE_DIR}/${DOCKER_DIR}.tar.gz" ]; then
    rm ${ROLE_FILE_DIR}/${DOCKER_DIR}.tar.gz
  fi
  mv ${DEPLOYMENT_DIR}/${DOCKER_DIR}.tar.gz ${ROLE_FILE_DIR}
}

run_deployment () {
  cd $DEPLOYMENT_DIR || exit 1
  ansible-playbook pb-app.yml \
    --extra-vars "token=${TOKEN} \
      ansible_become_pass=${PW} \
      app_name=${APP_NAME}
      db_name=${DB_NAME}
      app_version=${APP_VERSION} \
      db_version=${DB_VERSION} \
      registry=${REGISTRY} \
      build_dir=${DOCKER_DIR}" \
    -vvv
}

main
