#!/bin/bash

build_container () {
    echo "Building and deploying docker containers..."

    # Function variables.
    local CTR_NAME="$1"
    local CTR_VERSION="$2"
    local CTR_DIR="$3"
    local TOKEN="$4"
    local USERNAME="joel.martelleur"
    local CTR_REGISTRY="registry.gitlab.com"
    local PROJECT="martelleur1/martelleur.online"

    if [ -z "$CTR_NAME" ] ||
        [ -z "$CTR_VERSION" ] ||
        [ -z "$CTR_DIR" ] ||
        [ -z "$TOKEN" ]; then
        echo "Usage: $0 <CTR_NAME> <CTR_VERSION> <CTR_DIR> <TOKEN>"
        exit 1
    fi

    docker login -u "${USERNAME}" -p "${TOKEN}" "${CTR_REGISTRY}"
    docker build -t \
        "${CTR_REGISTRY}/${PROJECT}/${CTR_NAME}:${CTR_VERSION}" \
        "${CTR_DIR}"
    docker push "${CTR_REGISTRY}/${PROJECT}/${CTR_NAME}:${CTR_VERSION}"
}
