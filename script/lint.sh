#!/bin/bash

main () {
  set -e
  install_dependencies
  lint_packages
}

install_dependencies () {
  npm install --workspace=lib/webcraft
  npm install --workspace=lib/reactcraft
  npm install --workspace=server
  npm install --workspace=client
  npm install --workspace=client-react
  npm install
}

lint_packages () {
  npm run lint --workspace=lib/webcraft
  npm run lint --workspace=lib/reactcraft
  npm run lint --workspace=server
  npm run lint --workspace=client
  npm run lint --workspace=client-react
}

main
