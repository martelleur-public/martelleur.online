#!/bin/bash

# Variables.
APP_VERSION=$1
TOKEN=$2

main () {
  set -e
  echo "Building application container for production..."
  get_token
  check_args
  build_container_context
  source script/build-container.function.sh
  build_container \
    "martelleur.online" \
    "${APP_VERSION}" \
    "containers/martelleur.online" \
    "${TOKEN}"
}

get_token () {
  if [ -z "$TOKEN" ]; then
    read -sp "Enter token: " TOKEN
    echo
  fi
}

check_args () {
  if [ -z "$APP_VERSION" ] || [ -z "$TOKEN" ]; then
    echo "Usage: $0 <APP_VERSION> <TOKEN>"
    exit 1
  fi
}

build_container_context () {
  local ctr_dir="containers/martelleur.online"
  if [ -d "${ctr_dir}/server" ]; then
    rm -Rf "${ctr_dir}/server"
  fi
  if [ -d "${ctr_dir}/client" ]; then
    rm -Rf "${ctr_dir}/client"
  fi
  if [ -f "${ctr_dir}/package.json" ]; then
    rm "${ctr_dir}/package.json"
  fi
  if [ -f "${Cctr_dir}/package-lock.json" ]; then
    rm "${ctr_dir}/package-lock.json"
  fi
  cp -r server/target/* "${ctr_dir}/"
  cp ./server/package.json "${ctr_dir}/server/package.json"
  cp ./package.json "${ctr_dir}/package.json"
  cp ./package-lock.json "${ctr_dir}/package-lock.json"
}

main
