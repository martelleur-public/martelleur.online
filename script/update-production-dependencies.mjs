import fs from 'fs'
import path from 'path'

function main () {
    const [interpreter, script, action] = process.argv
    if (action === 'add' || !action) {
        addDependencyLibToClient(
            '../lib/webcraft/package.json',
            '../client/package.json'
        )
        addDependencyLibToClient(
            '../lib/webcraft/package.json',
            '../lib/reactcraft/package.json'
        )
        addDependencyLibToClient(
            '../lib/reactcraft/package.json',
            '../client-react/package.json'
        )
    } else if (action === 'remove') {
        removeDependencyLibFromClient(
            '../lib/webcraft/package.json',
            '../client/package.json'
        )
        removeDependencyLibFromClient(
            '../lib/webcraft/package.json',
            '../lib/reactcraft/package.json'
        )
        removeDependencyLibFromClient(
            '../lib/reactcraft/package.json',
            '../client-react/package.json'
        )
    } else {
        console.error(`Invalid action.
        Interpreter: ${interpreter}
        Script: ${script}
        Usage: add/remove dependencyName [dependencyVersion]
        `)
    }    
}

function addDependencyLibToClient (
    dependencyPackageJsonPath,
    clientPackageJsonPath
) {
    const dirname = path.dirname(new URL(import.meta.url).pathname)
    const dependencyPackageJsonPathFullPath = path.resolve(dirname, dependencyPackageJsonPath)
    const clientPackageJsonPathFullPath = path.resolve(dirname, clientPackageJsonPath)
    try {
        const dependencyPackageJson = JSON.parse(fs.readFileSync(dependencyPackageJsonPathFullPath, 'utf-8'))
        const clientPackageJson = JSON.parse(fs.readFileSync(clientPackageJsonPathFullPath, 'utf-8'))
        const dependencyVersion = dependencyPackageJson.version
        const dependencyName = dependencyPackageJson.name
        clientPackageJson.dependencies = clientPackageJson.dependencies || {}
        clientPackageJson.dependencies[dependencyName] = `^${dependencyVersion}`
        fs.writeFileSync(clientPackageJsonPathFullPath, JSON.stringify(clientPackageJson, null, 2))
    } catch (error) {
        console.error(`Error adding dependency to ${clientPackageJsonPathFullPath}: ${error.message}`)
    }
}

function removeDependencyLibFromClient (
    dependencyPackageJsonPath,
    clientPackageJsonPath
) {
    const dirname = path.dirname(new URL(import.meta.url).pathname)
    const clientPackageJsonPathFullPath = path.resolve(dirname, clientPackageJsonPath)
    const dependencyPackageJsonPathFullPath = path.resolve(dirname, dependencyPackageJsonPath)
    try {
        const dependencyPackageJson = JSON.parse(fs.readFileSync(dependencyPackageJsonPathFullPath, 'utf-8'))
        const dependencyName = dependencyPackageJson.name
        const clientPackageJson = JSON.parse(fs.readFileSync(clientPackageJsonPathFullPath, 'utf-8'))
        if (clientPackageJson.dependencies && clientPackageJson.dependencies[dependencyName]) {
            delete clientPackageJson.dependencies[dependencyName]
            fs.writeFileSync(clientPackageJsonPathFullPath, JSON.stringify(clientPackageJson, null, 2))
        } else {
            console.log(`Dependency ${dependencyName} not found in ${clientPackageJsonPathFullPath}`)
        }
    } catch (error) {
        console.error(`Error removing dependency from ${clientPackageJsonPathFullPath}: ${error.message}`)
    }
}

main()
