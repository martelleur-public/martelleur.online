#!/bin/bash

APP_VERSION=$1

main () {
  set -e
  check_arg
  update_production_dependencies
  export_app_version_to_subprocess
  install_dependencies
  build_packages
}

check_arg () {
  if [ -z "$APP_VERSION" ]; then
    echo "Usage: $0 <APP_VERSION>"
    exit 1
  fi
}

update_production_dependencies () {
  node script/update-production-dependencies.mjs
}

export_app_version_to_subprocess () {
  export APP_VERSION
}

install_dependencies () {
  npm install --workspace=lib/webcraft
  npm install --workspace=server
  npm install --workspace=client
  npm install
}

build_packages () {
  rm -Rf server/target
  npm run build:prod --workspace=lib/webcraft
  npm run build --workspace=server 
  npm run build:dist --workspace=client
  python3 script/update-version.py "${APP_VERSION}"
  mv client/dist server/target/client  
}

main
