PORT=5173
DOMAIN=content-mammal-previously.ngrok-free.app

main () {
  open_browser
  run_ssh_tunnel
}

open_browser () {
  local chrome=$(which google-chrome)
  local fx=$(which firefox)

  if [ ! -z "$chrome" ]; then
    google-chrome https://"$DOMAIN"
  elif [ ! -z "$fx" ]; then
    firefox https://"$DOMAIN"
  else
    echo "Open your browser with https://"$DOMAIN""
  fi
}

run_ssh_tunnel () {
  ngrok http --domain="$DOMAIN" "$PORT"  
}

main
