#!/bin/bash

APP_VERSION=$1
DB_VERSION=$2
TOKEN=$3
NET=local-martelleur-online-net
VOL=local-martelleur-online-vol
DEPLOYMENT_DIR=deployment/local

main () {
  set -e
  do_cleanup_when_exit
  get_token
  get_app_version
  get_db_version
  check_arg
  if [ "$#" -ne 3 ]; then
    manual_version_check
  fi
  update_production_dependencies
  export_app_version_to_subprocess
  install_dependencies
  build_packages
  build_container_context
  build_containers
  setup_network
  setup_volume
  run_local_installation
}

# OBS when user do ctrl+z / SIGTSTP node processer keep running
do_cleanup_when_exit() {
  trap cleanup EXIT SIGTSTP
}

cleanup () {
  echo "Cleaning up dev environment..."
  docker compose -f ${DEPLOYMENT_DIR}/docker-compose.yml down --volumes
  rm -Rf node_modules
  node script/update-production-dependencies.mjs remove
}

get_token () {
  if [ -z "$TOKEN" ]; then
    read -sp "Enter token: " TOKEN
    echo
  fi
}

get_app_version () {
  if [ -z "$APP_VERSION" ]; then
    read -p "Enter app version: " APP_VERSION
  fi
}

get_db_version () {
  if [ -z "$DB_VERSION" ]; then
    read -p "Enter db version: " DB_VERSION
  fi
}

check_arg () {
  if [ -z "$APP_VERSION" ] ||
    [ -z "$DB_VERSION" ] ||
    [ -z "$TOKEN" ]; then
    echo "Usage: $0 <APP_VERSION> <DB_VERSION> <TOKEN>"
    exit 1
  fi
}

# TODO. Automate or keep for manually controlling version.
# Input argument version should be sync with docker compose file versions.
manual_version_check () {
  local continue=""
  local docker_compose_file=deployment/local/docker-compose.yml
  echo "Input versions should be sync with "$docker_compose_file" service versions"
  echo "Current version selected: "
  echo "  App version "${APP_VERSION}""
  echo "  DB version "${DB_VERSION}""
  echo ""
  read -p "Continue [Y/n]: " continue
  if [ "$continue" != "Y" ]; then
    echo "Deployment cancelling..."
    exit 0
  else
    echo "Continuing deployment..."
  fi
}

update_production_dependencies () {
  node script/update-production-dependencies.mjs add
}

export_app_version_to_subprocess () {
  export APP_VERSION
}

install_dependencies () {
  npm install --workspace=lib/webcraft
  npm install --workspace=server
  npm install --workspace=client
  npm install
}

build_packages () {
  rm -Rf server/target
  npm run build:dev --workspace=lib/webcraft
  npm run build --workspace=server
  npm run build:dist:local --workspace=client
  python3 script/update-version.py "${APP_VERSION}"
  mv client/dist server/target/client  
}

build_container_context () {
  local ctr_dir="containers/martelleur.online"
  if [ -d "${ctr_dir}/server" ]; then
    rm -Rf "${ctr_dir}/server"
  fi
  if [ -d "${ctr_dir}/client" ]; then
    rm -Rf "${ctr_dir}/client"
  fi
  if [ -f "${ctr_dir}/package.json" ]; then
    rm "${ctr_dir}/package.json"
  fi
  if [ -f "${Cctr_dir}/package-lock.json" ]; then
    rm "${ctr_dir}/package-lock.json"
  fi
  cp -r server/target/* "${ctr_dir}/"
  cp ./server/package.json "${ctr_dir}/server/package.json"
  cp ./package.json "${ctr_dir}/package.json"
  cp ./package-lock.json "${ctr_dir}/package-lock.json"
}

build_containers () {
  source ./script/build-container.function.sh
  build_container \
    "martelleur.online" \
    "${APP_VERSION}" \
    "containers/martelleur.online"\
    "${TOKEN}"
  build_container \
    "martelleur.online.db" \
    "${DB_VERSION}" \
    "containers/database" \
    "${TOKEN}"
}

setup_network () {
  local network_exists=$(docker network ls | grep "$NET")
  if [ ! -z "$network_exists" ]; then
    docker network rm "$NET"  
  fi
  docker network create -d bridge --subnet 10.11.2.0/24 "$NET"
}

setup_volume () {
  local volume_exist=$(docker volume ls -q | grep "$VOL") 
  if [ ! -z "$volume_exist" ]; then
    docker volume rm "$VOL"
  fi
  docker volume create "$VOL"
}

run_local_installation () {
  docker compose -f ${DEPLOYMENT_DIR}/docker-compose.yml up
}

main
