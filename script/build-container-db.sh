#!/bin/bash

# Variables.
DB_VERSION=$1
TOKEN=$2

main () {
  set -e
  echo "Building db container for production..."
  get_token
  check_args
  source script/build-container.function.sh
  build_container \
    "martelleur.online.db" \
    "${DB_VERSION}" \
    "containers/database" \
    "${TOKEN}"
}

get_token () {
  if [ -z "$TOKEN" ]; then
    read -sp "Enter token: " TOKEN
    echo
  fi
}

check_args () {
  if [ -z "$DB_VERSION" ]|| [ -z "$TOKEN" ]; then
    echo "Usage: $0 <DB_VERSION> <TOKEN>"
    exit 1
  fi
}

main
