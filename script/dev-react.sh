#!/bin/bash

vol=dev-martelleur-online-volume

main () {
  set -ex
  do_cleanup_when_exit
  install_dependencies  
  setup_external_volume
  start_dev_environment
  link_pkg_webcraft_to_reactcraft_and_react_client
  link_pkg_reactcraft_to_react_client
  start_packages
}

# OBS when user do ctrl+z / SIGTSTP node processer keep running
do_cleanup_when_exit() {
  trap cleanup EXIT SIGTSTP
}

cleanup () {
  echo "Cleaning up dev environment..."
  rm -Rf node_modules
  docker compose -f deployment/development/docker-compose.yml down --volumes
}

install_dependencies () {
  npm install --workspace=lib/webcraft
  npm install --workspace=lib/reactcraft
  npm install --workspace=client-react
  npm install --workspace=server
  npm install
}

setup_external_volume () {
  local volume_exist=$(docker volume ls -q | grep "$vol") 
  if [ ! -z "$volume_exist" ]; then
    docker volume rm "$vol"
  fi
  docker volume create "$vol"
}

start_dev_environment () {
  docker compose -f deployment/development/docker-compose.yml up -d
}

link_pkg_webcraft_to_reactcraft_and_react_client () {
  local current_dir=$(pwd)
  cd ./lib/webcraft && npm link
  cd "$current_dir"
  cd ./lib/reactcraft && npm link @martelleur.online/webcraft
  cd "$current_dir"
  cd ./client-react && npm link @martelleur.online/webcraft
  cd "$current_dir"
}

link_pkg_reactcraft_to_react_client () {
  local current_dir=$(pwd)
  cd ./lib/reactcraft && npm link
  cd "$current_dir"
  cd ./client-react && npm link @martelleur.online/reactcraft
  cd "$current_dir"
}

start_packages () {
  local chrome=$(which google-chrome)
  local fx=$(which firefox)
  
  if [ ! -z "$chrome" ]; then
    start_with_chrome
  elif [ ! -z "$fx" ]; then
    start_with_fx
  else
    start_without_browser
  fi
}

start_with_chrome () {
  local sleep=35
  local sleep_short=20
  concurrently \
    "npm run-script dev --workspace=lib/webcraft" \
    "sleep $sleep_short && npm run-script dev --workspace=lib/reactcraft --loglevel verbose" \
    "sleep $sleep && npm run-script dev --workspace=server" \
    "sleep $sleep && npm run-script dev --workspace=client-react" \
    "sleep $sleep && google-chrome --incognito http://localhost:5173"
}

start_with_fx () {
  local sleep=35
  local sleep_short=20
  concurrently \
    "npm run-script dev --workspace=lib/webcraft" \
    "sleep $sleep_short && npm run-script dev --workspace=lib/reactcraft" \
    "sleep $sleep && npm run-script dev --workspace=server" \
    "sleep $sleep && npm run-script dev --workspace=client-react" \
    "sleep $sleep && firefox --private-window http://localhost:5173"
}

start_without_browser () {
  local sleep=35
  local sleep_short=20
  concurrently \
    "npm run-script dev --workspace=lib/webcraft" \
    "sleep $sleep_short && npm run-script dev --workspace=lib/reactcraft" \
    "sleep $sleep && npm run-script dev --workspace=server" \
    "sleep $sleep && npm run-script dev --workspace=client-react"
}

main
