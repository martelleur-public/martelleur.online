#!/bin/bash

vol=dev-martelleur-online-volume

main () {
  set -ex
  do_cleanup_when_exit
  install_dependencies  
  setup_external_volume
  start_dev_environment
  link_local_npm_dependencies
  start_packages
}

# OBS when user do ctrl+z / SIGTSTP node processer keep running
do_cleanup_when_exit() {
  trap cleanup EXIT SIGTSTP
}

cleanup () {
  echo "Cleaning up dev environment..."
  rm -Rf node_modules
  docker compose -f deployment/development/docker-compose.yml down --volumes
}

install_dependencies () {
  npm install --workspace=lib/webcraft
  npm install --workspace=client
  npm install --workspace=server
  npm install
}

setup_external_volume () {
  local volume_exist=$(docker volume ls -q | grep "$vol") 
  if [ ! -z "$volume_exist" ]; then
    docker volume rm "$vol"
  fi
  docker volume create "$vol"
}

start_dev_environment () {
  docker compose -f deployment/development/docker-compose.yml up -d
}

link_local_npm_dependencies () {
  local current_dir=$(pwd)
  cd ./lib/webcraft && npm link
  cd "$current_dir"
  cd ./client && npm link @martelleur.online/webcraft
  cd "$current_dir"
}

start_packages () {
  local chrome=$(which google-chrome)
  local fx=$(which firefox)
  
  if [ ! -z "$chrome" ]; then
    start_with_chrome
  elif [ ! -z "$fx" ]; then
    start_with_fx
  else
    start_without_browser
  fi
}

start_with_chrome () {
  local sleep=20
  concurrently \
    "npm run-script dev --workspace=lib/webcraft" \
    "sleep $sleep && npm run-script dev --workspace=server" \
    "sleep $sleep && npm run-script dev --workspace=client" \
    "sleep $sleep && google-chrome --incognito http://localhost:5173"
}

start_with_fx () {
  local sleep=20
  concurrently \
    "npm run-script dev --workspace=lib/webcraft" \
    "sleep $sleep && npm run-script dev --workspace=server" \
    "sleep $sleep && npm run-script dev --workspace=client" \
    "sleep $sleep && firefox --private-window http://localhost:5173"
}

start_without_browser () {
  local sleep=20
  concurrently \
    "npm run-script dev --workspace=lib/webcraft" \
    "sleep $sleep && npm run-script dev --workspace=server" \
    "sleep $sleep && npm run-script dev --workspace=client"
}

main
