#!/bin/sh

# -------------------------------
# A script used to start the app.
# -------------------------------

set -x

SLEEP=60

sleep $SLEEP
echo "Sleep for ${SLEEP}s, db is starting"

echo "Start app."

node /srv/www/martelleur.online/server/src/server.js

# TODO Add debug functionality without comment out code
# tail -f /dev/null
